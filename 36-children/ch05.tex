% !TEX root = 36-children.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{}
About a month after the trip to Cambridge I found a notice in my school mailbox. It stated that if by Thursday (it was then Monday) the children in my class could get signed parental permission slips we could take a trip to the Metropolitan Museum of Art, receive a guided tour of the special children’s exhibit on archaeology, and get free subway fare. Reluctantly I told the children, who were delighted with the chance to spend a day out of school.

I wasn’t so enthusiastic and didn’t feel that ``we'' were ready to take a trip, that is, I was afraid to travel with my thirty-six children and imagined the most chaotic and embarrassing scenes. But the children wanted to go and so we went. Twenty signed permission slips were returned by Wednesday. On Thursday morning Maurice handed me the other sixteen slips bearing signatures that resembled each other’s and his. I let it pass and we set out.

The first problem I had to face was how to walk through the streets with the children. Should the class stay in line, double file, holding hands as I had done in school fifteen years before on a similar trip to the Metropolitan Museum? I could insist and spend the day trying to keep the children in line, but to what point other than impressing the people we passed with my authority and the children’s obedience? I wanted the children to observe the city and enjoy the trip, so I let them walk naturally with whomever they pleased. We moved as -a casual group, and though we may have upset some people the walk from the school to the subway was leisurely and pleasant. The train ride from I25th Street to 86th Street was very quiet. The children huddled together, avoiding the white riders coming from the Bronx to spend a day downtown.

We emerged from the subway at 86th Street and Lexington Avenue and walked towards the museum. At Park Avenue Marie came up to me and said:

``Mr. Kohl, where are we? In Long Island?''

``Marie, this is Park Avenue and Eighty-sixth Street.''

She looked at me as if I were mad, then went to Pamela and told her Mr Kohl said it was Park Avenue. The rumour spread through the class until finally a delegation of boys headed by Sam and Ralph approached and challenged me to prove it was Park Avenue. I pointed to the street signs and they looked as if they wanted to cry.

``But where is Harlem? I live on Park Avenue -- where are the tracks?''

I pointed north. The children looked but could only see Christmas trees stretching up to 96th Street. Harlem wasn’t visible. They looked up at the sign again and we made our way to Madison Avenue.

``But, Mr Kohl, our bean school is on Madison Avenue.''

And then to Fifth and the museum where we were greeted by a tall young woman wearing a name tag proclaiming that she represented the Junior League. The exhibit was fascinating -- things to turn, push and pull, pictures of Sumer which we were studying in class, Egyptian remains, undersea diving equipment, salvaged gold and weapons. The kids forgot about Park Avenue and were ready to rush in. But first we had to pause and learn about artefacts, layers, some other things I couldn’t grasp -- the guide’s vocabulary was as stunning as her legs. She sat down on a stool and spoke to the children who crouched uncomfortably on the floor. Marie whispered to me.

``Mr Kohl, tell her to pull her skirt down. It isn’t decent.''

The kids were bored after fifteen minutes. Ralph and Robert Jackson wandered off to explore for themselves. Alvin and Michael started pushing each other. Brenda started a fight with Carol. After half an hour I was the only one who even pretended to listen. Twelve o’clock rescued us. It was time for lunch, but rather than risk the chaotic conditions in the museum lunchroom I took the class out to lunch in the park. It was cold but private. The children spread out, ate and talked.

I hadn’t brought any lunch, and they insisted that I share theirs. After they were finished the boys decided to race. They asked me to mark out a course and for the rest of the afternoon the children raced, roamed over the park, talked. Exhausted, we returned to school at four o’clock.

I couldn’t forget the children’s response to Park Avenue and 86th Street and because of it instituted my Friday trips. A week after we had gone to the museum I made a general invitation to the class to take a drive with me down Park Avenue. Seven children took me up and at 3.15 on Friday we set out from 99th Street and Park Avenue, passing the covered markets at 116th the smelly streets down to 96th, and the dismal row upon row of slum clearance projects all the way to 99th Street. On the left of us loomed the elevated tracks of the New York Central Railroad. We ascended from 99th to 96th, reaching the summit of that glorious hill where the tracks sink into the bowels of the city and Park Avenue is metamorphosed into a rich man’s fairyland. Down the middle of the street is an island filled with Christmas trees in winter and flowers during the summer, courtesy of \emph{The} Park Avenue Association. On either side of the broad street opulent apartment buildings, doormen, clean sidewalks. The children couldn’t, wouldn’t believe it

``Mr Kohl, where are the ash cans?’ This can’t be Park Avenue.''

``Mr Kohl, something’s wrong \ldots{}''

It was Pamela, not angry but sad and confused. We passed the gleaming office buildings further downtown. I was about to comment but sensed that the children were tired and restless. They had had enough and I had too. We returned to Harlem and then I drove home back downtown. The city was transformed for me through the eyes of the children. I saw a cruel contradictory New York and wanted to offer something less harsh to the children. Perhaps my apartment between Park and Madison Avenues, just a mile from the school yet in the white city, could offer something less strange and hostile. I wasn’t sure. The principal and other teachers had warned us about getting too friendly with the children, of transcending the traditional formal distance between pupil and teacher. They told me it wasn’t ``professional'' to develop relationships with the children outside the context of the school. Besides, one of the older teachers had warned me, ``You never can tell what `these' children might do'' (or, as I almost added, what the neighbours would say). No. I couldn’t tell, but the children couldn’t tell about me either. That was our greatest problem: we didn’t know each other’s lives.

Warily, yet convinced that it was right, I invited the class to visit my house the next Friday -- it must have been towards the middle of December. Alvin, Maurice, Leverne, Robert Jackson, Pamela, Grace, Brenda W., and Carol accepted. It was quite a squeeze but we all made it into the car and to my apartment which was on the fifth floor of a walk-up. The hall door was locked, the mailboxes as well as the buzzers worked. The children noted these wonders, then made the five flights with ease, I had one large room crammed with books, records, pictures, statues -- things I had picked up at college and in Europe. There was a record player, tape recorder, radio. The place was in chaos, but I knew where most things were.

The first thing that struck the children was that it was mine. No parents, sisters, brothers, cousins. They saw how completely mine it was and loved the idea.

I was anxious about what would happen that afternoon -- what the children would do, what we could talk about. Intuitively I knew we all felt the same way, nervous and awkward but of goodwill. Something would, had to occur to relieve the tension. It did very quickly. Alvin saw the tape recorder and asked me if he could work it. I showed him how and the children crowded around. Alvin put the machine on. The reels spun.

Nobody wanted to talk; there was an awkward silence. I uttered a few inanities, feeling embarrassed, wondering whether the whole thing wasn’t a grave mistake. Maurice, who was looking through my books, came to the rescue. 

``Alvin, sing `Vie Tanny'.''

``What’s Vic Tanny?''

Pamela turned to Alvin who blushed, begged encouragement and, receiving it, sang \emph{Vic Tanny’s Gonna Put You Down} a song he had made up the year before.

\begin{chapquot}
\emph{Vic Tanny’s gonna put you down,\\
Dum! Dum!\\
Vic Tanny’s gonna put your bones down!\\
Vic Tanny’s gonna put you down,\\
‘Cause they ain’t got time to be jiving around.}

\noindent Skinny Minny is my sister’s name\\
And I’ll always call her the same.\\
Vie Tanny’s gonna put her down,\\
'Cause her bones are weak and they’re turning around,\\
Dum! Dum!

\noindent \emph{Vic Tanny!\ldots{}}

\noindent My brother has a lot of muscle,\\
He always likes to run, jump and hustle.\\
Vic Tanny’s gonna put him down,\\
'Cause he ain’t worth a penny on that dirty ground.\\
Dum! Dum!

\noindent \emph{Vic Tanny!\ldots{}}

\noindent Mrs Wrenn she tried to swim,\\
She tried to play-a like-a Jungle Jim.\\
Vic Tanny’s gonna put you down,\\
'Cause her funeral was in the next town.\\
Dum! Dum!

\noindent \emph{Vic Tanny!\ldots{}}

\noindent His mother sent him to the store\\
And then she conked out on the floor.\\
Vic Tanny’s gonna put her down,\\
'Cause the smelling salts had to bring her around. Dum! Dum!

\noindent \emph{Vic Tanny!\ldots{}}

\noindent Sikie Mikie he tried to run,\\
He tried to act-a-like-a Peter Gunn.\\
Vic Tanny’s gonna put her down,\\
'Cause her sikes are dirty and her head is round.\\
Dum! Dum!
\end{chapquot}

I called Judy and she came upstairs. The girls ran up to her, drawing her into the circle that had formed around the tape recorder. The children talked and listened to each other. They seemed themselves in a way they couldn’t in the classroom. I looked and listened, discovering how much I missed by being up in front of the room, a teacher of thirty-sis souls I couldn’t know individually.

Pamela took over from Alvin. Thin, beautiful Pamela, so much herself, non-defiant and non-compliant, the smallest girl in the class but the one no other child dared to anger.

\begin{dialogue}
\speak{Pamela} Hello, my name is Pamela Reed. I’m at Mr Kohl’s house and I’m talkin' on a tape recorder and Brenda is laffin' at me and Carol’s wipin' her hands and Margie's playin' cards and Carol wants to sing and Mr Kohl’s smokin' a cigarette and Alvin is buildin' bricks and Leverne is, I mean Maurice is doing nothing playing1 with Tootsie Roll paper \direct{giggle} and Margie say, ``Are you all ready?'' \direct{A record comes on.}

\speak{Pamela} ``Do You Love Me's'' gonna sing for us. \direct{Record in background} You hear it? It’s singin’ very nice. \direct{\refer{Margie} sings along with the record.} Well now, you heard ‘Do You Love Me’ sing, so now you wanna hear me sing?

\speak{Margie} Now, everyone, we will hear ``Everybody Loves a Lover'' by - \direct{pause} Pamela can’t sing so I’ll sing for her.

\speak{Pamela} Well listen, how dare you insult me now? \direct{Record starts, \refer{Margie} sings along with it.} 

\speak{Margie} Now wasn’t that bee-u-ti-ful? I’ll punch you in the face, Pamela.

\speak{Pamela} Let me tell you now, you know Margie she don’t know no better, that’s why she always says that, but you know she’s so big I have to let her beat me or everybody would say, ``You let that little girl beat you?'' So that’s why I always let her beat me. Next time she start messing with me I’m going to have to hurt her.
\direct{Record: ``Twist and Shout''.}

\speak{Pamela} Now you just heard ``Shake It Up, Baby''. You gonna hear it again, but I’m sorry, oh there was a mistake, it’s ``Do You Love Me'' \direct{pause}. Um, you know, it’s a state of confusion at this moment so we don’t know yet. ``Do You Love Me'' is coming on for real now, so you have to wait until it comes on. Ready? \direct{Singing and giggling.} Now here’s the Wiggle-wobble. Having a good time \ldots{} the boys are wobbling, the girls are wobbling \ldots{} Grace doin' the walk \ldots{} Brenda’s shakin’ it up \ldots{} have to leave my duties, so, you know how it is. \direct{There is a long absence while the music plays in the background.} Here comes that horn\ldots{} it’s goin’ off\ldots{} oh, oh \ldots{} tsk \ldots{} \direct{\refer{Pamela} clucks while an argument goes on in the background over \refer{Pamela} Hogging the recorder. Record ‘Everybody begins and \refer{Pamela} sings along, stops.} I smell something. \direct{She doesn’t know many of the words to the song, pretends.} Margie, wanna talk? \direct{There is increasing confusion in the background, something about taking something out.}

\speak{Mr. Kohl} Put that down, Maurice! Margie, Margie, we’re all going home in parts if you take it out! \direct{‘Twist and Shout comes on accompanied by \refer{Pamela's} singing.}

\speak{From Background} Mr. Kohl, why don’t you wobble? 

\speak{Mr. Kohl} Later, later.

\speak{Pamela} Well since ``Shake It Up, Baby'' went off, one of the stars is here. The other star had to stay home, so, we stars of One hundred and nineteenth we know almost everything, but it’s a pity that some children can’t keep up with us but, you know some people have it and some don’t.
\end{dialogue}

The afternoon flew. As the children felt comfortable they began to explore the apartment, look at the pictures, ask about the books. We talked to each other in a way that I couldn’t yet do in class. The children probed for my interest and my commitments. They wanted to know what I cared about as much as I wanted to know about them. I talked about my writing; about the years I spent in Paris looking for myself, the joy of discovering that I was a good teacher. The children wanted to know how I got to Harlem, whether my parents were still alive, if I had brothers and sisters. I answered, thinking that the children really cared who I was and that it had never occurred to me that they would. The other teachers would have repeated that exposing myself to the children that way was ``unprofessional''. Perhaps they really would have felt exposed and found their authority threatened had their students known them as human beings and not as ``professionals''.

Until I found myself talking to my pupils about myself and my life in the context of my home I unconsciously held off from the children too. I avoided answering questions about my private life, even though I felt rejected because the children wouldn’t let me into theirs. Talking with the children, taking them home on occasion, seeing them relaxed and playful, I realized how narrow the view from the teacher’s desk is. I also realized that any successful classroom has to be based upon a dialogue between students and teachers, both teaching and being taught, and both able to acknowledge that fact.

Taking the children home wasn’t always pleasant. One wintry Friday afternoon six children climbed up to my apartment They were cold and angry. It had been a particularly chaotic day in school, and I had lost my patience. I wanted to be alone and dreaded the thought of taking children home with me. Still, I had given my word that on Friday the children could come if they wanted to, and I had to keep it. 

I opened the door and growled. 

``Throw your coats down wherever you want.''

Leverne responded by flipping his coat across the room. It landed on a cabinet, sweeping everything in its way on to the floor. One of my most precious possessions, a fragile pre-Columbian statue, was shattered beyond repair. Tears came to my eyes, then harsh anger. Leverne looked at me and fell to pieces. 

``I didn’t mean it, see I can put it together. Here’s the head.''

``Mr. Kohl, he didn’t mean it.''

Alvin held on to my arm as if to prevent me from hitting Leverne. I looked at them and the other four children whose bearing seemed to be telling me, ``See, we knew it would never work out.'' Then a smile came over my face.

``It isn’t worth anything. I’ll pick up the pieces while Alvin gets the tape recorder.''

Another time, Alvin, Maurice and Michael walked from the school down to my apartment on a Wednesday afternoon. Their coming was a delightful surprise, and I let them in. They came after school the next day as well, and I’m sure that they could have eaten up the whole week with their visits. I had to explain to them that I needed time to myself and could only see people from the class after school on Fridays. They were angry and told me they would never come to my house again. That Friday the three of them didn’t come, though they did the week after. I had to set limits on what I could do as an individual and have always had to balance what part of my life I could offer the children and what part had to remain private.

