% !TEX root = de-schooling.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Rebirth of Epimethean Man}

Our society resembles the ultimate machine which I once
saw in a New York toy shop. It was a metal casket which,
when you touched a switch, snapped open to reveal a
mechanical hand. Chromed fingers reached out for the lid,
pulled it down, and locked it from the inside. It was a box;
you expected to be able to take something out of it; yet all
it contained was a mechanism for closing the cover. This
contraption is the opposite of Pandora's ``box.''

The original Pandora, the All-Giver, was an Earth goddess
in prehistoric matriarchal Greece. She let all ills escape
from her amphora (\emph{pythos}). But she closed the lid before
Hope could escape. The history of modern man begins with
the degradation of Pandora's myth and comes to an end in
the self-sealing casket. It is the history of the Promethean
endeavor to forge institutions in order to corral each of the
rampant ills. It is the history of fading hope and rising
expectations.

To understand what this means we must rediscover the
distinction between hope and expectation. Hope, in its
strong sense, means trusting faith in the goodness of nature,
while expectation, as I will use it here, means reliance on
results which are planned and controlled by man. Hope
centers desire on a person from whom we await a gift.
Expectation looks forward to satisfaction from a
predictable process which will produce what we have the
right to claim. The Promethean ethos has now eclipsed
hope. Survival of the human race depends on its
rediscovery as a social force.

The original Pandora was sent to Earth with a jar which
contained all ills; of good things, it contained only hope.
Primitive man lived in this world of hope. He relied on the
munificence of nature, on the handouts of gods, and on the
instincts of his tribe to enable him to subsist. Classical
Greeks began to replace hope with expectations. In their
version of Pandora she released both evils and goods. They
remembered her mainly for the ills she had unleashed. And,
most significantly, they forgot that the All-Giver was also
the keeper of hope.

The Greeks told the story of two brothers, Prometheus and
Epimetheus. The former warned the latter to leave Pandora
alone. Instead, he married her. In classical Greece the name
``Epimetheus,'' which means ``hindsight,'' was interpreted to
mean ``dull'' or ``dumb.'' By the time Hesiod retold the story
in its classical form, the Greeks had become moral and
misogynous patriarchs who panicked at the thought of the
first woman. They built a rational and authoritarian society.
Men engineered institutions through which they planned to
cope with the rampant ills. They became conscious of their
power to fashion the world and make it produce services
they also learned to expect. They wanted their own needs
and the future demands of their children to be shaped by
their artifacts. They became lawgivers, architects, and
authors, the makers of constitutions, cities, and works of art
to serve as examples for their offspring. Primitive man had
relied on mythical participation in sacred rites to initiate
individuals into the lore of society, but the classical Greeks
recognized as true men only those citizens who let
themselves be fitted by \emph{paideia} (education) into the
institutions their elders had planned.

The developing myth reflects the transition from a world in
which dreams were \emph{interpreted} to a world in which oracles
were \emph{made}. From immemorial time, the Earth Goddess had
been worshipped on the slope of Mount Parnassus, which
was the center and navel of the Earth. There, at Delphi
(from \emph{delphys}, the womb), slept Gaia, the sister of Chaos
and Eros. Her son, Python the dragon, guarded her moonlit
and dewy dreams, until Apollo the Sun God, the architect
of Troy, rose from the east, slew the dragon, and became
the owner of Gaia's cave. His priests took over her temple.
They employed a local maiden, sat her on a tripod over
Earth's smoking navel, and made her drowsy with fumes.
They then rhymed her ecstatic utterances into hexameters
of self-fulfilling prophecies. From all over the
Peloponnesus men brought their problems to Apollo's
sanctuary. The oracle was consulted on social options, such
as measures to be taken to stop a plague or a famine, to
choose the right constitution for Sparta or the propitious
sites for cities which later became Byzantium and
Chalcedon. The never-erring arrow became Apollo's
symbol. Everything about him became purposeful and
useful.

In the \emph{Republic}, describing the ideal state, Plato already
excludes popular music. Only the harp and Apollo's lyre
would be permitted in towns because their harmony alone
creates ``the strain of necessity and the strain of freedom,
the strain of the unfortunate and the strain of the fortunate,
the strain of courage and the strain of temperance which
befit the citizen.'' City-dwellers panicked before Pan's flute
and its power to awaken the instincts. Only ``the shepherds
may play [Pan's] pipes and they only in the country.''

Man assumed responsibility for the laws under which he
wanted to live and for the casting of the environment into
his own image. Primitive initiation by Mother Earth into
mythical life was transformed into the education (\emph{paideia})
of the citizen who would feel at home in the forum.

To the primitive the world was governed by fate, fact, and
necessity. By stealing fire from the gods, Prometheus
turned facts into problems, called necessity into question,
and defied fate. Classical man framed a civilized context
for human perspective. He was aware that he could defy
fate\hyp{}nature\hyp{}environment, but only at his own risk.
Contemporary man goes further; he attempts to create the
world in his image, to build a totally man-made
environment, and then discovers that he can do so only on
the condition of constantly remaking himself to fit it. We
now must face the fact that man himself is at stake.

Life today in New York produces a very peculiar vision of
what is and what can be, and without this vision life in
New York is impossible. A child on the streets of New York
never touches anything which has not been scientifically
developed, engineered, planned, and sold to someone. Even
the trees are there because the Parks Department decided to
put them there. The jokes the child hears on television have
been programmed at a high cost. The refuse with which he
plays in the streets of Harlem is made of broken packages
planned for somebody else. Even desires and fears are
institutionally shaped. Power and violence are organized
and managed: the gangs versus the police. 

Learning itself is
defined as the consumption of subject matter, which is the
result of researched, planned, and promoted programs.
Whatever good there is, is the product of some specialized
institution. It would be foolish to demand something which
some institution cannot produce. The child of the city
cannot expect anything which lies outside the possible
development of institutional process. Even his fantasy is
prompted to produce science fiction. He can experience the
poetic surprise of the unplanned only through his encounter
with ``dirt,'' blunder, or failure: the orange peel in the gutter,
the puddle in the street, the breakdown of order, program,
or machine are the only take-offs for creative fancy.
``Goofing off'' becomes the only poetry at hand.

Since there is nothing desirable which has not been planned, the city
child soon concludes that we will always be able to design an
institution for our every want. He takes for granted the power of
process to create value. Whether the goal is meeting a mate,
integrating a neighborhood, or acquiring reading skills, it will be
defined in such a way that its achievement can be engineered. The man
who knows that nothing in demand is out of production soon expects
that nothing produced can be out of demand. If a moon vehicle can be
designed, so can the demand to go to the moon. Not to go where one can
go would be subversive. It would unmask as folly the assumption that
every satisfied demand entails the discovery of an even greater
unsatisfied one. Such insight would stop progress.  Not to produce
what is possible would expose the law of ``rising expectations'' as a
euphemism for a growing rustration gap, which is the motor of a
society built on the coproduction of services and increased demand

The state of mind of the modern city-dweller appears in the
mythical tradition only under the image of Hell: Sisyphus,
who for a while had chained Thanatos (death), must roll a
heavy stone up the hill to the pinnacle of Hell, and the
stone always slips from his grip just when he is about to
reach the top. Tantalus, who was invited by the gods to
share their meal, and on that occasion stole their secret of
how to prepare all-healing ambrosia, which bestowed
immortality, suffers eternal hunger and thirst standing in a
river of receding waters, overshadowed by fruit trees with
receding branches. A world of ever-rising demands is not
just evil -- it can be spoken of only as Hell.

Man has developed the frustrating power to demand
anything because he cannot visualize anything which an
institution cannot do for him. Surrounded by all-powerful
tools, man is reduced to a tool of his tools. Each of the
institutions meant to exorcise one of the primeval evils has
become a fail-safe, self-sealing coffin for man. Man is
trapped in the boxes he makes to contain the ills Pandora
allowed to escape. The blackout of reality in the smog
produced by our tools has enveloped us. Quite suddenly we
find ourselves in the darkness of our own trap.

Reality itself has become dependent on human decision.
The same President who ordered the ineffective invasion of
Cambodia could equally well order the effective use of the
atom. The ``Hiroshima switch'' now can cut the navel of the
Earth. Man has acquired the power to make Chaos
overwhelm both Eros and Gaia. This new power of man to
cut the navel of the Earth is a constant reminder that our
institutions not only create their own ends, but also have
the power to put an end to themselves and to us. The
absurdity of modern institutions is evident in the case of the
military. Modern weapons can defend freedom,
civilization, and life only by annihilating them. Security in
military language means the ability to do away with the
Earth.

The absurdity that underlies nonmilitary institutions is no
less manifest. There is no switch in them to activate their
destructive power, but neither do they need a switch. Their
grip is already fastened to the lid of the world. They create
needs faster than they can create satisfaction, and in the
process of trying to meet the needs they generate, they
consume the Earth. This is true for agriculture and
manufacturing, and no less for medicine and education.
Modern agriculture poisons and exhausts the soil. The
``green revolution'' can, by means of new seeds, triple the
output of an acre -- but only with an even greater
proportional increase of fertilizers, insecticides, water, and power. Manufacturing of these, as of all other goods, 
pollutes the oceans and the atmosphere and degrades 
irreplaceable resources. If combustion continues to increase 
at present rates, we will soon consume the oxygen of the 
atmosphere faster than it can be replaced. We have no 
reason to believe that fission or fusion can replace 
  combustion without equal or higher hazards. 
  
  Medicine men 
  replace midwives and promise to make man into something 
  else: genetically planned, pharmacologically sweetened, 
  and capable of more protracted sickness. The contemporary 
  ideal is a pan-hygienic world: a world in which all contacts 
  between men, and between men and their world, are the 
  result of foresight and manipulation. School has become 
  the planned process which tools man for a planned world, 
  the principal tool to trap man in man s trap. It is sup-posed 
  to shape each man to an adequate level for playing a part in 
  this world game. Inexorably we cultivate, treat, produce, 
  and school the world out of existence. 

The military institution is evidently absurd. The absurdity 
of nonmilitary institutions is more difficult to face. It is 
  even more frightening, precisely because it operates 
  inexorably. We know which switch must stay open to avoid 
  an atomic holocaust. No switch detains an ecological 
  Armageddon. 

In classical antiquity, man had discovered that the world
could be made according to man's plans, and with this
insight he perceived that it was inherently precarious,
dramatic and comical. Democratic institutions evolved and
man was presumed worthy of trust within their framework.
Expectations from due process and confidence in human
nature kept each other in balance. The traditional
professions developed and with them the institutions
needed for their exercise.

Surreptitiously, reliance on institutional process has
replaced dependence on personal good will. The world has
lost its humane dimension and reacquired the factual
necessity and fatefulness which were characteristic of
primitive times. But while the chaos of the barbarian was
constantly ordered in the name of mysterious,
anthropomorphic gods, today only man's planning can be
given as a reason for the world being as it, is. Man has
become the plaything of scientists, engineers, and planners.

We see this logic at work in ourselves and in others. I know
a Mexican village through which not more than a dozen
cars drive each day. A Mexican was playing dominoes on
the new hard-surface road in front of his house -- where he
had probably played and sat since his youth. A car sped
through and killed him. The tourist who reported the event
to me was deeply upset, and yet he said: ``The man had it
coming to him.''

At first sight, the tourist's remark is no different from the
 statement of some primitive bushman reporting the death of
a fellow who had collided with a taboo and had therefore
died. But the two statements carry opposite meanings. The
primitive can blame some tremendous and dumb
transcendence, while the tourist is in awe of the inexorable
logic of the machine. The primitive does not sense
responsibility; the tourist senses it, but denies it. In both the
primitive and the tourist the classical mode of drama, the
style of tragedy, the logic of personal endeavor and
rebellion is absent. The primitive man has not become
conscious of it, and the tourist has lost it. The myth of the
Bushman and the myth of the American are made of inert,
inhuman forces. Neither experiences tragic rebellion. For
the Bushman, the event follows the laws of magic; for the
American, it follows the laws of science. The event puts
him under the spell of the laws of mechanics, which for
him govern physical, social, and psychological events.

The mood of 1971 is propitious for a major change of
direction in search of a hopeful future. Institutional goals
continuously contradict institutional products. The poverty
program produces more poor, the war in Asia more
Vietcong, technical assistance more underdevelopment.
Birth control clinics increase survival rates and boost the
population; schools produce more dropouts; and the curb
on one kind of pollution usually increases another.

Consumers are faced with the realization that the more they
can buy, the more deceptions they must swallow. Until
recently it seemed logical that the blame for this pandemic
inflation of dysfunctions could be laid either on the limping
of scientific discovery behind the technological demands or
on the perversity of ethnic, ideological, or class enemies.
Both the expectations of a scientific millennium and of a
war to end all wars have declined.

For the experienced consumer, there is no way back to a
na\"ive reliance on magical technologies. Too many people
have had bad experiences with neurotic computers,
hospital-bred infections, and jams wherever there is traffic
on the road, in the air, or on the phone. Only ten years ago
conventional wisdom anticipated a better life based on an
increase in scientific discovery. Now scientists frighten
children. The moon shots provide a fascinating
demonstration that human failure can be almost eliminated
among the operators of complex systems - yet this does not
allay our fears that the human failure to consume according
to instruction might spread out of control.

For the social reformer there is no way back, either, to the
assumptions of the forties. The hope has vanished that the problem of
justly distributing goods can be sidetracked by creating an abundance
of them. The cost of the minimum package capable of satisfying modern
tastes has skyrocketed, and what makes tastes modern is their
obsolescence prior even to satisfaction.

The limits of the Earth's resources have become evident.
No breakthrough in science or technology could provide
every man in the world with the commodities and services
which are now available to the poor of rich countries. For
instance, it would take the extraction of one hundred times
the present amounts of iron, tin, copper, and lead to achieve
such a goal, with even the ``lightest'' alternative technology.

Finally, teachers, doctors, and social workers realize that
their distinct professional ministrations have one aspect-at
least-in common. They create further demands for the
institutional treatments they provide, faster than they can
provide service institutions.

Not just some part, but the very logic, of conventional
wisdom is becoming suspect. Even the laws of economy
seem unconvincing outside the narrow parameters which
apply to the social, geographic area where most of the
money is concentrated. Money is, indeed, the cheapest
currency, but only in an economy geared to efficiency
measured in monetary terms. Both capitalist and
Communist countries in their various forms are committed
to measuring efficiency in cost-benefit ratios expressed in
dollars. Capitalism flaunts a higher standard of living as its
claim to superiority. Communism boasts of a higher growth
rate as an index of its ultimate triumph. But under either
ideology the total cost of increasing efficiency increases
geometrically. The largest institutions compete most
fiercely for resources which are not listed in any inventory:
the air, the ocean, silence, sunlight, and health. They bring
the scarcity of these resources to public attention only
when they are almost irremediably degraded. Everywhere
nature becomes poisonous, society inhumane, and the inner
life is invaded and personal vocation smothered.

A society committed to the institutionalization of values
identifies the production of goods and services with the
demand for such. Education which makes you need the
product is included in the price of the product. School is the
advertising agency which makes you believe that you need
the society as it is. In such a society marginal value has
become constantly self-transcendent. It forces the few
largest consumers to compete for the power to deplete the
earth, to fill their own swelling bellies, to discipline smaller
consumers, and to deactivate those who still find
satisfaction in making do with what they have. The ethos of
nonsatiety is thus at the root of physical depredation, social
polarization, and psychological passivity.

When values have been institutionalized in planned and
engineered processes, members of modern society believe
that the good life consists in having institutions which
define the values that both they and their society believe
they need. Institutional value can be defined as the level of
output of an institution. The corresponding value of man is
measured by his ability to consume and degrade these
institutional outputs, and thus create a new -- even higher -- 
demand. The value of institutionalized man depends on his
capacity as an incinerator. To use an image -- he has become
the idol of his handiworks. Man now defines himself as the
fur-nace which burns up the values produced by his tools.
And there is no limit to his capacity. His is the act of
Prometheus carried to an extreme.

The exhaustion and pollution of the earth's resources is,
above all, the result of a corruption in man's self-image, of
a regression in his consciousness. Some would like to
speak about a mutation of collective consciousness which
leads to a conception of man as an organism dependent not
on nature and individuals, but rather on institutions. This
institutionalization of substantive values, this belief that a
planned process of treatment ultimately gives results
desired by the recipient, this consumer ethos, is at the heart
of the Promethean fallacy.

Efforts to find a new balance in the global milieu depend
on the deinstitutionalization of values.

The suspicion that something is structurally wrong with the
vision of \emph{homo faber} is common to a growing minority in
capitalist, Communist, and ``underdeveloped'' countries
alike. This suspicion is the shared characteristic of a new
elite. To it belong people of all classes, incomes, faiths, and
civilizations. They have 'become wary of the myths of the
majority: of scientific utopias, of ideological diabolism, and
of the expectation of the distribution of goods and services
with some degree of equality. They share with the majority
the sense of being trapped. They share with the majority
the awareness that most new policies adopted by broad
consensus consistently lead to results which are glaringly
opposed to their stated aims. Yet whereas the Promethean
majority of would-be spacemen still evades the structural
issue, the emergent minority is critical of the scientific \emph{deus
ex machina}, the ideological panacea, and the hunt for
devils and witches. This minority begins to formulate its
suspicion that our constant deceptions tie us to
contemporary institutions as the chains bound Prometheus
to his rock. Hopeful trust and classical irony (eironeia)
must conspire to expose the Promethean fallacy.

Prometheus is usually thought to mean ``foresight,'' or sometimes even
``he who makes the North Star progress.''  He tricked the gods out of
their monopoly of fire, taught men to use it in the forging of iron,
became the god of technologists, and wound up in iron chains.

The Pythia of Delphi has now been replaced by a computer 
which hovers above panels and punch cards. The 
hexameters of the oracle have given way to sixteen-bit 
codes of instructions. Man the helmsman has turned the 
rudder over to the cybernetic machine. The ultimate 
machine emerges to direct our destinies. Children 
phantasize flying their spacecrafts away from a crepuscular 
  earth. 

From the perspectives of the Man on the Moon,
Prometheus could recognize sparkling blue Gaia as the
planet of Hope and as the Arc of Mankind. A new sense of
the finiteness of the Earth and a new nostalgia now can
open man's eyes to the choice of his brother Epimetheus to
wed the Earth with Pandora.


At this point the Greek myth turns into hopeful prophecy
because it tells us that the son of Prometheus was
Deucalion, the Helmsman of the Ark who like Noah
outrode the Flood to become the father of a new mankind
which he made from the earth with Pyrrha, the daughter of
Epimetheus and Pandora. We are gaining insight into the
meaning of the Pythos which Pandora brought from the
gods as being the inverse of the Box: our Vessel and Ark.


We now need a name for those who value hope above
expectations. We need a name for those who love people
more than products, those who believe that

\begin{chapquot}
\hspace{2cm}No people are uninteresting.

\hspace{1.4cm}Their fate is like the chronicle of planets. \\%[5pt]

\hspace{1.4cm}Nothing in them is not particular, 

\hspace{1.4cm}and planet is dissimilar from planet.

\end{chapquot}

We need a name for those who love the earth on which each can meet the other,

\begin{chapquot}
\hspace{2cm}And if a man lived in obscurity 

\hspace{1.4cm}making his friends in that obscurity, 

\hspace{1.4cm}obscurity is not uninteresting.

\end{chapquot}




We need a name for those who collaborate with their
Promethean brother in the lighting of the fire and the
shaping of iron, but who do so to enhance their ability to
tend and care and wait upon the other, knowing that

\begin{chapquot}
\hspace{2cm}to each his world is private, 

\hspace{1.4cm}and in that world one excellent minute. 

\hspace{1.4cm}And in that world one tragic minute.

\hspace{1.4cm}These are private.\footnote{The three quotations are from
``People'' from the book \emph{Selected Poems} by Yevgeny
Yevtushenko. Translated and with Introduction by Robin
Milner Gulland and Peter Levi. Published by E. P. Dutton
\& Co. Inc., 1962, and reprinted with their permission.}

\end{chapquot}


I suggest that these hopeful brothers and sisters be called
Epimethean men.


\vspace{2cm}

\begin{center}
\textls[500]{END}
\end{center}



