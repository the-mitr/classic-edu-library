% !TEX root = de-schooling.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Phenomenology of School}

Some words become so flexible that they cease to be useful
``School'' and ``teaching'' are such terms. Like an amoeba
they fit into almost any interstice of the language. ABM
will teach the Russians, IBM will teach Negro children,
and the army can become the school of a nation.

The search for alternatives in education must therefore start
with an agreement on what it is we mean by ``school.'' This
might be done in several ways. We could begin by listing
the latent functions performed by modern school systems,
such as custodial care, selection, indoctrination, and
learning. We could make a client analysis and verify which
of these latent functions render a service or a disservice to
teachers, employers, children, parents, or the professions.
We could survey the history of Western culture and the
information gathered by anthropology in order to find
institutions which played a role like that now performed by
schooling. We could, finally, recall the many normative
statements which have been made since the time of
Comenius, or even since Quintilian, and discover which of
these the modern school system most closely approaches.
But any of these approaches would oblige us to start with
certain assumptions about a relationship between school
and education. To develop a language in which we can
speak about school without such constant recourse to
education, I have chosen to begin with something that
might be called a phenomenology of public school. For this
purpose I shall define ``school'' as the age-specific, teacher
related process requiring full-time attendance at an
obligatory curriculum.
\begin{enumerate}[label=\textbf{\arabic*}.,]


\item \textbf{Age --} School groups people according to age. This
grouping rests on three unquestioned premises. Children
belong in school. Children learn in school. Children can be taught only in
school. I think these unexamined premises deserve serious
questioning. 

We have grown accustomed to children. We
have decided that they should go to school, do as they are
told, and have neither income nor families of their own. We
expect them to know their place and behave like children.
We remember, whether nostalgically or bitterly, a time
when we were children, too. We are expected to tolerate the
childish behavior of children. Man-kind, for us, is a species
both afflicted and blessed with the task of caring for
children. We forget, however, that our present concept of
``childhood'' developed only recently in Western Europe
and more recently still in the Americas.\footnote{For parallel
histories of modern capitalism and modern childhood see
Philippe Aries, \emph{Centuries Of Childhood}, Knopf, 1962.}

Childhood as distinct from infancy, adolescence, or youth
was unknown to most historical periods. Some Christian
centuries did not even have an eye for its bodily
proportions. Artists depicted the infant as a miniature adult
seated on his mother's arm. Children appeared in Europe
along with the pocket watch and the Christian
moneylenders of the Renaissance. Before our century
neither the poor nor the rich knew of children's dress,
children's games, or the child's immunity from the law.

Childhood belonged to the bourgeoisie. The worker's child,
the peasant's child, and the nobleman's child all dressed the
way their fathers dressed, played the way their fathers
played, and were hanged by the neck as were their fathers.
After the discovery of ``childhood'' by the bourgeoisie all
this changed. Only some churches continued to respect for
some time the dignity and maturity of the young. Until the
Second Vatican Council, each child was instructed that a
Christian reaches moral discernment and freedom at the
age of seven, and from then on is capable of committing
sins for which he may be punished by an eternity in Hell.
Toward the middle of this century, middle-class parents
began to try to spare their children the impact of this
doctrine, and their thinking about children now prevails in
the practice of the Church.

Until the last century, ``children'' of middle-class parents
were made at home with the help of preceptors and private
schools. Only with the advent of industrial society did the
mass production of ``childhood'' become feasible and come
within the reach of the masses. The school system is a
modern phenomenon, as is the childhood it produces.

Since most people today live outside industrial cities, most
people today do not experience childhood. In the Andes
you till the soil once you have become ``useful.'' Before
that, you watch the sheep. If you are well nourished, you
should be useful by eleven, and otherwise by twelve.
Recently, I was talking to my night watchman, Marcos,
about his eleven-year-old son who works in a barbershop. I
noted in Spanish that his son was still a ``\emph{ni\~no}'' Marcos,
surprised, answered with a guileless smile: ``Don Ivan, I
guess you're right.'' Realizing that until my remark the
father had thought of Marcos primarily as his ``son,'' I felt
guilty for having drawn the curtain of childhood between
two sensible persons. Of course if I were to tell the New
York slum-dweller that his working son is still a ``child,'' he
would show no surprise. He knows quite well that his
eleven-year-old son should be allowed childhood, and
resents the fact that he is not. The son of Marcos has yet to
be afflicted with the yearning for childhood; the New
Yorker's son feels deprived.

Most people around the world, then, either do not want or cannot get
modern childhood for their offspring. But it also seems that childhood
is a burden to a good number of those few who are allowed it. Many of
them are simply forced to go through it and are not at all happy
playing the child's role. Growing up through childhood means being
condemned to a process of in-human conflict between self- awareness
and the role imposed by a society going through its own school
age. Neither Stephen Daedalus nor Alexander Portnoy enjoyed childhood,
and neither, I suspect, did many of us like to be treated as children.

If there were no age-specific and obligatory learning
institution, ``childhood'' would go out of production. The
youth of rich nations would be liberated from its
destructiveness, and poor nations would cease attempting
to rival the childishness of the rich. If society were to
outgrow its age of childhood, it would have to become
livable for the young. The present disjunction between an
adult society which pretends to be humane and a school
environment which mocks reality could no longer be
maintained.

The disestablishment of schools could also end the present
discrimination against infants, adults, and the old in favor
of children throughout their adolescence and youth. The
social decision to allocate educational resources preferably
to thosecitizens who have outgrown the extraordinary
learning capacity of their first four years and have not
arrived at the height of their self-motivated learning will, in
retrospect, probably appear as bizarre.

Institutional wisdom tells us that children need school.
Institutional wisdom tells us that children learn in school.
But this institutional wisdom is itself the product of schools
because sound common sense tells us that only children
can be taught in school. Only by segregating human beings
in the category of childhood could we ever get them to
submit to the authority of a schoolteacher.


\item \textbf{Teachers and Pupils --} By definition, children are pupils.
The demand for the milieu of childhood creates an
unlimited market for accredited teachers. School is an
institution built on the axiom that learning is the result of
teaching. And institutional wisdom continues to accept this
axiom, despite overwhelming evidence to the contrary.

We have all learned most of what we know outside school.
Pupils do most of their learning without, and often despite,
their teachers. Most tragically, the majority of men are
taught their lesson by schools, even though they never go
\emph{to} school.

Everyone learns how to live outside school. We learn to
speak, to think, to love, to feel, to play, to curse, to politick,
and to work without interference from a teacher. Even
children who are under a teacher's care day and night are
no exception to the rule. Orphans, idiots, and
schoolteachers' sons learn most of what they learn outside
the ``educational'' process planned for them. Teachers have
made a poor showing in their attempts at increasing
learning among the poor. Poor parents who want their
children to go to school are less concerned about what they
will learn than about the certificate and money they will
earn. And middle-class parents commit their children to a
teacher's care to keep them from learning what the poor
learn on the streets. Increasingly educational research
demonstrates that children learn most of what teachers
pretend to teach them from peer groups, from comics, from
chance observations, and above all from mere participation
in the ritual of school. Teachers, more often than not,
obstruct such learning of subject matters as goes on in
school.

Half of the people in our world never set foot in school.
They have no contact with teachers, and they are deprived
of the privilege of becoming dropouts. Yet they learn quite
effectively the message which school teaches: that they
should have school, and more and more of it. School
instructs them in their own inferiority through the tax
collector who makes them pay for it, or through the
demagogue who raises their expectations of it, or through
their children once the latter are hooked on it. So the poor
are robbed of their self-respect by subscribing to a creed
that grants salvation only through the school. At least the
Church gave them a chance to repent at the hour of death.
School leaves them with the expectation (a counterfeit
hope) that their grandchildren will make it. That
expectation is of course still more learning which comes
from school but not from teachers.

Pupils have never credited teachers for most of their
learning. Bright and dull alike have always relied on rote,
reading, and wit to pass their exams, motivated by the stick
or by the carrot of a desired career.

Adults tend to romanticize their schooling. In retrospect,
they attribute their learning to the teacher whose patience
they learned to admire. But the same adults would worry
about the mental health of a child who rushed home to tell
them what he learned from his every teacher.

Schools create jobs for schoolteachers, no matter what their
pupils learn from them.


\item \textbf{Full-Time Attendance --} Every month I see another list of proposals
  made by some U.S. industry to AID, suggesting the replacement of
  Latin-American ``classroom practitioners'' either by disciplined
  systems administrators or just by TV. In the United States teaching
  as a team enterprise of educational researchers, designers, and
  technicians is gaining acceptance. But, no matter whether the
  teacher is a schoolmarm or a team of men in white coats, and no
  matter whether they succeed in teaching the subject matter listed in
  the catalogue or whether they fail, the professional teacher creates
  a sacred milieu.

Uncertainty about the future of professional teaching puts
the classroom into jeopardy. Were educational
professionals tospecialize in promoting learning, they
would have to abandon a system which calls for between
750 and 1,000 gatherings a year. But of course teachers do
a lot more. The institutional wisdom of schools tells
parents, pupils, and educators that the teacher, if he is to
teach, must exercise his authority in a sacred precinct. This
is true even for teachers whose pupils spend most of their
school time in a classroom without walls.

School, by its very nature, tends to make a total claim on
the time and energies of its participants. This, in turn,
makes the teacher into custodian, preacher, and therapist.

In each of these three roles the teacher bases his authority
on a different claim. The \emph{teacher-as-custodian} acts as a
master of ceremonies, who guides his pupils through a
drawn-out labyrinthine ritual. He arbitrates the observance
of rules and administers the intricate rubrics of initiation to
life. At his best, he sets the stage for the acquisition of
some skill as schoolmasters always have. Without illusions
of producing any profound learning, he drills his pupils in
some basic routines.

The \emph{teacher-as-moralist} substitutes for parents, God, or the
state. He indoctrinates the pupil about what is right or
wrong, not only in school but also in society at large. He
stands in \emph{loco parentis} for each one and thus ensures that
all feel themselves children of the same state.

The \emph{teacher-as-therapist} feels authorized to delve into the
personal life of his pupil in order to help him grow as a
person. When this function is exercised by a custodian and
preacher, it usually means that he persuades the pupil to
submit to a domestication of his vision of truth and his
sense of what is right.

The claim that a liberal society can be founded on the modern school
is paradoxical. The safeguards of individual freedom are all canceled
in the dealings of a teacher with his pupil. When the schoolteacher
fuses in his person the functions of judge, ideologue, and doctor, the
fundamental style of society is perverted by the very process which
should prepare for life. A teacher who combines these three powers
contributes to the warping of the child much more than the laws which
establish his legal or economic minority, or restrict his right to
free assembly or abode.

Teachers are by no means the only professionals who offer
therapy. Psychiatrists, guidance counselors, and job
counselors, even lawyers, help their clients to decide, to
develop their personalities, and to learn. Yet common sense
tells the client that such professionals should abstain from
imposing their opinion of what is right or wrong, or from
forcing anyone to follow their advice. Schoolteachers and
ministers are the only professionals who feel entitled to pry
into the private affairs of their clients at the same time as
they preach to a captive audience.

Children are protected by neither the First nor the Fifth
Amendment when they stand before that secular priest, the
teacher. The child must confront a man who wears an
invisible triple crown, like the papal tiara, the symbol of
triple authority combined in one person. For the child, the
teacher pontificates as pastor, prophet, and priest-he is at
once guide, teacher, and administrator of a sacred ritual. He
combines the claims of medieval popes in a society
constituted under the guarantee that these claims shall
never be exercised together by one established and
obligatory institution -- church or state.

Defining children as full-time pupils permits the teacher to
exercise a kind of power over their persons which is much
less limited by constitutional and consuetudinal restrictions
than the power wielded by the guardians of other social
enclaves. Their chronological age disqualifies children
from safeguards which are routine for adults in a modern
asylum -- madhouse, monastery, or jail.

Under the authoritative eye of the teacher, several orders of
value collapse into one. The distinctions between morality,
legal. ity, and personal worth are blurred and eventually
eliminated. Each transgression is made to be felt as a
multiple offense. The offender is expected to feel that he
has broken a rule, that he has behaved immorally, and that
he has let himself down. A pupil who adroitly obtains
assistance on an exam is told that he is an outlaw, morally
corrupt, and personally worthless.

Classroom attendance removes children from the everyday world of
Western culture and plunges them into an environment far more
primitive, magical, and deadly serious. School could not create such
an enclave within which the rules of ordinary reality are suspended,
unless it physically incarcerated the young during many successive
years on sacred territory. The attendance rule makes it possible for
the schoolroom to serve as a magic womb, from which the child is
delivered periodically at the school days and school year's completion
until he is finally expelled into adult life. Neither universal
extended childhood nor the smothering atmosphere of the classroom
could exist without schools. Yet schools, as compulsory channels for
learning, could exist without either and be more repressive and
destructive than anything we have come to know. 

To understand what it
means to deschool society, and not just to reform the educational
establishment, we must now focus on the hidden curriculum of
schooling. We are not concerned here, directly, with the hidden
curriculum of the ghetto streets which brands the poor or with the
hidden curriculum of the drawing room which benefits the rich. We are
rather concerned to call attention to the fact that the ceremonial or
ritual of schooling itself constitutes such a hidden curriculum. Even
the best of teachers cannot entirely protect his pupils from
it. Inevitably, this hidden curriculum of schooling adds prejudice and
guilt to the discrimination which a society practices against some of
its members and compounds the privilege of others with a new title to
condescend to the majority. Just as inevitably, this hidden curriculum
serves as a ritual of initiation into a growth-oriented consumer
society for rich and poor alike.



\end{enumerate}