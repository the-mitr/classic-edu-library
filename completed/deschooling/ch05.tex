% !TEX root = de-schooling.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Irrational Consistencies}\footnote{This chapter was presented originally at a meeting of the American Educational Research Association, in New York City, February 6, 1971.}

I believe that the contemporary crisis of education demands
that we review the very idea of publicly prescribed
learning, rather than the methods used in its enforcement.
The dropout rate -- especially of junior-high-school students
and elementary-school teachers -- points to a grass-roots
demand for a completely fresh look. The ``classroom
practitioner'' who considers himself a liberal teacher is
increasingly attacked from all sides. The free-school
movement, confusing discipline with indoctrination, has
painted him into the role of a destructive authoritarian. The
educational technologist consistently demonstrates the
teacher's inferiority at measuring and modifying behavior.
And the school administration for which he works forces
him to bow to both Summerhill and Skinner, making it
obvious that compulsory learning cannot be a liberal
enterprise. No wonder that the desertion rate of teachers is
overtaking that of their students.

America's commitment to the compulsory education of its
young now reveals itself to be as futile as the pretended
American commitment to compulsory democratization of
the Vietnamese. Conventional schools obviously cannot do
it. The free-school movement entices unconventional
educators, but ultimately does so in support of the
conventional ideology of schooling. And the promises of
educational technologists, that their research and
development -- if adequately funded -- can offer some kind of
final solution to the resistance of youth to compulsory
learning, sound as confident and prove as fatuous as the
analogous promises made by the military technologists.

The criticism directed at the American school system by
the behaviorists and that coming from the new breed of
radical educators seem radically opposed. The behaviorists
apply educational research to the ``induction of autotelic
instruction through individualized learning packages.''
Their style clashes with the nondirective cooption of youth
into liberated communes established under the supervision
of adults. Yet, in historical perspective, these two are just
contemporary manifestations of the seemingly
contradictory yet really complementary goals of the public
school system. From the beginning of this century, the
schools have been protagonists of social control on the one
hand and free cooperation on the other, both placed at the
service of the ``good society,'' conceived of as a highly organized and smoothly working corporate structure.

Under the impact of intense urbanization, children became
a natural resource to be molded by the schools and fed into
the industrial machine. Progressive politics and the cult of
efficiency converged in the growth of the U.S. public
school.\footnote{See Joel Spring, \emph{Education and the Rise of the Corporate State}, Cuaderno No. 50. Centro Intercultural de Documentaci\'on, Cuernavaca, Mexico, 1971.} Vocational
guidance and the junior high school were two important
results of this kind of thinking.

It appears, therefore, that the attempt to produce specified
behavioral changes which can be measured and for which
the processor can be held accountable is just one side of a
coin, whose other side is the pacification of the new
generation within specially engineered enclaves which will
seduce them into the dream world of their elders. These
pacified in society are well described by Dewey, who wants
us to ``make each one of our schools an embryonic
community life, active with types of occupations that
reflect the life of the larger society, and \emph{permeate} it with the
\emph{spirit} of art, history and science.'' In this historical
perspective, it would be a grave mistake to interpret the
current three-cornered controversy between the school
establishment, the educational technologists and the free
schools as the prelude to a revolution in education. This
controversy reflects rather a stage of an attempt to escalate
an old dream into fact, and to finally make all valuable
learning the result of professional teaching. 

Most
educational alternatives proposed converge toward goals
which are immanent in the production of the cooperative
man whose individual needs are met by means of his
specialization in the American system: They are oriented
toward the improvement of what -- for lack of a better
phrase -- I call the schooled society. Even the seemingly
radical critics of the school system are not willing to
abandon the idea that they have an obligation to the young,
especially to the poor, an obligation to process them,
whether by love or by fear, into a society which needs
disciplined specialization as much from its producers as
from its consumers and also their full commitment to the
ideology which puts economic growth first.

Dissent veils the contradictions inherent in the very idea of
school. The established teachers unions, the technological
wizards, and the educational liberation movement reinforce
the commitment of the entire society to the fundamental
axioms of a schooled world, somewhat in the manner in
which many peace and protest movements reinforce the
commitments of their members -- be they black, female,
young, or poor -- to seek justice through the growth of the
gross national income.

Some of the tenets which now go unchallenged are easy to
list. There is, first, the shared belief that behavior which has
been acquired in the sight of a pedagogue is of special
value to the pupil and of special benefit to society. This is
related to the assumption that social man is born only in
adolescence, and properly born only if he matures in the
school-womb, which some want to gentle by
permissiveness, others to stuff with gadgets, and still others
to varnish with a liberal tradition. And there is, finally, a
shared view of youth which is psychologically romantic
and politically conservative. According to this view,
changes in society must be brought about by burdening the
young with the responsibility of transforming it -- but only
after their eventual release from school. It is easy for a
society founded on such tenets to build up a sense of its
responsibility for the education of the new generation, and
this inevitably means that some men may set, specify, and
evaluate the personal goals of others. 

In a ``passage from an
imaginary Chinese encyclopedia,'' Jorge Luis Borges tries
to evoke the sense of giddiness such an attempt must
produce. He tells us that animals are divided into the
following classes:
\begin{enumerate*}[label=(\alph*)]
\item ``those belonging to the emperor,
\item those that are embalmed, 
\item those that are domesticated,
\item the suckling pigs, 
\item the sirens, 
\item fabulous ones,
\item the roaming dogs, 
\item those included in the present classification, 
\item those that drive themselves crazy, 
\item innumerable ones, 
\item those painted with a very fine brush of camel hair, 
\item et cetera, 
\item those who have just broken the jug, 
\item those who resemble flies from afar.'' 
\end{enumerate*}
Now, such
a taxonomy does not come into being unless somebody
feels it can serve his purpose: in this case, I suppose, that
somebody was a tax collector. For him, at least, this
taxonomy of beasts \emph{must} have made sense, the same way in
which the taxonomy of educational objectives makes sense
to scientific authors.

In the peasant, the vision of men with such inscrutable
logic, empowered to assess his cattle, must have induced a
chilling sense of impotence. Students, for analogous
reasons, tend to feel paranoiac when they seriously submit
to a curriculum. Inevitably they are even more frightened
than my imaginary Chinese peasant, because it is their life
goals rather than their life-stock which is being branded
with an inscrutable sign.

This passage of Borges is fascinating, because it evokes the
logic of \emph{irrational consistency} which makes Kafka's and
Koestler's bureaucracies so sinister yet so evocative of
everyday life. Irrational consistency mesmerizes
accomplices who are engaged in mutually expedient and
disciplined exploitation. It is the logic generated by
bureaucratic behavior. And it becomes the logic of a society
which demands that the managers of its educational
institutions be held publicly accountable for the behavioral
modification they produce in their clients. Students who
can be motivated to value the educational packages which
their teachers obligate them to consume are comparable to
Chinese peasants who can fit their flocks into the tax form
provided by Borges.

At some time during the last two generations a
commitment to therapy triumphed in American culture, and
teachers came to be regarded as the therapists whose
ministrations all men need, if they wish to enjoy the
equality and freedom with which, according to the
Constitution, they are born. Now the teacher-therapists go
on to propose lifelong educational treatment as the next
step. The \emph{style} of this treatment is under discussion: Should
it take the form of continued adult classroom attendance?
Electronic ecstasy? Or periodic sensitivity sessions? All
educators are ready to conspire to push out the walls of the
classroom, with the goal of transforming the entire culture
into a school.

The American controversy over the future of education,
behind its rhetoric and noise, is more conservative than the
discourse in other areas of public policy. On foreign affairs,
at least, an organized minority constantly reminds us that
the United States must renounce its role as the world's
policeman. Radical economists, and now even their less
radical teachers, question aggregate growth as a desirable
goal. There are lobbies for prevention over cure in
medicine and others in favor of fluidity over speed in
transportation. Only in the field of education do the
articulate voices demanding a radical deschooling of
society remain so dispersed. There is a lack of cogent
argument and of mature leadership aiming at the
disestablishment of any and all institutions which serve the
purpose of compulsory \emph{learning}. For the moment, the
radical deschooling of society is still a cause without a
party. This is especially surprising in a time of growing,
though chaotic, resistance to all forms of institutionally
planned instruction on the part of those aged twelve to
seventeen.

Educational innovators still assume that educational institutions
function like funnels for the programs they package. For my argument
it is irrelevant whether these funnels take the form of a classroom, a
TV transmitter, or a ``liberated zone.'' It is equally irrelevant
whether the packages purveyed are rich or poor, hot or cold, hard and
measurable (like Math III), or impossible to assess (like
sensitivity). What counts is that education is assumed to be the
result of an institutional process managed by the educator. As long as
the relations continue to be those between a supplier and a consumer,
educational research will remain a circular process. It will amass
scientific evidence in support of the need for more educational
packages and for their more deadly accurate delivery to the individual
customer, just as a certain brand of social science can prove the need
for the delivery of more military treatment.

An educational revolution depends on a twofold inversion:
a new orientation for research and a new understanding of
the educational style of an emerging counterculture.

Operational research now seeks to optimize the efficiency
of an inherited framework -- a framework which is itself
never questioned. This framework has the syntactic
structure of a funnel for teaching packages. The syntactic
alternative to it is an educational network or web for the
autonomous assembly of resources under the personal
control of each learner. This alternative structure of an
educational institution now lies within the conceptual blind
spot of our operational research. If research were to focus
on it, this would constitute a true scientific revolution.
The blind spot of educational research reflects the cultural
bias of a society in which technological growth has been
confused with technocratic control. For the technocrat the
value of an environment increases as more contacts
between each man and his milieu can be programmed. In
this world the choices which are manageable for the
observer or planner converge with the choices possible for
the observed so-called beneficiary. Freedom is reduced to a
selection among packaged commodities.

The emerging counterculture reaffirms the values of
semantic content above the efficiency of increased and
more rigid syntax. It values the wealth of connotation
above the power of syntax to produce wealth. It values the
unpredictable outcome of self-chosen personal encounter
above the certified quality of professional instruction. This
reorientation toward personal surprise rather than
institutionally engineered values will be disruptive of the
established order until we dissociate the increasing
availability of technological tools which facilitate
encounter from the increasing control of the technocrat of
what happens when people meet.

Our present educational institutions are at the service of the
teacher's goals. The relational structures we need are those
which will enable each man to define himself by learning
and by contributing to the learning of others.
