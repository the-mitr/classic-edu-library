% !TEX root = de-schooling.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Institutional Spectrum}

Most utopian schemes and futuristic scenarios call for new and costly
technologies, which would have to be sold to rich and poor nations
alike. Herman Kahn has found pupils in Venezuela, Argentina, and
Colombia. The pipe dreams of Sergio Bernardes for his Brazil of the
year 2000 sparkle with more new machinery than is now possessed by the
United States, which by then will be weighted down with the antiquated
missile sites, jetports, and cities of the sixties and
seventies. Futurists inspired by Buckminster Fuller would depend on
cheaper and more exotic devices. They count on the acceptance of a new
but possible technology that would apparently allow us to make more
with less -- lightweight monorails rather than supersonic transport;
vertical living rather than horizontal sprawling. All of today's
futuristic planners seek to make economically feasible what is
technically possible while refusing to face the inevitable social
consequence: the increased craving of all men for goods and services
that will remain the privilege of a few.

I believe that a desirable future depends on our deliberately choosing
a life of action over a life of consumption, on our engendering a life
style which will enable us to be spontaneous, independent, yet related
to each other, rather than maintaining a life style which only allows
us to make and unmake, produce and consume -- a style of life which is
merely a way station on the road to the depletion and pollution of the
environment. The future depends more upon our choice of institutions
which support a life of action than on our developing new ideologies
and technologies. We need a set of criteria which will permit us to
recognize those institutions which support personal growth rather than
addiction, as well as the will to invest our technological resources
preferentially in such institutions of growth.

The choice is between two radically opposed institutional types, both
of which are exemplified in certain existing institutions, although
one type so characterizes the contemporary period. as to almost define
it. This dominant type I would propose to call the manipulative
institution.  The other type also exists, but only precariously. The
institutions which fit it are humbler and less noticeable; yet I take
them as models for a more desirable future. I call them ``convivial''
and suggest placing them at the left of an institutional spectrum,
both to show that there are institutions which fall between the
extremes and to illustrate how historical institutions can change
color as they shift from facilitating activity to organizing
production.

Generally, such a spectrum, moving from left to right, has been used
to characterize men and their ideologies, not our social institutions
and their styles. This categorization of men, whether as individuals
or in groups, often generates more heat than light. Weighty objections
can be raised against using an ordinary convention in an unusual
fashion, but by doing so I hope to shift the terms of the discussion
from a sterile to a fertile plane. It will become evident that men of
the left are not always characterized by their opposition to the
manipulative institutions, which I locate to the right on the
spectrum.

The most influential modern institutions crowd up at the right of the
spectrum. Law enforcement has moved there, as it has shifted from the
hands of the sheriff to those of the FBI and the Pentagon. Modern
warfare has become a highly professional enterprise whose business is
killing. It has reached the point where its efficiency is measured in
body counts. Its peace-keeping potential depends on its ability to
convince friend and foe of the nation's unlimited death-dealing
power. Modern bullets and chemicals are so effective that a few cents'
worth, properly delivered to the intended ``client,'' unfailingly kill
or maim. But delivery costs rise vertiginously; the cost of a dead
Vietnamese went from \$360,000 in 1967 to \$450,000 in 1969. Only
economies on a scale approaching race suicide would render modern
warfare economically efficient. The boomerang effect in war is
becoming more obvious: the higher the body count of dead Vietnamese,
the more enemies the United States acquires around the world;
likewise, the more the United States must spend to create another
manipulative institution -- cynically dubbed ``pacification'' in a futile
effort to absorb the side effects of war.

At this same extreme on the spectrum we also find social agencies
which specialize in the manipulation of their clients. Like the
military, they tend to develop effects contrary to their aims as the
scope of their operations increases. These social institutions are
equally counterproductive, but less obviously so. Many assume a
therapeutic and compassionate image to mask this paradoxical
effect. For example, jails, up until two centuries ago, served as a
means of detaining men until they were sentenced, maimed, killed, or
exiled, and were sometimes deliberately used as a form of
torture. Only recently have we begun to claim that locking people up
in cages will have a beneficial effect on their character and
behavior. 

Now quite a few people are beginning to understand that jail
increases both the quality and the quantity of criminals, that, in
fact, it often creates them out of mere nonconformists. Far fewer
people, however, seem to understand that mental hospitals, nursing
homes, and orphan asylums do much the same thing. These institutions
provide their clients with the destructive self-image of the
psychotic, the overaged, or the waif, and provide a rationale for the
existence of entire professions, just as jails produce income for
wardens. Membership in the institutions found at this extreme of the
spectrum is achieved in two ways, both coercive: by forced commitment
or by selective service.

At the opposite extreme of the spectrum lie institutions distinguished
by spontaneous use -- the ``convivial'' institutions. Telephone link-ups,
subway lines, mail routes, public markets and exchanges do not require
hard or soft sells to induce their clients to use them. Sewage
systems, drinking water, parks, and sidewalks are institutions men use
without having to be institutionally convinced that it is to their
advantage to do so. Of course, all institutions require some
regulation. But the operation of institutions which exist to be used
rather than to produce something requires rules of an entirely
different nature from those required by treatment-institutions, which
are manipulative.  The rules which govern institutions for use have
mainly the purpose of avoiding abuses which would frustrate their
general accessibility. Sidewalks must be kept free of obstructions,
the industrial use of drinking water must be held within limits, and
ball playing must be restricted to special areas within a park. At
present we need legislation to limit the abuse of our telephone lines
by computers, the abuse of mail service by advertisers, and the
pollution of our sewage systems by industrial wastes. The regulation
of convivial institutions sets limits to their use; as one moves from
the convivial to the manipulative end of the spectrum, the rules
progressively call for unwilling consumption or participation. The
different cost of acquiring clients is just one of the characteristics
which distinguish convivial from manipulative institutions.

At both extremes of the spectrum we find service
institutions, but on the right the service is imposed
manipulation, and the client is made the victim of
advertising, aggression, indoctrination, imprisonment, or
electroshock. On the left the service is amplified
opportunity within formally defined limits, while the client
remains a free agent. Right-wing institutions tend to be
highly complex and costly production processes in which
much of the elaboration and expense is concerned with
convincing consumers that they cannot live without the
product or the treatment offered by the institution. Left-
wing institutions tend to be networks which facilitate
client-initiated communication or cooperation.

The manipulative institutions of the right are either socially or
psychologically ``addictive.'' Social addiction, or escalation, consists
in the tendency to prescribe increased treatment if smaller quantities
have not yielded the desired results. Psycho-logical addiction, or
habituation, results when consumers become hooked on the need for more
and more of the process or product. The self-activated institutions of
the left tend to be self-limiting. Unlike production processes which
identify satisfaction with the mere act of consumption, these networks
serve a purpose beyond their own repeated use. An individual picks up
the telephone when he wants to say something to someone else, and
hangs up when the desired communication is over. He does not,
teen-agers excepted, use the telephone for the sheer pleasure of
talking into the receiver. If the telephone is not the best way to get
in touch, people will write a letter or take a trip. Right-wing
institutions, as we can see clearly in the case of schools, both
invite compulsively repetitive use and frustrate alternative ways of
achieving similar results.

Toward, but not at, the left on the institutional spectrum, we can
locate enterprises which compete with others in their own field, but
have not begun notably to engage in advertising. Here we find hand
laundries, small bakeries, hairdressers, and -- to speak of
professionals -- some lawyers and music teachers. Characteristically left
of center, then, are self-employed persons who have institutionalized
their services but not their publicity. They acquire clients through
their personal touch and the comparative quality of their services.

Hotels and cafeterias are somewhat closer to the center. The big
chains like Hilton -- which spend huge amounts on selling their
image -- often behave as if they were running institutions of the
right. Yet Hilton and Sheraton enterprises do not usually offer
anything more -- in fact, they often give less -- than similarly priced,
independently managed lodgings. Essentially, a hotel sign beckons to a
traveler in the manner of a road sign. It says, ``Stop, here is a bed
for you,'' rather than, ``You should prefer a hotel bed to a park
bench!''

The producers of staples and most perishable consumer goods belong in
the middle of our spectrum. They fill generic demands and add to the
cost of production and distribution whatever the market will bear in
advertising costs for publicity and special packaging. The more basic
the product -- be it goods or services -- the more does competition tend to
limit the sales cost of the item.  

Most manufacturers of consumer goods have moved much further to the
right. Both directly and indirectly, they produce demands for
accessories which boost real purchase price far beyond production
cost. General Motors and Ford produce means of transportation, but
they also, and more importantly, manipulate public taste in such a way
that the need for transportation is expressed as a demand for private
cars rather than public buses. They sell the desire to control a
machine, to race at high speeds in luxurious comfort, while also
offering the fantasy at the end of the road. What they sell, however,
is not just a matter of uselessly big motors, superfluous gadgetry, or
the new extras forced on the manufacturers by Ralph Nader and the
clean-air lobbyists. The list price includes souped-up engines, air-
conditioning, safety belts, and exhaust controls; but other costs not
openly declared to the driver are also involved: the corporation's
advertising and sales expenses, fuel, maintenance and parts,
insurance, interest on credit, as well as less tangible costs like
loss of time, temper, and breathable air in our traffic-congested
cities.

An especially interesting corollary to our discussion of socially
useful institutions is the system of ``public'' highways. This major
element of the total cost of automobiles deserves lengthier treatment,
since it leads directly to the rightist institution in which I am most
interested, namely, the school.

\begin{center}
\textcolor{OrangeRed}{\textbf{False Public Utilities}}
\end{center}

The highway system is a network for locomotion across relatively large
distances. As a network, it appears to belong on the left of the
institutional spectrum. But here we must make a distinction which will
clarify both the nature of highways and the nature of true public
utilities.  Genuinely all-purpose roads are true public utilities.
Superhighways are private preserves, the cost of which has been
partially foisted upon the public.

Telephone, postal, and highway systems are all networks, and none of
them is free. Access to the telephone network is limited by time
charges on each call. These rates are relatively small and could be
reduced without changing the nature of the system. Use of the
telephone system is not in the least limited by what is transmitted,
although it is best used by those who can speak coherent sentences in
the language of the other party-an ability universally possessed by
those who wish to use the network. Postage is usually cheap. Use of
the postal system is slightly limited by the price of pen and paper,
and somewhat more by the ability to write. Still, when someone who
does not know how to write has a relative or friend to whom he can
dictate a letter, the postal system is at his service, as it is if he
wants to ship a recorded tape.

The highway system does not similarly become available to someone who
merely learns to drive. The telephone and postal networks exist to
serve those who wish to use them, while the highway system mainly
serves as an accessory to the private automobile. The former are true
public utilities, whereas the latter is a public service to the owners
of cars, trucks, and buses. Public utilities exist for the sake of
communication among men; highways, like other institutions of the
right, exist for the sake of a product.  Auto manufacturers, we have
already observed, \emph{produce} simultaneously both cars and the demand for
cars. They also \emph{produce} the demand for multilane highways, bridges,
and oilfields. The private car is the focus of a cluster of right-wing
institutions. The high cost of each element is dictated by elaboration
of the basic product, and to sell the basic product is to hook society
on the entire package.

To plan a highway system as a true public utility would discriminate
against those for whom velocity and individualized comfort are the
primary transportation values, in favor of those who value fluidity
and destination.  It is the difference between a far-flung network
with maximum access for travelers and one which offers only privileged
access to restricted areas.

Transferring a modern institution to the developing nations provides
the acid test of its quality. In very poor countries roads are usually
just good enough to permit transit by special, high-axle trucks loaded
with groceries, livestock, or people. This kind of country should use
its limited resources to build a spiderweb of trails extending to
every region and should restrict imports to two or three different
models of highly durable vehicles which can manage all trails at low
speed. This would simplify maintenance and the stocking of spare
parts, permit the operation of these vehicles around the clock, and
provide maximum fluidity and choice of destination to all
citizens. This would require the engineering of all-purpose vehicles
with the simplicity of the Model T, making use of the most modern
alloys to guarantee durability, with a built-in speed limit of not
more than fifteen miles per hour, and strong enough to run on the
roughest terrain. Such vehicles are not on the market because there is
no demand for them. As a matter of fact, such a demand would have to
be cultivated, quite possibly under the protection of strict
legislation. At present, whenever such a demand is even slightly felt,
it is quickly snuffed out by counter-publicity aimed at universal sales
of the machines which currently extract from U.S. taxpayers the money
needed for building superhighways.

In order to ``improve'' transportation, all countries-even the
poorest-now plan highway systems designed for the passenger cars and
high-speed trailers which fit the velocity-conscious minority of
producers and consumers in the elite classes. This approach is
frequently rationalized as a saving of the most precious resource of a
poor country: the time of the doctor, the school inspector, or the
public administrator. These men, of course, serve almost exclusively
the same people who have, or hope one day to have, a car. Local taxes
and scarce international exchange are wasted on \emph{false public
utilities}.

``Modern'' technology transferred to poor countries falls into three
large categories: goods, factories which make them, and service
institutions -- principally schools -- which make men into modern
producers and consumers. Most countries spend by far the largest
proportion of their budget on schools. The school-made graduates then
create a demand for other conspicuous utilities, such as industrial
power, paved highways, modern hospitals, and airports, and these in
turn create a market for the goods made for rich countries and, after
a while, the tendency to import obsolescent factories to produce them.

Of all ``false utilities,'' school is the most insidious.  Highway
systems produce only a demand for cars. Schools create a demand for
the entire set of modern institutions which crowd the right end of the
spectrum. A man who questioned the need for highways would be
written off as a romantic; the man who questions the need for school
is immediately attacked as either heartless or imperialist.

\begin{center}
\textcolor{OrangeRed}{\textbf{Schools as False Public Utilities}}
\end{center}

Like highways, schools, at first glance, give the impression of being
equally open to all comers. They are, in fact, open only to those who
consistently renew their credentials. Just as highways create the
impression that their present level of cost per year is necessary if
people are to move, so schools are presumed essential for attaining
the competence required by a society which uses modern technology. We
have exposed speedways as spurious public utilities by noting their
dependence on private automobiles. Schools are based upon the equally
spurious hypothesis that learning is the result of curricular
teaching.

Highways result from a perversion of the desire and need for mobility
into the demand for a private car. Schools themselves pervert the
natural inclination to grow and learn into the demand for
instruction. Demand for manufactured maturity is a far greater
abnegation of self-initiated activity than the demand for manufactured
goods. Schools are not only to the right of highways and cars; they
belong near the extreme of the institutional spectrum occupied by
total asylums. Even the producers of body counts kill only bodies. By
making men abdicate the responsibility for their own growth, school
leads many to a kind of spiritual suicide.

Highways are paid for in part by those who use them, since tolls and
gasoline taxes are extracted only from drivers.  School, on the other
hand, is a perfect system of regressive taxation, where the privileged
graduates ride on the back of the entire paying public. School puts a
head tax on promotion. The underconsumption of highway mileage is not
nearly so costly as the underconsumption of schooling.  The man who
does not own a car in Los Angeles may be almost immobilized, but if he
can somehow manage to reach a work place, he can get and hold a
job. The school dropout has no alternative route. The suburbanite with
his new Lincoln and his country cousin who drives a beat-up jalopy get
essentially the same use out of the highway, even though one man's car
costs thirty times more than the other's. The value of a man's
schooling is a function of the number of years he has completed and of
the costliness of the schools he has attended. The law compels no one
to drive, whereas it obliges everyone to go to school.

The analysis of institutions according to their present placement on a
left-right continuum enables me to clarify my belief that fundamental
social change must begin with a change of consciousness about
institutions and to explain why the dimension of a viable future turns
on the rejuvenation of institutional style.

During the sixties institutions born in different decades since the
French Revolution simultaneously reached old age; public school
systems founded in the time of Jefferson or of Atat\"urk, along with
others which started after World War II, all became bureaucratic,
self-justifying, and manipulative. The same thing happened to systems
of social security, to labor unions, major churches and diplomacies,
the care of the aged, and the disposal of the dead.

Today, for instance, the school systems of Colombia, Britain, the
U.S.S.R., and the U.S. resemble each other more closely than
U.S. schools of the late 1890's resembled either today's or their
contemporaries in Russia. Today all schools are obligatory,
open-ended, and competitive. The same convergence in institutional
style affects health care, merchandising, personnel administration,
and political life.  All these institutional processes tend to pile up
at the manipulative end of the spectrum.

A merger of world bureaucracies results from this convergence of
institutions. The style, the ranking systems, and the paraphernalia
(from textbook to computer) are standardized on the planning boards of
Costa Rica or Afghanistan after the model of Western Europe.

Everywhere these bureaucracies seem to focus on the same task:
promoting the growth of institutions of the right. They are concerned
with the making of things, the making of ritual rules, and the
making -- and reshaping -- of ``executive truth,'' the ideology or fiat which
establishes the current value which should be attributed to their
product.

Technology provides these bureaucracies with increasing power on the
right hand of society. The left hand of society seems to wither, not
because technology is less capable of increasing the range of human
action, and providing time for the play of individual imagination and
personal creativity, but because such use of technology does not
increase the power of an elite which administers it. The postmaster
has no control over the substantive use of the mails, the switchboard
operator or Bell Telephone executive has no power to stop adultery,
murder, or subversion from being planned over his network.  At stake
in the choice between the institutional right and left is the very
nature of human life. Man must choose whether to be rich in things or
in the freedom to use them.  He must choose between alternate styles
of life and related production schedules.

Aristotle had already discovered that ``making and acting'' are
different, so different, in fact, that one never includes the
other. ``For neither is acting a way of making -- nor making a way of
truly acting. Architecture [\emph{techne}] is a way of making \ldots{} of
bringing something into being whose origin is in the maker and not in
the thing. Making has always an end other than itself, action not; for
good action itself is its end. Perfection in making is an art,
perfection in acting is a virtue.''\footnote{\emph{Nichomachean Ethics}, 1 140.} The
word which Aristotle employed for making was ``\emph{poesis}'', and the word he
employed for doing, ``\emph{praxis}.'' A move to the right implies that an
institution is being restructured to increase its ability to ``make,''
while as it moves to the left, it is being restructured to allow
increased ``doing'' or ``\emph{praxis}.'' Modern technology has increased the
ability of man to relinquish the ``making'' of things to machines, and
his potential time for ``acting'' has increased.

``Making'' the necessities of life has ceased to take up his
time. Unemployment is the result of this modernization: it is the
idleness of a man for whom there is nothing to ``make'' and who does not
know what to ``do'' -- that is, how to ``act.'' Unemployment is the sad
idleness of a man who, contrary to Aristotle, believes that making
things, or working, is virtuous and that idleness is bad.
Unemployment is the experience of the man who has succumbed to the
Protestant ethic. Leisure, according to Weber, is necessary for man to
be able to work. For Aristotle, work is necessary for man to have
leisure.

Technology provides man with discretionary time he can fill either
with making or with doing. The choice between sad unemployment and
joyful leisure is now open for the entire culture. It depends on the
institutional style the culture chooses. This choice would have been
unthinkable in an ancient culture built either on peasant agriculture or
on slavery. It has become inevitable for postindustrial man.

One way to fill available time is to stimulate increased demands for
the consumption of goods and, simultaneously, for theproduction of
services. The former implies an economy which provides an ever-growing
array of ever newer things which can be made, consumed, wasted, and
recycled. The latter implies the futile attempt to ``make'' virtuous
actions into the products of ``service'' institutions. This leads to the
identification of schooling and education, of medical service and
health, of program- watching and entertainment, of speed and effective
locomotion. This first option now goes under the name of development.

The radically alternative way to fill available time is a limited
range of more durable goods and to provide access to institutions
which can increase the opportunity and desirability of human
interaction.

A durable-goods economy is precisely the contrary of an economy based
on planned obsolescence. A durable-goods economy means a constraint on
the bill of goods. Goods would have to be such that they provided the
maximum opportunity to ``do'' something with them: items made for
self-assembly, self-help, reuse, and repair.

The complement to a durable, repairable, and reusable bill of goods is
not an increase of institutionally produced services, but rather an
institutional framework which constantly educates to action,
participation, and self-help.  The movement of our society from the
present -- in which all institutions gravitate toward post-industrial
bureaucracy -- to a future of postindustrial conviviality -- in which the
intensity of action would prevail over production -- must begin with a
renewal of style in the service institutions -- and, first of all, with
a renewal of education. A future which is desirable and feasible
depends on our willingness to invest our technological know-how into
the growth of convivial institutions. In the field of educational
research, this amounts to the request for a reversal of present
trends.


