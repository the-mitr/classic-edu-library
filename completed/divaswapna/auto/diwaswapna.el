(TeX-add-style-hook "diwaswapna"
 (lambda ()
    (TeX-add-symbols
     "HRule"
     "footnote")
    (TeX-run-style-hooks
     "hyperref"
     "endnotes"
     "fancyhdr"
     "quotchap"
     "graphicx"
     "pdftex"
     "color"
     "xcolor"
     "usenames"
     "dvipsnames"
     "slantsc"
     "boisik"
     "latex2e"
     "bk10"
     "book"
     "b5"
     "paper"
     "10pt"
     "twoside"
     "preface"
     "ch12")))

