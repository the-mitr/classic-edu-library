% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\NewPage



\thispagestyle{empty}
\begin{chapquot}
My grandmother wanted me to have an education, so she kept me out of school.
\begin{flushright}
\emph{Margaret Mead}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}
\newpage

\chapter{Introduction}

This book is the result of a conversation with Ivan Illich, which has
continued for fifteen years. We have talked of many things but increasingly
about education and school and, eventually, about alternatives to
schools.

Illich and I met in Puerto Rico, where I had come in 1954, as secretary of
the committee on Human Resources of the Commonwealth Government,
charged with assessing the manpower needs of the island and recommending
an educational programme to meet them. Puerto Rico was then in the course
of rapid industrialization and my calculations showed that dropout rates
would have to decline throughout the school system if the estimated
manpower needs of the economy were to be met. Everything was done to
reduce these rates and they actually declined for a while, but it soon became
apparent that the decline was at the expense of grade standards and,
therefore, was meaningless.

Illich came to Puerto Rico in 1956, at the request of the Late
Cardinal Spellman, to organize a training programme for New York
priests from parishes overrun by Puerto Rican migrants. We soon found
common interests and many parallels in the problems of church and
school. In 1960 Illich left Puerto Rico for Mexico,\footnote{In 1961
  Ivan Illich, Feodora Stancioff and Brother Gerry Morris founded
  CIDOC, originally called Center of Intercultural Formation,
  Cuernavaca. During its first six years the center was mainly devoted
  to educational programmes for missionaries to Latin America. This
  period ended about the time Illich published an article called `The
  seamy side of charity' in the Jesuit magazine \emph{America},
  which called into serious question the very idea of sending North
  American missionaries to the Third World countries. Since 1967 CIDOC
  (Centro Intercultural de Documentacion) has concentrated on the
  analysis of various modern institutions, especially
  schooling. During this period, CIDOC has hosted the seminar on
  alternatives in education which I direct.\\ Cidoc is meeting ground
  for people who want to teach. It has no curriculum, grants, no
  certificates or credits, imposes no academic requirements upon
  learners or teachers. Students and teachers come to CIDOC from all
  over the world, with the majority from North America. Dialogue
  occurs in English, Spanish, French, German, Portuguese and
  occasionally in other languages. Oral Spanish is taught intensively
  as a skill.} and shortly after I also left for Washington to join
the Alliance for Progress. We began to study the problems of Latin
American education at about the same time and these turned out to be
similar to the problems of Puerto Rico but on a vastly larger
scale. It was soon clear to both of us that the countries of Latin
America could not, for many years, afford schools for all of their
children. At the same time education seemed to be the basic need of
these countries, not only to us but to their political parties and
leaders as well. In 1968 we began a part-time formal study of this
dilemma and of possible ways out of it.

Our analysis of school was soon extended to other institutions and to
the structure of the society schools serve. At first we felt that
school was a lagging institution in an increasingly efficient
technological society. We later came to see schools as providing
indispensable support for a technological society which is itself not
viable. The simplest way to expose the contradictions of this society
is to point out that it promises unlimited progress to unlimited
numbers of people. The absurdity of this claim is conceded by the
recognition of the need for birth control, but birth control itself
turns out to depend on progress. Even at present rates of growth, by
the time children can be kept in school long enough for schools to
have a significant impact on birth rates, the population of the world
will have increased threefold. And this is using conservative
projections. If modern medical techniques were made generally
available, the rate of population increase would double or
triple\footnote{The current rate of population increase in Costa Rica
  is three times the current world rate (United Nations,
  \emph{Demographic Statistics, Annual Reports})} We retain
breathing space only by letting poor babies die at ten times the rate
of privileged babies.

During the past decade the actual rate at which the benefits of modern
institutions have been extended to the underprivileged peoples of the world
is scarcely greater than the rate of population growth. On a \emph{per capita} basis,
individuals have benefited marginally, if at all.

Part of the problem lies in the inefficiency of modern institutions,
including schools. The greater part lies in the promise of unlimited progress.
As the world is now organized, the standard of living in India can rise only if
the standard of living in the United States also rises. Yet a 3 per cent rise in
the United States average income is twice the total income of the average
Indian. Raising world consumption standards to US levels would multiply
the combustion of fossil fuels by fifty times, the use of iron a hundred times,
the use of other metals over two hundred times. By the time these levels
were reached consumption levels in the United States would again have
tripled and the population of the world would also have tripled. Such
projections lead to results as absurd as the premises from which they start.
There can be no open-ended progress. Yet this is what schools and other
modern institutions promise. This is the promise of science and technology,
unrestrained by reason. This is the promise which has bred schools and other
modern institutions and which they in turn propagate. Schools are an
essential element of a world in which technology is king.

The contradictions of such a world are becoming apparent. They are best
illustrated in the school and best corrected by freeing education from the
school so that people may learn the truth about the society in which they
live.

In the course of the above analysis, an interpretation of human history has
developed which, although sketchy and incomplete, must be shared with the
reader. When techniques, institutions and ideologies were primitive, man
lived in relative equality and freedom, because there were no adequate
means of domination. As techniques, institutions and ideologies developed,
they were used to establish and maintain relations of domination and
privilege. From that time on, the societies which succeeded in dominating
others, and thus world history, were also characterized by inter-class and
inter-personal domination, in tension with efforts to establish equality. As
techniques increased in efficiency and as ideologies gained in insight, they
repeatedly threatened to upset the privilege structure of society. Institutions
were used to counter these threats by controlling the use of the techniques
and by perverting the ideologies. When revolutionary breakthroughs
occurred, institutions were re-established on a broader base, extending
privilege to more people, but always at the same time maintaining the
structure of privilege. This view does not necessarily lead to a Jeffersonian
suspicion of institutions as such. Despite the historical record, the possibility
of democratic institutions remains -- if men determine to use institutions for
democratic purposes.

I would like to be able to say that the previous paragraph contains the last
over-simplification in this book. Regrettably I cannot. I have tried to qualify
my assertions, to provide evidence for them, to give supporting references
and to justify my value judgments, but I do not expect to satisfy the norms of
academic scholarship. To do that I would have to write not this book now
but a completely different book twenty years from now. I chose to write this
book now because I would like to live to see it become a self-fulfilling
prophecy. Twenty years from now I might not be alive.

My view of history does not result in an unbiased appraisal of schools nor
of the society to which schools belong, but rather in an indictment of both.
Schools are admittedly treated in stereotype rather than in terms of concrete
human behavior. Schools are simply too big to treat concretely. Alternatives
to school, as opposed to mere reform, require going beyond the experience
of individuals to an analysis of the essential characteristics of schools.
Nevertheless, in an abstract treatment people will not quite recognize
themselves, as students, teachers, parents, or in other concrete roles related
to schools. A few may feel themselves cast as the villains in a conspiracy of
injustice. But this would be a misreading of the book.

There are unjust men in the world and it is an unjust world, but it is not the
unjust men, primarily, who make it so. It is an unjust world largely because
it is composed of faulty institutions. These institutions, in turn, are merely
the collective habits of individuals, but individuals can have bad habits
without being bad people. In general, people are unconscious of their habits,
particularly their institutional habits. The positive thesis of this book is that
people can become aware of their bad institutional habits and can change
them. The world would then be a better world, made up of still imperfect
people. There are, of course, some people who know that they have bad
habits but do not want to change them. There are others who are afraid to
examine their habits for fear that their consciences would then be troubled.
This is the worst that can be said of most people. It is enough, however, to
explain how the world can be worse than the people who live in it.

The realities of school are both better and worse than they are here
described. Others have made more of an attempt at concrete portrayal
and their work is an indispensable supplement to the present
analysis. Tom Brown, Charlie Brown, Miss Peach and the children and
teachers from George Dennison's \footnote{George Dennison, \emph{The
    Lives of Children: The Story of the First Street School}, Random
  House, 1969.}and Jonathan Kozol's\footnote{Jonathan Kozol,
  \emph{Death at an Early Age}, Penguin, 1968.} accounts are a few
examples of attempts to show the reality of school. A model for
general description and analysis of institutional behavior is to be
found in Floyd Allport's book by that name \footnote{Floyd H. Allport,
  \emph{Institutional Behavior}, Chapel Hill Press, 1933.}. I regret
that recognition of such a great teacher -- encountered outside of
school -- should be accompanied by evidence of such poor learning.

This book owes far more debts than I can remember, let alone
acknowledge. Several, however, must be singled out. Except for the
insistence of Ralph Bohrson it might never have been written. Except for the
help of the Ford Foundation travel and study grant which he administered,
the time spent writing it might have been spent on other work. Cooperation
from CIDOC, which has permitted me to work intensively with Illich, to
publish earlier drafts and to discuss them with students and associates, has
been even more vital. Many educators, economists, administrators and
political leaders from Latin America, the United States and Europe have by
this time participated in the analysis which Illich and I have conducted. Most
of them are referred to in the text and in the appended notes. I want also to
recognize the invaluable editorial assistance of Jordan Bishop, John Holt,
Monica Raymond, Joan Remple, Dennis Sullivan and of many students at
CIDOC with whom I have discussed earlier drafts. Not least is the debt I
owe my wife and sons and daughters for years of critical conversation about
education and the rest of lie. None of my collaborators are directly
responsible for anything in the book. Even Illich may disagree with parts of
it. By the time it is published it is quite likely that I will too. (This sentence
concluded the introduction to the first printed draft published by CIDOC,
Cuernavaca, Mexico, in 1970. Four chapters of that draft have been
jettisoned and eight new ones have been added. The sentence stands.)

\theendnotes
\setcounter{endnote}{0}

%\end{document}