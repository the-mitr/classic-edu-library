% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\NewPage
\thispagestyle{empty}
\begin{chapquot}
In less than a hundred years industrial society has molded patent solutions
to basic human needs and converted us to the belief that man's needs were
shaped by the Creator as demands for the products we have invented. This is
as true for Russia and Japan as for the North Atlantic community. The
consumer is trained for obsolescence, which means continuing loyalty
toward the same producers who will give him the same basic packages in
different quality or new wrappings.

Industrialized societies can provide such packages for personal
consumption for most of their citizens, but this is no proof that these
societies are sane, or economical, or that they promote life. The contrary is
true. The more the citizen is trained in the consumption of packaged goods
and services, the less effective he seems to be in shaping his environment.
His energies and finances are consumed in procuring ever-new models of his
staples, and the environment becomes a by-product of his own consumption
habits.
\begin{flushright}
\emph{Ivan Illich:\\ Celebration of Awareness}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\chapter{Institutional Props For Privilege}


Schools are not the only institutions, which promise the world and then
become the instruments of its denial. This is what churches, to give one
plural label to all religious institutions, have always done: packaged the free
gift of God, or nature, so that a price could be exacted for it, and then
withheld it from people unable or unwilling to pay the price. Churches were
remarkable among other institutions, until recently, only for their hypocrisy.
Other traditional institutions never pretended to offer a universal gift. Even
the prehistoric practitioners of religious magic did not do so. It is the unique
mark of the great religions that their founders opened the doors of the spirit
to all and that their priests then succeeded in holding these doors open with
one hand while charging admission with the other.

Except for churches, traditional institutions were always openly run for the
benefit of those who ran them. Courts, kingdoms, armies, empires and
enterprises belonged to their possessors and shared their benefits only with a
few and only for a fee. Two non-religious institutions have recently begun to
make the claim of offering universal access: first, nation states and their sub-
systems such as schools and, second, modern production enterprises.

Something for nothing is not the issue. No religious leader has ever
promised something for nothing, but only that the door would open to all
those who would follow the path. This is the promise, which churches have
reneged on, in failing to keep their own doors open, and which many modern
enterprises and public bureaucracies falsely proclaim. They do more. They
take elaborate and effective steps, first to sell their promise, and then to
frustrate its fulfillment.

As provision for human needs is institutionalized, the institutions in
question define a product and control access to it.
They progressively:
\begin{enumerate}
\item Define the product or service, which satisfies the need (e.g. schools
define education as schooling).
\item Induce general acceptance of this definition among the needy (e.g.
people are persuaded to identify education as schooling).
\item Exclude part of the needy population from full access to the product or
service (e.g. schools, at some level, are available to only some people).
\item Pre-empt the resources available for satisfying the need (e.g. schools use
up the resources available for education).
\end{enumerate}

The above generalizations hold for health, transportation and many other
kinds of human needs, as well as for education.

Health is progressively defined and conceived as access to the services of
physicians and hospitals and to the products of the drug industry. This access
is notoriously unequal. The cost of hospitals, doctors and drugs is increasing
faster than the resources available to pay for them. It can also be argued that
the health of mature populations, those whose birth and death rates are
converging, is getting worse as expenditures for hospitals, doctors and drugs
increase. This is to say that we are achieving a longer sick-life by means of
these expenditures. People may indulge themselves more as more remedies
become available, but if more resources were devoted to preventive
measures, sickness as well as death rates would decline.

In the case of transportation, the facts are even clearer. The private
automobile has almost displaced its competitors in many countries. In
the United States, saturation is reaching the point of declining
utility, even for owners, yet half the adult population remains
without dependable access to a private car and has a harder time
getting transportation than if cars had never been invented. Even in
Los Angeles, which reputedly has more cars than people, and which is
drowning in its own exhaust, there are as many old and young people
who cannot or are not allowed to drive as there are qualified
drivers.\footnote{Mark Arnold-Forster, `Poison in the air ',
 \emph{Guardian}, 5 August 1970.} These people, even those who belong to
families which include qualified drivers, must wait upon the
convenience of their chauffeurs, or vice versa.

Provision for a category of human need is institutionalized to the extent
that there is a prevailing standard product or service, a standard production
and distribution process, and a standard price (with the concept of price
including all significant conditions of access). It is worth noting that the
people priced out of the market are convinced not only of their unworthiness
to participate in it, e.g., their inability to pursue college studies or to wear
stylish clothes, but also of their unworthiness to participate in the privileges
which college education and stylish clothes imply.

Until democracy was popularized and technology institutionalized, claims
of universal political and economic participation could not be made. Now
such claims sound plausible and are widely believed. The makers of these
claims come with specific products designed to meet specific needs. They
elaborate a package which becomes ever more complex, more exclusive of
access and more expensive. More basic, however, than product elaboration
is the identification of need with product. The words education and school,
health and hospital, transportation and automobile, become inseparable.
People forget that there were educated men before there were schools,
healthy men before there were hospitals, and that men walked and rode
before they drove or flew.

As institutions grow, more and more people accept the identification of
need and product. Only the Jews and the Moors of medieval Europe failed to
identify salvation with the Catholic Church. Women, who throughout history
have borne their babies in the fields, are now recruited into maternity wards.
Peasants who have never seen schools vote for the candidates who promise
them.

The elaboration process effectively prevents the realization of these
promises, even for the simplest products. Pins and needles can be
packaged in ever fender collections. Salt can be made a monopoly
product and a form of tax. One of Gandhi's first struggles in India
was against the salt monopoly maintained by the British
Government.\footnote{Mohandas K. Gandhi, \emph{Autobiography},
  Beacon Press, I957 and \emph{The Essential Gandhi}, ed. Louis
  Fisher, Random House, 1962.  } The Italian Government still
maintains a monopoly on salt except in Sicily, where salt is
produced. What happens in schools, hospitals and automobiles is common
knowledge.  People are priced out of the market not only directly but
also by increasingly complicated rules -- drivers' licenses, entrance
examinations, and insurance requirements. There are good reasons for
all the rules but their proliferation tends to shrink the proportion
of qualified consumers.

There are, of course, opposing processes. Because of consumer credit,
rising incomes, growth of public systems of schools and hospitals,
etc., net access to modern institutions may even gradually
increase. Beyond question, however, the excluded portion of the
population, even if declining in number, gets steadily worse off as
the monopoly of an institutional product is established. No resources
remain for alternative products. As school budgets grow, support for
educational alternatives must decline. Not only do school dropouts
find progressively less educational resources, they also have less job
opportunities. And, finally, they have fewer excuses. As automobiles
increase in number, there are less trains and buses; those which
survive are more expensive, less satisfactory and less
profitable.\footnote{Janet Reiner, Everett Reimer and Thomas Reiner, 
  `Client analysis and the planning of public programs', \emph{Journal of the
  American Institute of Planners}, November. 1963, and Bernard
  J. Frieden and Robert Morris (eds.), \emph{Urban Planning and Social
  Policy}, Basic Books, 1968.} The number of new owners of
automobiles increased by no more than twenty-five million during the
past decade. Perhaps a roughly equivalent number enjoyed for the first
time the benefits of modern medical services other than
inoculation. The number of school children may have increased by a
hundred million. But the population of the world increased by almost a
billion during the decade, so that the numbers without any of these
services increased vastly more than the numbers with them. Even more
were priced out of the market during this period. The price of
automobiles increased substantially, while the cost of medical
services and schools multiplied several times. Meanwhile, per capita
income, on a worldwide basis, rose very little. Even if there had been
no population growth and other things had remained equal, more people
would have been priced out of the markets for modern goods and
services during the 1960s than were added to them.

Nor can the above figures be written off by labeling the sixties as a bad
decade, in which major institutions did not function as they are supposed to.
In a world dominated by competition for privilege, there is no other way that
institutions can function. The already privileged continue to demand better
schools, better hospitals, better cars. As the number who enjoy these
commodities increases, there are ever more people to be supplied with ever
more expensive packages, making it increasingly difficult to extend this
privilege to an ever widening ring of an ever growing population. Even
without population growth, the above factors -- plus ecological limits -
might make it impossible ever to universalize even current standards of
living in Europe and America.

The excluded are not the only, perhaps not even the principal, sufferers.
Those who participate, but to a limited degree, feel sharper pain. Imagine the
anguish of pious folk whose relatives languished in purgatory while those of
more fortunate neighbors were professionally prayed into heaven. Imagine
the torment, today, of persons whose relatives die because donors of kidneys
are co-opted by those who can pay. The fortunate feel no pain but they may
be hurt worst of all, for they get hooked on a game, which has no end and
which, no one can win. The struggle of the rich against old age and death is
a grotesque example. Much worse, if less macabre, is the status scramble,
which, as it spreads to more products and more people, poisons the air, the
water and the earth and sucks the very meaning out of life. A squirrel in a
rotating cage is no more hopeless nor ludicrous than the Smiths and the
Joneses trying to keep up with each other.

When Veblen wrote his account of conspicuous consumption fifty years
ago, it was part of a theory of the leisure class.\footnote{Thorstein
  Veblen, \emph{The Theory of the Leisure Class}, Allen \& Unwin, 1925. First
  published 1899.} Confined to this class, competitive consumption
may have been morally offensive but remained socially
tolerable. Extended to the masses, competitive consumption destroys
man, his society and his environment. A limited leisure class could
consume at the expense of the masses. Open-ended consumption can occur
only at the cost of the consumer. But man can no more live in a
squirrel cage than can a squirrel. Society cannot survive class
conflict stoked in increasing heat by international warfare, universal
advertising and competitive schooling. The world cannot absorb the
waste it now receives, let alone the amount implied by present trends.

One critically important aspect of the competitive consumption of
institutionalized products is competition among nations. The early
products of modern institutions -- people as well as things and
services -- were exported from Europe to the New World and to European
colonies, thus providing opportunities for all members of the
populations of these European nations. Those who could not attend the
new schools or buy the new goods could migrate to the New World, be
drafted as soldiers to police the colonies, or take over the land of
those who left. They were, therefore, priced out of the new markets
only temporarily. The sons of these conquerors of new lands became, in
fact, the pioneers of new levels and types of human
consumption.\footnote{S. de Madariaga in \emph{The Rise of the
    Spanish American Empire} (Hollis \& Carter, 1947) claims that one
  factor in Spain's economic . decline was the number of people
  \emph{exported} to Latin America. If true, this merely supports
  the old saw that there can be too much of even a good thing. But
  there are major reasons to doubt Madariaga's explanation. First, of
  the European countries which were large exporters of manpower in the
  seventeenth and eighteenth centuries, only Spain and Portugal
  declined, and these only relative to the northern European
  countries. Not only these two countries but their colonies have not
  prospered so much as those which received the greatest number of
  northern European immigrants. The probable explanation lies in the
  different social structure of Spain, Portugal and, their colonies,
  on the one hand, and the northern European nations and the colonies
  which they populated -- not the ones they ruled -- on the
  other. Spain, Portugal and their colonies remained relatively feudal
  much longer than northern Europe, North America, Australia, New
  Zealand and South Africa. The majority of Spanish and Portuguese
  emigrants did not have the opportunities to acquire land and
  political power on the relatively more equal basis of the northern
  European masses. Thus, the Spanish and Portuguese masses have always
  remained more alienated from their own \'elites, both at home and in
  their colonies, than in the case of the northern Europeans. This is
  true even in Argentina, Chile and Brazil, which attracted many
  northern Europeans but which remained much more feudal, especially
  in the distribution of economic and political opportunity, than
  other areas to which large numbers of northern European migrants
  went. Many of the early northern European migrants to Argentina,
  Chile and Brazil succeeded in breaking into the elite but not in
  changing it, if indeed they tried. Many of them, certainly, have
  done as much as any Spanish or Portuguese emigrant to restrict the
  opportunities of even those of their own countrymen who came after
  them.} The presently developing nations are not, with some
exceptions, able to displace or conquer weaker peoples. Far from being
able to involve their total populations in export trade, migration or
conquest, they are instead required to compete inside their domestic
markets with imports of foreign products, including manpower. Far more
of the population of underdeveloped nations, compared with those,
which developed earlier, is priced out of schools, hospitals and
modern transportation. This part of the population is progressively
alienated from the life of its own nation, from those who do have
access to the products of modern institutions, foreign or
indigenous. The alienated masses become, in turn, a demographic drag,
an economic liability and, ultimately, a political
opposition.\footnote{Gunnar Myrdal, \emph{Rich Lands and Poor},
  Harper \& Row, 1958; P.R. Ehrlich, \emph{Population Bomb},
  Ballantine, 1968; and Alfred Sauvy, \emph{Fertility and Survival:
  Population Problems from Malthus to Mao Tse-tung}, Collier, 1962.}

This has failed to occur only in areas where development has
progressed very rapidly, with correspondingly heavy imports of foreign
capital and manpower. Israel, Puerto Rico and Taiwan -- significantly,
all very small -- are the only unequivocal examples. In these areas
war, migration and strategic location, respectively, have made it
possible to engage the entire population in the development process,
thus avoiding the social split and alienation of the non-participating
element. These exceptions, therefore, prove the rule -- unless it can
be shown that all nations would accept similar relationships with the
developed world, and that comparable rates of foreign investment could
be achieved on a world-wide scale.\footnote{\emph{UN Annual Statistics on
  Rates of Development and Investment from Foreign Sources.}}

Japan and the communist nations are special cases. The latter, by
taking over the pricing mechanism, were able to force their total
populations into their institutions. By doing so, they succeeded in
avoiding the dilemmas of the Third World, but the procrustean
character of their institutional frameworks and the attendant
difficulties are well known. These difficulties resulted, in
considerable part, from the wholesale adoption of pre-communist
institutions, unadapted to the purposes they were supposed to
serve. By \emph{fiat}, these institutions involved the whole
population, but not in ways, which resulted in high levels of
motivation for socially constructive activity.

Japan's ability to maintain the character of her traditional social
structure gave her a less costly means than that of the communist
states. Employers were responsible for employment security and
employees were willing to accept the wages offered.\footnote{James
  C. Abegglen, \emph{Japanese Factory}, Free Press, 1965.} But Japan
also shared the advantages of the nations, which pioneered economic
development, in that she enjoyed a substantial political and economic
empire, which permitted her to export goods, services and manpower.

Most institutions continued to serve the interests of their inventors and, at
the same time, the interests of those who were originally peripheral to them,
only at the cost of an even more peripheral group.

In the days when political empires were the salient institutions, the above
statement would have excited no interest. The privileges of Roman
citizenship were extended only as additional territories were conquered.
Marx applied the principle to capitalist institutions. We merely generalize
the principle to other institutions and, possibly, free it of dependence on the
notion of deliberate exploitation. Most of those attempting to universalize
schooling and hospital care sincerely believe that they act in the interest of
the as yet unschooled and uncured. Earlier missionaries, conquerors and
even traders frequently acted with the same conviction.

What this means for development strategy is that developing nations
must invent their own institutions. Obviously, these institutions must
be able to use foreign components -- machinery, materials, techniques,
knowledge, and even trained manpower.What must be indigenous are the
basic institutions which determine who gets what, when, and at what
price. They must not only be indigenous, however, but also new. The
church and the \emph{hacienda} will obviously not do, since they
originally opened the door to foreign institutions. The feudal
plantations of north-east Brazil, for example, originally provided a
role, however humble, for the inhabitants of that area.\footnote{Celso
  Furtado, \emph{Development and Underdevelopment}, tr. Ricardo de
  Aquilar and Eric C. Drysdale, University of California Press, 1963;
  Celso Furtado, \emph{Economic Growth of Brazil}, tr. Ricardo de
  Aqui lar and Eric C. Drysdale, University of California Press, 1963
  and Albert O. Hirschman, \emph{Journey Towards Progress: Studies
    of Economic Policy Making in Latin America}, The Twentieth-Century
  Fund, 1963.}  The importation of modern machinery reduced the demand
for labor, while the importation of modern drugs increased in supply
to the point where the masses of this region are now starving. A
return to the \emph{hacienda} would merely accelerate the current
rate of starvation.

What must be new and indigenous are the institutional patterns, which
determine how major classes of needs will be satisfied; how people
will be fed, clothed, sheltered, educated, and protected from danger,
disease, misfortune and exploitation.\footnote{Ivan Illich,
  `Outwitting the developed countries', \emph{New York Review of
    Books}, 6 November 1969, and Ivan Illich, \emph{Celebration of
    Awareness}, Calder \& Boyars, 1971 Ch. 11.} Much of the technology
of the modern world will be required, but it will have to be applied
in a way, which meets the needs of the needy.

This is impossible within the framework of the institutions of the
developed world. Using the same institutions, the Third World could
never catch up. The educationally more advanced nations must forever
remain better educated if schools are to be the means of
education. And Brazil, spending fifty dollars per student per year,
can never have the schools, which in North America cost a thousand
dollars. Transportation can never catch up unless the less-developed
nation installs more efficient factories than the developed
nation. But seldom can the less-developed nation install a factory
even nearly as good. If this is done in a particular case, major and
expensive components must be bought from the developed nation at a
price, which forces this case to be an exception. The follower must,
therefore, not only remain behind but also fall further behind as long
as he adopts the means of development of the leader.  

The underdeveloped society requires more efficient institutions than
those of the developed: more food, clothing, shelter, learning and
protection per unit of input than modern agriculture, manufacture,
construction, schooling, etc. can supply. This is possible only by
departing from different premises.  India may never be able to produce
the US diet, but only the producers of Coca-Cola, Scotch whisky,
corn-fed beef and the doctors and dentists who live on the consumers
thereof really benefit from this diet. Better clothing, shelter,
education and protection than the standard products of the Atlantic
community are no more difficult to design than is a superior
diet. Neither is there great difficulty in designing better means of
producing these goods and services than the ones now in vogue.

The difficulty is that we are the prisoners of our institutions rather than
their masters. Seldom do we consciously design them and, when we do, we
can scarcely finish the process before bowing down in reverence. So in their
thrall are we that we tremble lest we lose them inadvertently and fall
helplessly back into barbarism. Actually, this fear is largely confined to the
privileged, and what we really fear is that the specific bases of our own
privilege might get lost in the institutional shuffle.

There is, then, a political as well as psychological aspect of the difficulty.
There are those who benefit from present institutions and who consciously
desire to preserve them. Among these are owners, managers, political
leaders and other holders of power. But many with power have no conscious
desire to monopolize it, and many over whom power is wielded give in to
the illusion rather than the reality of power. Man cannot free himself from
existing institutions without struggle, but neither will struggle avail unless
preceded by imagination and invention. One of the major problems is that
the developed nations now have an effective, if not necessarily deliberate,
monopoly of the means of modern invention.

Theories of political revolution are not sufficient. Such theories assume
that if a new class gains control, the society will change in accordance with
the values of this class as expressed in its ideology. In practice, we see that a
spate of revolutions throughout this century has left most of the specialized
institutions, which compose societies intact. The schools and hospitals of
communist states are no different from those of capitalist states. Even the
recent revolution in Cuba is attempting to extend health and education
services to the masses largely by means of traditional school and hospital
systems. The agricultural and industrial institutions of communist and
capitalist states tend to converge, despite great efforts on both sides to make
them different. According to prevailing theory, technology provides the
force which defeats these efforts, but technology scarcely explains the case
of the school, of the church, of the family, or of many other institutions
which have, temporarily at least, defeated the efforts of revolutionary
governments to change them.

Yet there is ample evidence that institutions are by no means eternal.
During this century monarchies have disappeared, political empires have
broken up, churches have lost their power if not their membership, labor
unions have risen and declined, entrepreneurs have been replaced by
managers and technicians, major industries have disappeared and been born.
Many of these changes are almost totally unexplained; others, especially the
political changes, have resulted from specific plans, sometimes based on a
general theory of political revolution. Man has shown himself capable of
creating and destroying institutions, on a planned and unplanned basis, with
or without theory. At the same time, he remains the prisoner of his
institutions to an almost unimaginable degree. He can break his thralldom
only by first understanding it thoroughly, and then by deliberately planning
the renovation and replacement of his present institutional structures. Both
understanding and effective action will require a general theory
encompassing a set of specific sub-theories applicable to each major type of
institution.

We must develop conceptual tools for the analysis of major institutions, in
order to understand the historical process by which they were introduced, the
sociological process by which they became acceptable, and the limitations
which they now place on the search for alternatives (nor only limitations of
power and resources, but also limitations upon the creative imagination). We
must develop a language in which we can speak with precision about the
needs of modern man -- a language freed from the one which is shaped by
those institutions men have come to accept as the suppliers of their specific
demands.

\theendnotes 
\setcounter{endnote}{0}
