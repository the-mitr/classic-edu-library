% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\NewPage
\thispagestyle{empty}
\begin{chapquot}
If we continue to believe that the goals of the industrial system -- the
expansion of output, the companion increase in consumption, technological
advance, the public images that sustain it -- are coordinate with life, then all
of our lives will be in the service of these goals. What is consistent with
these ends we shall have or be allowed; all else will be off limits. Our wants
will be managed in accordance with the needs of the industrial system; the
policies of the state will be subject to similar influence; education will be
adapted to industrial need; the disciplines required by the industrial system
will be the conventional morality of the community. All other goals will be
made to seem precious, unimportant or anti-social. We will be bound to the
ends of the industrial system. The state will add its moral, and perhaps some
of its legal, power to their enforcement. What will eventuate, on the whole,
will be the benign servitude of the household retainer who is taught to love
her mistress and see her interests as her own, and not the compelled
servitude of the field hand. But it will not be freedom.

If, on the other hand, the industrial system is only a part, and relatively a
diminishing part, of life, there is much less occasion for concern. Aesthetic
goals will have pride of place; those who serve than will not be subject to the
goals of the industrial system; the industrial system itself will be subordinate
to the claims of these dimensions of life. Intellectual preparation will be for
its own sake and not for the better service to the industrial system. Men will
not be entrapped by the belief that apart from the goals of the industrial
system -- apart from the production of goods and income by progressively
more advanced technical methods -- there is nothing important in life.
\begin{flushright}
\emph{John Kenneth Galbraith:\\ The New Industrial State}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\chapter{Are Democratic Institutions Possible?}

In the admitted absence of an adequate language of institutions, the
hypothesis that institutions can be democratic may appear premature. In
times like these, however, it may be necessary to take a chance. While all
institutions tend to dominate, some do it less than others. Furthermore, other
characteristics of institutions seem to be correlated with the degree to which
these dominating tendencies are shown. The hypothesis of this chapter is
that institutions can be identified in which the tendency toward domination
can be restrained, and that encouragement of this kind of institution can
foster the growth of a just and democratic society.

Institutions are so identified with hierarchy, control, privilege and
exclusion that the very notion of democratic institutions seems
strange.\footnote{Alex Bavelas and others have experimented with small
  groups in which communication is variously channelled. While there
  is no direct ~il relationship between Bavelas' work and the
  hypothesis proposed here, there are certain parallels that suggest a
  possible, common theoretical explanation. Alex Bavelas,
  `Task-oriented groups ', \emph{Journalsof the American Acoustical
    Society}, vol. 22 1950 pp. 727-30.}  Jeffersonian democracy was
based on the relative absence of large institutions and came to grief
with the growth of corporations and public bureaucracies. According to
Galbraith and others, today's technology requires large
institutions.\footnote{As indicated in the quotation that begins the
  chapter, J. K. Galbraith's theme in \emph{The New Industrial State}
  (Hamish Hamilton, 1967; Penguin, 1969) parallels in many ways
  the argument of this chapter. His argument, is however much more
  detailed and much more carefully qualified. Without the support of
  Galbraith's documentation the argument presented here would be much
  less persuasive. Galbraith does not, however, suggest an
  institutional dichotomy or continuum of the type outlined in
  this chapter. 
  
  In \emph{The Affluent Society} (Penguin, 1962) which is in a sense
  a companion volume to \emph{The Nero Industrial State}, Galbraith
  suggests : the need for a massive shift of resources from private to
  public enterprise. Again, the major difference from the argument of
  the present chapter lies in Galbraith's much greater caution coupled
  with an immensely greater documentation of his assertions and better
  support, therefore, for his recommendations. For Galbraith, all
  hope, seems to lie in a shift in basic values. In the absence of
  such a shift, little else is possible. He may be right. But this
  chapter suggests that, if such a shift should begin to occur, there
  is a programme of institutional development which men could follow,
  which could lead to something better than battle between
  institutions and humanistic social goals.} Must we choose, then,
between institutional domination and a return to primitive means of
production? There are some reasons to hope for escape from this
dilemma. First, there have been at least quasi-democratic
institutions: the Greek city state, the New England town, the
Jeffersonian republic, some of the early temples, churches and
religious fraternities, the Chinese market networks.\footnote{G. William Skinner, `Marketing and social structure in rural China', in \emph{Peasant Society: A Reader}, Little, Brown, 1967.}
Second, there are some modern institutions, which seem to serve
democratic purposes: postal systems, telephone exchanges and road
networks, for example. If Chinese markets and road networks do not
appear to qualify for institutional status, this is at least partly
because of the way we have been trained to think about institutions.

The history of institutions is a history of domination. Armies, temples,
courts and empires established the institutional mould and, despite
exceptions, their pattern has continued to determine man's thinking almost to
the point of defining deviations from this pattern as non-institutional. Even
people who admit that an organization, which is neither hierarchical nor
exclusive, might be called an institution will argue that hierarchy and
selective membership increase institutional efficiency. They probably do --
for purposes of domination. Institutions, which are better at dominating their
members, may also be better at dominating their rivals. Despite Pericles'
eloquent claim for the superiority of Athenian democracy, Sparta won the
war. The relatively democratic Greek city-states were later conquered by
much less democratic Rome. The record begins to get fuzzy with England's
triumph over Spain and with the results of the two world wars. But even
English history credits the better discipline of her navy, obtained partly by
means of the lash, and the record is not yet closed on the battle between
dictatorship and democracy. Currently the loudest clamor for control comes
from the leadership of the democracies.

Hierarchy is, of course, related to size, both as cause and effect. Conquest
and the subordination of one people to another was one of the earliest causes
of hierarchy. Span of control is another factor. Size at least invites, if it does
not require, more layers. If size is introduced as a correlate of hierarchy, the
advantages of both in achieving domination are more apparent. But the
admission that large hierarchically controlled institutions may be better at
dominating others will not satisfy the adherents of hierarchy. They claim
greater productive efficiency as well. This is a little like claiming that big
sharks have more efficient digestive systems than little sharks because they
succeed in eating them. Is General Motors more productive because it is
bigger, or bigger because it is more productive? The answer is neither. It is
bigger because it is the product of a merger; its size gives if the resources to
dominate other companies that remain independent. It is not; in general, a
more efficient producer, or it would not buy so many large and small
components from smaller companies. Its size does help it to dominate the
market, which in turn permits some economies of scale in production.
General Motors is currently the most efficient player of the game in which it
is engaged, just as the United States and Russia are currently the leaders in
the world struggle for domination. No one would claim that both of these
nations are models of efficiency in all other respects.

Basically, General Motors allows millions to play the game, which only
thousands could play when Veblen wrote his \emph{The Theory of the
  Leisure Class}.\footnote{Thorstein Veblen, \emph{The Theory of the
    Leisure Class}, Allen \& Unwin, 1925.} The thousands now play with
Bentleys and Ferraris instead of Cadillac's and Chevrolets. If getting
in on this game remains the major preoccupation of human beings, then
General Motors must remain the model of institutional efficiency. But
this is the same game, at the consumer level, as General Motors plays
with other producers; the game of getting ahead of the Joneses. If men
have other goals, then other institutional models are possible.

Consider American Telephone \& Telegraph in contrast with General
Motors. It is just as technical, just as profit oriented, just as large as General
Motors, but there is a great difference in what the two companies do for and
to their clients. The Telephone Company installs a phone and there it stays,
whatever its age, shape or color, unless the customer, for special reasons of
his own, decides he wants a different model. The telephone subscriber pays
a few dollars a month, and, unless he has teenage children, forgets about his
phone until someone calls him or he wants to call someone else. The
telephone doesn't have to be washed, waxed or serviced, except rarely,
requires no insurance and is not liable to theft. It is not, on the other hand, a
source of pride or envy, of concern or comfort, of thrill or trepidation. It is
just there, in case it is needed to call next door or half way around the world,
imposing no constraints on what is said and not intruding itself in any way
into the use that is made of it. Anyone who has a dime or a friend or an
honest face or an emergency can use it, even if he can't afford or doesn't
want a phone of his own. And the essential service the user gets, the value it
has for him, has nothing to do with what he pays or who he is. Clearly,
people who are better off are more conveniently served, but the telephone
network is essentially democratic -- so long as it serves individuals rather
than computers. The emergency user actually gets more value for his money
than the regular subscriber.

How different is the Cadillac or Chevrolet -- not so much owned as
owning its possessor. Long before purchase, the which, with what, how
much of this or that, dominate the family council. Subsequent payments
dominate the family budget as much as the new acquisition controls
family life. Triumphs and tragedies succeed each other in rapid
succession as the new car succeeds or fails one relative test after
another. Utility is usually the last concern. Car clubs are joined to
avoid exposing the new member of the family to the dangers of the
parking lot. Gasoline mileage is important only as a comparison
point. Emission of lead and other air-borne poisons is not even
considered.

Human nature? Perhaps. But elsewhere people compete on horses, or on
foot, or with sticks or stones, without surrendering their lives (including
employment, taxes, education, the condition of the air, water and land on
which they live) to the producers of these horses, sticks or stones. And yet,
human nature is involved. The automobile, like the modern house or
household utility, is too big a toy to be resisted. Offered even in exchange
for submission to its supplier, it is as hard to resist as were the early cults
with their idols, incense and temple prostitutes. Life is hard and dull and
what else is there to relieve the tedium?

This is one of three traditional ways of dominating men. The others are
force and the withholding of necessities. Force is used between nations to
obtain or maintain relative overall advantage. Withholding of necessities is
used to assure ourselves of the menial services of the poor. The grown-up
plaything game is used on us to keep us available for use against other
countries and the lower class.

It is important to recognize the various aspects of the game. International
competition, inter-class competition and inter-personal competition are all
related. The first requires the military institution, the second the police and
penal institutions, and the third requires General Motors. What these games
or processes and institutions have in common is the advantage they try to
establish one group or individual over another. In the case of international
and class conflict this is quite straightforward. The means by which
individuals are placed in competition with each other are a little more
complex.

Keeping up with, or surpassing, the Joneses is simple enough. There is a
variation of this game, however, in which nor the Joneses but the whole
human race becomes the reference group -- when men try to play God. This
might also be called the escape from reality game. It can be played with
alcohol or drugs, with sex, with many kinds of variously staged fantasy and
with grown-up playthings that fly, roar, tickle the imagination or titillate the
senses. Keeping up with the Joneses may, in fact, be just a variant of this
game, with the sense of superiority, or inferiority, coming from advantage
over some rather than all.

What all forms of advantage have in common is that they exact a price for
the advantage as well as for the product, which provides it. When the
advantage is permanent this price must be paid continuously. When the
advantage is on all counts the price is total. The point is made elegantly in
the Faust legend and the theme appears throughout human mythology. It
provides the touchstone for distinguishing democratic from dominating
institutions.

Democratic institutions offer a service, satisfy a need, without conferring
advantage over others or conveying the sense of dependence that institutions
such as welfare agencies do. They take the form of net-works rather than
production systems; networks, which provide an opportunity to do
something, rather than make and sell a finished product. Public
communication and transportation systems are examples, as are water works
and sewers, electricity and gas distribution systems and general markets that
facilitate the flow of various kinds of goods. Public utilities are democratic
institutions if they are truly public and provide something really useful.

Everyone has access to a true public utility, either free of charge or at a fee
everyone can afford. Access is at the option and initiative of the user, who
may also leave the service when he wishes. Use is not obligatory. The most
useful products such as electricity or water can be used for a variety of
purposes. So can roads or the mails. Public utility networks show true
economy of scale. The bigger they get and the more of the population they
serve the more useful they are for everyone. Water and sewer systems,
which might seem to be exceptions, are seen not to be when public health is
considered. Superhighways, as opposed to road networks, are false public
utilities. They are actually the private preserves of car owners, built partly at
public expense. Utilities serve basic, universal needs. Everyone needs water,
power, communication, transport, food, raw materials, a piece to exchange
fabricated products and many other things. Nevertheless, basic needs are
limited. They cannot be indefinitely multiplied. They can, therefore, be
satisfied without exhausting all available time, labor, raw materials and
human energy. After they have been satisfied them are things left for people
to do, if and when they like, and them are remaining resources for doing
them. The managers of democratic institutions can and must be largely
responsive to the expressed desires of their clients.

Institutions, which confer or maintain advantage over others, fit a
description almost diametrically opposed to that above. They tend to
be production systems rather than networks. If networks are involved
they have the secondary purpose of distributing a particular
product. Access is limited and access costs are frequently high. Once
on it isn't easy to get off; participation is often either obligatory
or addictive.\footnote{It might be argued that electricity, for
  example, is as addictive as are the household appliances which use
  it. Judged in terms of withdrawal symptoms this might appear to be
  so, but a sharper criterion of addiction is the need for ever more
  of a product in order to produce the same degree of
  satisfaction. Here electricity, in the absence of appliances or
  other devices, does not qualify, while the appliances clearly
  do. Water works the same way. People do not drink more or wash
  more except as more clothes, better detergents or water-wasting
  washing machines are introduced.} The product tends to be specific,
elaborate and all-purpose. There are important dis-economies of
scale. At some point extension of the service to new clients becomes a
disservice to former clients. The needs served are not basic but at
least partly induced. Once induced, however, these needs are
open-ended and can never be fully satisfied. Surfeit leads to excess
rather than to satiation. Dominating institutions tend, therefore, to
become total, to exhaust the life space of human beings and the
life-sustaining capacity of the biosphere. The managers of dominating
institutions must take and maintain the initiative.  Clients must be
seduced, manipulated or coerced. True initiative or choice on the part
of clients tends to disrupt the maintenance requirements of dominating
institutions.  

Most actual institutions fit these opposing prototypes only
partially. Some fit fairly well, including certain public utilities,
on the one hand, and military establishments, prisons and asylums, on
the other. Most products, services and institutions fall somewhere in
between. Automobiles, modern houses and home utilities are not merely
pawns in a status game, but are also useful.  Making long-distance
calls on credit cards, on the other hand, may be pure
one-upmanship. The management of the telephone company and its
institutional advertising differ little from those of General Motors,
which also produces buses as well as private cars. The buses are,
however, almost incidental. The policies of General Motors and its
role in society are determined by the private automobile, considered
not primarily as transportation -- as Henry Ford viewed the Model T -
but as status symbol.  With the development of the videophone, the
telephone company could easily embark on the same route. A video-phone
is not for everyone, and might seem to require private viewing
space. This in turn could lead to elaboration not unlike that which
has afflicted the automobile. If the choice were left to management,
there is little doubt that A T \& T would follow the route of General
Motors. The management of a true public utility must be at the service
and command of its clientele. The management of a dominating
institution must manage its clientele as well as its personnel. The
choice is a fateful one. It is, hopefully, not too late for the public
to choose.  

The choice is not between high and low technology. It is
not necessarily between private and public management, of either
`public' utilities or `private' factories for necessary products. It
is a choice of shopping lists, of the kinds and varieties of products
that will be available. An unlimited market basket for the rich is not
compatible with freedom for either the rich or poor. But this metaphor
is inexact. The 'unlimited' market basket can be filled only with
those products of high technology which enough people can be induced
to buy. It can, of course, be filled with custom-made products for
those who can afford them, but even these will be the products of an
inherited artisan- ship, living on the past. A vigorous artisanship
cannot survive in unrestricted competition with high technology. The
choice ultimately is between two completely different styles of
life. One is egalitarian, pluralistic and relatively sparse in the
kinds of products and services it provides. People have to do things
for themselves, but have time and freedom to do what they want. The
other kind of life is based on a unified hierarchy of privilege,
maintained by international, inter-class and inter-personal
competition. The kinds of competition are limited and highly
structured but the prizes are relatively glamorous, at least on the
surface.  

It may seem idealistic in the extreme to believe that people
who have the second option already in their hands will voluntarily
exchange it for the first.  Yet there are signs that this could
happen. But the choice may not be entirely voluntary. Pollution of the
environment, pressure from the underprivileged and the horrors of war
may help to decide the issue. However, blind forces cannot achieve
intelligent solutions of issues. Only intelligence can do that.  This
is why education is so important, and why it cannot be left to
schools.  

Schools are themselves dominating institutions rather than opportunity
networks. They develop a product, which is then sold to their clients
as education. Concentrating on children gives them a less critical
clientele, to which they offer the prizes that other dominating
institutions produce.  Parents want these prizes for their children,
even more than for themselves, and can be sold a rosy future even more
easily than a current illusion. Seen at a distance the fallacies of
competition are harder to perceive clearly.  Foreigners can have what
we have if they will follow the course we have followed. Workers can
achieve our standard of living if they will educate themselves to earn
it. Our children can have what we couldn't have if they will prepare
themselves to produce it. These propositions sound so plausible and
yet are so patently false when viewed in perspective. An open-ended
consumption race must always result in a hound who gets the rabbit, a
bunched pack in the middle who get some of the shreds, and a train of
stragglers. This must be the outcome in terms of nations, of classes
and of individuals. Schools not only cloud this perspective, they
actively foster the illusions, which contradict it. They prepare
children precisely for inter-personal, inter-class and international
competition. They produce adults who believe they have been educated
and who, in any case, have no remaining resources with which to pursue
their education.


\theendnotes 
\setcounter{endnote}{0}
