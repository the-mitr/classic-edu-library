% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage
\thispagestyle{empty}
\begin{chapquot}
An expedient was therefore offered, that since words are only names for
Things, it would be more convenient for all Men to carry about them, such
Things as were necessary to express the particular Business they are to
discourse on. And this Invention would certainly have taken Place, to the
great Ease as well as Health of the Subject, if the Women in Conjunction
with the Vulgar and Illiterate had not threatened to raise a Rebellion, unless
they might be allowed the Liberty to speak with their Tongues, after the
Manner of their Forefathers: Such constant irreconcilable Enemies to
Science are the common People.

However, many of the most Learned and Wise adhere to the new Scheme
of expressing themselves by Things; which hath only this Inconvenience
attending it; that if a Man's Business be very great, and of various Kinds, he
must be obliged in Proportion to carry a greater Bundle of Things upon his
Back, unless he can afford one or two strong Servants to attend him. I have
often beheld two of those Sages almost sinking under the Weight of their
Packs, like Pedlars among us; who when they met in the Streets would lay
down their Loads, open their Sacks, and hold Conversation for an Hour
together; then put up their Implements, help each other to resume their
Burdens, and take their Leave.

\begin{flushright}
\emph{Jonathan Swift:\\ Gulliver's Travels}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\chapter{Networks Of Things}


The prior division of things into those, which have general and those,
which have special educational significance can be carried further. Those,
which have special educational value, are again of two kinds: those, which
are, symbol systems of some kind, and those, which produce, translate,
transmit or receive messages. These are things, which serve as means of
communicating not merely a specific message but large classes of messages.
All objects can serve as means of communication but, as Swift pointed out
in his `Voyage to Laputa', some serve it better than others. Among those,
which serve it best are records, objects especially convenient for the storage
of symbols. Records are so relatively easy and cheap to store and keep that
they can be organized for quick access enormously more efficiently than the
things they represent. This is the virtue of human brains, but also of
computers, libraries, microfilm stores and the like. Large collections of
records, such as central libraries or national archives, are like collective
memories serving societies as brains serve individuals. Further organization
of such record collections, by means of computers, is certain to increase
their utility greatly and to warrant the comparison with human brains.
Effective access to records is sure to become even more necessary to the
educated man than it is today. Even today, records are very significant
extensions of educated brains. Much that could be kept in human memory is
deliberately passed on to these supplementary memory systems.

One of the things, which make almost unlimited universal education so
potentially cheap, is the great economy with which record systems of many
types can now be organized for very rapid access, by almost unlimited
numbers of people. Anyone who learns to use these systems, for which only
elementary skills are needed -- at least for certain levels of use -- is then
capable of carrying on his own education to almost any degree. This has
always been true for people who knew how to read and to find books. The
new development will simply make it easier. It may be worth noting that this
too has always been true for anyone who knew how to observe and to find
information. Reading merely made things easier, just as computers now do.
They make things so much easier, however, that education can now become
universal.

The quality of this education will depend only upon the quality and
completeness of the records, which are available to the public. Information
upon which corporate and national advantage presumably depends will not
be available. Neither will other information deemed vital by some groups for
the maintenance of their advantage over others. These are problems which
organization alone cannot solve.

Libraries are partial models for the organization of records and similar
objects. Only an extension of the library system is necessary to enable these
kinds of educational objects to be located and placed at the disposal of
learners. But the scope of the extension required is very great. Libraries do
not yet take full advantage of the ease and economy with which most records
can now be reproduced. They are, of course, seriously handicapped by
property rights and consequent restrictions on reproduction. These
restrictions and the novelty of cheap reproduction methods account for the
carry-over of the custodial tradition, which opens libraries to the charge of
being more concerned with their records than with their clients. This
tradition will have to be overcome, as will the tradition of serving an elite
rather than the general public. The reading public is an elite and, because
libraries were founded on books, an elite as well as a custodial tradition has
limited their educational scope. It might not even be a good idea to use the
name library in the network of special educational objects, which must
become one of the major institutional alternatives to schools.

In addition to vastly expanded directories and repositories of all kinds of
records, we need similar means of access to other kinds of educational
objects, which have special value in the transmission of information. First
among these are the instruments, which play records, produce them or
transmit their message. Books and papers are among the few records, which
do not require special decoders, although even with books, microfilm
viewers are beginning to play a significant role. In the production of books
and papers, however, instruments are indispensable. Pencils at least, much
better typewriters; mimeograph machines or presses are necessary to
produce written records. Universal access to these instruments is just as
important as the ability to read what has been written. This is why freedom
of the press was included in the American Bill of Rights. Its original purpose
was to protect the rights of common people like Thomas Paine to make his
ideas public; only later was it converted into protection of the freedom of the
commercial press.

Most records other than books require instruments for their production and
also for their use. Musical instruments and microphones may be needed to
produce and record sound and record players to hear it. Typewriters and
computers are needed for the production and reading of punch cards, tapes,
disc packs and other types of computer records. Cameras and projectors are
another pair of basic instruments, which can be combined with telescopes;
microscopes, stethoscopes and many other devices, and can use
television or telephone lines as transmitting devices. Then there are simpler kinds of
materials for producing records such as paint and brushes, knives and
chisels, knitting needles and string, a great variety of common tools and
materials of the various practical and fine arts. Libraries have begun to stock
some of these instruments and materials but usually only for privileged
clients. They are much more commonly available for decoding than for the
production of records and much more fully developed in the traditional
media, related to books and written records, than in the newer
communications techniques or in the huge variety of means of
communication represented in the practical arts.

Message coding and decoding devices shade off into general types of
instruments or machines, which transform one kind of energy into another.
Musical instruments and printing presses, for example, are not so strictly
communications devices as tape recorders or typewriters. All general types
of energy transformers have special educational value, not only because of
their general usefulness in facilitating communication but also because they
reveal some important features of the world; clocks, for example, reveal the
relationship between motion and time, motors between motion and
electricity, telescopes between distance and size. Names for relationships of
this kind -- but not confined to the world of physical science -- make up the
basic vocabulary of educated men.

Tools, instruments and machines are much less available now to most of
the people of technological societies than they used to
be. Specialized large- scale production removes them from the common
scene. There are still artisans and mechanics in South America, Asia
and Africa, but in Europe and North America they are disappearing
rapidly. Not only children but also neighbors, friends, clients and
passers-by are deprived of access to first-hand demonstrations,
opportunities to experiment with tools and to see the insides of the
gadgets which now come out of factories encased in shiny shells.
Worse than this, many modern devices can no longer be taken apart
without destroying them. They are not made to be repaired but to be
replaced. As a result, modern man is becoming richer in devices and
poorer in his understanding of them.\footnote{Jos\'e Ortega y Gasset,
  \emph{Revolt of the Masses}, Allen \& Unwin, 1951.} The
multiplication of product shells and factory walls, behind which
tools, instruments and machines are hidden, has the same effect,
educationally, as the hiding of records behind the veils of national
security and corporate privilege. The result is to deny people the
information they need to act intelligently in their own interests. The
reasons behind all this secrecy are also the same, even though the
conscious motives may be different. Manufacturers guard their
equipment and their products from the eyes of their customers, perhaps
not consciously to keep them ignorant, but certainly in order to
maintain an advantage in which ignorance is a critical factor. Secrecy
is by no means confined to capitalist countries. Professionals,
managers and specialized workers guard their privileges as jealously
as owners. The techniques of modern production play into their hands
equally well and are, indeed, partly responsible quite apart from any
conscious motivation. Large-scale production has, in and of itself,
profoundly anti- educational implications, as Jane Jacobs points out
in her \emph{The Economy of Cities}.\footnote{Jane Jacobs, \emph{The
  Economy of Cities}, Random House, 1967.}

So long as large-scale production continues to monopolize tools,
instruments, machines and other products which have special educational
value, it will be necessary to include such products in educational directories
and to arrange for general access to them. Vocational schools are one
attempt to supply this access but they are far more expensive and much less
educational than junkyards. Vocational schools can never serve the needs of
the entire population while junkyards could, although today junkyards are in
some respects more difficult of access.

Toys and games are a special class of objects with great potential for
offsetting the educational disadvantages of a technological society. They can
simulate many real objects and situations, sometimes to advantage and
sometimes not. Traffic rules in the classroom, for example, may be a
dangerously safe simulation of a truly dangerous situation. But simple toys
and games, made easily and widely accessible at the option of individuals,
could provide skill practice and intellectual insights with an effectiveness
and economy not easy to match. Games have three great educational assets.
First, they are a pleasant way of learning many skills, the practice of which
might otherwise be onerous. Second, they provide a means of organizing
activities among peers, which make minimum demands upon leadership or
authority. Finally, they are paradigms of intellectual systems, based on
elements, operations and rules just as mathematical systems and other
intellectual models are. People familiar with games can easily be introduced
to a basic understanding of the most important models of science and
mathematics. Games are, of course, open to the objection of stressing
scientific and technological outlooks over those of nature and the
humanities. Games are also open to the objection of pitting persons against
each other and of producing losers and winners. It is doubtful, however,
whether competition can or should be organized out of life. Games can be so
organized as to equalize advantages and, thus, the pleasures of winning.
While everyone still knows who is best, it is usually rather difficult for one
person to be best at everything.

The problems of organizing access to toys and games fall largely into the
ambit of libraries. Physical sports constitute an important exception, with
problems similar to those involved where access to nature and natural things
are involved.

Nature is not only becoming more distant, it is also being increasingly
denatured, by exploitation and pollution on the one hand, and by attempts to
sterilize adventures on the other. The exploitation and pollution of the
natural environment have been well publicized and in terms of man's
continued enjoyment of nature are of great importance. Educationally,
however, cleaning up the environment may be worse than getting it dirty.
The clean-up occurs at two levels. Protecting children from dirt, animals,
birth, sickness, death and other natural things distorts their sense of what is
real and natural. For the typical city child, nature is man-made like
everything else. Even as an adult, he has decreasing opportunities to
discover the truth. Jet planes and highways keep nature at a distance and
even after he gets there, modern man's dude-ranch and stage-managed safari
will do little to penetrate his urban aplomb. Only a few rivers, forests and
mountain ranges remain unspoiled and these are being invaded. Nature can
no longer be left to herself but has to be protected by man against man.
Educational access complicates the problem of conservation, but education
should be the main function of nature in the life of man. If proper boundaries
are established between man and nature, and if his weapons are removed
before he enters her, nature can continue to be man's mentor. For many
people, a new guide will have to be written and new kinds of encounters
devised. It is surprising and hopeful, however, to see what small enclaves of
nature, truly protected, turn out to be viable.

Access to records, tools, machines, games, natural preserves and other
extraordinarily useful educational objects is relatively easy to
organize.  Logical classifications already exist. Directories can
easily be developed; storage in libraries and arrangements for other
kinds of access can be made without major difficulty. But this leaves
the whole of the rest of the world, not as concentrated educational
perhaps, but outweighing in its total educational value all of the
special kinds of objects put together. This world of ordinary objects
is one of the worlds men's minds must penetrate in order to act
intelligently. The barriers to this world of ordinary objects are of
several kinds. One is characterized by the automobile. Cities and many
rural areas have become so unsafe for pedestrians, especially for
children, that streets and roads -- the physical paths to the world -
are off limits to many of the world's inhabitants. If streets could
again be opened to pedestrians, the city itself could again become a
network of educational objects, the natural school that it has been
throughout history. A second barrier, however, would still lie between
the customers' area of the various shops and the workrooms where most
of the really educational objects and processes are shut away. In
older cities these barriers do not exist. The tradesman works where he
sells, open to public view. In the modern city, however, there is yet
a third line of defense. Many machines and processes are not there at
all but hidden outside of the city or in places where only the persons
who already know can find them. Directories, which will locate this
world for people who want to learn about it are indispensable, but
even directories may not be easy to prepare and access, will be still
more difficult to arrange. For these most interesting objects are also
the most carefully guarded -- the scientific, military, economic or
political objects hidden in laboratories, banks and governmental
archives.

Secrets seem natural and inevitable in the world to which we are
accustomed, but the cost of keeping them is very great. Science, for
example, used to be a network of people, working all over the world,
and exchanging information freely. One of the original premises of
science, which has never been repealed, was that progress depends
precisely upon the open sharing of the results of scientific work. Now
the members as well as the artifacts of the scientific community have
been locked into national and corporate prisons, impoverishing even
the citizens of these nations and the stockholders of these
corporations. The special privileges they gain are more than offset by
the barriers to the growth of knowledge. In a world controlled and
owned by nations and corporations, only limited access to educational
objects will ever be possible. Increased access to those objects,
which can be shared, however, may increase men's insight enough to
enable them to break through these ultimate educational barriers.



\theendnotes 
\setcounter{endnote}{0}
