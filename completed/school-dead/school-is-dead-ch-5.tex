
% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage
\thispagestyle{empty}
\begin{chapquot}
 A single shelf of a good European Library was worth the whole native
  literature of India and Arabia\ldots{} It is, I believe, no
  exaggeration to say that all the historical information which has
  been collected from all the books written in the Sanskrit language
  is less valuable than what may be found in the most paltry
  abridgements used at preparatory schools in England \ldots{} I think
  it clear that \ldots{} neither as the languages of the law nor as the
  languages of religion, have the Sanskrit and Arabic any peculiar
  claim to our engagement, that it is possible to make natives of this
  country thoroughly good English scholars and that to this end our
  efforts ought to be directed \ldots{} We must at present do our best
  to form a class who may be interpreters between us and the millions
  whom we govern, a class of persons Indian in
  blood and color, but English in taste, in opinions, in morals and in intellect.
\begin{flushright}
\emph{Lord Macaulay:\\ Parliamentary minute on Indian education}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\newpage

\chapter{Where Schools Came From}

School is a stage in a succession of specialized institutions. Prehistoric
rites, myths and shamans, temples and priestly castes, Sumerian, Grecian,
Alexandrian and Roman schools, monastic orders, early universities,
common and grammar schools -- all have played a part in the history of the
national and international school system of today. Counterparts can be found
for the functions and elements of modern schools at each stage of this
history. Comparisons, which span millennia, run the risk of distorting
meanings, but this risk is worth taking in order to learn from the educational
trends and inventions of the past. One of the most general and instructive
trends is the progressive specialization of content, method, personnel and
location in something, which was originally much more than education and
which, in schooling, has become something much less.

Since 1830 archaeology and anthropology have extended the history of
man many tens of thousands of years.\footnote{Joseph Campbell,
  \emph{The Masks of Gods}, 3 vols., Viking Press, 1969.} As far
back as the record goes, man has engaged in specialized activities
which have important things in common with what goes on in
schools. Rites and rituals -- symbolic practices seemingly unnecessary
to the satisfaction of elementary physical needs -- have always been
part of man's repertoire. In this man is not unique; animals engage in
similar practices which seem to serve no elementary function, and even
sometimes to harm the individual or the species. As far back as man's
record goes, we also find evidence of people and places specialized in
and for the use of myth and ritual. Some of the earliest records are
in just such places: the caves of southern France and Spain, famous
for their paintings of prehistoric animals, were apparently used
largely for ritual practices. The only human figures included in the
paintings are those of shamans, who combined the teacher's role with
that of priest, magician, actor, artist, poet and ideologue. These, at
any rate, are roles combined in shamans of existing tribes, whose
technology and art resemble those of prehistoric man.

Reasoning both from archaeological and modern anthropological
evidence, it appears that prehistoric rites shared certain elements of
current curricula.  They had an age-specific character, acting out the
myths related to birth, adolescence and death. They explained and
celebrated both the everyday and the unusual aspects of the
world. They provided activity for idle periods that followed hunt or
harvest. They allowed young people to try on adult roles for size.

The line between prehistoric and historic time is marked by the
invention of writing, which corresponds roughly in time to the
establishment of cities and the great religions. Education emerged out
of the practice of worship and government. Its early home was the
temple-court and its early practitioners were specialized
priests. Writing itself was probably invented by such
specialists. Shamans and priests are, thus, in the central line not
only of the development of teachers and schools but of the evolution
of man.  Brain, hand and tongue; horde, village and city; magic,
religion, art and science -- these are the milestones of man's
physical, social and spiritual development. The priest-hoods of
city-based religions inherited from their country-cousin shamans a
mixture of magic, religion, art and science, which they began to
disentangle and specialize. It is relatively well established that not
only writing but accounting and mathematics, astronomy and chemistry,
music, painting and poetry had their early development in the
temple-courts of Egyptian, Sumerian and other ruling castes, which
combined the functions of priest and king.\footnote{H. L Marrou,
  \emph{A History of Education in Antiquity}, tr. George Lamb,
  Mentor Books, 1964.} The first formalized teaching of there arts,
which still make up most of the modern core-curriculum, must have been
a master-apprentice type of teaching. Even earlier, a kind of teaching
among equals must have occurred, as one individual shared his
discoveries or developments with others. Here is one of the two main
roots of the modern school, at the very origin of systematic
knowledge. This root does not appear again, in an institutionally
prominent form, until the rise of the universities of the Middle Ages.

The other, much humbler, root makes its first appearance in a Sumerian
classroom built to accommodate about thirty children, the discovery of
which led to the speculation that modern modal class-size may have
been based on the limitations of Sumerian brick and
architecture.\footnote{Edward Chiera, \emph{They Wrote on Clay}, University
  of Chicago Press, 1938.}

Plato and Aristophanes were the first to leave surviving written
records of classroom and school.\footnote{Richard L. Nettleship, \emph{The
  Theory of Education in the Republic of Plato}, Teachers College
  Press, University of Columbia, 1968. See also Marrou, \emph{A History of
  Education in Antiquity}, ch. 6, `The masters of the classical
  tradition'.} \footnote{ Aristophanes, 'The Clouds', in William
  Barton (ed.), \emph{The Plays of Aristophanes}, tr. Benjamin B. Rogers,
  Encyclopaedia Britannica Press, Great Books of the Western World,
  no. 5, z95z. See also Marrou, \emph{A History of Education in Antiquity},
  ch. 4, `The old Athenian education'.} These first schools of
classical Athens were humble indeed -- mere appendages to an
educational programme which stressed military training, athletics,
music and poetry, and which taught reading, writing and arithmetic
almost as an afterthought. Originally all education in Athens was
tutorial -- one aspect of personal relationships which were often
erotic as well. As Athens became more democratic, and pupils began to
outnumber masters, group instruction gradually replaced the tutorial
relationships.

Soon after the first reference in Greek writing to group instruction
in literary arts and skills, schools of medicine and philosophy are
also mentioned and, soon afterwards, a class of schools conducted by
Sophist philosophers.\footnote{Marrou, \emph{A History of Education
    in Antiquity}, 1964.} These first models of intermediate schools
were based on contracts between a master and a group of parents for
the instruction of their sons during a three or four year period of
their adolescence. The Sophists were the first paid teachers on
record, and their aim, appropriately, was practical: to make of their
students successful men of affairs.

From these meagre beginnings in the golden age of Greece, there
flourished in the Hellenistic colonies scattered by the conquests of
Alexander all over the ancient world school systems prophetic of our own in
organization, curriculum and the age-span of students. Children first learned
reading, writing and numbers, and later were taught gymnastics, music, and
the classics of literature, geometry and science. The museums of Alexandria
and other cities specialized in the teaching of medicine, rhetoric and
philosophy. Most of these centers, patronized largely by Greek families,
were privately financed, although a few small cities had public systems
while others were supported by foundations established by wealthy men.
One of the main purposes of these schools was to keep alive the Hellenic
tradition in a barbarian world. Only a small minority of the Greek population
of the Alexandrian world was ever able to take full advantage of them.

The Romans adopted the Hellenistic school and, with minor modifications,
used it for the education of their own elite. From the fall of Athens to the fall
of Byzantium, therefore, a tiny minority of the world's population was
schooled in somewhat the fashion of today. The school was not, however, an
important institution in Greco-Roman or in Byzantine times except for its
role in preserving the memory and some of the culture of ancient Greece
until the time of the Renaissance in western Europe.

Except in Byzantium, the fall of Rome resulted in a reunion of
education and religion, which lasted for a thousand years. The
educational institutions of the Middle Ages were the cathedral schools
and the monasteries. More specialized in their purpose than the
ancient temples, they also had a more limited educational role; they
did, nevertheless, introduce a number of important ideas into the
history of western education. In the earliest Benedictine monasteries,
space and time became the parameters of learning as well as of
living. \footnote{ David Knowles, \emph{The Monastic Order in England},
  Cambridge University Press, 1941.} Every hour of Benedictine life
had its appointed place and task. Adherence to this regimen
constituted the good life; no external product or other sign presumed
to attest the efficacy of the life thus lived.

The subsequent Dominican and Franciscan orders were based on different
principles. Dependence upon the charity of others and identification
with the poor replaced the bondage of time and space.\footnote{
  M. H. Vicaire, \emph{Histoire de Saint Dominique}, 2 Vols., Cerf,
  1957, and W.A. Hinnebusch, \emph{History of the Dominican Order},
  Alba, 1966.}\footnote{Kajetan Esser, \emph{Anf\"ange und
  UrsprunglicheZielsetzungen des Ordens der Minderbr\"uder}, E. J. Brill,
  1966.  } As in the case of the Benedictine rituals, begging and the
care of the sick and destitute were not designed as training for
subsequent living but as a way of life.

The preparatory principle of education was revived by the Jesuits, who
in the sixteenth century extended and rationalized schooling well
beyond the limits of Graeco-Roman times.\footnote{ H. Outram Evennett,
  \emph{The Spirit of the Counter-Reformation}, Cambridge University Press,
  1969; and James Broderick, \emph{The Origin of the jesuits}, Doubleday,
  I960.} The ancient schools had never been more than a small part of
an educational programme, which was a product of tradition rather than
of rational forethought. The Jesuits developed a curriculum and an
educational method deliberately designed to prepare men not merely for
an ordinary life but for a life of unprecedented scope and
challenge. At least part of the subsequent growth of schooling is
undoubtedly due to their initial brilliant successes.

Originally intended for members of an \'elite religious order, Jesuit
schooling was soon extended to the lay elites of the European medieval
world. The rate of this extension and the circumstances under which it
occurred are strongly reminiscent of the sudden growth of Greek schooling
following the conquest of Alexander. It was the insecurity rather than the
dominance of the Greek colonies of Alexandrian times, which caused them
to build and depend upon schools; it was the insecurity of the Roman church
in the time of Ignatius, which accounts for the formation and rapid growth of
the Jesuit system of schools. In both cases, the school was seen as a way of
preserving a set of values, which were losing their dominance.

This chronology of the Christian orders has run ahead of at least one
major event in the history of schooling -- the foundation of the first
medieval universities.\footnote{Paul Goodman, \emph{The Community of
  Scholars}, Random House, 1962.} Originally devoted primarily to the
study of Christian theology, they quickly branched into other fields
of knowledge and, long before the Reformation, had become independent
institutions in so far as this was possible in medieval Europe. Along
with their counterparts in the Moslem world, the universities of
Bologna, Salemo and Paris became the first institutions of any size
devoted primarily to the development and propagation of
knowledge. They were also, of course, the direct predecessors of
modern universities, and thus of the upper layer of the present school
system.

Luther and his followers, coinciding so neatly in time with
Gutenberg's invention of movable type, gave a vast stimulus to the
growth of the lower schools in northern Europe. The large-scale
printing of bibles, and the doctrine that salvation was directly
derivable from them, made the teaching of reading a moral imperative
for Protestants who could afford it. The industrial revolution, coming
so closely on the heels of the Reformation, supplied the last
condition necessary for the rapid proliferation of schools, providing
not only the means, but also a secular rationale for widespread
literacy.\footnote{ In addition to the invention of printing the
  fifteenth century brought on an expansion of commercial activities
  which created a demand for a different kind of instruction. `Reading
  and writing' schools appeared in England and northern Germany. These
  were opposed by the clergy, and a compromise was reached under which
  they were allowed to continue, but forbidden to teach Latin, thus
  preserving the clerical monopoly on \'elite education. Luther
  deplored students going to these schools when, with the breakdown of
  monastic education and the old church system, studies no longer led
  places in the ecclesiastical bureaucracy. In sixteenth-century
  England the reading and writing schools were establishments in
  which `letter writing and practical accounting were taught, for a new
  class' (Raymond Williams, \emph{The Long Revolution}, Chatto \&
  Windus, 1965; Penguin, 1961)}

Mere growth in the number of schools did not result in school systems.
This dimension of schooling came with the development of the nation
state.  Thus, while public schools first burgeoned in the federated
United States, the first integrated systems of schooling developed in
France and Prussia. The Prussian development, although later, was the
clearer cut and became an important international model. In Prussia
and later in Germany, the development of the school system was
coterminous with the development of the nation state and was
deliberately designed to be one of its principal
pedestals.\footnote{Johann G. Fichte, \emph{Addresses to the German Nation},
  Harper \& Row, 1968.}

One aspect of the German school system was the teaching of high German,
the language of the school and the unifying language of the state. A
common, graded, integrated curriculum was another -- designed to serve the
military, political and manpower needs of the nation. A hierarchically
organized teaching profession was still another. Most important of all was a
carefully thought out philosophy of education, reflected in school
organization, logistics, curriculum, teacher recruitment, teaching methods
and scholastic ritual, and aimed at producing a citizenry tailored to the
specifications of the architects of the German nation state. No other national
system has been so systematically designed. But all nations, in copying to a
greater or lesser degree the major features of the German system, have in
effect adopted its objectives and its methods. England has perhaps copied
them least, but even England's former colonies have followed Germany
more than England.

In France the idea of a national school system first arose partly in
opposition to the Jesuits who, in the sixteenth century, were among
the principal educators of the \'elites.\footnote{Cf. Louis Ren\'e de
  Caradeux de la Chalotais, \emph{Essay on National Education},
  Arnold, 1934, and F. de la Fontainerie, \emph{ French Liberalism
    and Education in the Eighteenth Century: The Writings of La
    Chalotais, Turgot, Diderot and Condorcet on National Education}
  McGraw-Hill, 1932.} Despite the suppression of the order in 1763,
and the attempts of the legislators of the French Revolution, public
schools made little headway. After the restoration of the Monarchy the
Jesuits and, at the primary level, the Christian Brothers again played
an important role in French education. The educational reform law of
1834 called for friendly relations between church and state, but the
resulting collaboration did not survive the crisis crested by the
French defeat in 1870.\footnote{Cf. Mons Ozouf, \emph{L'Ecole, I'Eglise et
  la R\'epublique, 1871-1914}, Armand Colin, 1963.}  The power of
Prussian arms was attributed by many to the efficiency of their
national school system and no effort was spared to initiate a similar
system in France.

Public schools in the United States have also had a long and
complicated history. Despite the early establishment of public schools
in New England, Pennsylvania and Virginia, these schools remained for
a long time under local control and, except in New England, the
privilege of a relatively small minority.\footnote{. Merle E. Curti,
  \emph{The Social Ideas of American Educators}, Littlefield, Adams,
  1959.  }\footnote{Joel H. Spring, `Education as a form of social
  control', the first of a series of conferences given at CIDOC in
  February 1970, CIDOC document no. 70/221, 1970.} The original New
England schools were quasi-universal, without being compulsory,
because their promoters shared a common conception of man, God and the
world. Even in New England, public schooling became much less than
universal with the influx of non-Puritan immigrants beginning early in
the nineteenth century. It was, in fact, this lapse from a brief
tradition of universal schooling that led Horace Mann to formulate the
modern American concept of the public school.\footnote{Horace Mann,
  \emph{The Republic and the School: Horace Mann on the Education of
    Free Men}, ed. Lawrence A. Cremin, Teachers College Press,
  Columbia University, 1957.}

Mann's public schools required attendance because persons of different
origins, values and faiths had to be brought to share a common
conception, which the Pilgrims had taken for, granted. These two
approaches to universal public schooling illustrate the
contradictions, which have brought a glorious promise to a dismal
end. Thomas Jefferson\footnote{Thomas Jefferson, \emph{Crusade
    against Ignorance: Thomas Jefferson on Education}, ed. Gordon
  C. Lee, Teachers College Press, Columbia, University, 1961.},
Orestes Brownson\footnote{Cf. Michael Katz, `From voluntarism to
  bureaucracy in American education ', in \emph{Formative
    Undercurrents of Compulsory Knowledge}, ! CIDOC 1970, pp. 2-14.}
and John Dewey\footnote{ John Dewey, in Reginald D. Archambault (ed.)
  \emph{ John Dewey on Education: Selected Writings}, Random House,
  1964.} saw universal education as the means of equipping men to
discover their beliefs and to create their institutions. St Ignatius
of Loyola, Johann Gottlieb Fichte and Horace Mann saw a similar
process as the means of shaping men to the requirements of social
goals and institutions assumed to have prior validity.

These ideological strains combined to make public schools popular with
both the privileged and the deprived. For the latter, they held the promise of
equal opportunity; for the former, the promise of orderly progression under
control of the elite. To a degree, both promises were realized, but the
contradictions inherent in them have become steadily more obvious as the
balance of power has shifted from citizens to the state. In their time, Locke
and Jefferson prevailed. John Dewey's more recent effort to put man back in
the saddle was merely given lip service.

The organizational, legal and procedural steps which have welded tens of
thousands of nominally independent local school districts and thousands of
colleges and universities into a national school system are the logical
outcome of a philosophy which views schools as serving national ends.

The popularity of such a philosophy, in a century, which has seen the
number of nations in the world more than tripled, is not surprising. The
proliferation of nation states is clearly one of the major factors in the growth
of the international school system. Regardless of reasons, however, the
actual development of such a system is one of the amazing facts of human
history. Schools are, of course, only one of the technological institutions,
which have spread from Europe and North America over the rest of the
world; but all of the others are more easily explained, while none has spread
like schooling. Universal schooling has become part of the official
programme of almost every nation. Every state must have an university,
every city its high school, every hamlet its primary school. All nations look
to the leading nations for models of curriculum, scholastic organization and
scholastic standards. Capitalist and communist nations compete in schooling
their populations, with as little argument about the standards of competition
as in the Olympics.

How is all this to be explained? Technology, the profit motive and the
world struggle for power explain most of the growth of international
institutions. None of them directly explains the case of schooling. Similarity
of constitutions and codes of law can in some cases be explained as the
residue of empire and in others as ideological emulation. The spread of
hospitals and medical technology can be attributed to the demonstrated
efficacy of at least some aspects of modern medicine. There is nothing
comparable in modern schooling; schools are as free of the obligation to
justify themselves as were the Benedictine monasteries.

European world domination during the eighteenth and nineteenth centuries
helps to explain the existence of school systems in former colonies. Japanese
schools can also be explained in part as a colonial phenomenon, developed
as part of a general western pattern and adopted to avoid colonization.
Schools have clearly lagged the most in those parts of the world least
influenced by European and American industrialization.

Schools have served a major purpose in the consolidation of the new
nation states, which grew out of the wreckage of empire. They also
serve the elites of these new nations, providing access to
international politics, economics and culture. This does not, however,
explain the international popularity of mass education. The real
explanation can be traced back to the two previous historic explosions
of schooling, the Alexandrian and the Jesuit. As noted, both of these
occurred at times when traditional value systems were in
jeopardy. This is again the case, but this time the values involved
are more basic and more universal than those of Hellas or medieval
Europe. In question now are the assumptions of a society based on
hierarchy of privilege. The technology which invalidates there
assumptions has created the antidote for its own effects: a school
system which promises access to the goods of technology but denies it
in fact.  

Modern technology relieves man, for the first time in
history, of the need to earn his bread by the sweat of his brow. All
pre-industrial societies required close to 80 per cent of the labor
force to be engaged in agriculture.  Now, using existing techniques, 5
per cent of the labor force of a modern society could produce all of
the agricultural and industrial goods currently consumed.  

Even now, 10 per cent of the labor force of the United States produces
90 per cent of its agricultural and industrial output. And this is
almost entirely prior to the application of existing methods of
automation, and in the face of widespread job-protective pressures
from organized labor. Nevertheless, the United States is producing
record agricultural surpluses while paying farmers billions of dollars
to restrict production. Additional billions of dollars of industrial
goods are being produced for commercial export, over and above the
value of goods imported. A huge bill of military goods is being
produced while massive space and research programmes are under way. If
labor and management could agree upon the objective, 5 per cent of the
United States labor force could within a very few years produce the
goods currently being consumed by the domestic civilian
population. This bill of goods, although badly distributed, leaving
many people deprived, is nevertheless enormously wasteful in its
average composition. It provides an excessive and otherwise
unhealthful diet, clothing which is discarded because of style changes
rather than wear, so-called durable goods which are made to wear out
in a few years, a tremendous packaging component which merely
multiplies the pollution problem, and an unbelievable quantity of junk
which serves only to relieve the boredom of people whose lives,
devoted to the consumption and production of goods and services, have
been emptied of real meaning. We would live much better on half as
much. Such a society does not require a hierarchy of privilege for any
of the reasons, which have justified such a hierarchy in the
past. Modern institutions have assumed the burden of maintaining and
justifying a continuing hierarchy of privilege. Among these
institution, school plays a central role. School qualifies men for
participation in other institutions and convicts those who do not meet
its requirements of not deserving desirable roles in these
institutions.




\theendnotes 
\setcounter{endnote}{0}
