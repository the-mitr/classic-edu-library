% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage
\thispagestyle{empty}
\begin{chapquot}
Learning is but an adjunct to our self, \\
And where we are our learning likewise is.
\begin{flushright}
\emph{William Shakespeare}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\chapter{Networks Of People}

Although people could learn a lot in a world where things were freely
accessible to them, it would still be helpful to have the assistance of other
people. Each person might eventually learn to type, given a typewriter, but
each might learn to type in a different way. Having a typist who could
demonstrate the skill would help to avoid this, especially if more than one
learner took advantage of the same model. If that happened, those two could
compare notes and, thus, learn something from each other. If, finally, there
were in addition to the skill model and the two learners, someone who had
taught typing before, had compared the progress of various learners, and
drawn some valid conclusions, this person might also be useful in reducing
the time required to learn to type.

The indispensable resource for learning to type is, of course, the typewriter
itself. The skill model, while not indispensable, might, nevertheless, reduce
the learning time by quite a bit and improve the final product as well. The
fellow learner, the peer, is also important, especially in providing motivation
to learn and the opportunity for practice. Less important than the others is
the typing teacher.

Schools reverse this kind of logic. They do not, it is true, try to teach
typing without a typewriter, but they frequently try to teach foreign
languages without the help of anyone who speak them, without anyone to
speak to in them, and without anything to say in them that could not just as
well be said in the native language of the learner. Geography is similarly
taught without benefit of people who come from the places in question.
Music is taught without instruments or musicians, science and mathematics
by people who do not know them. Schools assume that the indispensable
resource for learning is the teacher. Ideally this teacher should have the
essential equipment for the practice of the skill and should also be able to
demonstrate the skill, but these are secondary considerations. The need for
learning peers is given lip service but little use is made of peers in the
learning process.

Schools are not just willfully perverse. Learning a skill, learning to
practice it with someone else who is learning it, and learning how others
have learned it are three different things, sometimes related but also
frequently not. Schools try to find teachers who combine all three kinds of
leaning but, understandably, they often fail. The combination is much
scarcer than its elements. When they do succeed in finding the scarce
combination, schools use it as if it were not scarce at all. The experienced
teacher is required to act as skill model and as practice partner for individual
students, to say nothing of the many duties, which are unrelated to learning
or teaching. The scarcest skill of the teacher is usually the ability to diagnose
learning difficulties, a skill acquired by observation of learning under
various circumstances. In school, the use of this scarce skill must share time
with all of the other functions built into the teacher's role. This is how
schools succeed in taking plentiful leaning resources and making them
scarce. They package them all together, and then stand the package on its
head.

What schools do, nevertheless, provides an excellent model for the
organization of educational resources. The model has merely to be used in
reverse. Educational resources must be administered independently of each
other, and given priority in reverse order to that of the school. First, attention
must be given to the availability of information in the form of records, the
instruments, which produce and interpret these records and other objects in
which information is stored. Second priority must be given to the availability
of skill models, people who can demonstrate the skill to be acquired. Third
priority must go to the availability of real peers, fellow learners with whom
leaning can actually be shared. Fourth and last priority must go to the
provision of educators who by virtue of experience can facilitate the use of
the more essential learning resources. It might appear that educators are of
first importance, if only to see that the other resources are properly valued
and used. It is evident, however, that this is what educators when
incorporated into schools do worst, not because they are educators, but
because schools give them powers, which corrupt their judgment.

Skill models are different from educational objects in two important
respects. First, they must personally consent to their use as
educational resources. Second, they frequently have enough additional
flexibility and other secondary advantages to make it worth the
trouble of gaining this consent. In this technical age they are not
strictly necessary, since their skills can all be demonstrated on
records of one kind or another, but they are convenient. The superior
flexibility of human models was recently demonstrated in Patrick
Suppe's experiment in computerized instruction at Stanford
University.\footnote{The author observed this experiment in person in
  1966, saw the attendants behind each child and talked to the
  computer programmers and to the research staff.} The computers were
programmed to teach reading and numbers to beginning first
graders. The computers worked well so long as one teacher stood behind
each child to deal with his unexpected responses.  Typical of these
was the insertion of a pencil under the keys, which operated the
computer. The computer could, of course, be programmed to deal with
each one of these unanticipated reactions but at the end of the third
year of the experiment the programmers were lagging further behind the
children than at the beginning. Computers may be able to teach other
computers but it appears that human learners may, for a time at least,
continue to be better served by human models.

Skill models are in plentiful supply. There are almost always more people
in any vicinity who possess a particular skill than there are people who want
to learn it. The major exceptions are when a new skill is invented or
imported into a new territory. When this happens, skill models proliferate
rapidly and are soon in balance with the demand for their services. Only
schools and similar monopolistic institutions make skill models scarce.
Schools try to forbid the use of models who have not joined the teachers'
union. Some of the most famous musicians in the world, who fled from
Germany at the time of the Nazi terror, were not allowed to teach music in
the schools of the United States. Unions and professional associations also
restrict the unauthorized use of skills, frequently creating serious shortages
of vital services. Nurses, for example, are scarce in the United States,
primarily because the training curriculum has been extended again and again
by schools of nursing, placing the cost of training beyond the means of the
girls for whom the profession offers opportunities for social mobility.
Restrictions on the practice of a skill are usually justified in terms of
professional standards and protection of the public. Sometimes these claims
are true, but more often they are patently false. The best skill models are
frequently those who have just learned a skill. Children learn to read from
older siblings, sometimes with ridiculous ease. English schools were really
economical for a time, when Joseph Lancaster introduced the systematic use
of older students to teach younger ones. This system was better than schools
usually are and was very much cheaper. It shares with other forms of
schooling the fatal flaw of not allowing the learner to choose his model, his
subject matter and his place and time of instruction.

Skill models should be organized as an educational resource so as to give
each learner the widest choice of models and each model the greatest
latitude in accepting or rejecting learners. This requires first an absence of
restrictions and second a directory of skill models of all kinds. Ideally, there
would be no special restrictions of any kind. Model-pupil relationships an
subject to risk and abuse as are any kind of human relationships but the
general laws and customs which cover all such relationships provide the best
available protection. Learning in and of itself creates no additional hazards.
The advantage of permitting the learner to seek and find a model from whom
he will learn is, in general, worth all the additional risk that such latitude
entails. Not learning what needs to be learned is likely to involve the greatest
risks of all.

Developing directories of skill models is not intrinsically difficult. Truly
convenient and comprehensive directories might be so valuable, however, as
to warrant considerable investment. Responsibility for developing and
administering such directories should probably be vested in a public utility.
Skill models willing to offer evidence of their skills would be offered free
registration. Those who chose not to do this would, nevertheless, retain the
freedom to make such arrangements as they could, using their own means of
publicity.

The financing of skill training contracts can best be dealt with after the
organization of other educational resources has been discussed. It may be
that skills, which seem to be required for intelligent participation in the
modern world should be taught at public expense. In this case the public
utility, which maintained the directory of skill models, could also be charged
with paying them. Public payments should not be made, however, except
upon evidence that an essential skill had been learned to an acceptable
standard. Private contracts could be left to the wishes of the contracting
parties.

Having learned a skill, people need someone with whom to practice. But
peers are important even before practice. Who would bother to learn a skill
unless there were others with whom to share it, fellow explorers of the new
ground opened by the skill? Many skills are learned primarily with peers,
taking advantage of the skill models in the general environment. Often peers
and skill models are hard to distinguish. In ordinary inter-personal relations
there is no need, nor advantage, in distinguishing skill models from peers.
On the contrary, learning occurs best when such distinctions are not made.
There is, however, an important distinction, which can be ignored only if the
individuals involved are willing to ignore it. Peers are, by definition, equals
deriving mutual benefits from their relationship. They can play tennis
together, go exploring together, study mathematics together, or build a camp
together. If they are peers, they contribute more or less equally to each
other's objectives. Helping a smaller brother or sister learn what he wants to
learn is different. It may be equally enjoyable for a while, but the enjoyment
palls more quickly. Peer relationships are freely chosen, freely kept. Skill
modeling frequently requires some sort of compensation for the model, if the
relationship is maintained as long as the learner would like. A method for
compensating skill models is needed, therefore, which is not needed in
relationships of peers.

Finding peers is merely a matter of knowing where they are and being able
to get there, write or call on the telephone. Neighborhoods free of
automobile hazards are all that most children need for the purpose. As skills
develop, however, the better ball players go farther a field to find worthy
competitors, the botany bug outgrows his neighborhood pals, the serious
ballet addicts find their ranks thinning out.

Schools supplement neighborhoods, as things are now organized, but
schools create as many barriers to peer groups as opportunities for
them. In schools, peer groups form around the goals of teachers or
around the interests of dope-pushers. Student-initiated groups have a
hard time competing with either. But for teenagers the neighborhood no
longer serves as an adequate base for contacts. If it did, the
telephone and the automobile would be more dispensable. These
instruments are often charged with breaking up the face-to-face
community, but actually, along with \emph{Main Street and Peyton
  Place}, they mainly expose its limitations.

For adults, with their frequently highly specialized interests, even the
largest cities cannot always provide true peers. The best illustration of this is
the scientific community, which must be worldwide for the most fruitful
peer encounters to occur. The scientific community also illustrates how peer
matches can be fostered or frustrated.

The logical structure of science provides a framework for identifying
persons of similar interests. Its journals provide of communication. Its rules
of logic and criteria of evidence provide the parameters for fruitful
encounters. Its achievements unfailingly generate new problems, which
beckon explorers with common interests. All these advantages of science, as
a network of related interests, which provide an ideal basis for peer-group
formation, have now been offset by institutional barriers. National and
corporate interests now dictate who may speak or write to whom, in what
terms, when, how and where. Only the model of a scientific community is
left, available for other interest groups which have been less successful in
forming fruitful peer groups than scientists used to be.

The advantages of science in fostering communication among peers are no
more natural than the forces, which have now disrupted them. The modern
`logical' structure of science did not exist a century ago. Neither did its
current journals, nor its present rules of logic or standards of evidence. The
beginnings were already there but, two centuries ago, even those were only
dim foreshadowing.

Fortunately, other communities of interest do not have to retrace the steps
of science. Her example, as well as her products, make it possible to shortcut
these steps. Today any area of interest can be so described that a computer
can match the persons who share it. Learners in search of peers need only
identify themselves and their interests in order to find matches in the
neighborhood, city, nation or world. The computer is not indispensable. In
the neighborhood a bulletin board will do, in the city a newspaper, in the
nation a national magazine, in the world an international journal. All of these
media and others are and should be used to find peer matches, but computers
can make the matching easier and more flexible.

The operation of a peer-matching network would be simple. The user
would identify himself by name and address and describe the activity he
wanted to share. A computer would send him back the names and addresses
of all who had inserted similar descriptions. People using the system would
become known only to their potential peers.

As in the case of skill models, a public utility might provide free service
for the finding of peers. This would be justified not only in educational
terms but also as buttressing the right of free assembly. The same right
should also be extended to include prohibition of involuntary assembly, in
the form of compulsory attendance at school. If freedom of the press and
free assembly were taken seriously and public means provided to make them
available to everyone, compulsory school attendance, military service and
other common current compulsions would become unnecessary.

As schools are replaced by networks of educational objects, skill
models and peers, the demand for educators, rather than declining, will increase.
These educators will perform different functions from those now performed
in school and not all of them will be the same people. The need for people
with real ability in administration, teaching and scholarship will increase,
and their rewards in terms of educational achievement, professional freedom
and income will also increase. Schoolmen whose skills are primarily in the
hiring, supervision and firing of teachers, public relations with parents,
curriculum making, textbook purchasing, maintenance of grounds and
facilities and the supervision of inter-scholastic athletic competition may not
find a market for their skills. Neither will all of the baby-sitting, lesson-
planning and record-keeping teachers, unless they have skills, which can be
turned to other uses, or unless they leave education for more honestly
designated employment.

At least three kinds of professional educators will be in strong demand:
first, architects and administrators of the educational resource networks
which have been briefly described; second, pedagogues who can design
effective individual educational programmes, diagnose educational
difficulties and prescribe effective remedies; and third, leaders in every
branch of learning.

Educational resource networks are simple in principle and will be effective
only if they are kept simple in operation. The kind of simplicity required,
however, is frequently the mark of genius. People who can multiply the
services available to people and still stay out of their way are relatively rare.
'You always find the other kind', in the words of an old refrain.

The designers of the new networks will have to understand knowledge,
people and the societies they live in. They will have to be dedicated to the
idea of student-directed, individualized education. They will have to
understand the barriers to the how of relevant information and how to reduce
them without generating counter-actions, which would annul their efforts.
They will above all have to be able to resist the eternal temptation of subtly
directing the studies of their clients instead of opening ever new and
possibly dangerous doors for their investigation.

Teachers were greatly honored before there were schools, and will be
again when they are freed to practice their profession without the constraints
of forced attendance, mandatory curriculum and classroom walls. The core
of an independent educational profession will be the pedagogue, a bad word
now but one which will come into its own when students, parents and
teachers are free to make responsible educational decisions. They will find,
then, that they need advice and assistance in selecting learning programmes,
choosing skill models, discovering peers, finding leadership in difficult
endeavors. Unlike the network administrator, the independently practicing
pedagogue will not have to suppress his opinions and values in favor of
those of his client. He will be free to make value judgments because his
clients will also be free. He will have no way of shaping their decisions
except through the persuasiveness of his advice. And he had better be right
as well as persuasive, for his clients will hold him responsible. This
profession will not be for the faint-hearted, but the competent pedagogue
will find in it the rewards of the old family doctor, the man who made the
reputation, which modern medical specialists are still living on. There will,
of course, be room for many pedagogic specialties, such as testing and
educational psychiatry -- which will be in less demand as the damage done
by schools declines. Eventually the new pedagogic profession may succumb
to the over-specialization, which now afflicts medicine, but by that time it
will have made its contribution.

The role of the educational leader is somewhat more elusive than that
of the network administrator or the pedagogue. This is because
leadership itself is hard to define. Walter Bagehot's description of a
leader, as a man out in front when people decide to go in a certain
direction, has not been improved upon.\footnote{Walter Bagehot,
  \emph{Physics and Politics}, in Norman St John-Stevas (ed.), \emph{Collected
  Works of Walter Bagehot}, vols. 5 and 6, Economist, 1971.} It takes
nothing away from the leader who positions himself for relevant
reasons.

Leadership, like education, is not confined to intellectual
pursuits. It occurs wherever people do things together and especially
when the going gets rough. This is when true leadership distinguishes
itself, usually depending more on relevant prior learning than on the
personal qualities so clear to the world of fiction. There is no valid
test of leadership, even past experience. As Thomas Kuhn points out in
\emph{The Structure of Scientific Revolutions}, even in fields as
rigorous as physical science, the most distinguished leaders are,
periodically, bound to be proven basically wrong.\footnote{Thomas
  Kuhn, \emph{The Structure of Scientific Revolutions}, University of Chicago
  Press, 1962.} There is, on the other hand, no substitute for
leadership and leaders remain one of the vital educational resources
which learners must be helped to find.  Leaders will, of course,
recommend themselves and this must be depended upon even in systematic
attempts to match learners with leaders.  

In practice, there will always be a fuzzy line between skill models
and leaders. Both are specific to the content of what is being learned
or done; both are subject-matter specialists. Mountaineers cannot
substitute for physicists or vice verse. The directories and
administrative means that serve to locate skill models can also be
used to locate leaders, who will identify themselves by the claims
they make, the terms they insist upon, and the behavior they exhibit
in actual encounter. Networks can be helpful in finding potential
leadership but the true commodity will always be recognized only after
the fact.  

There are other kinds of human beings who can be called educational
resources only by stretching the concept. These people are,
nevertheless, more important educationally than all of the resources
described above.  They are mothers, fathers, children, lovers and all
of the other kinds of people who distinguish human beings from
featherless bipeds. These people cannot be treated as educational
resources because they are primarily something else. Familial, erotic,
economic and political relationships cannot be organized as if their
main purpose were educational. All of them, however, have enormously
important educational implications, which cannot be neglected even
though they cannot be manipulated for specific educational
purposes. Unless people can enjoy, in the main, good human
relationships, they cannot be educated nor educate themselves. The
most fundamental educational resource, then, is a world in which most
people can have good relationships with others. Perhaps paradoxically,
universal education may itself be the principal means of realizing
such a world.  

It should be emphasized again that distinguishing the various kinds of
human educational resources makes sense only in economic and
administrative terms. It would be an error for individuals to treat
each other or to regard themselves as restricted to one or another of
these categories.  For economic reasons, however, the distinctions are
vital. Whether educational resources are scarce or plentiful,
expensive or cheap, depends entirely upon maintaining these
distinctions. And this in turn determines whether education, and all
other privilege, is to remain the prerogative of the few, or whether
education and justice are to be made available for all.



\theendnotes 
\setcounter{endnote}{0}
