% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage

\thispagestyle{empty}
\begin{chapquot}
The way to establish an institution is to finance it.
\begin{flushright}
\emph{Justice William O. Douglas}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\newpage

\chapter{Financing Education}

Any attempt to say who now pays for education and who benefits from it
is bound to be incomplete. This is partly because education is not well
defined, while attempts to define it usually generate more controversy than
agreement. The errors of incompleteness are now reduced, however, by the
overwhelming preponderance of the school. School is the world's largest
enterprise; larger than agriculture, industry or warfare.

Of school's three rivals for the educational dollar, all but one are
demonstrably small by comparison. The mass media are the most
prominent, the easiest to define and probably the smallest. Large as
the press and television are, along with movies, radio and all other
forms of publishing, broadcasting and public entertainment, they
represent less than half the money, time and clientele involved in
schooling.\footnote{National accounts data, US and other countries,
  published anually by the United Nations.} On-the-job training, while
harder to estimate, may be a little closer but still a losing rival to
schools.  There are a few more people at work throughout the world
than in school, but not many more. Not enough more to offset the much
smaller proportion of their work time devoted to learning.\footnote{In
  comparing the cost of education in school and on the job, all school
  costs are attributed to the education which occurs in school a while
  only training time on the job is charged to the education which
  occurs at work. Our analysis of the functions of schooling might
  appear to invalidate such an attribution and for other purposes it
  would. For this specific purpose, of comparing educational costs in
  school and at work, it seems justified. The money the public pays
  for production is paid for the products of work. The money paid for
  schooling, so far as the public is concerned, goes for education. In
  neither case does the public get what it pays for, but in comparing
  costs it must be assumed that it does.}  Perhaps the greatest rival
of schooling is the personal time people spend in learning outside of
institutional frame-works. This time and its imputed value may still
exceed even current investment in schools, but it is very difficult to
measure. One of the worst things that can be said about schools is
that they have made such serious inroads upon this time and that they
threaten to capture it all. This personal learning time is man's
principal hold on freedom, doubly in danger now, first, because
schools threaten to absorb it and second, because schools use up the
resources which could enrich it. A further capture of the resources,
which still go into personal learning, could have only reactionary
consequences. It would also be undesirable to try to capture the
resources, which still go into training on the job. Work has been
dehumanized too much already by separating it from learning. Better
use of educational resources by the mass media is a desirable goal,
but it may be that a better- educated public is the best way to
achieve this. Attempts to achieve it by government control run the
risk of further manipulation of the people on the other hand, and of
an ineffective cultural snobbery on the other. Only the resources now
reserved for the school are open for reallocation among educational
objectives, at no loss to any interests except those that seek to
perpetuate the present structure of privilege.  

There are still people who believe that we could finance the education
we need by means of schooling, if only we were willing to give it
priority, but this is to ignore the competitive character of
schools. No sooner is universal high-school attendance approached than
the competition shifts to colleges, at higher costs. There is already
agitation for degrees above the Ph.D. on the grounds that the
doctorate has become common and degraded. There can be no end to
schooling in a world, which puts no limits on consumption and where
degrees determine people's position at the trough.  

Schools are an almost perfectly regressive form of taxation, paid for
by the poor to benefit the rich. Schools are supported, largely, by
general taxes, which ultimately fall more upon the poor than their
direct incidence suggests. Property taxes, for example, are paid by
those who occupy dwellings rather than by those who own them, excise
taxes by consumers rather than producers. Meanwhile, the benefits of
even public expenditures for schooling are distributed in direct
proportion to present economic privilege.  

The children of the poorest one-tenth of the United States population
attend school for an average of less than five years. The schools they
attend, at this grade level, spend no more than \$500 per pupil per
year. These children cost the public, in schooling, less than \$2,500
each over a lifetime.  The children of the richest one-tenth of the
population finish college and a year of graduate school, which costs
about \$35,000. Assuming that one-third of this is private
expenditure, the richest one-tenth still get ten times as much of
public funds for education as the poorest one-tenth.\footnote{These
  calculations were made by the author, based on school cost data from
  the annual reports of the U S Office of Education, supplemented by
  information contained in such special reports as the Coleman study
  on equal opportunity. There are no official data on enrolment by
  family income nor on expenditure by income nor on expenditure by
  family income, but there are many bases for estimate and for cross
  checking the validity of estimates. I believe that I have been, in
  all instances, conservative and invite others to make independent
  estimates and to check my calculations.}

Compared to most of the rest of the world, the schools of the United
States are relatively fair. In Bolivia, for example, one-half of all
public allocations for schools are spent on one per cent of the
population. The ratio of educational expenditures on the upper and
lower one-tenth of the population, respectively, are about three
hundred to one.\footnote{These calculations were made by Ivan Illich,
  based on official enrolment and expenditure data supplied by the
  Bolivian Government. They were personally reported by Illich to a
  specially constituted policy board of the Bolivian Government and
  were accepted as valid. They reflect, primarily, the fact that
  only a tiny percentage of the population survives the early
  elementary grades, and that all the cost, of secondary and higher
  education must be attributed to this tiny minority.} Most parts
of the world are nearer to the Bolivian than to the United States
ratio.

Schools constitute a regressive tax because the privileged go to
school longer and because costs increase with the level of
schooling. Graduate schools, for example, provide by far the highest
student subsidies not only in relative but also in absolute
terms. Graduate students come largely from the upper income levels of
the society. Nevertheless, students pay almost nothing at this level,
in fact are frequently paid, while graduate-school support, even in
private universities, comes largely from public funds. Costs in the
sciences, for example, run up to several hundred thousand dollars per
student per year. At the under-graduate level there is a higher
proportion of private expenditure but, even here, public subsidies
average several thousands of dollars per student per year, as compared
with hundreds of dollars at the elementary level, where most of the
poor children are.\footnote{Based on compilations prepared by the US
  Office of Education, which in turn are based on data reported by
  the various states, and published anuually by the USOE.}

Schools make it impossible to equalize educational opportunity, even
in terms of the allocation of public resources. Unless they abandon
scholastic standards altogether, they can never keep poor children in
school as long as rich ones and, unless they reverse the cost ratios
which have always characterized schooling, they will always spend more
at higher than at lower levels. Even compensating programmes
specifically designed to help poor children cannot achieve their
purpose within the structure of the school. Of three billion dollars
earmarked by the federal government of the United States for
supplementary services to poor children, less than a third was
actually spent on eligible children; these children showed no
measurable improvement, while the non-eligible children, with whom
they were mixed and who also benefited from the money, did make a
measurable gain.\footnote{Based on an Evaluative Report on U S special
  allocations for the education of the disadvantaged (Title I)
  entitled, `Education of the disadvantaged', published by the U S
  Office of Education, fiscal year 1968. This report found among other
  things, that less than one dollar of every three allocated to Title
  I projects, which were supposed to supplement the education of
  children whose families had incomes below \$3ooo per year, was
  actually spent on these children. The rest was spent on other
  children, of higher family income, enrolled in the same school
  system or on support of the system in general.}

In a just world, or in a world trying to achieve justice, public
expenditures on education should be inversely proportional to the
wealth of the student.  Private expenditures on education are made
almost entirely on behalf of the better-off, so that merely equal
expenditures on education would require that public funds for
education go in a higher proportion to the poor. Even this would not
equalize educational opportunity, since the parents and the homes of
the better-off represent an investment in their education, which would
also have to be offset. Finally, the poor suffer the handicap of the
culture of silence, the inheritance of magic and myth designed to
ensure their continued docility. It is this, rather than deficient
genes, which handicaps the learning of their children; this plus the
punishment of failure and disapproval which is their customary lot in
schools. These disadvantages, not inherent in them and not of their
own making, require an additional expenditure on the education of the
poor to offset them. If all of the public funds allotted to education
in every nation were spent exclusively upon the poor it would still
take many generations to offset the handicaps, which generations of
exploitation have imposed upon them.  

It should now be clear that even the first step in equalizing
educational opportunity among social classes requires an allocation of
educational resources outside the school system. The only ways of
making sure that poor children get even an equal share of public funds
for education are either to segregate them completely in schools of
their own, or to give the money directly to them. The first of these
alternatives has been tried and has conspicuously failed. The second
provides the key to the proper allocation of educational resources.

Putting command of educational resources in the hands of learners does
not solve all problems, but it is an indispensable step toward a
solution. Not only the problem of equalizing opportunity across class
lines but all of the other problems discussed above become manageable
with the aid of this principle. Schools would stand, adjust or fail
according to the satisfaction of their clients. Other educational
institutions would develop in accordance with their ability to satisfy
client needs. Learners would choose between learning on the job and
full-time learning, which skills they wanted to learn, at what age
they wanted to use their educational resources, and how. This
presupposes a system of lifetime educational accounts, administered in
early childhood by parents, in which resource credit could be
accumulated, and over which the learner, from a very early age at
least, would have a veto power.

In the world as seen by Adam Smith, this would solve all of the
problems.  In the real world, there is still an imperfect distribution
of the supply of the real resources required for education and there
is, furthermore, imperfect knowledge of what resources are needed and
where they are to be found.  This imperfect knowledge is not confined
to learners. The would-be suppliers of educational resources also lack
knowledge of where their potential clients are and of what they
need. There is finally the age-old problem of imperfect character;
people who want something for nothing and those who will sell anything
to make a profit. If perfect competition and complete knowledge
existed they would offset the problem of man's venality. Since they do
not, it remains to compound the evil.  

A system of personal educational accounts and a system of public
educational utilities, such as those suggested in the two previous
chapters, would mutually supplement each other. They could be combined
in a number of ways. One way would be to make the public utilities
self- supporting, as the postal system is supposed to be, charging
each user for the approximate cost of the service rendered
him. Another way would be to make the services of the resource
networks completely free. This would require dividing public funds for
education between the support of the proposed utilities and provision
for the proposed personal accounts.  

Besides these alternatives, various mixtures are possible: providing
some services free while charging for others, charging some people and
not others, according to age, income or other criteria. The possible
combinations are too numerous to permit exhaustive discussion but a
few are worth considering.  The networks might, for example, provide a
directory service free, locating skill models, objects, peers, etc.,
but doing nothing further to facilitate economic access. This would
leave it to the learner to buy or borrow the indicated book, or pay
the skill model, out of his personal account.  Alternatively,
libraries and stores of educational objects in common demand might be
maintained for the free use of the public; reading and mathematics
teachers might be publicly paid to teach elementary skills.  

These alternatives contemplate only the use of public funds derived
from tax revenues, divided between educational resource networks and
personal educational accounts. Additional financial arrangements are
conceivable. An educational bank might be established in which
educational services could be deposited and from which educational
credit could be received. Skill models, for example, might elect not
to be paid in cash but to accumulate credit for themselves and their
children. Learners might borrow from the bank either credit to pay a
skill model who is accumulating credit, or cash to pay one who wants
cash, and the learner might later repay the bank either in cash or in
service as a skill model for someone else.  

An educational bank could not compete in the market with banks less
restricted in their activities. Government support for such a bank, or
for any free service by an educational resource network, or even for
the establishment of such networks, would constitute an educational
subsidy.  The use of tax funds for education is, of course, already a
subsidy, but so long as these funds went entirely into educational
accounts there would be no subsidy for one kind of education in
preference to another. Today there is. Graduate schools are highly
favored over elementary levels. Science is favored over other
subjects. Nor are subsidies confined to the public sector.
Monopolized industries are able to pass on the costs of educating both
their employees and the public, because they can set their prices to
absorb such costs. Government bureaus have even greater latitude of
this kind. The military have almost unlimited funds for training and
almost unlimited capacity to educate the public, not only by spending
money but by manipulating information. The mass media are currently
subsidized by the advertisers of products and services, while
producing corporations in turn are able to charge the public for the
education they impose upon them. Some educational subsidies are paid
directly out of the public purse. Others are collected by private
corporations under permissive public laws. All are expressions of
public policy more or less openly admitted.

Educational policy would be most open to public view if all public
funds were channeled into personal educational accounts, and all
educational institutions, including the proposed resource networks,
had to be self- supporting. At present levels of expenditure, this
would provide every man, woman and child in the United States with
\$250 per year to spend on education. It would also force all
elementary and high schools, universities and graduate schools to
charge tuition high enough to pay all costs. At first sight, many a
scholastic career would appear to be nipped in the bud. Each newborn
child, on the other hand, would have a lifetime fund to finance his
personal education, valued at \$17,000. Each twenty year old could
count on \$12,000, each forty year old on \$7,000, each sixty year old
on \$2,000. This is the average amount of schooling the American
public is buying now, through taxes, for each of its
citizens.\footnote{These calculations are based on projected public
  expenditures for education for 1971 of \$50,000 million. They assume
  an eligible population of 200 million and an average lifetime of
  sixty-eight years. \emph{Projections of Educational Statistics},
  US Government Printing Office, OE-10030-68.} It is roughly
equivalent to a college education. The fact that so few people get it
merely underscores the inequality of the present
distribution. Actually few of those who now receive more would be
seriously deprived if public funds were divided equally. All but a few
would be able to finance their education privately. Poor children, on
the other hand, would have five times as much educational support as
they now receive. On the average, these poor children would get the
same amount they now get up to age fifteen. They would then, however,
still have four times as much left. Many poor children become able to
profit from formal training only at about this age, although this may
be only because the present system of schooling is so poorly adapted
to their needs.  

What about those who, today, spend at a much faster rate? Would the
average middle-class child who now uses up his quota of public funds
by about age twenty-one be retarded by having to keep within his
annual quota?  Under at least three circumstances he would not. First,
his family might supplement his allowance. Current private investment
in education is only one-fourth of public expenditure, one-fifth of
the total. Second, he might not attend schools but get his education
elsewhere at lower cost. Third, he might borrow against his lifetime
allowance. Under favorable circumstances this would appear to be
relatively safe. If the student's family was fairly well off but
reluctant to invest their own money, they could probably, in one way
or another, insure against the risk of failure. If, on the other hand,
the family were poor but the student able and promising for his chosen
career, insurance might be bought at public expense to cover his risk.

What about people who didn't use their allowance? The tax-payers might
be allowed to benefit. Allowances might be increased in subsequent
years.  Families might be permitted to pool their allowances. Adults
would probably learn fairly quickly to use what they had coming, and
this could work to the great benefit of education, the adults
themselves, and their society. Adults acting on their own behalf would
be shrewd buyers of education. Their demand would bring educational
resources into the market, which younger people would also use, once
the demonstration had been made. Many now passive middle-aged and
older people would come alive and begin to rake a more informed
interest in their own and in public affairs.  

How would educational allowances actually be spent? What would happen
to schools? How would profiteering be controlled? Answers depend very
much on development of the resource networks previously described.
Capital investment in these networks would have to have a prior claim
on public funds for education, even though this investment were to be
repaid by the fees charged for service. If these networks were well
designed and operated they would influence the investments of many
learners, especially adults at first. Many of these adults would be
parents, however, whose children would also soon begin to use the
networks.  

Schools would, for a time, continue to be used by parents dependent on
the custodial care they offered and by students far enough alone to
be dependent upon them. Schools could not, however, continue to
function at today's level since the public funds available to the age
group they serve would be less than one-third of the public funds that
schools receive now. A few schools might survive -- old established
private schools and a few public schools in rich suburbs whose pawns
could afford to keep them up and could also afford not to worry too
much about the economic future of their children.  Most people would
soon discover more efficient ways of learning and more pleasant ways
of spending time than in school.

Charlatans and profiteers might have a field day for a time. But, if the
networks functioned properly, they would soon be checked, not by
suppression primarily but by the competition of honest suppliers and skill
models, aided by competent pedagogues advising parents and students.
Controls of the type offered by better business bureaus would be sufficient,
since they would be too inefficient to do much harm but available for use
against a real mountebank.

The money which is now so insufficient for schools would be more than
enough to support an enormous network of educational objects and to
partially support a number of skill models, pedagogues and educational
leaders far in excess of the number of teachers employed today. Many would
work only part time as skill models and educational leaders, practicing their
skills and pursuing their explorations for other purposes as well. Pedagogues
working full time could serve a clientele of a thousand persons, giving each
on the average a couple of hours per year.

Financial magic? Not at all. The cost of custodial care would in large part
be transferred back from the public to the private purse. Most of it is now
provided for people too old to need it -- old enough to work and, in many
cases, better off working. The additional education provided, at lower cost,
would also produce a citizenry able to solve the pseudo-problem of how to
employ the additional `un-needed' hands and brains released from the tedium
of school.





\theendnotes
\setcounter{endnote}{0}