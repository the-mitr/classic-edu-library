% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\newpage
\thispagestyle{empty}
\begin{chapquot}
 Ritual is play: it defines our Utopias and gives expression to our
  dreams.  Ritual is serious play, but if we believe that our
  ritualized Utopia is an accomplished fact, it ceases to be play and
  becomes an ideological instrument of oppression.

\begin{flushright}
\emph{Jordan Bishop}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\newpage

\chapter{How Schools Work}

As the songwriter said, long before Marshall McLuhan, `It ain't what
you do, it's the way that you do it'. In the case of schools the
medium really \emph{is} the message. Schooling is social ritual,
bridging the gap between social theory and social practice. In
psychological terms, ritual makes it possible to live with cognitive
dissonance which arises from discrepancies between ideals and
actions.\footnote{Leon Festinger, \emph{A Theory of Cognitive Dissonance},
  Harper \& Row, 1957.} Corresponding to ideals and actions at the
personal level are ideologies and practices at the level of
society. To discuss schools in these terms is to ask, not what they do
functionally, or what they are in essence, but how they achieve the
effects on behavior, which they observably do achieve.

It may be useful to begin with an example from religion, where ritual is
more familiar. The discrepancies between Christian precept and Christian
practice are well known, thanks to ministers and priests. Christians are
supposed to do unto others, as they would have others do unto them. They
are supposed to give others double what they ask. They are supposed to
share all they have and to seek out those in trouble so as to minister to their
needs. In practice, most nominal Christians are businessmen, professional
men or workers who drive a hard bargain, look out for their own, regard the
poor as shiftless and undeserving and stay as far away from prisons, slums
and charity hospitals as they can. The discrepancies between teaching and
practice are reconciled by participation in the rituals of church attendance,
baptism, communion, prayer and Christian burial, to name but a few. There
are, of course, a minority of Christians who live according to Christ's
teachings. Among them are such modern martyrs as Camille Torres and
Martin Luther King, many recent victims of Brazilian torture, and many
more un-distinguished men and women, all over the world, fighting for
justice or serving the poor.

Just how rituals serve to help people believe they are Christians, when
their behavior deviates sharply from Christian teaching, is not easy to
understand. It is clear that they do, however, just as saluting the flag helps
the man who cheats on income tax believe he is a first-class citizen. One
explanation is that people are fallible; that their intentions matter more than
their actions, and that ritual provides a means of expressing what is in their
hearts. This explanation may be plausible where religion and patriotism are
concerned. It will have hard going when we begin to look at schools.

Let us look at four ideologies, which play a prominent role in society,
examine the corresponding realities, and then identify the scholastic rituals,
which help to bridge the gap between ideology and reality. The ideologies
selected are those, which deal with equal opportunity, freedom, progress and
efficiency.

There is equal opportunity, according to the ideology of modern societies,
for every man to achieve whatever his ambitions dictate and his abilities
permit. This ideology asserts that all levels and branches of schooling are
open to all and that only their dedication and brains limit scholars. It
proclaims that all occupations and social levels are open to anyone with
enough drive and the ability to deliver the goods. Increasingly the school is
recognized as the major avenue to occupations and social roles, and the
openness of scholastic channels is, therefore, stressed as guaranteeing access
not only to academic but also to social advancement. This is the ideology of
equal opportunity: pretending to make everyone's advancement depend
solely upon his own personal qualities.

The reality is that all advancement is at the expense of others. Schools,
occupational ladders and social class structures are all hierarchies in the
shape of pyramids. In school, each higher grade is also smaller. Seldom can
grades or levels be skipped. Each successive competition must be survived,
therefore, in order to reach the top. In industry the picture is the same. For
every president of Standard Oil, ten thousand office boys are left behind.

At what age are the opportunities equal? At birth? It seems unlikely
that at birth the son of a president, even though he should begin as
office boy, would have no better chance than the son of an office
boy. But, if not equal at birth, the chances certainly become less
equal with every year of life. By the time school begins, no one has
irrevocably lost, but as soon as kindergarten is over, grades and IQ
scores are recorded, and from then on the door is almost closed for
those whose grades and scores are low. This is not because these
grades and scores are valid. Even those who believe in them admit that
at this age they are highly unreliable.\footnote{Joseph M. Hunt,
  \emph{ Intelligence and Experience}, Ronald Press, 1961. } But
judgments have to be made. Judgment about which school, which track,
which teacher; all judgments that vitally influence the chances for
the future. Once the elementary school is passed, it is nonsense to
speak any longer of equal opportunity for those who have not done well
enough to go on to a good academic secondary school. One trade-school
boy in thousands may wind up as head of a construction company, but
this is the great exception. In fact, every step up the ladder for one
is a step down for another; one can rise to the top only over the
heads of thousands. Corresponding to the ideology of equal opportunity
is the reality of enforced inequality, with the odds of staying near
the bottom many times higher than the odds of getting to the
top.\footnote{`Another alternative to working-class solidarity offered
  is the idea of ; individual opportunity- of the ladder. It has been
  one of the forms of service to provide such a ladder, in industry,
  in education and elsewhere \ldots Yet the ladder is a perfect symbol
  of the bourgeois idea of society, because while undoubtedly it
  offers the opportunity to climb, it is a device which can only be
  used individually: you go up the ladder alone \ldots My own view is
  that the ladder version of society ,, is objectionable in two
  related aspects: first, that it weakens the principle of common
  betterment, which ought to be an absolute value; second, that it
  sweetens the poison of hierarchy, in particular by offering the
  hierarchy of merit as a thing different in kind from the hierarchy
  of money or of birth.' Raymond Williams, \emph{Culture and
    Society, 1780-1950}, Chatto \& Windus, 1958; Penguin, 1961.}

`But of course!' will be the reply. This is the nature of hierarchy. Everyone
understands what equal opportunity means. If this is so, then why not tell it
like it is? Call it the social lottery. In truth, it would have to be called the
loaded social lottery, with each child getting as many chances as his father
has dollars. But this would not suit the purposes served by calling it equal
opportunity. Everyone is supposed to think he has an equal chance whether
he does or not. It is better for his morale. For the moment, the question is not
whether this should or should not be. This is how it is and the question is,
how is it kept that way? How are people induced to believe, or at least act as
though they believed, in equal opportunity when in fact there is no such
thing?

They are induced to believe it by ritual progression up the ladder -- the
school ladder, the promotion ladder, the income ladder, the social status
ladder. As long as people are climbing, it is easy to maintain the illusion that
all roads lead to the top. One step at a time, that's how one gets there. The
fact that there isn't enough time in a normal life span for the man who gets to
the top to touch all the steps on the way is easily overlooked. It makes sense
that if you climb, step by step, you get to the top.

There are enough steps so that everyone can climb a few. Grades in school
are easy enough at first, in rich countries, and almost everyone passes these
early grades. By the time the going gets rough the lesson has been learned:
there is equal opportunity, but men just aren't all equal. The job ladder works
the same way. Everybody except those who really did badly in school gets to
go up a few steps. Then the intervals get longer. People get a little older. It
doesn't matter so much anymore.

Everyone's income also is allowed to rise a little, even if he doesn't get
promoted. Annual increments, each year a little more; and by the time the
plateau is reached the illusion is also established that everyone has had a
chance. Some are just luckier than others. Obviously this is not the whole
truth. But people are induced to believe it by participation in the ritual of
progression.

The ideology of freedom is that all men have certain inalienable rights: the
right of assembly, the right of petition for redress of grievances, the right to
be free from unreasonable searches and seizures, the right to counsel, the
right not to bear witness against themselves -- i.e. to be free from torture in
the first, second or third degrees. The facts are that all over the world the
flickering lights of freedom are going out. In the communist world,
deviationists and enemies of the people have no civil rights. In the capitalist
world, over half of the nations which were democracies twenty years ago
now have military regimes, many of which use torture as an everyday
instrument of government. The remaining `democratic' governments include
South Africa, where civil rights extend only to non-Africans and non-
Asians, and then only if they are careful to leave the issue of apartheid alone.
In the United States there is the South, where blacks have the rights, which
whites deign to give them. In the rest of the country peoples' rights are
increasingly determined by police and national guardsmen. Black Panthers,
dissident Democrats and college students are ever more in danger of having
rites instead of rights.

How is belief in freedom maintained in the face of these facts? Largely by
the rituals of democratic process. Among other things, the last presidential
election in the United States helped people forget the police power used at
the Chicago Convention by one wing of the Democratic Party against the
other. Equally, the last national election in France helped people forget the
military and police suppression of students and workers, which had occurred
just a few months before. These dramatic examples, however, are not as
important as the daily rituals of democracy in convincing people there is
freedom where there is increasing domination and suppression. The
professor, exhibiting his academic freedom by denouncing the
establishment, students flaunting hair styles and kicking up their bare feet,
sit-ins, paint-ins, sleep-ins and pot parties -- useful as these things are, they
serve to reassure people that they are still free when in fact they may not be.

In the outside world, we have the angry editorial, the enterprising
reporter's expos\'e, the new magazine going the old ones one better, and the
legislative investigation. A few serve good purposes. Many more merely
help to maintain the illusion of freedom. These examples are deliberately
chosen not to represent pure ritual. Whether the acts cited above are real or
ritual depends on who is doing what for what purpose. Ritual plays a
positive as well as a negative role in human affairs. The fact that the rituals
of freedom are equivocal testifies to the duality of ritual as well as to the
subtleties of freedom. With few exceptions, only those who know how to
play the game -- those already in positions of privilege even though dissident
are able to -- use the liberty which democratic process provides. Those who
are deprived have little real access to democratic process. This is one reason
Jefferson despaired of orderly reform, carried out within the rules. But the
rules themselves, ritually followed, disguise the basis for Jefferson's reliance
on periodic revolution. The democratic process, in school and society, helps
people accept the discrepancy between the assumption of freedom and the
facts of domination and suppression. We do not want to lose our democratic
process but neither do we want to delude ourselves about how much
freedom we have. We can extend the limits of our own and other people's
freedom only if we know what obstacles stand in the way.

The ideology of progress is that our situation is improving and will
continue to improve, without any demonstrable limits upon the degree or
scope of future improvements. The facts are that we are near the limits at
which the atmosphere can absorb more heat or the seas more pollution, near
the limits of the earth to support more population, near the limits of the
patience of the poor to subsist on the bounty of the rich, near the limits of the
rich themselves to either tighten further the screws they have fastened on
themselves or to live much longer with the indulgences they have invented.
People who do not want to face the implications of these facts say that the
problems they imply will be solved by new discoveries and inventions. But
the discoveries and inventions of the past have merely brought us to our
present predicament. Future discoveries and inventions can only sharpen this
predicament. For regardless of how near or far away the limits are there can
be little doubt that they exist, while the ideology of progress knows no
limits. The earth, human population and human nature are all finite while
progress is infinite. This theoretical problem might not have to bother people
if various kinds of progress were in reasonable balance, but this is not the
case. Our ability to kill each other and ourselves is growing much faster than
our productive capacity. The gap between the rich and the poor is rapidly
widening. Psychological tensions are increasing much faster than our ability
to deal with them.

The ideology of progress is, then, faced with a set of very hard facts,
which contradict its assumptions. How are these contradictions reconciled?
They are kept from consciousness primarily by the ritual of research -- the
continuing quest for new knowledge, new insights, new techniques.
Research is a very important non-ritual fact, but it is also an important ritual.
The ritual of research induces the belief that new discoveries change the
whole picture, that every day is a new day with a new set of rules and
possibilities. This is clearly false. Even the most important new discoveries
and inventions leave almost everything else unchanged. The invention of
breeder reactors stretched the world's supply of fissionable material
enormously. Nuclear fission extends the limits of possible energy sources
even more. But these far-reaching developments do not affect the absorption
capacity of the atmosphere at all. They have no effect on human population
except to threaten its total annihilation. They influence man's ability to think
and to govern himself in only the slightest degree. Yet the myth of renewal
by research, the belief that major new discoveries can renew all terms of all
problems -- this myth and belief permit men to avoid seeing the very hard
barriers to further progress, which in fact exist.

Research is so identified with school that its impact as ritual
renewal affects students even more than it does the general
population. The greatest impact of research on students, however,
comes by way of its effects on curriculum. One of the hallmarks of
modern schooling, which separates it most sharply from its own
tradition, is that its offerings are labeled as ever new. Yesterday's
knowledge is out of date. In Norway serious thought is being given to
declaring degrees more than five years old invalid. The merit of this
proposal is that it belatedly recognizes what was always true, that
degrees have little validity. Its rationale, however, is that
five-year-old knowledge is no longer valid. Every worker is obliged to
come back periodically to the school to refurbish the knowledge he
received the last time. Real education is, of course, a lifetime
process. But real education and real research are also continuous, not
periodic processes. Genuine research and education integrate the new
into the much greater mass of the old, and this can only be done in
the course of work, in the actual discovery and application of the
new. The ritual of renewal does serve its purpose, the purpose of
recreation, which need not be superficial. When it clothes the
nakedness of the myth of progress, however, it does a great
disservice. And this it does particularly by way of the school. The
illusion that knowledge must be contemporary to be valid stands
between the generations. This conceit of the young is largely the
result of the ritual of curriculum renewal as practiced by the
school.\footnote{New knowledge has meaning only in the perspective of
  the past. Schools were once such champions of this principle that
  the word scholastic still carries opprobrium in certain
  circles. Modern schools have done a complete turnabout. They pay lip
  service to meaning and reserve their obeisance for relevance. Nor
  can they blame students for this. Having made themselves the gateway
  to all other secular delights, they cannot wonder that students
  storm the gates.}

The ideology of efficiency is that modern man has solved his
production problems by means of efficient organization, that other men
can do likewise, and that most of man's remaining problems can be
solved by a similar approach. The fact is, as Kenneth Boulding has
recently suggested, that gross national product, the current measure
of a nation's output, is actually a measure of economic
inefficiency. Employment in the wealthier countries increasingly
follows Parkinson's Law -- that employment increases as production
decreases. More and more people in the wealthier countries are
employed in the service sector, doing things of dubious
value. Consider government and corporate bureaucrats, salesmen,
advertisers, bankers, accountants, lawyers, teachers, policemen,
soldiers, polltakers, social workers, for example. There is no doubt
that all of these people do something that someone values, but there
is also little doubt that as many people hate what they do. Lawyers
provide the clearest example; for every winner at law there is a
loser. The same thing holds, a little less obviously, for all of the
kinds of workers listed above and for many other services as
well. Many physical goods are also of dubious value. Military weapons,
pornography and billboards certainly have as many detractors as
supporters.  Super-jets, automobiles, gravestones and schools are also
in the doubtful category. Meat products, tobacco, alcohol, marijuana
and fluoridated water all have varying degrees of opposition. The
point is not that work is bad or even, in itself, of doubtful
value. It is rather that the value of work depends upon what it
produces.

Most people would agree that efficiency is misapplied to killing,
loving and eating, for example. But the ideology of efficiency does
not reflect these reservations. Its high priests have no qualms about
drugging children to keep them quiet in school, using electric shock
to torture prisoners or napalm to burn Vietnamese villages. How, then,
are the discrepancies between the ideology and the facts of efficiency
kept from overwhelming the public conscience? The answer is that it is
done through the ritual of activity.

Schools learned long ago that the way to keep children from thinking
is to keep them busy. Classes, clubs, athletics, cultural activities,
homework; the devil finds work for idle hands to do. This is also how
attacks on the efficiency of the school are met -- more courses, more
degrees, more activities, more enrolment. The graduates of school are
well prepared to participate in the activity rites of the outside
world: more committees, more projects, more campaigns, more products,
more industries, more employment, and more gross national product. Not
all activity is ritual. But in a nation capable of producing all of
its agricultural and industrial products with 5 per cent of its labor
force, as is the United States, ritual must account for much of the
remaining 95 per cent. It must also account for much of the time of
adults who are not in the labor force and of students who are in
school.

Schools have succeeded in ritualizing education because they serve
societies, which dedicate themselves to consumption, which assume that
man wants principally to consume and that in order to consume
endlessly he must bind himself to the wheel of endless production. The
whole theory of schooling is based on the assumption that production
methods applied to learning will result in learning. They do result in
learning how to produce and consume -- so long as nothing fundamental
changes. As a means of learning to adapt to changing circumstances,
production methods are ridiculous. The need to distinguish these two
kinds of learning is kept from our attention mainly by our
participation in the scholastic ritual.



\theendnotes 
\setcounter{endnote}{0}
