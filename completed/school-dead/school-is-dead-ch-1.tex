% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{The Case Against Schools}
%\addcontentsline{toc}{chapter}{The Case Against Schools}


Most of the children of the world are not in school. Most of those who
enter drop out after a very few years. Most of those who succeed in
school still become dropouts at a higher level. UNESCO data show that
only in a small minority of nations do even half the children complete
the first six grades.\footnote{Enrolment figures by grade, compiled by
  UNESCO, show that only a minority of even those more advanced
  countries which have such data do more than a small fraction of the
  students enrolled in the first grade complete elementary school
  (UNESCO, Enrolment Data, Annual Reports).} No child, however, fails
to learn from school. Those who never get in learn that the good
things of life are not for them. Those who drop out early learn that
they do not deserve the good things of life. The later dropouts learn
that the system can be beaten, but not by them. All of them learn that
school is the path to secular salvation, and resolve that their
children shall climb higher on the ladder than they did.\footnote{For
  a further development of the `ladder' image of society see the
  concluding chapter in Raymond Williams, \emph{Culture and
    Society 1780 -- 1950}, Chatto and Windus, 1958; Penguin, 1961}

For most members of the present generation, this hope that their children
will benefit more from school than they, is doomed to disappointment.
Schools are too expensive for this hope to be realized. For many, it may
appear to be realized but the appearance will be a delusion, fostered by
inflationary debasement of the academic currency. More college and high-
school degrees will be granted but they will mean less, both in terms of
amount and kind of learning and in terms of job qualification and real
income.

In all countries, school costs are rising faster than enrolments and
faster than national income. While the school's share of national
income can afford to grow slowly as this income increases, it cannot
continue to grow at present rates. In Puerto Rico, for example, the
national income was ten times greater in 1965 than in 1940. School
enrolment more than doubled during this period while school costs
multiplied twenty-five times.\footnote{\emph{The Public School}, an
  illustrated report, Department of Publiv Instruction, Commonwealth
  of Puerto Rico, 1967.} Yet, even in 1965, less than half of all
Puerto Rican students finished nine years of
school\footnote{\emph{The Public School}, 1967}, and the proportion
who reached the higher grades without learning to read was higher than
it was twenty-five years earlier. Puerto Rico is atypical in its
absolute rates of growth, but not in the vital relationships among
them.  Monographs on the cost of schooling in African and Asian
countries, sponsored by the International Institute for Educational
Planning, paint a similar picture, as do studies in Britain and most
of the countries of Western Europe.\footnote{Monographs, International
  Institute for Educational Planning, Paris} \footnote{From data
  compiled by the Organization for Economic and Cultural Development
  for member countries (Paris). See, for example, \emph{Background
    Study No. 1} for the Conference on Policies for Educational
  Growth.} Recent studies in the United States suggest that it would
cost eighty billion additional dollars to meet educators' estimates of
what is needed to provide adequate schooling.\footnote{This estimate
  is based on two studies commissioned by the United States Office of
  Education in 1969. Two groups of educators, working independently,
  were asked to set forth the currently unmet needs in American
  elementary and high-school education. The costs of meeting these
  needs were then calculated in terms of current unit costs as
  published in the USOE annual report. These statistics have not been
  published but their results were made available by Kenneth Parsley,
  Director of the Office of Organization and Methods of the USOE,
  Department of Health, Education and Welfare, Washington, D C.} Even the
settlement of the war in Indochina would provide only a small fraction
of this amount.

The conclusion is inescapable: no country in the world can afford the
education its people want in the form of schools. Except for a few
rich nations, and some not yet exposed to the development virus, no
country in the world can afford the schools its people are now
demanding from their political leaders. Continued attempts to supply
the demand for college study in the United States will condemn the
black and rural minorities to an indefinite wait for an adequate
education. In India, Nigeria and Brazil the majority must, for
generations, be denied all but marginal educational resources if a
tiny minority is to enjoy the luxury of schooling, which would still
be regarded as pitifully inadequate by United States standards.
Development economists argue that peasants in India, sharecroppers in
Alabama and dishwashers from Harlem do not need more education until
the world is ready to absorb them into better jobs, and that these
better jobs can be created only by others who must, therefore, be
given educational priority.\footnote{Frederick Harbison and Charles
  A. Meyer, \emph{Manpower and Education: Country Studies in
    Economic Development}, McGraw-Hill, I965. } But this argument
ignores many of the economic, demographic and political facts of
life. Economic growth, where it is occurring, principally bolsters the
level of living of the already better off, fattens military and
security-police budgets, and supports the markets of more developed
nations. Population is growing so much faster than the rate at which
real educational opportunities can be expanded by means of schools
that deferring the education of the masses merely leaves a more
difficult task for the future. On the other hand, people will not
voluntarily curtail birth rates until they have a minimum not only of
education but also of the social mobility which education
implies.\footnote{ Analysis of demographic data in many developing
  countries shows that the number of children born to women during
  their total child-bearing period declines markedly for women with
  more than four to six years of schooling, especially in urban
  areas. A detailed study of declining birth rates in Japan shows that
  while schooling and urban residence are both related statistically
  to lower birth rates, the most critical factor is a shift in the
  vocational basis of family support from traditional rural to modern
  urban occupations (Irene Taeuber, \emph{Population of Japan},
  Princeton University Press, 1958). } If there were a monopoly of
power in the world, population growth might be curtailed
arbitrarily. In the world as it is, to ignore popular demands for
education is not only morally indefensible, it is politically
impossible, except by military governments. For most people, forcing
others not to have children would be a completely unacceptable policy.

While children who never go to school are most deprived, economically
and politically, they probably suffer the least psychological pain. Andean
Indians, tribal Africans and Asian peasants belong to communities which
have no schools or which have them only for the children of the elite. Their
parents and grandparents have never known schools as places they expected
their children to attend. They do know, however, what schools imply. Going
to school means leaving the traditional life, moving to a different place,
laying aside physical burdens for the work of the tongue and the mind,
exchanging traditional food, clothing and customs for those of the larger
town or distant city. Parents often prefer to keep the child in the traditional
community, bearing the familiar burdens, confined to the enjoyments, which
primitive means can provide. They know, however, that this implies
continuing domination by others, continuing dependence in times of hunger,
war and sickness, increasing distance from those who enjoy wealth, power
and respect. When the choice becomes a real one, most unschooled parents
all over the world decide to send their children to school.

These first attenders have a harder time than their older brothers and
sisters, for whom the school came too late. They do not last long in
school.  In 1960, half the children who entered school in Latin
America never started the second grade, and half the second graders
never started the third.\footnote{From data compiled by the Economic
  Commission for Latin America, Santiago de Chile.} Three- fourths
dropped out before they learned to read. They did learn, however, how
unsuited they were to school, how poor their clothing was, how bad
their manners, how stupid they were in comparison with those who went
on to higher grades. This helped them accept the greater privilege and
power of the deserving minority and their own relative poverty and
political impotence. Yet they were not as ready as their older
brothers and sisters to accept the limitations of their traditional
lot. A little schooling can induce a lot of dissatisfaction. The more
schooling a dropout has, the more it hurts him to drop out. The child
who never learns to read can still accept his inferiority as a fact of
life. The child who goes on to higher grades may learn that he is
really no different from the sons of the mayor, the merchant or the
school teacher, except that they have the money or the influence
needed to go on to secondary school or college, while he must stay
behind for lack of it. For him, it is much harder to accept their
getting the better jobs, enjoying the higher offices, winning the more
beautiful girls, all because they were able to stay in school longer
than he.

A winner's world? If this were all, schools might still be defended;
but the winners of the school game are a strange lot. The Ottomans
used to geld their candidates for managerial posts. Schools make
physical emasculation unnecessary by doing the job more effectively at
the libidinal level. This is, of course, a simplistic metaphor. While
there is evidence that girls do better in school than boys and that
boys do less well the more masculine they are, this undoubtedly
results more from social than from physical factors. The metaphor
understates rather than overstates the facts. School domesticates --
socially emasculates -- both girls and boys by a process much more
pervasive than mere selection by sex. School requires conformity for
survival and thus shapes its students to conform to the norms for
survival.\footnote{Richard Hoggart brilliantly describes the
  psychological effects of schooling on scholarship students recruited
  from England's working class in \emph{The Uses of Literacy:
    Changing Patternsin English Mass Culture}, Chatto \& Windus, 1957;
  Penguin, I958} If learning the official curriculum of the school
were the principal criterion, this might still not be so bad, although
it would substitute the learning of what Whitehead and other
philosophers of education have called dead knowledge for true
learning. The actual survival criteria are much worse. In addition to
the wealth or influence of parents, they include the ability to beat
the game, which, according to John Holt and other perceptive teachers,
is mainly what successful students learn in school.\footnote{ John
  Holt, \emph{How Children Fail}, Pitman, 1964; Penguin, 1969. } Or, as
H. L. Mencken put it, the main thing children learn in school is how
to lie. This can hardly be avoided when survival in school ever more
determines the degree of privilege and power which the student will
enjoy as an adult.

To say that schools teach conformity and also teach students to beat the
game is not contradictory. Beating the game is one form of conformity.
Individual teachers may be concerned with what children learn, but school
systems record only the marks they get. Most students learn to follow the
rules, which schools can enforce and to break those they cannot. But also,
different Students learn in varying degrees to conform, to ignore the rules
and to take advantage of them. Those who ignore them in the extreme
become dropouts, and learn mainly that they do not belong in school or in
the society it represents. Those who conform to the rules become the
dependable producers and consumers of the technological society. Those
who learn to beat the School game become the exploiters of this society.
Those on whom the discipline of the school falls lightly, who easily per-
form its assignments and have little need to violate its rules, are least
touched by school. They are, or become, the social aristocrats and the rebels.
This, at any rate, is what happened before schools began to break down.
Now all kinds of students join in the rush for the door, while schools engage
in a similar scramble to recapture their dropouts by any means.
As late as the turn of the century schools were still a minor institution and
all who were not suited for them had other educational options. Fifty years
ago, no country in the world had 10 per cent of its teenage population in
school. Schools have grown so fast partly because they happened to be doing
what was important to a technological era when this era began. Their
monopoly of education has been achieved as one aspect of the monopoly of
technology. The main reason we need alternatives to schools is because they
close the door to humanity's escape from this monopoly. They ensure that
those who inherit influence in a world dominated by technology will be
those who profit by this domination and, even worse, those who have been
rendered incapable of questioning it. Not only the leaders but also their
followers are shaped by the school game to play the game of competitive
consumption -- first to meet and then to surpass the standards of others.
Whether the rules are fair or the game worth playing is beside the point.
School has become the universal church of a technological society,
incorporating and transmitting its ideology, shaping men's minds to accept
this ideology, and conferring social status in proportion to its acceptance.
There is no question of man's rejecting technology. The question is only one
of adaptation, direction and control. There may not be much time, and the
only hope would seem to lie in education -- the true education of free men
capable of mastering technology rather than being enslaved by it, or by
others in its name.

There are many roads to enslavement, only a few to mastery and freedom.
Technology can kill by poisoning the environment, by modern warfare, by
over-population. It can enslave by chaining men to endless cycles of
competitive consumption, by means of police states, by creating dependence
on modes of production, which are not viable in the long run. There are no
certain roads of escape from these dangers. There can be no mode of escape
at all, however, if men remain enthralled in a monolithic secular orthodoxy.
The first amendment to the Constitution of the United States was a landmark
in history. `There shall be no establishment of religion.' Only the terms and
the scope of the problem have changed. Our major threat today is a
worldwide monopoly in the domination of men's minds.

\theendnotes
\setcounter{endnote}{0}