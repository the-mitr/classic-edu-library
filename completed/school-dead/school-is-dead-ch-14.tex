% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage
\thispagestyle{empty}
\begin{chapquot}
He that will not apply new remedies must expect new evils.
\begin{flushright}
\emph{Roger Bacon}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

%\newpage

\chapter{Strategy For A Peaceful Revolution}


Whether a peaceful revolution is possible only time will tell; it may be in
some countries and not in others. The necessary conditions for it, however,
are easy to state. It cannot occur unless most people are persuaded that it
should, nor unless the verdict of this majority is accepted. However, it is
unlikely first, that a majority could be persuaded of the need for
revolutionary change while control of the mass media and the powers of the
state remained committed to the status quo, and second, that a minority
holding these powers would yield them peacefully even if convinced of its
minority status.

An educated minority of substantial size could, under certain conditions,
create a majority in favor of revolutionary change. This could happen only if
there were deep disillusionment with existing institutions and a compelling
formulation of alternatives. The following attempt to show how these
conditions might be brought about in the area of education is obviously no
blueprint for revolution. It is merely a sketch of a possible series of
preliminary steps.

The first step is already implied. Large numbers of people would have to
become disillusioned with schools. This is currently happening, but the
numbers are not nearly large enough, nor is the disillusion deep enough. The
basic contradictions of the school system must become publicly apparent:
that schools are too expensive to serve as a universal system of education,
that schools perpetuate inequality, that schools inoculate the vast majority
against education by forcing unwanted learning upon them, that a schooled
society is blinded to its own errors. The contradictions are there. To get them
generally recognized is difficult, since the people most looked up to are the
most schooled. It is always difficult to recognize what has been done to you,
when the recognition is itself degrading. Unless this recognition occurs,
however, nothing else can.

The programme described below assumes that deep and widespread
disillusionment with schools will occur. If it does, the following proposals
for legal, fiscal, institutional and educational programmes will become
progressively more feasible and these, if carried out, will themselves help to
speed the loss of faith in schools. People hang on to what they recognize as
very faulty institutions until they see what else they can do. While they hang
on, it is difficult for them not to rationalize their actions by hoping that
things are not as bad as they seem.

On the legal front, a two-part strategy is needed: the first consisting of
action under existing laws, the second of proposals for new legislation.
Some kinds of legal action already have a long history, especially the
objections to compulsory schooling traditionally associated with dissident
religious sects. More recently, such defense has been based upon non-
religious grounds, including the claim that children are not receiving the
education, which schools purport to give. Suits claiming an equal share of
public resources earmarked for education have also been filed, based on the
promise of equal protection of the laws contained in the fourteenth
amendment. Further legal action possible under existing law may be
suggested by the following legislative programme.

We need legislation which would parallel the first amendment to the
Constitution of the United States, prohibiting any law with respect to an
`establishment of religion'. Institutional monopoly of education, especially
by the state, has all of the evils of a state church, compounded by the fact
that a secular school system can be made to seem neutral with respect to
basic values. Since such a claim is obvious nonsense, the defense of a
national school system falls back upon the overriding needs and prerogatives
of the state. But this involves a contradiction with democratic theory, which
holds the state to be the instrument and not the molder of its citizens. The
school, in modern times, has become more powerful than the church of the
middle ages. The career and, therefore, the life of the individual depend
upon his success in school. The law makes him a criminal if he does not
attend it. He is subject to its influence far more than medieval man was ever
subject to the church. The case for a prohibition of educational monopoly is
stronger than the case against a state church, which in times of crisis could
oppose the state and claim heavenly authority for its position. The claim for
academic freedom is the nearest schools can come to a similar role and we
have recently seen how relatively feeble it is. Churches, on the wane, did
infinitely better against the Nazis and Fascists than universities, in the
fullness of their power. The school is completely an instrument of the state
and creates subservience to it.

Along with prohibition of an established school we need to extend our
anti-discrimination laws to include schooling. We must forbid favoritism
based on schooling as well as on race or religion, and for the same reasons.
Where and how one has been schooled is as irrelevant to one's capacity to do
a job as race or religion. All affect aspects of job performance which are of
interest to the employer but which the law has decided, in the cases of race
and religion, are not his legitimate concern. Neither is the school the job
applicant went to, nor whether he went to school at all, if he can demonstrate
the ability to do the job. We are so used to schools that this statement
appears strange, yet the logic is simple. We now reserve the best-paid jobs
for those whose training has cost the most. If schooling were privately
financed this might make limited ethical sense, but its economics would still
be ridiculous. The public has, indeed, been schooled to believe that a more
expensive item must be better, but economists explain this on the
assumption of price competition among suppliers. Schools have precisely
the opposite kind of competition. Even Harvard would be suspect if it were
cheap.

We would have to distribute educational resources in an inverse ratio to
present privilege in order to equalize educational opportunity. The argument
against such a policy is that it would spend the most money on those with
the least aptitude and would produce the least total education. This argument
can be challenged, since aptitude judgments are based on success in a school
system, which discriminates against the poor. But in the end such arguments
will not decide a political issue. Most people believe that public resources
are equally shared and if not that they ought to be. A law requiring equal
sharing of public educational resources is, thus, a third item in a legislative
programme. A system of individual educational accounts would be the only
feasible way co enforce such a law.

These three laws would effectively disestablish the school system as an
educational monopoly. They would not prevent the development of a new
one. By creating an educational market they would open the way to already
powerful economic institutions, which might easily take advantage of their
power to establish a new educational monopoly.

Effective extension of anti-monopoly laws to the field of education would,
therefore, be a necessary fourth legal step. Since such laws are now
relatively ineffective in many other fields, this step would be far from
routine. The weakness of and monopoly legislation is no accident; its
enforcement would threaten the present system. This illustrates the more
general principle that revolutions are not made by passing laws. Litigation
and legislation can play only a part in a revolutionary strategy, and even that
only if properly supported by economic, institutional and educational
programmes.

If the best way to establish an institution is to finance it, then one part of a
strategy to replace schools is obvious. The obvious is not, however, always
easy. We are, for one thing, talking about a great deal of money. The
combined public-school budgets of the world total almost as much as is
spent on armaments and soldiers. If student time were given its market value
it would come to much more. This much money will not be liberated from
its present channels unless very attractive alternatives can be found.
Fortunately, not only educationally but politically attractive alternatives
exist.

Public funds for education might be redirected to students, teachers,
taxpayers or businessmen.

Educational accounts would channel funds, which now go directly to
schools through the hands of students. Students might still spend them on
schools, either because the law gave them no option or because the schools
succeeded in hoodwinking them or because the schools, under the impact of
necessity, succeeded in delivering the goods. In all probability, schools
would get a steadily declining share of the educational dollar if it went
directly to students.

Students would probably elect to spend some of their dollars directly on
teachers, bypassing the schools, but there are also other ways in which
teachers could profit at the expense of schools. Any significant weakening in
the regulatory power of school would tend to have this effect; their power to
compel attendance, for example, or to certify compliance with curriculum
requirements. If students were to receive credit by examination without
attending classes, teachers would be in increased demand as tutors. For them
to increase their earnings would, of course, require a transfer of the savings
resulting from reduced school attendance to a tutorial account. Most school
laws would not have to be changed to make this possible.

One way of transferring funds from schools to taxpayers is merely to
reduce the resources earmarked for education. Another way, however, is to
shift educational emphasis from children to adults. Since there are more
taxpayers than parents, education distributed over the adult population
would tend to match educational benefits with tax liability. The taxpayer, of
course, would get back a dollar earmarked for education in place of one
which had no string attached, but this might have good educational by-
products. The taxpayer would insist upon maximum control of his dollar and
would spend it on education valued by him rather than dictated by another.

There are several ways to shift the educational dollar from schools to
businessmen. Educational accounts are one. Performance contracts are
another. Such contracts have recently been written by a number of schools
with entrepreneurs who guarantee to teach a testable skill and who are not
paid unless they do. Still another way to benefit business is to shift the
burden of teaching from people to reproducible objects, be these books or
computers.

There is an understandable reluctance on the part of educational liberals to
make common cause with taxpayers and businessmen. Some academicians
claim, for example, that mass media, especially television, already have
more educational influence than schools and that weakening the schools
would merely strengthen the hold of business interests on the minds of men,
women and children. Galbraith even argues that the academic community is
one of our main hopes of escaping the worst implications of the new
industrial state. The evidence does not support him. There is no major
issue -- war, pollution, and exploitation, racism -- on which the academic
community, as such, has a discernible stand. There are intelligent
courageous, in schools and universities, as there are elsewhere, but they
receive no effective support from their institutions. In the exceptional case
when an institution has shielded an unpopular dissident, it has also muffled
his voice. The worst causes, on the other hand, have had no difficulty in
recruiting academics to their support while institutions of learning have
entered into contracts with all kinds of other institutions for all kinds of
purposes.

But this does not meet the argument that jumping out of the school pan
might land us in the television fire. If one believes in people and in freedom,
this would still be a step forward. No one is compelled to watch television
and everyone knows who is talking and why. People learn if they are given a
chance, although not always what we want them to learn. God had to gamble
on people and has frequently lost. Without this gamble, however, there
would be no humanity.

If educational liberals are unwilling to cooperate with tax-payers and
businessmen, they must submit to an ever-waxing educational bureaucracy
whose efficiency is steadily declining. If, on the other hand, real education
were to result from this proposed unholy alliance, this would in itself reduce
the danger of economic domination. Responsibility is best divided between
private and public interests if policy is made by those who do not been
written by a number of schools with entrepreneurs who do. Taxpayers,
businessmen and educational liberals can be satisfactory allies if none are
permitted to write the rules under which all will operate.

It would be a mistake, however, to conclude that a competitive market in
educational resources would necessarily result in good education. If
everyone were already educated it might, but this is to assume the end we
are seeking. Starting from where we are, certain educational institutions
would have to be subsidized.

One of the most important tasks would be to induce parents and employers
to reassume their proper educational responsibilities. Every thinking person
knows that real education occurs primarily at home and at work, but a
number of facts have conspired to rob this truth of its former general
acceptance. The modern organization of society in offering free schooling
rewards both parents and employers, in the short run, for reducing their
normal educational roles. Schools benefit politically potent parents, while
business gains from any reduction in production costs. Similarly, the
competitive consumption in which modern households engage induces them
to save on costs for which there is nothing to display. Except perhaps in
certain kinds of academic households, bright children are harder to show off
than shiny automobiles.

This kind of dominantly economic competition among producers and
households is itself the product of a particular kind of legal structure. Until
this is changed, some kind of special inducement, some kind of subsidy, will
be needed to put educational processes back where they occur most
rationally and economically, in the home and on the job.

Another place where a great deal of effective education used to occur was
in the practice of the arts. Before modern technology took over, all
production involved the practice of art. What we now call the fine arts were
integrated with the practice of other crafts, and people learned not only by
working with more experienced masters but also from associates practicing
related arts. In the early stages of industrialization, now rapidly passing, this
kind of learning was disrupted. It could now be reestablished, since the
demand for industrial labor is rapidly declining, but only if people who
learn, teach and practice the various arts are given a dependable claim on the
goods and services produced by modern technology. Unrestricted
competition between men and machines is not a natural phenomenon, but
one, which has been deliberately engineered in modern societies. Most
countries of the world are frustrated in their efforts to bring this competition
about. Modern countries may find it no less difficult to undo, but they must
undo it if the arts are to reclaim an educational role which fan in no other
way be performed. There is no comparable way of teaching everyone the
essential skills of hand and foot, ear and eye, mind and tongue.

In addition to the revival of traditional educational institutions, the modern
world requires new ones, both to make available to learners the objects they
require and also to make available the human assistance they need. Some of
these institutions have been described as networks, or directories of
educational objects, skill models, learning peers or professional educators. A
public utility designed to match educational resources with educational
needs could be self-supporting, once established. It might even be
established with minimum public investment, but some experimental
operation and testing would probably be required for it to reach its full
potential.

Schools cannot be disestablished and education made freely available to all
while the rest of the world remains unchanged. Competition among nations,
classes and individuals will have to be replaced by cooperation. This means
placing limits upon what any individual or group can consume, produce or
do to others; limits which are being widely transgressed today by many
individuals and groups. Acceptance of such limits implies an increased self-
insight into what the real interests and possibilities of groups and individuals
are. More than that, it implies a sacrifice of short-term interests for those,
which are more enduring. This is not the kind of behavior for which the
human race is noted. It remains quite probable, therefore, that it will not be
learned except under the impact of catastrophe -- if then. There is no need,
however, to plan for catastrophe. There is every need to plan for its
avoidance.

The final and critical component of a strategy for peaceful educational
revolution is education itself. People have been schooled to regard an
unschooled world as fantasy. Alternatives, which lack the familiar
characteristics of schools, have an unreal aspect to a schooled mind. From
what kind of fulcrum, with what kind of lever, can the required change in
perspective be achieved? One answer is the school system itself, and efforts
to change it. Today's disillusioned students, teachers, taxpayers and
administrators include those who have tried and failed to change the system.
The architects of educational revolution will be recruited from these ranks.

Paulo Freire provides the educational means by which the revolutionary
rank and file can be assembled. Poor people, black and white, peasant and
urban proletariat, can learn to see what schools so obviously do to their
children. Taxpayers can learn to interpret the meaning of trends in the costs
of schooling. Even the privileged, oppressed by the stink of pollution and
their growing fear of the poor, may come to wonder why they did not learn
about these things in school. If they do not, their children are likely to tell
them. These children of the privileged may turn out to be the teachers of the
educational revolution. Not all of them, for many seek inconsistent goals.
Oppressed by the hypocrisy around them, they seek the truth. But they also
seek the warm social womb, which a technological world denies them. Some
will find one, some the other, perhaps most neither. Since the total number
of dissatisfied students can only increase, enough may eventually find the
truth.

Today's educational tasks do not, in the main, require genius but they may
require heroes. Some room for talent, if not genius, remains. To bring the
truths of modern science, economics, politics and psychology to the masses
without simplistic distortion will take some doing. Nevertheless, the sincere
desire to do this remains a scarcer commodity than the ability. There are
good reasons why desire, great enough to lead to action, should not be in
plentiful supply. Those who seriously undertake to bring truth to those who
because of their large numbers are most capable of bringing fundamental
change -- such people run considerable risk. The Gandhi's and Martin Luther
King's do not die in bed. Che Guevara took weapons into the jungle with the
intent to arm followers who could protect themselves and him. He was,
beyond doubt, a hero and a martyr. Those who go armed only with the truth
will be still more dangerous and also more vulnerable. No one, while he
himself remains safe, can in conscience call for heroes. But the heroes have
always appeared and -- when the time is ripe -- will again.






%\theendnotes
%\setcounter{endnote}{0}