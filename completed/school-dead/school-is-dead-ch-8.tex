% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage
\thispagestyle{empty}
\begin{chapquot}
Men had better be without education than be educated by their rulers; for
this education is but the mere breaking in of the steer to the yoke; the mere
discipline of the hunting dog, which by dint of severity is made to forego the
strongest impulse of his nature, and instead of devouring his prey, to hasten
with it to the feet of his master.
\begin{flushright}
\emph{Thomas Hodgkins:\\ 1823}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\chapter{Education For Freedom}

Alternatives to schools must be more economical than schools: cheap
enough so that everyone can share in them. They must also be more
effective so that lower costs do not imply less education. Monopoly must be
avoided. The school system must not be replaced by another dominant
system: alternatives must be plural. There should be competition between
alternatives, but some of them, at least, should not involve competition
between students, especially for lifetime prizes. One student should not learn
at the expense of another, nor should success for one student imply failure
for another. Alternatives to schools should not manipulate individuals but,
on the contrary, should prepare individuals to direct and re-create
institutions, including their governments. Education should not be separated
from work and the rest of life, but integrated with them. Educational
environments should be protective only to an unavoidable degree. Education
should not, primarily, prepare for something else nor be a by-product of
something else. It should be a self-justified activity designed to help man
gain and maintain control of himself, his society and his environment.

Alternatives to schools must, above all, allow everyone the opportunity to
learn what he needs to know in order to act intelligently in his own interests.
No educational programme can guarantee that everyone will learn what he
needs to know, much less that if he did he would act upon it. It should be
possible, however, to keep open the opportunity for such learning, not only
during the youth but also throughout the lifetime of every man. One of the
major complications arises from the almost universal predilection to feel that
we know better than others what is in their interests. Schools, for example,
are almost wholly concerned with vying to teach some people what other
people want them to know. Those who make the decisions about what
should be taught in school pretend to know and to act in the interest of the
learners. It should be clear by now that either they do not know or that, if
they do, they act in contradiction to their knowledge.

What man's true interests are and what he needs to know to pursue them
are the starting points not only of educational philosophy but also of
any general philosophical basis for social policy. Defining freedom,
in its clearest if not its most complete sense, as freedom
\emph{from} rather than freedom \emph{for}, leads to a definition
of basic values and factual propositions in largely negative
terms. The question becomes, as Paul Goodman says, not what shall we
do but what will we tolerate.\footnote{An oral statement made by Paul
  Goodman in the course of a seminar held by the International
  Association for Cultural Freedom at Aspen, Colorado, 29 August to 3
  September 1970.} Philosophies which state in positive terms what is
and what should be seen to lead to the constraint of one human being
by another, to the imposition of enlightenment upon the heathen. A
philosophy based on the right of maximum freedom from human constraint
begins by denying the right of any man to impose either truth or
virtue upon another.

The implications of such a philosophy of freedom are very far-reaching.
They include, for example, denial of the right to monopolize anything which
other men need, since such monopoly is and always has been used to violate
their freedom. Needs, moreover, cannot be restrictively defined as those
things immediately needed to sustain life. Denial of information, for
example, leads to denial of fresh air, pure water and nutritious food.
Information denial is being used in the modern world -- generally,
consistently and systematically -- to keep people from knowing, and thereby
from getting what they need, of even such elementary things as air, water
and food.

A philosophy of freedom need not specify in advance which positive
things must be done in order to avoid the constraint of one man by another.
It can reserve the application of the test of freedom to specific situations.
The world of schools conceives the problem of education as one of
inducing students to learn what they are supposed to know. From this point
of view, it seems nonsensical to think of people as being blocked from
knowing and learning. Yet clearly they are. Most of the people of the world
sweat their lives out on land which belongs to others, constantly in debt to
their landlords with no control over the prices of what they buy or sell,
helpless in their misery and kept helpless not only by denial of information
and opportunity to learn but by deliberate distortion of the facts of their
lives. Witch doctors, priests, politicians and purveyors of profitable
nostrums vie with each other to keep these people sunk in their ignorance
and unaware of their true condition. They are aided in these attempts by the
physical misery in which their victims are forced to live, misery so great and
so hopeless that it must somehow be justified, somehow disguised, in order
to make it bearable.

Education for these people does not consist primarily in learning to read,
but in learning to understand and to do something about their miserable
situation. This may involve learning to read but it must obviously include
other things, without which the ability to read would not be of any real
value. Suppose that a few children in such an environment do learn to read
and thus escape. This does nothing to help those who stay behind to breed
more children.

Much of what people in this situation need to know, in order to
basically improve their lot, is actively withheld or hidden from them,
even though the resources for learning to read may be offered. Paulo
Freire found in working with Brazilian peasants that they immediately
learned to read those words, which helped them to discover their
true-life situation.\footnote{Paulo Freire, Cultural Action: A
  Dialectic Analysis, CIDOC document no. 1004, 1970; `The adult
  literacy process as cultural action for freedom', \emph{Harvard
  Educational Review}, vol. 40, no. 2, 1970, pp. 205-25; and `Cultural
  action and conscientization', \emph{Harvard Educational
  Review},vol. 40. no. 3, 1970, pp. 452-78.} Unearthing this
vocabulary requires an insight into the lives of these peasants, which
penetrates the secrets, misinformation and mystification with which
their landlords, priests and political leaders surround than. Paulo
Freire's clients no sooner learnt to read than they organized peasant
leagues through which they tried to bargain with their
employers. Although they were scrupulously careful to observe the law
and the customs of the region, their employers, government authorities
and the Church turned upon them in unison. Their leaders were fired
and jailed, while the Church denied its sacraments to members of the
league until Protestant missionaries began to make converts among
them.  

This sounds like an exaggerated case, but Paulo Freire's Brazilian clients
are representative, in their essential features, of over half the people of the
earth. Freire himself had to flee Brazil when the present military government
took over. Although invited to Chile by the governing Christian Democratic
Party of that country, he was not allowed to work freely. After a year at
Harvard he is now with the World Council of Churches in Geneva, safely
and unhappily insulated from the poor and uneducated masses of the world.

This is easy to understand, given the nature of society and the probable
consequences of educating its impoverished majority. Withholding
education from these masses may still appear to be a special case, justified
by the inability of society to cope with the impact of education on such a
massive scale. But this is not a special case. We treat our own children in the
same way and we ourselves are so treated by our governments, public
bureaucracies and privately owned corporations.

We take as great pains to hide the facts of life from our children, as do the
Brazilian landlords to hide them from their peasants. Like them, we not only
hide and distort facts but also invoke the assistance of great institutions and
elaborate mythologies. Facts of life are not confined to sex: about this we are
becoming less inhibited even with children. But relative incomes of families
on the block, the neighborhood power structure, why father didn't get his
promotion or mother her chairmanship, why Jimmy is smoking pot or Susie
is having an out of town vacation: these things which children perversely
want to know are obviously not for them. Neither is ballet at the expense of
reading, karate in place of math's, or the anatomy of flies as a substitute for
botany out of a book. Schools are obviously as much designed to keep
children from learning what really intrigues them as to teach them what they
ought to know. As a result, children learn to read and do not read, learn their
numbers and hate mathematics, shut themselves off in class-rooms and do
their learning in cloakrooms, hangouts and on the road. The exceptions are
those who are allowed to learn to read when and what they wish, and to do
likewise with mathematics, science, music, etc.

We ourselves are treated no better than our children. Attempts to get the
contents of cans and boxes clearly labeled are treated as attacks on private
enterprise. Any real probing of foreign policy is labeled subversive, while
the basic facts are hidden behind the screen of national security. The
enemies of every military power in the world know more about its military
capacities and intentions than is available to the makers of its own laws.
Spies, bellhops, charwomen, valets and ladies' maids are the privileged
exceptions to the conspiracy of secrecy in which we live.


There are, of course, the best of reasons for all the secrecy and
mystification. Children might be permanently scarred by premature exposure
to death, suffering, sex or the sordid facts of economic and political life.
Mechanics might be embarrassed if car owners were given detailed
descriptions of their own motorcars. A little knowledge is dangerous, not
only for laymen, but for their donors, lawyers and accountants as well. If
customers were allowed to know what they were buying, then competitors
could also find it out. If national security barriers were lowered, the enemy
would learn even more at lower cost. These reasons range from valid to
ludicrous but even the valid ones are valid only in the context of the society
in which we live. In this society, Paulo Freire is a threat and so is the free
education of our children and of ourselves. At present, this education is not
free. People are systematically prevented from learning those things, which
are most important for them to know. They are deliberately misled by
elaborate distortions of facts and by the propagation of religious, political
and economic mythologies which make it extraordinarily difficult to get a
clear glimpse of relevant truth. Protestants see clearly through the Catholic
smoke screen. Capitalists have no difficulty in seeing how the communists
brainwash their victims. Englishmen and Frenchmen see through each
other's Common Market machinations without the slightest trouble. Only our
own camouflage confuses us.

It might be said in rebuttal that every society must have its secrets,
its mythologies, and its propaganda. Some information will always be
off limits for children and for adults. We have a choice of what will
be hidden and obscured, but not of whether anything will be. This may
be true, but it is not true by definition. Only where there are major
differences of interim and where one group has the power to distort
reality does it seem inevitable that this will occur. All societies in
recorded history have been of this kind. All have had ruling classes,
which exploited other members of their own populations and fought with
other elites for the privilege of exploitation.  Societies need not be
organized in this way. In fact most current societies claim that they
are not. The ideal of a non-exploiting, mutual-interest society has
long been proclaimed and many people believe that such a society is
possible. If it is, then there should be no need for major systems of
secrecy not for mythologies whose major purpose is to distort the
truth. In such a society, education for freedom should be possible. It
may also be that such a society can be created only by men who have
been educated for freedom, by whatever means. This possibility and
hope justifies the proclamation of the ideal of education for freedom,
even in a society in which it does not truly fit. The hope stems from
the fact that our society does proclaim the ideals of justice and
freedom. In the name of these ideals it is possible to call for free
education, realizing that it can be fully achieved only as greater
social justice is also achieved, and that each can and must be
instrumental in the achievement of the other.

Specific barriers to information are not as important as mythologies and
the institutions through which these are propagated. In the case of tribal
societies, these mythologies take the form of what we generally and
somewhat inaccurately label witchcraft. Various systems of witchcraft
contain much valid knowledge and significant powers to cure the sick, solve
social problems and give meaning to the lives of people. They also,
however, serve to hide the power structures of their respective societies,
disguise the dominant means of exploitation, and make people feel that the
evils they suffer are inevitable and, sometimes, even for their own good.

The major traditional religions, with all of their differences from
witchcraft and from each other, serve the same social purposes both
positive and negative. Catholicism as it affects the peasant
populations of Latin America illustrates the point very aptly because,
in many regions, it is Catholicism in name only. Actually it is often
merely an overlay of popular saint culture upon a base of indigenous
witchcraft, the content of the Catholic and witchcraft elements of the
amalgams varying from one region to another.\footnote{See for
  example Jacques Monast, \emph{L'Univers religieux des Aymaras de Bolivie},
  CIDOC document no. 10, 1966. } The social functions performed by
these amalgams are the same, however, with no discernible differences
in efficacy. They all effect cures, control anti-social impulses, or
re-establish order when the controls fail, and provide a structure of
meaning for celebration of birth, death, marriage and other critical
events in the lives of individuals. Most important of all, from a
social point of view, they legitimize ownership of the land by the
rich, condone and justify the privileges enjoyed by the elites at the
expense of the poor, exalt the charitable acts of the elite and their
symbolic roles in religious, political, economic and familial
affairs. They provide the peasants with a set of after-life rewards to
console them for their misery in the present life, represent their
current suffering as the will of God and acquiescence in these
sufferings as the height of virtue.

There are, of course, many Catholic priests in Latin America whose
behavior contradicts every line of the above description, who castigate the
rich and assist, arouse and lead the peasants in their search for justice. These
priests are sometimes killed, more often locked up as mad, and still more
often assigned to innocuous duties or dismissed. Occasionally their work is
welcomed and supported by bishops who agree with them.

When Latin American peasants move to the cities and become urban
workers they are converted in large numbers to a variety of fundamentalist
Protestant sects. Even Catholic employers frequently prefer these converts as
workers and hire them selectively. They are likely to be more sober, faithful
to their wives and families, earnest about their work, about keeping their
children in school, acquiring possessions and getting ahead in the world.
Their behavior supports perfectly Weber's hypothesis on the relationship
between Protestantism and industrialization. The personally permissive
nominal Catholicism -- so perfectly adjusted to rural Latin America and so
sharply in contrast with its counterparts in rural Ireland and northern Spain -
is poorly suited to Sao Paulo, Buenos Aires, Mexico City and the lesser
industrial centers of Latin America. So is the more sophisticated urban
Catholicism of Latin American cities with its emphasis on the rights of
labor, the obligations of the employer and, in general, on social justice in the
modern industrial world. The teachings of some of the older Protestant seers
are no more in the interests of urban employees and these seers make fewer
converts among the workers.

It may seem odd that workers join the seers whose teachings are in their
employers' interest rather than their own. This is, first of all, because
employers select as workers the members of these sects. It is, secondly,
because relatively powerless workers psychologically need a religion which
will reconcile the contradictions between their employers' interests and their
own.

In Europe, the United States, Japan and the former British dominions,
religion no longer plays the major role in adjusting men to their societies.
This role falls to mythologies of science, professionalism, consumption, the
state, the corporation, and the welfare agency and, above all, the school. The
mass media and the advertising industry are, of course, important but they
are the carriers and arrangers of the various myths and do not provide their
substance.

Progress toward freedom occurs in cycles. At some point in the cycle, part
of the myth is discredited. Peasants move to the city, a scandal occurs, some
defector lifts the veil, a penetrating analysis is made. People glimpse the
truth, make demands, win concessions, are a little better off and not so much
in need of hiding reality from themselves. The stage is set for a new lifting
of the veil and a new cycle. Men do not arrive suddenly at total truth nor at
total justice. Myth is replaced by myth, institution-by-institution, injustice-
by-injustice, but when things go well, each myth is less embracing, each
institution more transient, and each injustice more bearable and less in need
of being mythologized.

This does not mean that change must come gradually or slowly. When
major myths and institutions are replaced change may be swift and
radical -- radical enough to be called revolution, regardless of the
degree of violence involved. Violence is not the proper mark of
revolution, nor are dramatic changes in the political facade. Too
often these occur accompanied by no significant changes at all in
myths or institutions, or in relationships between oppressors and
oppressed. Significant change occurs when people stop believing in
what may once have been true, but has now become false; when they
withdraw support from institutions which may once have served them but
no longer do; when they refuse to submit to what may once have been
fair terms but which are no longer. Such changes, when they occur, are
a product of true education. The problem is not one of motivating
people to learn what others want them to learn. It is rather to
provide the resources, which enable them to, learn what they want and
need to know.


\newpage

\theendnotes 
\setcounter{endnote}{0}
