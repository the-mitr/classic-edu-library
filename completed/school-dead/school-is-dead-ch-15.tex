% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\newpage

%\NewPage
\thispagestyle{empty}
\begin{chapquot}
One makes the revolution because it is the best way to live.
\begin{flushright}
\emph{Danny the Red}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\newpage

\chapter{What Each Of Us Can Do}

Most of us are not cut out to be heroes, but heroes will be of no avail
unless we support them. What we can all do, and must do if there is ever to
be a just world, is to begin to live as we would have to live in such a world.
If this sounds as if it has been said before -- it has. Every great religious
leader has said it in one form or another. This makes it neither untrue nor
irrelevant but is, on the other hand, the best evidence of both its relevance
and truth. If it promises no new magic, guarantees no ultimate victory, but
claims only to put humanity back on the main path out of the past, this also
is evidence of its truth and relevance.

Power and security, whether based on magic, religion or science, have
always been false beacons leading to the repeated shattering of human
hopes. They are false because the attainment of either ultimate power or
security would be the final end of everything worthwhile in human life.
People have repeatedly pursued these goals -- the prophets of Israel called it
idolatry -- but people have also, again and again, recognized their idols and
turned away from them.

Our modern idols are science and technology and their temples are the
institutions, which propagate their worship and profit from the proceeds. The
promise of this modern cult -- every man a king, with his private palace and
his royal chariot -- has captured the fancy of most of mankind. Suppose,
even, that it were realized. What would there be to do? Only an endless
exchange of royal visits, comparing the quantity and quality of royal johns,
and an endless drag race of royal chariots, starting from countless
crossroads. Eliminate the crossroads, with over and under-passes, and the
whole point of the game would be lost. It would be like checkers with the
red and the black playing on separate boards.

But this dream of universal royalty is a pipe dream -- only a few can win.
Does that make the game worth playing? Not for those who lose. And for
those who win? Already they are establishing private arsenals in their
suburban redoubts and servicing their Cadillac's with bottled air while they
drive through valleys of smog which grow longer and wider and deeper by
the day.

So what can people do? They can begin to live as they would have to live
in a sane, just world. They can begin again to guard their health, avoiding
poisonous air, water and food and the over-eating and under-exercise which
more than defeat the miracles of modern medicine. There are other millions,
of course, who under-eat and over-exercise because they are forced to work
so hard in order to eat at all. What can they do? They can do different things
depending on whether they have the cooperation of the affluent or not. In
either case it comes to a matter of sharing -- but sharing can be mutual or
unilateral.

People can either increase or decrease their consumption and production,
depending on whether they are now doing more or less than their share of
the world's work and using more or less than their share of the world's
goods. They can share their possessions or their needs. They can conserve,
among other things, the natural environment they live in.

Lowering consumption, sharing and conserving are three actions most of
us can take and yet, jointly, they constitute a powerful revolutionary
programme. Consider the consequences if this programme were consistently
practiced in Europe and North America only by those who believe in its
principles.

There would be an immediate sharp drop in demand, especially for the
more elaborate goods and services. Ever since the Second World War,
production in the North Atlantic community has been directed more and
more to the sale of luxury goods, including food, clothing, shelter and
transport for those already fed, clothed, sheltered and transported much
better than most. No small part of the problem of the American economy
today is the failure of young people from the upper middle class to establish
themselves in the style their parents would gladly support. If only those who
share the basic values and beliefs of these young people would follow their
example, the market for automobiles, housing and appliances would
collapse. The resulting unemployment would involuntarily curtail the
consumption of other people, who would then come to see on what a false
foundation their pseudo-prosperity had been built. For these people, sharing
and conservation would at first be forced consequences of involuntary
unemployment but once -- or once more -- experienced they would begin to
recommend themselves on their own merits.

Sharing and conservation, applied especially to goods, which are
supposedly durable, would vastly multiply the economic effect of a mere
curtailment of excess consumption. Conserved and shared, there is enough
clothing, shelter and transport in the North Atlantic community to last for a
decade. Much of it would need repair and improvement, but this could be
supplied without generating much demand even for new raw materials.

Exchange of services, rather than their purchase, whether voluntary or
forced, would further deflate the market for goods, services, raw materials
and land. The gross national product of the United States could be cut in half
without any reduction of goods and services, which actually benefit anyone.
What would happen to the foreign commitments of the United States under
these circumstances is another matter, but most of the people of the world
would be better off if these commitments were curtailed. What now looks
like a scarcity of real resources in much of the world would turn out to be an
ample supply, with only the problem of rational use and distribution
remaining to be solved, once faced directly, stripped of its deliberately
obscuring facade, this problem too might be rationally attacked.

This last statement makes more sense if we remember the political
psychology of the great depression. During the 1930's, masses of people
became aware of the contradictions involved in some people owning and
controlling what they could not use and other people needed. Only the
Fascist movements saved Europe from the consequences of this insight and
only the war saved America. Would Fascism occur again if conspicuous
consumption broke down for a second time? The fear and even the covert
threat of this prospect keeps many otherwise constructive people
immobilized. But only those who do not learn from history are fated to
repeat it. Learning does not mean that because one opportunity is missed all
others must be foregone.

This is not intended to minimize the dangers, which a breakdown of the
present system, absurd as it has become, would necessarily entail. These
dangers are great and any way of minimizing them should certainly be
grasped. They cannot be avoided, however, by pretending that they result
merely from calling attention to the need for basic change or by pretending
that the only change needed is to improve the operation of our current
institutions. These institutions are basically unsound. They are designed not
to meet people's needs but to keep people under control. It would have been
safer to change them in the 1930s before we had the atom bomb. It is safer to
change them now than to wait until still more esoteric weapons are invented.
The risks of change are great but the risks of delay are greater.

What, it may be asked, is to prevent the defenders of the status quo from
again using totalitarian nationalist parties, international provocation and war
as the means of preserving or reestablishing the present order? Certainly
nothing will prevent them from trying. Only enough people deeply
committed to a world of sanity and justice, cooperating wisely in their
efforts to achieve such a world, would have any chance against those who
will assuredly use all means to perpetuate their power and their privilege.
But the power of \'elites declines rapidly when they are forced to rely on
violence. They lose the support of even those of their fellows who enjoy
their privilege but are unwilling to purchase it once they become aware of its
true price. There is no doubt that those who fight for justice, even passively,
will pay a price in heightened suffering if privilege is challenged. But the
history of Europe during the last Great War provides many examples of how
people can protect each other even in the face of overwhelming power
ruthlessly applied.

No one can know in advance how, in his own case, the battle will be
joined. In some places, mere withdrawal from excessive consumption and
peaceful sharing and conservation in cooperation with likeminded people
will produce a majority capable of taking power by legal political means. In
other places, civil disobedience as practiced by Gandhi and Nehru in India
and by Martin Luther King and his followers in the United States may be
necessary. In still other places, the guerilla tactics, which characterized the
resistance in Nazi-dominated Europe from 1940 to 1945, may be forced
upon people who refuse to continue to support injustice. Strategy and tactics
at the three levels of democratic politics, passive resistance and guerilla
warfare have some important things in common. They all depend upon
cooperation rather than coercion. They depend upon local as opposed to
central leadership. They depend upon a relatively equal sharing of burdens,
responsibilities and rewards. But these are also characteristics of a sane just
world. This is how a worthwhile, viable world will have to be run. People
will, therefore, learn how to run it in the course of bringing it about.

There is probably no other way of creating a society, which is of, by and
for the people than through personal cooperation in which each person first
orders his own life, in the light of shared principles, and then cooperates
with others who share these principles. This is how all communities which
have enjoyed a reasonable period of sanity and justice have been established
- the early United States, Costs Rica and Uruguay, the ancient Greek and
Roman republics, to name only those which I know something about.

The Soviet state and the first French Republic, established on even nobler
principles, but by the replacement of one central power with another, were
never able to enjoy the fruits of the sacrifice entailed by their revolutions.
Their struggles for power engendered, predominantly, new struggles for
power. No sooner was power and responsibility concentrated than their
downfall began, even in the case of communities originally founded on
voluntary cooperation.

What individuals must do, then, is not merely to curtail consumption, share
and conserve. They must also learn how a just world is organized and
governed. For while a great deal is known about past attempts, none of these
attempts succeeded and, thus, evidently not enough was known by those
who made them. They were all helped to fail by power seekers who did their
successful best to corrupt them, but they were also helped to fail by the
neglect, over-confidence, laziness, interest in other matters and, perhaps
most of all, by the ignorance of their members. Brutus and Cassius
understood the danger that Caesar represented, but they were too few.
Jefferson understood and tried to share his understanding by universalizing
education, but was betrayed, unwittingly perhaps, by Horace Mann and other
inheritors of the public schools he pioneered.

Perhaps the most important single thing that individuals can do is to take
back their responsibility for the education of their children. Children learn
very young how power is used by the strong in their relations with the weak
and it is at least possible that this early learning shapes the individual's
behavior in all of his subsequent relations with those who are stronger or
weaker than he.

There is so much that each of us can do to create a just world that the
problem is not one of elaborating the ways but rather one of defining
principles of selection. One thing that all ways have in common is some kind
of sacrifice. The affluent must enjoy less than they could enjoy. The poor
must demand more than they can safely demand. Everyone must learn ways
of living and perceiving which are new to him, must risk his own security to
help his neighbor in trouble. Affluent parents will have to deny an
automobile for each teenage son and daughter. Poor parents will have to vent
their frustrations elsewhere than upon their children. Both will have to allow
their children to take risks from which they could protect them.

A strategy of sacrifice must necessarily be selective. People can
consistently sacrifice only those things for which the reward exceeds the
sacrifice. Especially is this true since the strategy of sacrifice here described
is as necessary after the revolution as during the heat of the battle. A just
society cannot first be won and then enjoyed. It must be won anew every day
and must, therefore, be enjoyed while it is being won.

For sacrifice is not opposed to enjoyment but is its proper \emph{alter ego}.
Health, strength, love and respect, for example, can be enjoyed only while
being earned -- or for the briefest time thereafter. Even wealth and power are
easily squandered.

Each individual, then, must choose the kind and degree of sacrifice, which
he can sustain, which for him is rewarding. Some must even be allowed to
sacrifice their earned share in a sane just world. Most people, when they
come to understand what is at stake, will not find it so difficult to choose a
burden they can bear with pleasure -- a discipline they can enjoy. What
follows is not intended to restrict but to amplify these choices.

One form of sacrifice which many people find rewarding is to trade
quantity for quality. When, as frequently happens, this entails choosing the
products of artisans over manufactured goods, the revolutionary
consequences in an industrial society may be very great. There is no doubt
that manufactured goods are cheaper, in most instances, so that more can be
enjoyed for a given amount of money. But one original painting may give
more satisfaction than a dozen prints and this can be true even though the
painter is a member of the family. The principle holds for food, clothing,
furniture, home construction and the assembly of jalopies from junkyard
parts. Among the affluent, hobbies can generate demand for tools and new
materials more expensive than the finished products. They do not have to,
however, nor need art be left to hobbies. It can be a way of life for entire
communities and could be used to advantage by and for the poor on a scale
not yet imagined.

The average American household, the top 80 per cent, contains not less
than a thousand dollars' worth of unused durable goods which with a little
work could be made more useful and attractive than goods the poor can
afford to buy new at the prices they have to pay. This represents a potential
capital of four thousand dollars for each poor American household, the
transfer of which would benefit the affluent -- they could use the space -- the
poor and the entrepreneurs who affect the transfer. These entrepreneurs
would probably have to have a social rather than an economic motivation or
else a very good facade. Too crass an approach might foul the wellsprings of
`charity'. Nevertheless, the use of the term entrepreneur is not mere allegory.
There would not only have to be middlemen to effect the transfer in
question, but thousands of them might come from the ranks of neighborhood
businessmen who cannot compete, in the sale of new goods, with nationally
organized chains.

In addition to its stock of unused goods, the average affluent American
household has several thousand dollars' worth of unused space, above what
is usually needed for its part-time members and occasional guests. If this
excess space were fully utilized, the poor could be as well housed as the
affluent. Complete utilization may be hard to imagine but there are many
ways in which a substantial part of this space could be salvaged which
would benefit everyone except real-estate speculators and snobs. Much of
this idle space is owned by older people whose families are grown and gone
and who cannot sell because the families who could buy do not meet the
ethnic requirements of their neighbors. This problem could be overcome by
enough acceptable people willing to buy houses as agents for those who
suffer discrimination. Another large block of housing space is immobilized
because people are afraid to rent rooms or apartments to tenants they feel
might expose them to unpleasantness or even danger. This problem could
also be overcome by a proper matching of homeowners and tenants.

The two tactics suggested in the paragraph above are opposed in the sense
that the first is based on deceit while the second is based upon restoring
trust. This does not make them incompatible. If both were used successfully
in the same neighborhood, each would result in the more complete use and,
therefore, in the greater value of the neighborhood. Nor are the two tactics
politically incompatible. Most people understand the distinction that a man's
home is his castle, in which he should be able to choose his associates, while
his neighbor's home is not. Nor are the two tactics morally incompatible. It
is not that the end justifies the means but rather that the means chosen in
each case appear to be the best available. If this is not actually the case the
suggestions are invalid.

This principle for the selection of means has a broad range of application.
Even the law against murder recognizes the right to kill in self-defense or in
the defense of others. There are few judicial systems, anymore, which would
convict a Jean Valjean for stealing bread to avoid starvation. New laws, on
the other hand, complicate current choices. Should people send their
children to schools, which actually harm them even though the law requires
that they do so? Should not people help those who are suffering political
persecution even though the law has labeled them criminal? Neither the law
nor the prevailing moral code provides a fully dependable criterion for
action. In Nazi Germany, both law and current morality required people to
collaborate in the murder of their Jewish neighbors. When such major issues
as power and privilege are at stake both laws and morals are used as
weapons.

Nevertheless, neither private morality nor the slogan of political revolution
- that the ends justify the means -- will do. Choosing the best available
means is not itself a foolproof formula. But there may often be no other way
of choosing courses of action than to apply relevant economic, political,
strategic, legal or moral criteria to the available choices. There may be no
better guide to choosing and applying the criteria than that others agree they
would be chosen and applied in the sane, just world which is the goal of
action.

A third principle is implicit in the above idea of the best available means,
namely, the principle of cooperation. It is essential for people to choose and
to act as individuals since only the true consent of individuals can validate
the concept of justice. Nevertheless, purely individual action is ineffective
and hard to sustain. Individuals must find ways of acting together, but they
must personally choose these ways rather than be coerced by superior
power. Leadership there can and probably must be, but the leaders rather
than the followers should be drafted and should follow the policies of those
they lead.

Cooperation, which results in community, physical or functional, has
especially great advantages. The future world must be based on community
and, throughout most of the modern world, community has been lost. Even
the traditional communities which remain may have to be done over since it
is doubtful that people, given a real choice, will settle for the drudgery and
hierarchy which characterizes most traditional communities. But there are
also important strategic advantages in community effort. Communities can
often legitimately take over much of the law making and police power. They
can also serve as examples of sanity and justice to the outside world.

In all of the above there is, obviously, nothing new. Even what may seem
original is merely a new dress on an old-fashioned idea. We have become
used to thinking of reality in terms of pentagons and atom bombs or, before
that, in terms of armies, navies, banks and corporations. People's movements
are thought to belong to the dark ages. Yet the oldest part of the United
States is less than two hundred years old; the independent nations of South
America have scarcely more than a century of history, while those of Africa
and Asia, including Israel, were founded by popular movements only a few
decades ago. In Vietcong, among others, seem to have a pretty good thing
going right now.

It is true that, up to now, military action has often been decisive, but the
present rulers of China won their battle with weapons captured from the
enemy, while in India there was no military showdown at all.

Three things are new which lend substance to what used to be, and still is
called, Utopian dreaming. First, drudgery is out of date. Second, modern
institutions require increasingly universal cooperation. Third, universal
education is now possible. It was only yesterday that the labor of four people
was required to feed five. England had to fence people off the land in order
to fill her factories. Today most people are in the way, both in the factory
and in the field. Food and all other physical products can be produced more
efficiently without them. People are indispensable, however, as passive
clients and supporters of the production complex; taking what they are
offered and doing what they are told. Without their cooperation, the current
system for control of production and distribution would collapse.
Increasingly people are becoming aware of these facts: realizing that their
bodies are no longer required for mass production and that they are being
asked instead to surrender their judgment and their will to its dictates.

But to whose dictates? Production is a mindless thing. It knows no
imperatives as to what shall be produced, where and when and for whom.
The invisible hand is somebody's hand behind the curtain. More and more
people are beginning to realize that they should be deciding these things,
along with others, and that actually everyone has an equal right to decide
them. When everyone understands this, universal education will be well on
its way. There will still be a lot to learn. How to do one's share, how to claim
one's share and no more, how to co-operate with others in things, which
cannot be done alone. These lessons will never be completely learned but
when everyone has a reasonable opportunity to learn and to apply them,
things will begin to move.

All this, if it were mere exhortation, would still be whistling in the dark.
But things are happening. Charles Reich, in \emph{The Greening of America}, says
that a people's revolution has begun. Young people all over the world are
tuning out the system and turning themselves on. Older people, including
many who are concerned by the tactics of the young, still share their sense of
the absurdity of the world they live in. The deprived, especially those who
live in the prosperous countries, are opening their eyes and flexing their
muscles. The great masses of the deprived, in Africa, Asia and South
America, are still largely quiescent. When they begin to stir the thunder may
at first roll softly but it will be heard. We can turn it on.



\vspace{2cm}

\begin{center}
\textls[500]{END}
\end{center}









%\theendnotes
%\setcounter{endnote}{0}