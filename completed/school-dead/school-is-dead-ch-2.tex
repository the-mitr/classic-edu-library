% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\NewPage


\thispagestyle{empty}
\begin{chapquot}
I learned something in school today.\\
I signed up for folk guitar, computer programming, stained glass art,
shoemaking and a natural foods workshop.\\
I got Spelling, History, Arithmetic and two study periods. So what did you
learn?\\
I learned that what you sign up for and what you get are two different things.
\begin{flushright}
\emph{Charles Schulz:\\ Peanuts}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}



\chapter{What Schools Do}
%\addcontentsline{toc}{chapter}{The Case Against Schools}
Schools are supposed to educate. This is their ideology, their public
purpose. They have gone unchallenged, until recently, partly because
education is itself a term which means such different things to different
people. Different schools do different things, of course, but increasingly,
schools in all nations, of all kinds, at all levels, combine four distinct social
functions: custodial care, social-role selection, indoctrination, and education
as usually defined in terms of the development of skills and knowledge. It is
the combination of these functions, which makes schooling so expensive. It
is conflict among these functions, which makes schools educationally
inefficient. It is also the combination of these functions, which tends to make
school a total institution, which has made it an international institution, and
which makes it such an effective instrument of social control.

Custodial care is now so universally provided by schools that it is
hard to remember earlier arrangements. Children must, of course, be
cared for -- if they really are children, that is, and not just young
members of the community taking part in its normal productive and
social affairs. Most youngsters still get along without special care,
all over the world, in the tribal, peasant and urban dwellings of the
poor.\footnote{ It is only mothers who have been freed from the
  drudgery of food production and preparation who find it necessary
  to turn the care of their children over to others. This is because
  of other differences between modern and traditional societies. Older
  children are taken out of the home by the school, fathers go to
  work, grandparents and other members of the extended family are left
  behind in rural or older urban, settlements. Were it not for the
  school, child care in the modern family would fall exclusively upon
  the mother. Schools thus help to liberate the modern woman, but only
  by imprisoning her child and by tying her and her man more tightly
  to their jobs so that they can support the schools. Women clearly
  need not only the liberation schools provide but much more. Men and
  children, however, need liberation too. The same point holds for
  blacks and others who suffer special discrimination. While each
  group must formulate its own demands and fight its own battles,
  unless they also join forces with other oppressed groups they can
  win only battles, never the war. }

Child care costs money, and although schools provide it relatively
cheaply, this is where most of the school budget goes.\footnote{\emph{Digest
  of Educational Statistics} 1968, U S Government Printing Office,
  OE-10024-68.} Time studies conducted in Puerto Rico by Anthony
Lauria show that less than 20 per cent of a teacher's time is
available for instructional activities. The rest is spent on behavior
control and administrative routine. Lauria's data support a statement
John Gardner once made, long before he was Secretary of Health,
Education and Welfare in the federal government of the United
States. He said that everything a high-school graduate is taught in
twelve years of schooling could easily be learned in two years and
with a little effort in one. Since childcare is the most tangible
service schools provide, and since parents are naturally concerned
about the quality of such care, this function has a priority claim on
school resources. Other functions must compete for what is left after
prevailing local standards of safety, comfort end convenience have
been met.

As children get older, childcare, paradoxically, becomes both more
extensive and more expensive. Actual hours spent in school increase,
buildings are more luxurious, the ratio of paid adults to students
increases, and the salaries of these adults also increase. Where there
are no schools, children contribute more to the community and require
less support, as they grow older.\footnote{In this light see the
  history of labour opposition to high schools in Merle E. Curti, \emph{The
  Social Ideas of American Educators}, Littlefield, Adams, 1959.} High
schools, however, take more of the students' time than primary
schools, and cost more too, while most colleges and universities
occupy the full time of the student, at an ever increasing hourly
cost, as students progress up the academic ladder. The costs of higher
education, admittedly, cover more than mere custodial care, but at
upper as well as lower levels, the time students spend in school is an
important cost factor.  Space is also costly; the commodious college
campus, insulated from the non-academic environment, is much more
expensive than the neighborhood kindergarten.

Money, however, is the least of the costs of providing custodial care
in schools. The really important consequence of packaging custody with
the other functions of the school is the extension of childhood from
age twelve to twenty-five, and from the sons and daughters of the rich
to the youth of the whole society. This, in turn, is only one aspect
of the division of modern life into school, work and retirement.

So long as children remain full-time students they remain children --
economically, politically, even legally. While no formal legal sanctions are
available against students, as such, they can always be deprived of their
rights to schooling, and thus to preferred employment and social status. The
school schedule remains, also, one of the major supports for age restrictions
on the right to vote, to work, to contract and to enjoy other constitutional
privileges and protections. The school itself, as custodian of ever larger
numbers of people, for increasing proportions of their life span, for an ever
growing number of hours and interests, is well on the way to joining armies,
prisons and insane asylums as one of society's total institutions. Strictly
speaking, total institutions are those which totally control the lives of their
inmates, and even armies, prisons and asylums do this completely only for
certain inmates. Only vacation-less boarding schools could strictly be called
total institutions, but perhaps the strict definition gives too much attention to
the body and too little to the mind and spirit. Schools pervade the lives and
personalities of their students in powerful and insidious ways and have
become the dominant institution in the lives of modern men during their
most formative years.

Studies of prisons and asylums indicate how overwhelmingly such
institutions produce the very behavior they are designed to
correct. In one experiment, almost all the members of a group of
persons diagnosed as hopelessly insane, asylum inmates for over twenty
years, were discharged as cured within a few months of being placed in
a `normal; environment. In another experiment, a group of persons
diagnosed as dangerously insane were allowed to institute
self-government among themselves and managed without
incident.\footnote{R. Gurjoy, unpublished paper read at the Conference
  on Development Planning, sponsored by the Public Administration
  Department of the State University of New York, held at Saratoga
  Springs, 11-24 November 1969. For similar evidence see the various
  books of R. D. Laing.} A similar cute for student unrest would be to
stop making children out of people old enough to have children,
support them and fight for them. This would, of course, require other
social changes in addition to the divorce of custodial care from
education at the higher levels of schooling, but they are equally
necessary changes if society is to survive.

A second function of schools, more directly in conflict with their
educational aims than custodial cam, is the sorting of the young into
the social slots they will occupy in adult life. Some of this sorting
occurs at the high-school and college level, when students begin to
opt for this or that profession or trade and enter special curricula
of one to a dozen years in length for vocational preparation. The
results of even this accepted aspect of job selection in school are
wasteful and often disastrous. Part of the waste is in the high
proportion of dropouts, not only from professional and trade schools
but from the professions and trades themselves, frequently after long
and expensive investments have been made. Many people find that
medicine or teaching is not for them -- something they could have found
out much sooner and much cheaper if they had begun as orderlies,
nurses or teachers' aides. Even those who stay in the field of their
choice do not escape extensive waste of time and money. According to
the folklore of many occupations, the first several years of work are
spent forgetting what was learned about the vocation in
school. Counseling and many other sincere and systematic efforts are
made to minimize this kind of waste but it is doubtful that, even at
great additional cost, they can do more than slow its
acceleration. The ever-greater separation of school from the rest of
life widens a gap, which no amount of effort can bridge.

The major part of job selection is not a matter of personal choice at
all, but a matter of survival in the school system. Age at dropout
determines whether boys and girls will be paid for their bodies, hands
or brains and also, of course, how much they will be paid. This in
turn will largely determine where they can live, with whom they can
associate, and the rest of their style of life. Within this century,
any profession could still be entered at the bottom. Today this is
difficult even in countries, which provide schools for only a tiny
minority. In the United States, it is now hard to become a carpenter
without having graduated from high school. In New York City even a
garbage collector needs a diploma.

While economic status is largely a function of the level at which a
student drops out, power in the society depends more upon the sorting
that occurs when high-school graduates enter college. Admission to
Harvard College practically guarantees access to the groups, which
will control the major hierarchies of the United States. State and
local as well as national hierarchies are the products of the college
lottery. Even international agencies are ruled by the graduates of a
dozen world-famous universities.

Power and wealth are not everything, of course, but almost everything else
depends upon them in many parts of the world. Especially where the school
system is dominant, respect, reputation, health, even affection of many
kinds, can either be commanded or purchased -- if they are not tendered as
gifts to those who could order or buy them.

The school system has thus amazingly become, in less than a century, the
major mechanism for distributing values of all kinds among all the peoples
of the world, largely replacing the family, the church and the institution of
private property in this capacity. In capitalist countries it might be more
accurate to say that schools confirm rather than replace the value-
distribution functions of these older institutions. Family, religion and
property have such an important influence on access to and success in school
that schooling alters only slowly and marginally the value distributions of an
earlier day. Jefferson put it well when he said, in arguing for public schools,
that by this means we shall each year rake a score of geniuses from the ashes
of the masses. The result of such a process, as the English aristocracy
learned long before Jefferson, is to keep the elite alive while depriving the
masses of their potential leaders.
Communist countries have, of course, abolished private property, have
attempted to abolish organized religion and have vied to weaken the role of
the family. There are few data, unfortunately, to show how much
redistribution of values has taken place in these countries but the general
impression is that it is much less than had been expected. One of the
strongest supports for this impression comes from the great similarity of
school systems in capitalist and communist countries. They perform the
same functions and share the same defining characteristics. There is not the
slightest doubt that communist schools sort their students into jobs,
vocational levels, pay differentials, power and privilege strata in just the
same way as capitalist schools. The only question is whether the prizes go to
the sons and daughters of the previously privileged in quite the same degree.

Leaders in communist China are greatly preoccupied with this
question. In 1966, schools throughout China were closed for over a
year in an attempt to make education more egalitarian and down to
earth. Mao's own policy statements make clear his desire to rid
education of elite control and to make it universally available to the
masses. The difficulties encountered in this effort are obviously
formidable, however, even for Mao, and it appears that most schools
reopened in 1967 with significant but not really fundamental changes
in the nature of the system. How much of this compromise was due to
ideological and how much to practical considerations is hard to
say. The pressures of building a nation in competition with such
super-powers as Russia and the United States must be tremendous, and
schools appear to be an almost indispensable tool for nation
building. On the other hand, the educational controversy in China has
never abandoned the rhetoric of schools. Despite all the emphasis on
universal access, practicality and revolutionary objectives, the
debate always refers to the reform of the school system rather than to
its replacement.\footnote{Paul Bady, 'L'enseignement en Chine',
  \emph{Esprit}, January 1971, pp. 73-88.}

It should now be clear why schools have grown so fast. To the masses, and
their leaders, they have held out unprecedented hope of social justice. To the
elite they have been an unparalleled instrument, appearing to give what they
do not, while convincing all that they get what they deserve. Only the great
religions provide an analogy, with their promise of universal brotherhood
always betrayed.

Betrayal of the hopes of schooling is implicit in the selection function
which schools perform. Selection implies losers as well as winners and,
increasingly, selection is for life. Furthermore, school is a handicap race in
which the slower must cover a greater distance bearing the growing burden
of repeated failure, while the quicker are continually spurred by success.
Nevertheless, the finish line is the same for all and the first to get there win
the prizes. All attempts to disguise the reality of this situation fail. Parents
know the truth, while teachers and administrators are often compelled to
admit it. Euphemisms about the transcendent importance of learning and
about doing your best are, thus, self-defeating. It is no wonder, under these
circumstances, that some children drop out while others work to win rather
than to learn.

Consistently punishing half of the children who are trying to learn what
society is trying to teach them is not the worst aspect of combining social-
role selection with education. Such punishment is an unavoidable result of
the relative failure, which half the school population must experience while
climbing the school ladder in competition with their more successful peers.
Such punishment can scarcely help but condition this half of the school
population to resist all future efforts to induce them to learn whatever is
taught in school. But this is only the lesser evil. The greater is that school
necessarily sorts its students into a caste-like hierarchy of privilege. There
may be nothing wrong with hierarchy nor with privilege, nor even with
hierarchies of privilege, so long as these are plural and relatively
independent of each other. There is everything wrong with a dominant
hierarchy of privilege to which all others must conform. Birth into a caste,
inheritance of wealth and the propagation of a governing party are all means
by which such a dominant hierarchy can be maintained. In the modern
technological world, however, all of these means either depend upon or are
replaced by the school. No single system of education can have any other
result nor can a dominant hierarchy of privilege be maintained in a
technological world by any means except a unified system of education.

If schools continue for a few more generations to be the major means
of social-role selection, the result will be a meritocracy, in which
merit is defined by the selection process that occurs in
schools. Michael Young describes this outcome in his \emph{Rise of
  the Meritocracy}.\footnote{ Michael Young, \emph{The Rise of the
    Meritocracy}, Thames \& Hudson, 1958; Penguin,1961. } His picture
of English society, fifty years from now, is a projection of
Galbraith's New Industrial State with the technocrats in the
saddle.\footnote{John Kenneth Galbraith, \emph{The New Industrial State},
  Hamish Hamilton, 1967; Penguin, 1969.} He describes a school
system which becomes a super-streaming system, constantly shuffling
its students into the channels where their past performance suggests
that they belong. The slow students are not kidded in this system;
they quickly learn where they stand and where they are going, but they
are taught to like it. The quick also know where they are going and
like it so well they end up trying to reestablish an hereditary
aristocracy based on merit. This reverse English twist leads to a
happy ending which takes humanity off the hook, but not until the
author has made his -- and repeated Dante's -- point. Any system in
which men get just what they deserve is Hell.

Schools define merit in accordance with the structure of the society served
by schools. This structure is characterized by the competitive consumption
of technological products defined by institutions. Institutions define products
in a way, which is consistent with the maintenance of a dominant hierarchy
of privilege and, as far as possible, with the opportunity for members of the
currently privileged class to retain their status in the new `meritocracy'.

What schools define as merit is principally the advantage of having
literate parents, books in the home, the opportunity to travel,
etc. Merit is a smoke screen for the perpetuation of privilege. The
ability of IQ tests to predict school performance does not rebut this
statement. As Arthur Jensen, the most recent defender of IQ, points
out, the intelligence measured by tests is operationally defined as
the ability to succeed in school.\footnote{Arthur R. Jensen, `How much
  can we boost I Q and scholastic achievement? ', \emph{Harvard Educational
  Review}, Winter, 1969.} The significance of Michael Young's book is
to show that merit is an ideological as well as an actual smoke
screen; that we would be even worse off if `true merit' were to
replace more primitive means of perpetuating privilege.

The third function of schooling is indoctrination. Indoctrination is a
bad word. Bad schools, we say, indoctrinate. Good ones teach basic
values. All schools, however, teach the value of childhood, the value
of competing for the life prizes offered in school and the value of
being taught -- not learning for one's self -- what is good and what
is true. \footnote{ A. S. Neill's Summerhill and schools patterned
  after it may be, partial exceptions. They still, however, teach
  dependence on the school.} In fact, all schools indoctrinate in
ways more effective than those which are generally recognized.

By the time they go to school, children have learned how to use
their bodies, how to use language and how to control their emotions. They
have learned to depend upon themselves and have been rewarded for
initiative in learning. In school these values are reversed. The what, when,
where and how of learning are decided by others, and children learn that it is
good to depend upon others for their learning. They learn that what is worth-
while is what is taught and, conversely, that if something is important
someone must teach it to them.

Children learn in school not only the values of the school but to
accept these values and, thus, to get along in the system, they learn
the value of conformity and, while this learning is not confined to
school, it is concentrated there. School is the first highly
institutionalized environment most children encounter. For orphans and
children who are sick or handicapped this is not the case, and the
retarding effects of institutionalizing infants is impressively
documented.\footnote{ W. Dennis and P. Najarian, `Infant development
  under ' environmental handicap', \emph{Psychological Monographs}, vol. 71
  no 7.} Orphans learn so well not to interfere with institutional
requirements that they seldom become capable of making a useful
contribution to society. The argument for schools, of course, is that
they strike the balance between conformity and initiative, which the
institutional roles of adult life will require.

Other values are implicit in those aspects of curriculum, which are alike in
schools all over the world. These include the priorities given to dominant
languages, both natural and technical. Examples of the first are the priority
given to Spanish over Indian tongues in Latin America, and to Russian over
provincial languages in the Soviet Union. Examples of the second are the
priorities given mathematics over music, and physics over poetry. There are
obviously good reasons for these priorities, but they are reasons derived
from the world as it is, ignoring the claims of both the world of the past and
the desirable world of the future. More than this, these decisions reflect not
even all of the major aspects of today's world, but preponderantly the
balance of political and economic power. Less people speak English than
Chinese and far fewer speak physics than poetry. English and physics are
simply more powerful -- at the moment.

Another value implicit in school is that of hierarchy. Schools both reflect
dominant values and maintain a stratified world. They make it seem natural
and inevitable that hierarchies are inherently correlated and cannot be
independent of each other. Schools do not have to teach this doctrine. It is
learned by studying an integrated curriculum arranged in graded layers.

Finally, after performing childcare, social screening and
value-teaching functions, schools also teach cognitive skills and both
transmit and -- at graduate levels -- create knowledge. The first three
functions are performed necessarily; because of the way schools are
organized. Cognitive learning, although it is declared the principal
purpose of schools, occurs only in so far as resources remain after
the in-built functions are performed. In urban ghetto schools of the
United States and in rural Brazilian schools, attempting to operate on
a budget of fifty dollars per year per child, very little cognitive
learning occurs.\footnote{The failures of ghetto schools in the United
  States are detailed in more documents than can be listed. Among them
  are: Coleman, \emph{Report on Equality of Opportunity in
    Education}, US Department of Health, Education and Welfare, US
  Government Printing Office, Washington, D C, 1966; Herbert Kohl,
  \emph{36 Children}, Gollancz, 1968, Penguin, 1971; Jonathan Kozol,
  \emph{Death at an Early Age}, Penguin, 1968.} \footnote{The
  failures of Brazilian rural schools are best illustrated in the
  dropout figures. More than half drop out before the beginning of the
  third grade and according to many studies, conducted by UNESCO ,
  among other organizations, dropouts at this level have not achieved
  functional literacy.}Of course exceptional teachers can teach and
exceptional students can learn within the confiner of the school. As
school systems expand, claiming an increasing proportion of all
educational resources, absorbing more students and teachers and more
of the time of each, some true educational experiences are bound to
occur in schools. They occur, however, despite and not because of
school.\footnote{Paulo Freire and Paul Goodman, two current
  philosophers of , education, proceeding from quite different
  premises, both provide carefully reasoned support for this point of
  view. Paulo Freire is a Brazilian educator known for his success in
  teaching peasants to read , and write effectively with a minimum
  investment of time and facilities. A gross over-simplification of
  his position is that people learn only in the process of becoming
  conscious of their true life situation, eventually seeing this
  situation clearly under circumstances which permit them to act
  effectively upon it. Schools could never provide their
  students. with the action potential which this programme
  requires. It is interesting that Dewey called for something like
  this action potential in his proposed experimental schools which
  never actually came into being. 

  Goodman's position, in equally over-simplified form, is not easily
  'distinguished from Freire's. Goodman holds that people learn what
  they need to learn in the course of real life
  encounters. Professions and trades are learned by practising
  them. Scholars develop in communities of scholars. Schools can teach
  only alienated knowledge: knowledge divorced from both its origins
  and its applications and therefore dead knowledge. 

  The effect of transmitting dead knowledge, according to Freire, is
  to domesticate rather than educate. Domestication is training in
  conformity and the development of either magical or mythical
  attitudes toward those aspects of life which contradict the
  pressures toward conformity. According to Goodman the attempt to
  transmit dead knowledge either has no effect or leads to a sense
  that the world is absurd. Both men are probably right. The real
  world of the Brazilian peasant is perhaps too grim to be seen as
  absurd and must, therefore, be either repressed or enshrouded in
  magic. The New Yorker, on the other hand, may be protected enough to
  be able to view the grimness of his world through the
  semi-transparent veil of absurdity. 

  Paulo Freire, `The adult literacy process as cultural action for
  freedom', \emph{Harvard Educational Review}, vol. 40, no. 2 1970,
  pp. 205-25. Paul Goodman, \emph{Growing Up Absurd}, Sphere, 1970. Joel
  Spring, unpublished lectures on Marx and Dewey, CIDOC, 1970.}

Schools rest much of their case on their claim to teach skills,
especially language and mathematical skills. The most commonly heard
defense of schools is `Where else would children learn to read?'
Literacy has, in fact, always run well ahead of schooling. According
to census data, there are always more literate members of a society
than persons who have gone to school. Furthermore, where schooling is
universal, there are always children attending school who do not learn
to read. In general, the children of literate parents learn to read
even if they do not attend school, while the children of illiterate
parents frequently fail to learn even in school.\footnote{The first
  United States census which included Puerto Rico, in 1910 showed a
  majority of the adult population of the island, less than 10 per
  cent of which had ever attended school, to be literate. Examination
  of any census series in which literacy and schooling are reported
  shows similar, if less extreme, contrasts.}

In universally schooled societies, of course, most children learn to
read in school. Considering when they learn to read and when they
begin to go to school, it could hardly be otherwise. Even in a fully
schooled society, however, few children learn to read easily and well,
although almost all learn to speak easily and well, a skill learned
outside of school.\footnote{Coleman's \emph{Report on Equality of
    Opportunity in Education} shows that home background, including
  the education of parents, explains much more of the difference in
  student achievement than the quality of schools attended by these
  students, as measured in costs, teacher preparation, etc.} Children
who do learn to read well read a lot for their own pleasure, which
suggests that good reading -- like other skills -- is the result of
practice. Data on mathematics give even less support to
school. Illiterates, who participate in a money economy all learn to
count, add, subtract, multiply and divide, while only a small
percentage of people in a fully schooled society ever learn much
more. Of those who take algebra in high school, only a small
percentage do better than chance on an objective
test.\footnote{Analysis of achievement-test scores for all students
  who took high- school algebra in the Puerto Rican public schools in
  1964 shows a random distribution of answers on all items except the
  four or five easiest, which were essentially simple arithmetic
  problems. Other student populations may do somewhat better, but both
  Project Talent, conducted by N. Flanagan at the University of
  Pittsburgh, and Torsten Hus\'en, \emph{Achievement in Mathematics in Twelve
  Countries} (International Educational Achievement Association Project
  of the Institute of Pedagogy, Hamburg, Germany) show that
  differences between school systems and between countries are
  relatively small. Item analyses are not available for these other
  populations. 

  It will be noted that statements of fact, in the text, are
  frequently supported with Puerto Rican data. This is partially
  because the author worked for fifteen years with Puerto Rican school
  data, but also because very few school systems have data as
  comprehensive, in as much detail, and so well analysed. This, again,
  is because Puerto Rico has one of the largest integrated school
  systems of the world. In the United States only New York City's is
  larger. Another reason is that school systems in general are not
  given to quantitative evaluation. Puerto Rico has limited
  resources, but from 1946 to 1968 enjoyed a very progressive
  government. Finally, its school system had a series of outstanding
  directors of evaluation culminating in the work of Charles
  O. Hammil, who is responsible for most of the analyses cited.}

There is a body of data collected by Jerome Bruner and his students
showing that children who go to school learn concepts, which are not
learned by those who do not go to school. \footnote{Jerome S. Bruner,
  Rose R. Olver, Patricia M. Greenfield, \emph{Studies in Cognitive Growth
  A Collaboration at the Center for Cognitive Studies}, Wiley,
  1967.}The concepts studied were those made famous by Jean Piaget,
who found that most French and Swiss children learn at about ages six
to eight that water poured from a short fat cylinder into a tall thin
one is still the same amount of water. Bruner's students found that
African bush children who go to schools, which are patterned after the
French, learn such concepts much better than similar children who do
not go to school. These experiments did not, however, test the effect
of relevant learning environments other than school. Until a unique
effect of schooling is demonstrated, with everything else controlled,
Bruner's data show only that environments affect concept learning and
suggest that the more relevant the environment is to the concept the
more effect it has.  Another claim is that schools teach the grammar
of language, the theories of mathematics, science and the
arts. Undoubtedly they do; but the real question is whether these
things are learned in school more than they would be
otherwise. Achievement tests give little support to schools. As in the
case of mathematics, only a small minority of students do better than
chance on the formal aspects of any subject matter. Students who are
interested in these matters learn them and those who are not do
not. Whether interest in them is stimulated by schools remains very
doubtful. Einstein, commenting upon a short period he had to spend in
school preparing for a degree examination, said that as a consequence
he was, for several years afterwards, unable to do any creative work.
The pernicious effect of schools on cognitive learning, of which
Einstein complains, is best seen by contrasting the impact of
schooling on privileged and underprivileged children. The
underprivileged, whose home environments are lacking in the
specialized resources schools provide, are relatively unsuccessful in
school and soon leave it with an experience of failure, a conviction
of inadequacy and a dislike for the specialized learning resources of
which they are subsequently deprived. The privileged, whose home
environments rare rich in the specialized resources of the school, who
would learn on their own most of what the school has to teach, enjoy
relative success in school and become hooked on a system which rewards
them for learning without the exercise of effort or initiative. Thus,
the poor are deprived both of motivation and of the resources, which
the school reserves for the privileged. The privileged, on the other
hand, are taught to prefer the school's resources to their own and to
give up self-motivated learning for the pleasures of being taught. The
minorities of Einstein's and Eldridge Cleavers lose only a little
time. The majority loses their main chance for an education.


\theendnotes
\setcounter{endnote}{0}