% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\newpage
\thispagestyle{empty}
\begin{chapquot}
At the treaty of Lancaster, in Pennsylvania, anno 1744, between the
Government of Virginia and the Six Nations, the commissioners from
Virginia acquainted the Indians by a speech, that there was at Williamsburg
a college with fund for educating Indian youth; and that if the chiefs of the
Six Nations would send down half a dozen of their sons to that college, the
government would take care that they be well provided for, and instructed in
all the learning of the white people.

The Indians' spokesman replied:

``We know that you highly esteem the kind of learning taught in those
colleges, and that the maintenance of our young men, while with you, would
be very expensive to you. We are convinced, therefore, that you mean to do
us good by your proposal and we thank you heartily.''

``But you, who are wise, must know that different nations have different
conceptions of things; and you will not therefore take it amiss, if our ideas of
this kind of education happen not to be the same with yours. We have had
some experience of it; several of our young people were formerly brought up
at the colleges of the northern provinces; they were instructed in all your
sciences; but, when they came back to us, they were bad runners, ignorant of
every means of living in the woods, unable to bear either cold or hunger,
knew neither how to build a cabin, take a deer, nor kill an enemy, spoke our
language imperfectly, were therefore neither fit for hunters, warriors, nor
counselors; they were totally good for nothing.''

``We are however not the less obligated by your kind offer, though we
decline accepting it, and to show our grateful sense of it, if the gentlemen of
Virginia will send us a dozen of their sons, we will take care of their
education, instruct them in all we know, and make men of them.''
\begin{flushright}
\emph{Benjamin Franklin:\\ Remarks Concerning the Savages of North America}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\newpage

\chapter{What People Need To Know}

The title of this chapter has a double meaning. It refers both to the
objectives of education and to the resources required to pursue these
objectives. They are discussed together because, as will be seen, they are
intimately interrelated and cannot properly be separated. Education should
lead to a world based on freedom and justice; where freedom means a
minimum of constraint by others, and justice means a distribution of wealth,
power and other values consistent with this kind of freedom.

Men in society necessarily constrain each other. The problem is one of
defining the boundary between one man's fist and the other's nose. Where
fists and noses are literally in question, this is easy, and it is not much harder
in other matters, so long as men are assumed to be enough alike to validate
the golden rule. If we assume that all men value health, wealth, skill, power,
respect and affection, then justice requires that no man should have so much
of any of these as to deprive another of his share. This does not mean that all
shares must be equal. Monogamy would not have to be restrictively defined.
The test could be whether anyone is deprived of a mate by the existence of a
Mormon or Tibetan household. There can also be other trade-offs. A true
urbanite might be glad to make his share of acreage for a Manhattan
apartment. Obviously, no one else -- his children for example -- should be
bound by such a trade.

The details of a just, free world must be based upon the freely given and
mutual consent of relative equals, of persons who, as far as possible, have
equal knowledge and equal understanding of what their consent implies.
Educational objectives must be defined, not only in terms of how to maintain
such a world, but in terms of how to attain it and keep it growing. Education
must be a major instrument in the development of a just world. The basic
objective of education must be an understanding of the world we live in and
the world we hope for, understanding which can lead to effective action.
Such action is assumed to be voluntary and cooperative, but unavoidable
conflict is not ruled out. The education in question must prepare individuals
to act with others as well as by themselves. But before a man can engage in
intelligent collective action he must understand his own situation, not as a
social atom, but as member of a family and other groups.

People understand the world by means of language. Not exclusively but
in the main, especially if language is broadly defined, as Suzanne
Langer defines it, to include music, dancing, poetry and other
affective modes of communication.\footnote{Suzanne Langer,
  \emph{Philosophy in a New Key}, Oxford University Press, 1957.}
But language is used, as we have seen, to obscure and to distort
reality as well as to render it lucid. People must have the
opportunity to learn to use language critically -- in such a way as to
reflect upon the thing discussed and also upon the language in which
it is discussed. It is not so important how much or how many languages
are learned, as it is to learn not to be naive. At the same time, it
is not enough to be shrewd about only a few things, as the peasant or
the beggar often is. A minimum breadth of language still is necessary
in order to protect one's interest in the world; to understand one's
situation well enough to act upon it significantly. This is impossible
without some knowledge of physical science, politics, economics and
psychology.

As these disciplines are talked about today it would, obviously, be
impossible for everyone to learn their languages. But this is because these
languages have become so esoteric that those who go to the trouble of
learning them can then use them to confuse and exploit others. Almost
everyone knows what aspirin is except when doctors prescribe if as
acetylsalicylic acid. Everyone knows what damage is except when lawyers
call it a ton. Many specialists, of course, are free of any intention to confuse
or exploit. Furthermore, specialized languages are clearly necessary for the
creation of new knowledge. Specialists cannot forget, however, that they
practice their specialty at the sufferance of the rest of society and that if they
fail to maintain effective communication, they open society to exploitation -
directly or indirectly -- by practitioners of their specialty. New knowledge
may benefit or injure mankind depending on how it is used. The creators of
new knowledge are as obliged, therefore, to create a language in which
new knowledge can be generally communicated as to create one in which new
knowledge can be developed. A famous scientist said recently that any
scientific concept can be explained to anyone by someone who himself
understands it concretely. This does not mean, however, the kind of
simplifying textbook explanations that characterize today's schools. The
outlines of the great classics, best sellers in every university bookstore,
illustrate the point. If books written to divert voluntary readers had never
been made classics, from which curricula were then constructed, there would
be no need for outlines. The peasants of old Russia would have understood
and preferred Dostoyevsky in the original.

But the problem cannot be fully understood at this level. It is
necessary to go back to the peasant culture in which Paulo Freire
worked, and even beyond this to the origins of this culture. Freire
calls the rural culture of Latin America a Culture of Silence. By this
he means that the rural masses, having been deprived of any real voice
in the matters, which concern them most, have forgotten how to speak
or even to think about these matters except in terms of the
rationalizing mythologies supplied by their superiors.  In Freire's
terms they have lost the 'word'. The term is used here as it is in the
first verse of the Gospel according to St John: `In the beginning was
the word and the word was with God and the word was God.' To
understand how the word might be lost to a whole class of men, it is
necessary only to remember the origins of this class in the twin
institutions of slavery and serfdom. Slaves and serfs were allowed to
sing, to chant, to chatter and to gossip -- and in ancient times much
more -- but they were not allowed to say anything serious about their
own condition or about the society which kept them in this
condition. For generations the children of this class were raised
without reference to such matters, and even with conscious repression
by parents of references that innocent children might make. It is easy
to see how the word was lost to slaves and serfs and their successors.

The essentials of the Culture of Silence are reflected in the Culture
of Childhood. Children, too, are allowed to sing and chatter, but not
to know or talk about grown-up things. Theirs too is a culture of
silence but, at the higher social levels, more like that of the Greek
slaves in Roman times who were allowed to learn everything except the
arts of war and politics and who might even, after years of proof of
loyalty, be freed.

Similarities between children, slaves and peasants have often been
noted and have served even as the basis for ethnic myths, explaining
the supposed inferiority of lower classes and oppressed races in terms
of their childlike character, thus justifying the maintenance of
domination over them. The mythic character of such beliefs is
completely exposed by anthropologists, such as L\'evi-Strauss, who
show that the intellectual systems and accomplishments of so-called
primitives are as complex as those of scientifically and technically
advanced populations.\footnote{Claude L\'evi-Strauss, \emph{The
    Savage Mind}, Weidenfeld \& Nicolson, 1966.} The kinship, plant
and animal classification systems, which are learned in these
cultures, are as difficult to learn as any of our scientific and
technical codes. For us they would be much harder.

People learn what they need and are allowed to learn. What they need
is a function of the culture they live in and of their role within
it. What they are allowed to learn is also a function of this
culture. But cultures contain ideals, and these as well as the
institutions, which pervert them, help to determine what needs to be
and may be learned.

In a free, just world, or in progress toward one, all people need to
know how the universal values of their society are created and
distributed and how the methods of creation and distribution are
governed, i.e. how the society is governed. Laswell and Kaplan in
their book \emph{Power and Society} list such universal values as
well being, wealth, power and respect, and give examples of the
processes by which each value is converted into others.  Power, for
example, may be based upon respect or obtained through bribery.
Wealth may be obtained through productivity or extorted from others by
threats to their well-being.\footnote{Harold Lasswell and Abraham
  Kaplan, \emph{Power and Society: A Framework for Political Society},
  Yale University Press, 1950. Lasswell and Kaplan list four welfare
  values: well-being, wealth, skill and enlightenment, and four
  deference values: power, respect, rectitude and affection. In a
  cross listing of these values they indicate how each is converted
  into the others (p. 86).} Not everyone needs to know all of the
techniques of how each value is created and exchanged but they do need
to know enough to prevent any group of specialists from attaining a
dominant role -- or to reduce this role if it has already been
attained. Basic educational policy needs to be concerned with
providing universal access to only this much learning, and with
preventing obstacles to any more specialized learning which
individuals might choose. To put this more concretely: basic
educational policy must guarantee not only freedom of access but an
adequate supply of the resources required for everyone to learn how
society really works. Provision of resources for more specialized
learning can be left to specialized groups and agencies of the
society, as long as access to these resources is not restricted.

The universal educational objectives stated above imply both much more
and much less learning than occurs today, either in schools or in the
normal process of growing up outside them. Very few people today learn
anything about international finance, practical politics or military strategy, or about
who marries whom for what reasons. Few people learn much about the foods
and poisons they eat, drink or breathe, about why they feel as they do or why
other people treat them as they do.

At the same time, people all over the world are learning the details of
elaborate kinship networks, plant classification systems, agricultural
folklore, histories and geographies, categories of arts no longer practiced,
age-old myths and superstitions and a host of other things. Some of these
things may need to continue to be learned, but only by particular individuals
or groups. How society works needs to be learned by every responsible
member of a free, just world. As the world changes, the necessary content of
universal education will also change. The present formulation is merely
illustrative, but will serve to indicate the kinds of information, which must
be disclosed, and the kinds of learning resources, which must be provided.

The secular significance of the great religious teachers of the past can be
seen in the important role of disclosure in true education. Apart from the
transcendental content of their teaching, Moses, Jesus, Mohammed,
Gautama, Lao Tse, to mention only a few of the most famous, were able to
disclose the significant truths of their time to millions of people. Each of
their teachings was, of course, subsequently used to obscure the very
enlightenment they brought, to justify what they denounced, and to convict
those whom they justified. Nevertheless, the truths they revealed could never
again be totally hidden. Today's injustice stands convicted by their teachings
and today's ideals are built upon them. Their teachings were not esoteric.
Many of their contemporaries must have seen what they saw, felt what they
felt, but lacked the security to trust their judgment, the courage to speak their
mind, or the charisma to attract disciples.

In our time, the great teachers have spoken in secular terms. Marx, Freud,
Darwin, to again name only the most famous, have revealed to millions,
truths that many others sensed but could not equally well express. Thanks in
part to the great teachers of the past; today's significant truths lie closer to
the surface. They are there for everyone to see. The veneer, which covers
them, is frequently transparent; the hypocrisy with which they are disguised
is often so blatant as to be insulting. Today, no genius is required to
discover, reveal and proclaim the truths, which could set men free. But it
still takes doing. This is the role of the true teacher, the one educational
resource that will always be in short supply.

Aside from people who discover truth, all educational resources are
potentially plentiful. Schools make them scarce and costly, but freed from
the perverse packaging which schools impose; there are enough resources to
provide lifetime education for everyone.

Schooling has become the most costly of human enterprises. More people
are enrolled in school than are employed in agriculture. More man-hours are
spent in the classroom than in the fields. If the time of students is valued at
its market potential, more money is tied up in schooling than in agriculture,
industry or warfare. Schools are so costly because they separate what should
be combined and combine what should be kept separate. Schools separate
learning from work and play, parcel the world into subject matters and
divide learners into teachers and students. They then package subject matters
into curricula; package baby-sitting, skill modeling, administration,
pedagogy, research and intellectual leadership into teaching; and package
fraternities, athletics, professional training and intellectual life into colleges
and universities.

Separation of learning from other activities is the most serious
error.  Learning occurs naturally at work and at play, but must be
artificially stimulated when separated from them. Learning occurs
naturally in the course of encountering real-world problems, but when
these are subdivided into mathematics, economics, accounting and
business practice they become so artificial that for most people
learning can be induced only at great cost.  Learning occurs naturally
in the course of true teaching, but only with great difficulty in the
role of classroom student or classroom teacher.\footnote{`To be a
  teacher does not mean simply to affirm that such a thing is so, or to
  deliver a lecture, etc. No, to be a teacher in the right sense , is
  to be a learner.' S\o ren Kierkegaard, \emph{Point of View for My Work as
  as Author}, ed. Benjamin Nelson, Peter Smith Publishers, n.d.}

Every thing and every person in the world is a learning resource. All are
needed but all are plentiful, in relation to the need for them, unless
deliberately made scarce. Educationally, things are of two kinds: ordinary
things to which only occasional access must be provided and special things
to which regular access is required. The special things may be records such
as books, tapes, discs, films, papers, punch cards, and computer memories.
Other types of special things are instruments for producing or interpreting
records such as pencils, typewriters, presses, tape recorders or computers;
toys and equipment for games; natural elementary things such as rocks, sand
and water. People can also be classified as ordinary or special educational
resources but here the need for access is just the opposite as in the case of
things. Ordinary people are commonly needed and require regular access
while only occasional access is required to the special people. These special
people are the ones we call educators, who have a very different role in the
organization of educational resources we shall propose than they now have
in the service of schools.

For the purpose of organizing access, educational resources are
conveniently grouped into special and general things, and into three types of
people: skill models, peers and educators. These classes of resources involve
either different problems of access or else need to be kept distinct in order to
avoid the packaging and artificial shortages which now make schooling so
expensive.


\theendnotes 
\setcounter{endnote}{0}
