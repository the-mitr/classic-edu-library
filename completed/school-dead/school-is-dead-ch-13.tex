% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode




\NewPage
\thispagestyle{empty}
\begin{chapquot}
Whereas it appeareth that however certain forms of government are better
calculated than others to protect individuals in the free exercise of their
natural right, and are the same time themselves better guided against
degeneracy, yet experience hath shown, that even under the best form, those
entrusted with power, have, in time, and by slow operations perverted it into
tyranny, and it is believed that the most effectual means of preventing this
would be, to illuminate, as far as practicable the minds of the people at large,
and more especially to give them knowledge of those facts, which history
exhibiteth, that, possessed thereby of the experiences of other ages and
countries, they may be enabled to know ambition under all its shapes, and
prompt to exert their natural powers to defeat its purposes.
\begin{flushright}
\emph{Thomas Jefferson: A Bill for the more general diffusion of knowledge}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

%\newpage

\chapter{The Revolutionary Role Of Education}

Effective alternatives to schools cannot occur without other wide spread changes in society. But there is no point in waiting for other changes to bring about a change in education. Unless educational alternatives are planned and pursued there is no assurance they will occur no matter what else happens. If they do not, the other changes are likely to be superficial and short-lived. Educational change, on the other hand, will bring other fundamental social changes in its wake.

True education is a basic social force. Present social structures could not
survive an educated population, even if only a substantial minority were
educated. Something more than schooling is obviously in question here;
indeed, almost the opposite of schooling is meant. People are schooled to
accept a society. They are educated to create or re-create one.

Education has the meaning here which students of education and of
human nature have always given it. None has defined it better than
Paulo Freire, the Brazilian educator, who describes the process as
becoming critically aware of one's reality in a manner, which leads to
effective action upon it.\footnote{ Paulo Freire, `The adult literacy
  process as cultural action for freedom', \emph{Harvard
  Educational Review}, vol. 40, no.  2, 1970, pp. 205-25.} An
educated man understands his world well enough to deal with it
effectively.  Such men, if they existed in sufficient numbers, would
not leave the absurdities of the present world unchanged.

Some such men do exist: men who understand reality well enough to deal
with it effectively. Today they exist in small numbers, most of them engaged
in running the world for their own convenience, a few in trying to stop them.
If, in any society, the proportion of persons so educated were 20 per cent
instead of 1, or 30 instead of 3, such a society could no longer be run by a
few for their own purposes but would have to be run for the general welfare.
The laurels of leadership lose their appeal if spread over more than a few
and an educated minority, above a certain size, would have to opt for justice
and sanity. Whenever, as in pilgrim New England, ancient Athens or early
Rome, a reasonable proportion of the population has been educated, in the
sense of knowing what the local score was, their societies have been run not
by the few for their own interests, but by the many in the common interest.

Nation states as they exist today could not for long survive an educated
population. Nations made up of educated citizens, or containing a substantial
minority of such citizens, would tend to merge with other nations. This
could, of course, begin to happen within the nominal framework of the
nation state. Geographical boundaries would not have to change. If
immigration and tariff restrictions change sufficiently, political frontiers
become meaningless.

Class distinctions would also tend to disappear in educated societies; as
indeed they have tended to do in certain periods of history. This does not
mean that individual differences of value-position or privilege would
disappear. In a changing society new differences would tend to occur as
rapidly as old ones were equalized. It would be difficult, however, to
identify differences resulting from fairly constant change either with class,
race, or any other socially identifying label. An educated society would
become and remain highly pluralistic, with many loosely related, build
hierarchies based on a large number of fairly independent value criteria.
Some people might be rich, some powerful, others popular, still others loved
or respected or strong, but not very many could be all of these for very long.

An educated population would make not only their nations but also their
specialized institutions responsive to the needs and desires of clients and
workers in addition to those of managers. An educated minority of any size
would never put up with current health and education services,
environmental pollution, political policy control by military-industrial
cliques or advertiser control of mass media, to say nothing of traffic jams,
housing shortages and the host of other absurdities which afflict modern
societies.

No educational magic is implied. Not even educated people could solve
these problems in their present context. What they could and would do is
recognize an impossible context and change it. They would realize, for
example, that competitive consumption is an impossible way of life for more
than short periods or small minorities. Once these were grasped, much of our
present production and employment would be seen as not only unnecessary
but actually harmful. War materials are an obvious case, but schooling,
pretentious short-lived consumer durables, advertising, corporate and
governmental junketing and a host of other products and activities are
scarcely less so.

What makes it so difficult to do anything about these matters is that the
present way of life of so many privileged people depends upon keeping
things as they are. Education alone cannot solve this problem. It can help
people to see what shifting sand their present security rests upon. It can help
them visualize feasible alternatives, although something more may be
required to realize them. This is merely to say that education alone cannot
bring about revolutionary social change.

Theories of political revolution provide some basis for a more general
theory of institutional revolution, but important revisions and additions are
needed. Political institutions are uniquely based upon power and the use of
violence. In political matters, ideology and rationality tend to be subservient
to the use of power and violence. In the case of other institutions -- even
including the religious -- ideology and rationality are relatively more
important. This may not always be apparent in the declining days of
decedent institutions bolstered by naked power. It is nevertheless true that
people choose their markets, their schools, their hospitals and transport,
somewhat less blindly, somewhat more in consideration of costs and benefits
- including sentimental attachments -- than they do in choosing and
defending their citizenship. Changes in non-political institutions are, at least
on the surface, subject to rational discussion. Major changes in non-political
institutions are at times carried through without violence, although this
might be less the case if legitimate violence were not a monopoly of political
institutions. It is conceivable, at any rate, that revolutionary change in non-
political institutions could take place without violence, could be semi-
rational, could be affected by analysis, research, debate, legislation, resource
allocation, market behavior and peaceful political participation.
Socialization in the Scandinavian countries and Britain and the formation of
the European Common Market provide examples of changes that have taken
place in relative peace, though certainly not without pressure and the threat
of violence.

Scientific and religious revolutions are worth looking at for
ideas. In mature sciences, one major theory controls research and
teaching in the field until gradually its deficiencies become more and
more widely recognized, it fails to satisfy an increasing set of
requirements made upon it, and is finally discarded in favor of a more
successful rival. The necessary conditions for this kind of peaceful
change are easy to identify. There is a common language which members
of a branch of science uses and jointly understand.  There is regular
communication among scientists. There is an ultimate court of appeal,
namely empirical evidence produced under controlled and published
conditions. Finally, there are agreed upon canons of reason and
logic. These conditions are hard to match outside the mature sciences
but they provide useful standards, which have actually been
approximated in the examples of peaceful institutional change cited
above. Thomas Kuhn's recent book and the controversy it has engendered
show that even scientific revolutions are vastly over-simplified in
the above account.\footnote{Thomas S. Kuhn, \emph{The Structure of
  Scientific Revolutions}, University of Chicago Press, 1962.} They do
proceed without major violence, however, and with an apparent
rationality, at least after the fact.

Religious revolutions have not commonly been peaceful but some have
been, and these illustrate an important principle. They show that
something comparable to loss of faith in an old version of truth, like
that which occurs in a scientific community, also occurs among the
rank and file of a population. New religious faiths have swept over
large areas at a rapid rate, and the conditions under which this has
happened have something in common, both with each other and with the
conditions under which scientific revolutions have occurred. Sweeping
religious movements have always occurred among miserable people, under
deteriorating social conditions leading to disillusionment and
despair. The other condition for their occurrence has been a powerful
and attractive new revelation of truth.  Sometimes, but not always,
charismatic leaders and disciples have proclaimed the new truth. As in
the case of science, common language, communication and commonly
accepted standards of reason and logic are necessary conditions for a
religious revolution. The logical standards are not, of course, the
same as those of science and the ultimate test of truth is very
different. Evidence for the emotions, not evidence for the senses, is
the touchstone of religious truth. Deeply felt needs must be
satisfied.  Nevertheless, the parallels between scientific and
religious conversion are much more impressive than the
differences. Religious revolutions, too, may hold lessons for a theory
of institutional revolution.\footnote{Norman Cohn, \emph{The Pursuit
    of the Millenium}, Seeker \& Warburg, 1957; Paladin, 1970.}

The annals of violence itself may support the idea that violence need
not accompany change. Military history is full of instances of
battles, which were not fought because one side had a demonstrable
preponderance of power. Usually this was the side that began with the
most power, but not always. A peaceful revolution is not one in which
the holders of power give up meekly. This is truly romantic
nonsense. A peaceful revolution is one in which the nominal holders of
power discover that they have lost their power before they begin to
fight.

There is no assurance that institutional revolution can be
peaceful. There is only a hope and not necessarily a very good
one. The peaceful character of revolution is not, however, the only
consideration. It is important partly because of its critical relation
to an even more important criterion. This is that revolution be
effective, that it achieve its purposes. The history of political
revolution is a history of betrayal, both of the idealists who helped
create the conditions for it, but even more of the common people on
whose behalf it was made and who themselves made the major sacrifices.
Revolution will result in only those positive changes, which are in
the course of being made when the revolution occurs. If it consists in
nothing but these changes, so much the better.





\theendnotes
\setcounter{endnote}{0}