% !TEX root = school-is-dead.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\NewPage
\thispagestyle{empty}
\begin{chapquot}
First-hand knowledge is the ultimate basis of intellectual life. To a large extent book learning conveys second-hand information, and as such can
never rise to the importance of immediate practice \ldots{} What the learned world
tends to offer is one second-hand scrap of information illustrating ideas
derived from another second-hand scrap of information. The second-
handedness of the learned world is the secret of its mediocrity. It is tame
because it has never been scared by facts.
\begin{flushright}
\emph{Alfred North Whitehead:\\ The Aims of Education and Other Essays}
\end{flushright}
\thispagestyle{empty}
\end{chapquot}

\newpage

\chapter{What Schools Are}


It may seem academic to distinguish what schools do from what schools
are, but the purpose of the distinction is very practical. Alternatives to
schools cannot be formulated unless we can define school. Schools perform
necessary social functions, which, in some form and combination, always
have to be performed. Some people believe that schools can be reformed
while others hold that no specialized alternatives to schools are needed,
since all the education men require can come as a by-product of their other
activities. Neither position can be refuted or properly evaluated without a
definition of school. The reasons for defining schools are heuristic; to know
better what to do and what to avoid in planning alternatives. Precision,
therefore, is less important in the definition than usefulness in practice. We
define schools as \emph{institutions, which require full-time attendance of specific
age groups in teacher-supervised classrooms for the study of graded
curricula}. The better this definition fits an institution the more nearly does
the institution correspond to the stereotype of school. Alternatives in
education can be most generally defined as moving away from this
stereotype. Unless they move far enough and fast enough, however, to
escape the `gravitational pull' of the school system, they will be
re-absorbed.

By specifying the age of required attendance, schools institutionalize
childhood. In schooled societies, childhood is now assumed to be a
timeless and universal phenomenon. But children, in the modern sense,
did not exist three hundred years ago and still do not exist among the
rural and urban poor who make up most of the population of the
world. In his \emph{Centuries of Childhood} Phillipe Ari\'es shows
that, before the seventeenth century, children dressed as adults,
worked with adults, were imprisoned, tortured and hanged like adults,
were exposed to sex, disease and death, and in general did not have a
special status. The sub-culture of childhood did not
exist.\footnote{Phillipe Ari\'es, \emph{Centuries of Childhood},
  Cape, 1962.} The medieval church assumed that children, baptized in
infancy, reached the age of reason at about the age of seven; this
meant that they were from then on fully responsible for their acts,
not only to men but also to God. They were capable, that is, either by
positive acts or by neglect, of consigning themselves to everlasting
torment. In its time this doctrine was not unique. Children were
treated no more tenderly in the Arab or Oriental worlds or, for that
matter, in Africa or America.

All cultures, of course, distinguish infants and sexually immature youth
from adults. All cultures have initiation rites which signal entrance into full
adult status. All cultures make some distinction between what adults and
non-adults may do and have done to them. This is not to say, however, that
all cultures have a sub-culture of childhood. Children in the western culture
are not expected to work, except at their studies. Children are not
responsible for any nuisance, damage or crime they commit upon society.
Children do not count, legally or politically. Children are supposed to play,
enjoy themselves and prepare themselves for adult life. They are supposed to
go to school, and the school is supposed to be responsible for them, guide
them and, temporarily at least, take the place of their parents. Childhood
explains the priority, which schools give to custodial care.

Childhood must also be viewed in contrast with modern, pre-retirement
adult life. Childhood and the adult world of work have been drawing apart.
While children have been increasingly indulged, pre-retirement adults --
women as well as men -- have been increasingly molded to the world of
machines and institutions. Childhood has become more child-centered, more
indulgent, while adults have been increasingly constrained. The argument
for schools is that they provide a necessary bridge from childhood to adult
life, that they gradually transform the indulged child into the responsible
adult. Schools take the child from his garden, by carefully graded steps, to a
prototype of the world of work. They enroll the `complete child' and
graduate the `complete man'.

As in the case of the school, childhood has probably served a useful
purpose. The pre-childhood treatment of children was, and is,
undesirably brutal. Many of the protections childhood has brought to
children are important and necessary -- so much so that it is vital to
extend them, not only to other children, but also to adults. Sexual
abuse, under conditions, which make consent a farce, is one
example. The exploitation of labor under similar conditions is
another: one parry has the choice of working or starving while the
other has to choose merely between one laborer and another. The
enforcement or neglect of conditions which stunt the growth or
unnecessarily limit the opportunities of children, or adults, need to
be prohibited and prevented wherever they occur. But this is
impossible if the indulgence of already indulged children is endlessly
multiplied.  Furthermore, while some protections and indulgences are
necessary and good, too many are bad, and we have reached and passed
many thresholds in the institution of childhood at which benefits
become liabilities. Many of these are obvious and need no
argument. One, which has already been noted, is the extension of the
age of childhood to include fully mature adults, so long as these
adults remain in school. Much of the protest on the part of youth is
related to this fact, as is the resentment by adults of this
protest. The case for youth is obvious. Old enough to have children of
their own and to fight for them, they are encouraged to do only the
latter and are denied the right to participate fully in the economic
product of the society. The adult case is also easy to
understand. These children, they say, want to remain children and yet
to enjoy the privileges of adults. In part, the adults are right.
What they forget is that youth did not create the institution of
childhood but were created by it.

Schools, as creators of social reality, do not stop with
children. They also create schoolteachers. Before there were schools
there were caretakers of children, gymnastic disciplinarians enforcing
practice, and masters with disciples.\footnote{H. I. Marrou, \emph{A History
    of Education in Antiquity}, tr. George II Lamb, Mentor Books, 1964.}
None of these three assumed that learning resulted from teaching,
while schools treat learning as if it were the \emph{product} of teaching.

The role of the schoolteacher in this process is a triple one, combining the
functions of umpire, judge and counselor. As umpire, the teacher rules
answers right or wrong, assigns grades and decides upon promotion. As
judge, the teacher induces guilt in those who cheat, neglect their homework,
or otherwise fail to live up to the moral norms of the school. As counselor,
the teacher hears excuses for failure to meet either academic or moral
standards and counsels the student on choices to be made both inside the
school and out. This description fails to sound strange only because students
are regarded as people without civil rights. Imagine combining the role of
policeman, judge and attorney for the defense -- or the role of buyer,
appraiser and economic counselor -- or the role of referee, athletic
commissioner and coach. In a purely formal sense, the student in this
situation is helpless, while the teacher is omnipotent.

In American schools the teacher is obviously not omnipotent. Almost the
reverse seems to be true. How can this be explained? Teachers have the roles
and powers described above. They also exercise them, but usually not
effectively. This is because the system has broken down, but the breakdown
is itself inherent in the distortion which school creates in the true roles of the
teacher. As suggested above, these were exemplified in the preschool era: in
the Greek slave who safeguarded his young charges in their excursions about
the city, in the disciplinarian who kept them at their practice of arms, in the
master prepared to dispute with them in matters of politics, ethics and
philosophy. Of these, only the disciplinarian survived without major
distortion in the early schools. Drill with the pen rather than the sword
involved only a change of instrument, and the method was equally effective.
Schools stopped being effective in teaching skills when this method was
abandoned. The other two roles were totally distorted in their incorporation
into school. The caretaker role depended completely for in educational
validity upon not over-stepping its bounds. The caretaker slave had nothing
to say about place, time or activity except to keep his charges within the
bounds of safety. The educational value of the activities depended upon
student selection and conduct. The master was also transformed into his
opposite when placed within the school. His true role was to be questioned
and to answer in such a way as to provoke ever-deeper questions. In the
school, this role is reversed -- the master becomes the questioner and is
forced to propound orthodoxy rather than provoke exploration.

Children and teachers do not yet make a school. Without required
attendance in specialized space, teachers and children could be a home, a
nursery or a crusade. Required classroom attendance adds the time and space
dimensions, which imply that knowledge can be processed, and that children
have an assigned time and place. During infancy they belong in the home. At
kindergarten age they begin to belong, for a few hours a day, in school. The
amount of school time increases with age, until college becomes Alma
Mater, sacred or soul mother, the social womb in which the child develops
and from which he is finally delivered into the adult world. Classrooms may
be varied to include laboratory, workshop, gymnasium and year abroad, but
this is all scholastic space -- sanitized, sealed off from the unclean world,
made fit for children and for the transmission of knowledge. In this
specialized environment, knowledge must be transmitted; it cannot merely
be encountered, since in most instances it has been taken out of its natural
habitat. It must also be processed, not only to clean it up but also to facilitate
transmission.

The transmission of knowledge through teaching, and it's processing to fit
school and school children, seems perfectly natural in a technological age,
which engineers a product to fit every human need. Once knowledge
becomes a product, the graded curriculum follows -- an ordered array of
packets of knowledge each with its time and space assignment, in proper
sequence and juxtaposed with related packages. The graded curriculum is
the fourth dimension of the school. As in the case of the other defining
characteristics, its quantitative aspects are critically important. Childhood
becomes a problem when extended over too many years and too many
aspects of life. Teaching becomes a problem when students begin to depend
upon it for most learning. Classroom attendance becomes a problem when it
builds sterile walls around too much of normal life. Similarly, curriculum
becomes a problem as it approaches international universality. How much
required attendance, classroom teaching and curriculum is tolerable is not a
matter for academic discussion. Free people, choosing freely as individuals
and in voluntary groups among an ample array of alternatives, can best make
these decisions.

Recent international achievement studies demonstrate quite clearly
that the universal international curriculum is now a
fact.\footnote{Torsten Hus\'en, \emph{International Study of
    Achievement in Mathematics}, 2 vols., Wiley, 1967.}
International norms for mathematics and science have been
established. These are admittedly the areas of greatest uniformity but
others are not far behind. Nor is the proliferation of vocational
schools, black studies and life-adjustment classes a significant
counter-trend. These auxiliary curricula are either tied to the core
curriculum, in terms of prerequisites and grade standards, or else the
degrees they lead to are meaningless in the market place.

The graded curriculum may be the most significant characteristic of the
school, especially in terms of the school's role in society. This, however, is
only because the curriculum is the keystone of a system based on
institutionalized childhood, teaching and classroom attendance. Curriculum
gives structure to these other elements, uniting them in a way, which
determines the unique impact of school on students, teachers and society.

In itself, the central idea of curriculum is both simple and inevitable.
Learning must occur in some sequence and there must also be some
correlation between different sequences of learning. These sequences and
correlations could, of course, be different for each individual. To some
extent they must be, and every educator pays lip service to this idea. Almost
no one, on the other hand, would insist upon avoiding all attempts to
correlate the learning programmes of different individuals.

For a teacher to impose a preferred order on his subject matter is natural
and desirable. To adjust this order to the needs of each individual student is
also desirable, but at some point may become self-defeating. It is also
desirable that teachers learn from each other and adjust their own order of
teaching accordingly. But imposing upon teachers an order not of their own
choosing is undesirable, and requiring students to follow a particular order,
except in deference to a particular teacher, is self-defeating. Only people
committed to the idea of a knowledge factory, which must run on a
prearranged schedule, will disagree. The argument that students and teachers
must be able to transfer from place to place without losing time is valid only
if the synchronized knowledge factory is assumed.

Synchronized learning requires, however, not merely a standard order
imposed on all students and teachers, but also the integration of the different
orders of the various subject matters. This integration of curricular sequence
creates the school system, which in turn constrains individual schools in all
of their major characteristics. Thus, the core curriculum of secondary
schools is dictated by standard requirements for college entrance. Since the
economic value of other curricula depends upon their relationship to the core
curriculum, this curriculum directly or indirectly determines hours of
attendance, classroom standards, teacher qualifications and entrance
requirements for the entire secondary-school system. Schools, which deviate
significantly from any of these norms, lose their accreditation and their
ability to qualify students for college entrance. Even primary-school reforms
can survive only if they do not threaten the progression of their graduates
through the higher reaches of the system.

It is by way of the standardized graded curriculum, therefore, that
schools become a system, which then acquires an international monopoly
of access to jobs and to political and other social roles. It can be
argued that this monopoly is not by any means complete. Some
corporations will still employ the unschooled genius, while Roosevelt
and Churchill did not have to pay more than lip service to the schools
they attended. But these are exceptions and, if the trend continues,
they will not exist for long.\footnote{Ivar Berg, \emph{Education and Jobs:
  The Great Training Robbery}, Praeger, 1970.} Organization by grade,
standardized intelligence and achievement testing, promotion within
the system and certification for employment, are all justified by a
curriculum which determines the internal structure and operations of a
school, relationships between schools, and relationships between
school and other institutions.

Would an educational institution lacking one of the defining
characteristics proposed above still be recognizable as a school?
Perhaps theoretically an institution based on children, teachers and
classrooms, but without curriculum, might be called a school but it
would have little resemblance to schools, as we know them, and could
more accurately be called a collection of training centers. Schools
without children can be imagined but adults would not, for long, put
up with teachers, classrooms, required attendance and
curriculum. Schools for children without teachers are hard to imagine,
while a child / teacher / curriculum combination appears intrinsically
unstable without the restraining influence of the classroom. Teachers
and children alone might work, but they would be hard to recognize as
an institution, let alone a school. It is easy, on the other hand, to
see children, teachers, classroom attendance and curriculum as created
by and for each other. It is also easy to see schools as a stable
element in a fully technological world.  Schools treat people and
knowledge the way a technological world treats everything: as if they
could be processed. Anything can, of course, be processed, but only at
a price, part of which involves ignoring certain aspects of the thing
and certain by-products of the process. The price of processing people
is intrinsically high. They tend to resist. What has to be left
unprocessed may be the most important part of the person. Some of the
by-products of educational processing are already evident. The
greatest danger, however, lies in the prospect of success. A
successfully processed humanity would lose the little control of its
destiny which has always distinguished man from the rest of the
world.\footnote{Richard Hoggart, \emph{The Uses of Literacy: Changing
  Patterns in English}, Mass Culture, Chatto \& Winduss 1957; Penguin,
  1958.}



\theendnotes
\setcounter{endnote}{0}