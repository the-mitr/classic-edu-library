% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Acknowledgements}

\vspace*{-5pt}
Portions of this book were first published in the following
publications: `Not Foreigners, yet Foreign' as `Puerto Ricans in New
York: Not Foreigners, yet Foreign', in \textit{Commonweal}, 1956; `The
Futility of Schooling' in Saturday Review, 1968; `Vanishing Clergyman'
in the \textit{Critic}, 1967; `The Seamy Side of Charity' as `To be
Perfectly Frank' and `Violence: A Mirror for Americans' from
\textit{America}, 21 January 1967, and 27 April 1968; `School: The
Sacred Cow' as `Commencement At the University of Puerto Rico' and
`Planned Poverty: The End Result of Technical Assistance' as
`Outwitting the ``Developed" Countries' in the \textit{New York Review
  of Books}; `A Constitution for Cultural Revolution' as `The need for
Cultural Revolution' in \textit{Great Books Today} 1970.


\NewPage

\pagestyle{frontm}


%\chapter*{Introduction}
\chapter{Introduction}


%\vspace*{-15pt}
There is no need for an introduction of the following papers or of
their author. If, nevertheless, Dr. Illich has honoured me by the
invitation to write such an introduction, and if I gladly accepted,
the reason in both our minds seems to be that this introduction offers
an occasion that permits clarifying the nature of a common attitude
and faith, in spite of the fact that some of our views differ
considerably. Even the author's own views today are not always the
same as those he held at the time he wrote these papers, on different
occasions, over the course of the years. But he has remained true to
himself in the very core of his approach and it is this core that we
share. 

It is not easy to find the proper word to describe this core. How can
a fundamental approach to life be caught in a concept without
distorting and twisting it? Yet since we need to communicate with
words, the most adequate - or rather, the least inadequate - term
seems to be ``humanist radicalism''.

What is meant by radicalism? What does \emph{humanist} radicalism
imply?

By radicalism I do not refer primarily to a certain set of ideas, but
rather to an attitude, to an ``approach'', as it were. To begin with
this approach can be characterized by the motto: \emph{de omnibus
dubitandum}; everything must be doubted, particularly the ideological
concepts which are virtually shared by everybody and have consequently
assumed the role of indubitable commonsensical axioms. 

To ``doubt'' in this sense does not imply a psychological state of
inability to arrive at decisions or convictions, as is the case in
obsessional doubt, but the readiness and capacity for critical
questioning of all assumptions and institutions which have become
idols under the name of common sense, logic, and what is supposed to
be ``natural''. This radical questioning is possible only if one does
not take the concepts of one's own society or even of an entire
historical period - like Western culture since the Renaissance - for
granted, and furthermore if one enlarges the scope of one's awareness
and penetrates into the unconscious aspects of one's thinking. Radical
doubt is an act of uncovering and discovering; it is the dawning of
the awareness that the Emperor is naked, and that his splendid
garments are nothing but the product of one's fantasy. 

Radical doubt means to question; it does not necessarily mean to
negate. It is easy to negate by simply positing the opposite of what
exists; radical doubt is dialectical in as much as it comprehends the
process of the unfolding of oppositions and aims at a new synthesis
which negates \emph{and} affirms. 

Radical doubt is a process; a process of liberation from idolatrous
thinking; a widening of awareness, of imaginative, creative vision of
our possibilities and options. The radical approach does not occur in
a vacuum. It does not start from nothing, but it starts from the
roots, and the root, as Marx once said, is man. But to say ``the root
is man'' is not meant in a positivistic, descriptive sense. When we
speak of man we speak of him not as a thing but as a process; we speak
of his potential for developing all his powers; those for greater
intensity of being, greater harmony, greater love, greater
awareness. We also speak of man with a potential to be corrupted, of
his power \emph{to} act being transformed into the passion for power
\emph{over} others, of his love of life degenerating into the
passion to destroy life.

Humanistic radicalism is radical questioning guided by insight into
the dynamics of man's nature; and by concern for man's growth and full
unfolding. In contrast to con temporary positivistic thinking it is
not ``objective'', if objectivity means theorizing without a
passionately held aim which impels and nourishes the process of
thinking . But it is exceedingly objective if it means that every step
in the process of thinking is based on critically sifted evidence, and
furthermore if it takes a critical attitude towards common-sensical
premises. All this means that humanist radicalism questions every idea
and every institution from the standpoint of whether it helps or
hinders man's capacity for greater aliveness and joy. This is not the
place to give lengthy examples for the kind of common-sensical
premises that are questioned by humanist radicalism. It is not
necessary to do so either, since Dr Illich's papers deal precisely
with such examples as the usefulness of compulsive schooling, or of
the present function of priests. 

Many more could be added, some of
which are implied in the author's papers. I want to mention only a few
like the modern concept of ``progress'', which means the principle of
ever-increasing production, consumption, time saving, maximal
efficiency and profit, and calculability of all economic activities
without regard to their effect on the quality of living and the
unfolding of man; or the dogma that increasing consumption makes man
happy, that the management of large-scale enterprises must necessarily
be bureaucratic and alienated; that the aim of life is having (and
using), not being; that reason resides in the intellect and is split
from the affective life; that the newer is always better than the
older; that radicalism is the negation of tradition; that the opposite
of `law and order' is lack of structure. In short, that the ideas .and
categories that have arisen during the development of modem science
and industrialism are superior to those of all former cultures and
indispensable for the progress of the human race. 

Humanistic radicalism questions all these premises and is not afraid
of arriving at ideas and solutions that may sound absurd. I see the
great value in the writings of Dr Illich precisely in the fact that
they represent humanistic radicalism in its fullest and most
imaginative aspect. The author is a man of rare courage, great
aliveness, extraordinary erudition and brilliance, and fertile
imaginativeness, whose whole thinking is based on his concern for
man's unfolding -- physically, spiritually and intellectually. The
importance of his thoughts in this as well as his other writings lies
in the fact that they have a liberating effect on the mind by showing
entirely new possibilities; they make the reader more alive because
they open the door that leads out of the prison of routinized,
sterile, preconceived notions. By the creative shock they communicate
-- except to those who react only with anger at so much nonsense -- they
help to stimulate energy and hope for a new beginning.
%\\%[0.3cm]
\begin{flushright}
\textit{Erich Fromm}
\end{flushright}

\NewPage