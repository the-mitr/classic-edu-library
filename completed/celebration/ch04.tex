% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{The Eloquence of Silence}

\begin{chapquot}
Five years on the streets of New York made me aware of the need for
some method of bringing native, New Yorkers to friendship with Puerto
Ricans. Ministers, teachers, social workers, all were submerged in a
Spanish-speaking crowd. They needed to learn the language, but even
more they needed to attune their ears and open their hearts to the
anguish of a people who were lonely) frightened and powerless. 

Quite evidently the mere study of Spanish was not enough. The man who
can construct sentences with words and grammar may be much further
from reality than he who knows that he does not speak a language. I
saw how intensely Puerto Ricans rejected the Americano who studied
them for the purpose of ``integrating them'' in the city. They even
refused to answer in Spanish, because behind his benevolence they
sensed the condescension and often the contempt. A programme was
needed to help native New Yorkers to enter into the spirit of
poverty. 

In 1956 I became Vice-Rector of the Catholic University of Puerto Rico
and this gave me a chance to prepare people for work in the Spanish
ghettos. We offered workshops combining the very intensive study of
spoken Spanish with field experience and with the academic study of
Puerto Rican poetry, history, songs and social reality. Many of my
students came at great personal sacrifice. More than half were
priests, mostly below the age of thirty-five. They bad decided to
spend their lives among the poor in the inner city. I t is now
difficult to remember how the Catholic clergy then felt about its
duty. It was hard to convince an Irish-American pastor to permit his
curate to spend his time on people who never came to church. The
Spanish language was a potent tool for curates who wanted to use their
time and the resources of the Church for work among the poor. Because
- presumably - the Spanish language identified those poor who were
born Catholics, and to whom the Church under no circumstances could
deny an equal share of its ministry. When seven years later the war on
poverty broke out, a substantial number of recognized leaders and
critics were these men who bad met each other in Puerto Rico. With
this group of students I could explore the deeper meaning involved in
the learning of a foreign language. In fact, I believe that properly
conducted language learning is one of the few occasions in which an
adult can go through a deep experience of poverty, of weakness, and of
dependence on the good will of another. Every evening we gathered for
an hour of silent prayer. At the beginning of the hour one of us would
offer points for meditation. The following is one of the sessions
recorded by a participant.
\end{chapquot}

The science of linguistics has brought into view new horizons in the
understanding of human communications. An objective study of the ways
in which meanings are transmitted bas shown that much more is relayed
from one man to another through and in silence than in words. Words
and sentences are composed of silences more meaningful than the
sounds. The pregnant pauses between sounds and utterances become
luminous points in an incredible void: as electrons in the atom, as
planets in the solar system. Language is as a cord of silence with
sounds the knots as nodes in a Peruvian \emph{quipu}, in which the
empty spaces speak. With Confucius we can see language as a wheel. The
spokes centralize, but the empty spaces make the wheel.

It is thus not so much the other man's words as his silences which we
have to learn in order to understand him. It is not so much our sounds
which give meaning, but it is through the pauses that we will make
ourselves understood. The learning of a language is more the learning
of its silences than of its sounds. Only the Christian believes in the
Word as coeternal Silence. Among men in time, rhythm is a law through
which our conversation becomes a \emph{yang-yin} of silence and
sound.

To learn a language in a human and mature way, therefore, is to accept
the responsibility for its silences and for its sounds. The gift a
people gives us in teaching us their language is more a gift of the
rhythm, the mode and the subtleties of its system of silences than of
its system of sounds. It is an intimate gift for which we are
accountable to the people who have entrusted us with their tongue. A
language of which I know only the words and not the pauses is a
continuous offence. It is as the caricature of a photographic
negative. 

It takes more time and effort and delicacy to learn the silence of a
people than to learn its sounds. Some people have a special gift for
this. Perhaps this explains why some missionaries, notwithstanding
their efforts, never come to speak properly, to communicate delicately
through silences. Although they ``speak with the accent of natives''
they remain forever thousands of miles away. The learning of the
grammar of silence is an art much more difficult to learn than the
grammar of sounds. 

As words must be learned by listening and by painful attempts at
imitation of a native speaker, so silences must be acquired through a
delicate openness to them. Silence has its pauses and hesitations, its
rhythms and expressions and inflections; its durations and pitches,
and times to be and not to be. Just as with our words, there is an
analogy between our silence with men and with God. To learn the full
meaning of one, we must practice and deepen the other.

First among the classification of silences is the Silence of the pure
listener, of womanly passivity; the silence through which the message
of the other becomes ``he in us'', the silence of, deep interest. It is
threatened by another silence - the silence of indifference, the
silence of disinterest which assumes that there is nothing I want or
can receive through the communication of the is the ominous silence of
the Wife who woodenly cloth. This listens to her husband relating the
little things he so earnestly wants to tell her. It is the silence of
t he Christian who reads the gospel with the attitude that he knows it
backwards and forwards. It is the silence of the stone-dead because it
is unrelated to life. It is the silence of the missionary who never
understood the miracle of a foreigner whose listening is a greater
testimony of love than that of another who speaks.

The man who shows us that he knows the rhythm of our silence is much
closer to us than one who thinks that he knows how to speak. The
greater the distance between the two worlds, the more this silence of
interest is a sign of love. It is easy for most Americans to listen to
chit-chat about football; but it is a sign of love for a Mid-westerner
to listen to the jai alai reports. The silence of the city priest on a
bus listening to the report of the sickness, of a goat is a gift, truly
the fruit of a missionary form of long training in patience.

There is no greater distance than that between a man in prayer and
God. Only when this distance dawns on consciousness can the grateful
silence of patient readiness develop. This must have been the silence
of the Virgin before the \emph{Ave} which enabled her to become the
eternal model of openness to the Word. Through her deep silence the
Word could take Flesh. 

In the prayer of silent listening, and nowhere else, c:an the
Christian acquire the habit of this first silence from which the Word
can be born in a foreign culture. This Word conceived in silence is
grown in silence too.

A second great class in the grammar of silence is the silence of the
Virgin after she conceived the Word -- the silence from . which not so
much the \emph{Fiat} as the \emph{Magnificat} was born. It is the
silence which nourishes the Word conceived rather than opening man to
conception. It is the silence which closes man in on himself to allow
him to prepare the Word for others. It is the silence of syntony; the
silence in which we await the proper moment for the Word to be born
into the world.

This silence too is threatened, not only by hurry and by desecration
of multiplicity of action, but by the habit of verbal confection and
mass production which has no time for it. It is threatened by the
silence of cheapness which means that one word is as good as another
and that words need no nursing. 

The missionary, or foreigner, who uses words as they are in the
dictionary does not know this silence. He is the man who looks up
English words in himself when he wants to find a Spanish equivalent,
rather than seeking the word which would syntonize; rather than
finding the word or gesture or silence which would be understood, even
if it has no equivalent in his own language or culture or background;
the man who does not give the seed of a new language time to grow on
the foreign soil of his soul. This is a silence \emph{before} words,
or \emph{between} them; the silence within which words live or
die. It is the silence of the slow prayer of hesitation; of prayer in
which words have the courage to swim in a sea of silence. It is
diametrically opposed to other form of silence before words -- the
silence of the artificial flower which serves as a remembrance of
words which do not grow, the pause in between repetition. I t is the
silence of the missionary who waits for the dispensation of the next
memorized platitude because he has not made the effort to penetrate
into the living language of others. The silence before words is also
opposed to the silence of brewing aggression which can hardly be
called silence -- this too an interval used for the preparation of
words, but words which divide rather than bring together. This is the
silence to which the missionary is tempted who clings to the idea that
in Spanish nothing means what he wants to say. It is the silence in
which one verbal aggression -- even though veiled -- prepares the
other. 

The next great class in the grammar of silence we will call the
silence \emph{beyond} words. The farther we go, the farther apart
does good and bad silence grow in each classification. We now have
reached the silence which does not prepare any further talk. It is the
silence which has said everything because there is nothing more to
say. This is the silence beyond a final \emph{yes} or a final
\emph{no}. This is the silence of love beyond words, as well as the
silence of \emph{no}, forever; the silence of heaven or of hell. It is the
definite attitude of a man who faces the Word which is Silence, or the
silence of a man who has obstinately turned away from Him.

Hell is this silence, deadly silence . Death in this silence is
neither the deadness of a stone, indifferent to life, nor the deadness
of a pressed flower, memory of life. It is the death after life a
final refusal to live. There can be noise and agitation and many words
in this silence. It has only one meaning which is common to the noises
it makes and the gaps between them. \emph{No}. 

There is a way in which this silence of hell threatens missionary
existence. In fact with the unusual possibilities of witnessing
through silence, unusual ability to destroy through it are open to the
man charged with the Word in a world not his own. Missionary silence
risks more: it risks becoming a hell on earth. 

Ultimately, missionary silence is a gift, a gift of prayer -- learned
in prayer by one infinitely distant, infinitely foreign and
experienced in love for men, much more distant and foreign ever than
men at home. The missionary can come to forget that his silence is a
gift, a gift in its deepest sense gratuitously given, a gift
concretely transmitted to us by those who are willing to teach us
their language. If the missionary forgets this and attempts to conquer
by his own power that which only others can bestow, then his existence
begins to be threatened. The man who tries to buy the language like a
suit, the man who tries to conquer the language through grammar so as
to speak it ``better than the natives around here'', the man who forgets
the analogy of the silence of God and the silence of others and does
not seek its growth in prayer, is a man who tries basically to rape
the culture into which he is sent, and he must expect the
corresponding reactions. If he is human at all he will realize that he
is in a spiritual prison, but he will not admit that he has built it
around himself rather he will accuse others of being his jailers. The
wall between himself and those to whom he was sent will become ever
more impenetrable. As long as he sees himself as ``missionary'' he will
know that he is frustrated; that he was sent but got nowhere; that he
is away from home but has never landed anywhere; that he left his
home and never reached another. 

He continues to preach and is ever more aware that he is not
understood, because he says what he thinks and speaks in a foreign
farce of his own language. He continues to ``do things for people'' and
considers them ungrateful because they understand that he does these
things to bolster his ego. His words be the silence of come a mockery
of language, an expression of death. 

It requires much courage at this point to return to the patient
silence of interest or to the delicacy of the silence within which
words grow. Out of numbness, muteness has grown. Often out of the fear
of facing the difficulty late in life of trying again to learn a
language, a habit of despair is born. The silence of hell -- a typically
missionary version of it has been born in his heart. 

At the pole opposed to despair there is the silence of love, the
holding of hands of the lovers. The prayer in which the vagueness
before words has given place to the pure emptiness after them. The
form of communication which opens the simple depth of the soul. It
comes in flashes and it can become a lifetime -- in prayer just as much
as with people. Perhaps it is the only truly universal aspect of
language, the only means of communication which was not touched by the
curse of Babel. Perhaps it is the one way of being together with
others and with the Word in which we have no more foreign accent.

There is still another silence beyond words, but the silence of the
Piet\'a. It is not a silence of death but the silence of the mystery of
death. It is not the silence of active acceptance of the will of God
out of which the \emph{Fiat} is born nor the silence of manly
acceptance of Gethsemane in which obedience has its roots. The silence
you as missionaries seek to acquire in this Spanish course is the
silence beyond bewilderment and questions; it is a silence beyond the
possibility of an answer, or even reference to a word which
preceded. It is the mysterious silence through which the Lord could
descend into the silence of hell, the acceptance without frustration
of a life, useless and wasted on Judas, a silence of freely willed
powerlessness through which the world was saved. Born to redeem the
world, Mary's Son had died at the hands of His people, abandoned by
His friends and betrayed by Judas whom He loved but could not save --
silent contemplation of the culminating paradox of the Incarnation
which was useless for the redemption of at least one personal
friend. The opening of the soul to this ultimate silence of the Piet\'a
is the culmination of the slow maturing of the three previous forms of
missionary silence.











