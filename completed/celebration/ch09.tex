% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{School: The Sacred Cow}

\begin{chapquot}
Only if we understand the school system as the central myth-making
ritual of industrial societies can we explain the deep need for it,
the complex myth surrounding it, and the inextricable way in which
schooling is tied into the self-image of contemporary man. A
graduation speech at the University of Puerto Rico in Rio Piedras
provided me with an opportunity to probe this relationship.
\end{chapquot}

This is a time of crisis in the institution of the school, a crisis
which may mark the end of the ``age of schooling'' in the Western
world. I speak of the ``age of schooling'' in the sense in which we are
accustomed to speak of the ``feudal age'' or of the ``Christian
era''. The ``age of schooling'' began about two hundred years
ago. Gradually the idea grew that schooling was a necessary means of
becoming a useful member of society. It is the task of this generation
to bury that myth. 

Your own situation is paradoxical. At the end and as a result of your
studies, you are enabled to see that the education your children
deserve, and will demand, requires a revolution in the school system
of which you are a product. 

The graduation rite that we solemnly celebrate today confirms the
prerogatives which Puerto Rican society, by means of a costly system
of subsidized public schools, confers upon the sons and daughters of
its most privileged citizens. You are part of the most privileged 10
per cent of your generation, part of that minuscule group which has
completed university studies. Public investment in each of you is
fifteen times the educational investment in the average member of the
poorest 10 per cent of the population, who drops out of school before
completing the fifth grade. 

The certificate you receive today attests to the legitimacy of your
competence. It is not available to the self-educated, to those who
have acquired competence by means not officially recognized in Puerto
Rico. The programmes of the University of Puerto Rico are all duly
accredited by the Middle States Association of Colleges and Secondary
Schools. 

The degree which the university today confers upon you
implies that over the last sixteen years or more your elders have
obliged you to submit yourselves, voluntarily or involuntarily, to the
discipline of this complex scholastic rite. You have in fact been
daily attendants, five days a week, nine months a year, within the
sacred precinct of the school and have continued such attendance year
after year, usually without interruption. Governmental and industrial
employees and the professional associations have good reasons to
believe that you will not subvert the order to which you have
faithfully submitted in the course of completing your ``rites of
initiation''.

Much of your youth has been spent within the custody of the school. It
is expected that you will now go forth to work, to guarantee to future
generations the privileges conferred upon you. 

Puerto Rico is the only society in the Western hemisphere to devote 30
per cent of its governmental budget to education. It is one of six
places in the world which devote between 6 and 7 per cent of national
income to education. The schools of Puerto Rico cost more and provide
more employment than any other public sector. In no other social
activity is so large a proportion of the total population of Puerto
Rico involved. 

A huge number of people are observing this occasion on television. Its
solemnity will, on the one hand, confirm their sense of educational
inferiority, and on the other, raise their hopes, largely doomed to
disappointment, of one day themselves receiving a university
degree. 

Puerto Rico has been schooled. I don't say educated but, rather,
schooled. Puerto Ricans can no longer conceive of life without
reference to the school. The desire for education has actually given
way to the compulsion of schooling. Puerto Rico has adopted a new
religion. Its doctrine is that education is a product of the school, a
product which can be defined by numbers. There are the numbers which
indicate how many years a student has spent under the tutelage of
teachers, and others which represent the proportion of his correct
answers in an examination. Upon the receipt of a diploma the
educational product acquires a market value. School attendance in
itself thus guarantees inclusion in the membership of disciplined
consumers of the technocracy -- just as in past times church attendance
guaranteed membership in the community of. saints. From governor to
\emph{jibaro}, Puerto Rico now accepts the ideology of its teachers
as it once accepted the theology of its priests. The school is now
identified with education as the Church once was with
religion. 

Today's agencies of accreditation are reminiscent of the royal
patronage formerly accorded the Church. Federal support of education
now parallels yesterday's royal donations to the Church. The power of
the diploma has grown so rapidly in Puerto Rico that the poor blame
their misery on precisely the lack of that which assures to you,
today's graduates, participation in society's privileges and
powers. 

Research shows that twice as many high-school graduates in Puerto Rico
as in the States want to pursue university studies; while the
probability of graduating from college for the Puerto Rican
high-school graduate is much lower than it would be in the
States. This widening discrepancy between aspirations and resources
can result only in a deepening frustration among the inhabitants of
the Island.

The later a Puerto Rican child drops out of school the more keenly
does he feel his failure. Contrary to popular opinion, increasing
emphasis on schooling bas actually increased class conflict in Puerto
Rico, and has also increased the sense of inferiority which Puerto
Ricans suffer in relation to the United States. 

Upon your generation falls the obligation of developing for Puerto
Rico an educational process radically different from that of the
present and independent of the example of other societies. It is yours
to question whether Puerto Rico really wants to transform itself
irrevocably into a passive product of the teaching profession. It is
yours to decide whether you will subject your children to a school
that seeks respectability in North American accreditation, its
justification in the qualification of the labour force, and its
function in permitting the.children of the middle class to keep up
with the Joneses of Westchester County, New York. 

The real sacred cow in Puerto Rico is the school. Proponents of
commonwealth, statehood and independence all take it for
granted. Actually, none of these political alternatives can liberate a
Puerto Rico which continues to put its primary faith in
schooling. Thus, if this generation wants the true liberation of
Puerto Rico, it will have to invent educational alternatives which put
an end to the ``age of schooling''. This will be a difficult
task. Schooling has developed a formidable folklore. The begowned
academic professors whom we have witnessed today evoke the ancient
procession of clerics and little angels on the day of Corpus
Christi. The Church, holy, catholic, apostolic, is rivalled by the
school, accredited, compulsory, untouchable, universal. Alma Mater has
replaced Mother Church. The power of the school to rescue the denizen
of the slum is as the power of the Church to save the Muslim Moor from
hell. (Gehenna meant both slum and hell in Hebrew.) The difference
between Church and school is mainly that the rites of the school have
now become much more rigorous and onerous than were the rites of the
Church in the worst days of the Spanish Inquisition. 

The school has become the established Church of secular times. The
modern school bad its origins in the impulse towards universal
schooling, which began two centuries ago as an attempt to incorporate
everyone into the industrial state. In the industrial metropolis the
school was the integrating institution. In the colonies the school
inculcated the dominant classes with the values of the imperial power
and confirmed in the masses their sense of inferiority to this
schooled elite. Neither the nation nor the industry of the
pre-cybernetic era can be imagined without universal baptism into the
school. The dropout of this era corresponds to the lapsed marrano of
eleventh-century Spain. 

We have, I hope, outlived the era of the industrial state. We shall
not live long, in any case, if we do not replace the anachronism of
national sovereignty, industrial autarchy and cultural narcissism --
which are combined into a stew of leftovers by the schools. Only
within their sacred precincts could such old potage be served to young
Puerto Ricans. 

I hope that your grandchildren will live in an Island where the
majority give as little importance to attending class as is now given
to attending the Mass. We are still far from this day and I hope that
you will take the responsibility for bringing it to pass without fear
of being damned as heretics, subversives or ungrateful creatures. It
may comfort you to know that those who undertake the same
responsibility in socialist lands will be similarly denounced. 

Many controversies divide our Puerto Rican society. National resources
are threatened by industrialization, the cultural heritage is
adulterated by commercialization, dignity is subverted by publicity,
imagination by the violence which characterizes the mass media. Each
of these is a theme for extensive public debate. There are those who
want less industry, less English and less Coca-Cola, and those who
want more. All agree that Puerto Rico needs many more schools.

This is not to say that education is not discussed in Puerto
Rico. Quite the contrary. It would be difficult to find a society
whose political and industrial leaders are as concerned with
education. They all want more education, directed towards the sector
which they represent. These controversies merely serve, however, to
strengthen public opinion in the scholastic ideology which reduces
education to a combination of classrooms, curricula, funds,
examinations and grades. 

I expect that by the end of this century, what we now call school
will be a historical relic, developed in the time of the railroad and
the private automobile and discarded along with them. I feel sure that
it will soon be evident that the school is as marginal to education as
the witch doctor is to public health. 

A divorce of education from schooling is, in my opinion, already on
the way, speeded by three forces: the Third World, the ghettos and the
universities. Among the nations of the Third World, schooling
discriminates against the majority and disqualifies the
self-educated. Many members of the ``black'' ghettos see the schools as
a ``whitening'' agent. Protesting university students tell us that
school bores them and stands between them and reality. These are
caricatures no doubt, but the mythology of schooling makes it
difficult to perceive the underlying realities. 

The criticism today's students are making of their teachers is as
fundamental as that which their grandfathers made of the clergy. The
divorce of education from schooling has its model in the
demythologizing of the church. We fight now, in the name of education,
against a teaching profession which unwillingly constitutes an
economic interest, as in times past the reformers fought against a
clergy which was, often unwillingly, a part of the ancient power
elite. Participation in a ``production system'', of no matter what kind,
has always threatened the prophetic function of the Church as it now
threatens the educational function of the school.

School protest has deeper causes than the pretexts enunciated by its
leaders. These, although frequently political, are expressed as
demands for various reforms of the system. They would never have
gained mass support. however, if students had not lost faith and
respect in the institution which nurtured them. Student strikes
reflect a profound intuition widely shared among the younger
generation: the intuition that schooling has vulgarized education,
that the school has become anti-educational and antisocial, as in
other epochs the Church has become anti-Christian or Israel
idolatrous. This intuition can, I believe, be explicitly and briefly
formulated. 

The protest of some students today is analogous to the dissidence of
those charismatic leaders without whom the Church would never have
been reformed: their prophecies led to martyrdom, their theological
insights to their persecutions as heretics, their saintly activity
often led to the stake. The prophet is always accused of subversion,
the theologian of irreverance and the saint is written off as crazy.

The Church has always depended for its vitality upon the sensitivity
of its bishops to the appeals of the faithful, who see the rigidity of
the ritual as an obstacle to their faith. The churches, incapable of
dialogue between their ruling clerics and their dissidents, have
become museum pieces, and this could easily happen with the school
system of today. It is easier for the university to attribute
dissidence to ephemeral causes than to attribute this dissidence to a
profound alienation of the students from the school. It is also
easier for student leaders to operate with political slogans than to
launch basic attacks upon sacred cows. The university that accepts the
challenge of its dissident students and helps them to formulate in a
rational and coherent manner the anxiety they feel because they are
rejecting schooling exposes itself to the danger of being ridiculed
for its supposed credulity. The student leader who tries to promote in
his companions the consciousness of a profound aversion to their school
(not to education itself) finds that he creates a level of anxiety
which few of his followers care to face.

The university has to learn to distinguish between sterile criticism
of scholastic authority and a call for the conversion of the school to
the educational purposes for which it was founded, between destructive
fury and the demand for radically new forms of education -- scarcely
conceivable by minds formed in the scholastic tradition; between, on
the one hand, cynicism which seeks new benefits for the already
privileged and, on the other, Socratic sarcasm, which questions the
educational efficacy of accepted forms of instruction in which the
institution is investing its major resources. It is necessary, in
other words, to distinguish between the alienated mob and profound
protest based . on rejection of the school as a symbol of the \textit{status
quo}. 

In no other place in Latin America has investment in education, demand
for education, and information about education, increased so rapidly
as in Puerto Rico. There is no place, therefore, in which members of
your generation could begin the search for a new style of public
education so readily as in Puerto Rico. It is up to you to get us
back, recognizing that the generations which preceded you were misled
in their efforts to achieve social equality by means of universal
compulsory schooling. 

In Puerto Rico three of every ten students drop out of school before
finishing the sixth grade. This means that only one of every two
children, from families with less than the median income, completes
the elementary school. Thus half of all Puerto Rican parents are under
a sad illusion if they believe that their children have more than an
outside chance of entering the university. 

Public funds for education go directly to the schools, without
students having any control of them. The political justification for
this practice is that it gives everyone equal access to the
classroom. However, the high cost of this type of education, dictated
by educators trained largely outside Puerto Rico, makes a public lie
of the concept of equal access. Public schools may benefit all of the
teachers but benefit mainly the few students who reach the upper
levels of the system. It is precisely our insistence on direct
financing of the ``free school'' that causes this concentration of scarce
resources on benefits for the children of the few. 

I believe that every Puerto Rican has the right to receive an equal
part of the educational budget. This is something very different and
much more concrete than the mere promise of a place in the school. I
believe, for example, that a young thirteen year old who has had only
four years of schooling has much more right to the remaining
educational resources than students of the same age who have had eight
years of schooling. The more ``disadvantaged'' a citizen is, the more he
needs a guarantee of his right. 

If in Puerto Rico it were decided to honour this right, then the free
school would immediately have to be abandoned. The annual quota of each
person of school age would obviously not support a year of schooling, at
present costs. The insufficiency would, of course, be even more
dramatic if the total educational budget for all levels were divided
among the population from six to twenty-five years of age, the period
between kindergarten and graduate studies, to which all Puerto Ricans
supposedly have free access. 

These facts leave us three options: leave the system as it is, at the
cost of justice and conscience; use the available funds exclusively to
assure free schooling to children whose parents earn less than the
median income; or use the available public resources to offer to all
the education that an equal share of these resources could assure to
each. The better-off could, of course, supplement this amount and
might continue to offer their children the doubtful privilege of
participating in the process which you are completing today. The poor
would certainly use their share to acquire an education more
efficiently and at lower cost. 

The same choices apply, \textit{a fortiori}, to other parts of Latin
America where frequently not more than twenty dollars a year in public
funds would be available for each child if the 20 per cent of tax
receipts now destined for education were distributed equally to all
children who should be in school under existing laws. This amount
could never pay for a year of conventional schooling. It would
however be enough to provide a good many children and adults with one
month of intensive education year after year. It would also be enough
to finance the distribution of educational games leading to skills
with numbers, letters and logical symbols. And to sponsor successive
periods of intensive apprenticeship. In North-East Brazil, Paulo
Freire (who was forced to leave the country) showed us that with a
single investment of this amount he was able to educate 25 per cent of
an illiterate population to the point where they could do functional
reading. But this, as he made clear, was only possible when his
literacy programme could focus on the key words that are politically
controversial within a community.

My suggestions may mortify many. But it is from the great positivists
and liberals that we inherited the principle of using public funds for
the administration of schools directed by professional educators; just
as, previously, tithes had been given to the Church to be administered
by priests. It remains for you to fight the free public school in the
name of true equality of educational opportunity. I admire the
courage of those of you willing to enter this fight. 

Youth wants educational institutions that provide them with
education. They neither want nor need to be mothered, to be
certified or to be indoctrinated. It is difficult, obviously, to get
an education from a school that refuses to educate without requiring
that its students submit simultaneously to custodial care, sterile
competition and indoctrination. It is difficult, obviously, to finance
a teacher who is at the same time regarded as guardian, umpire,
counsellor and curriculum manager. It is uneconomical to combine these
functions in one institution. It is precisely the fusion of these four
functions, frequently antithetical, which raises the cost of education
acquired in school. This is also the source of our chronic shortage
of educational resources. It is up to you to create institutions that
offer education to all at a cost within the limits of public
resources. 

Only when Puerto Rico has psychologically outgrown the school will it
be able to finance education for all, and only then will truly
efficient, non-scholastic forms of education find
acceptance. Meanwhile, these new forms of education will have to be
designed as provisional means of compensating for the failures of the
schools. In order to create new forms of education, we will have to
demonstrate alternatives to the school that offer preferable options
to students, teachers and taxpayers.

There is no intrinsic reason why the education that schools are now
failing to provide could not be acquired more successfully in the
setting of the family, of work and communal activity, in new kinds of
libraries and other centres that would provide the means of
learning. But the institutional forms that education will take in
tomorrow's society cannot be clearly visualized. Neither could any of
the great reformers anticipate concretely the institutional styles
that would result from their reforms. The fear that new institutions
will be imperfect, in their turn, does not justify our servile
acceptance of present ones. 

This plea to imagine a Puerto Rico without schools must, for many of
you, come as a surprise. It is precisely for surprise that true
education prepares us. The purpose of public education should be no
less fundamental than the purpose of the Church, although the purpose
of the latter is more explicit. The basic purpose of public education
should be to create a situation in which society obliges each
individual to take stock of himself and his poverty. Education implies
a growth of an independent sense of life and a relatedness which go
band in hand with increased access to, and use of, memories stored in
the human community. The educational institution provides the focus
for this process. This presupposes a place within the society in which
each of us is awakened by surprise; a place of encounter in which
others surprise me with their liberty and make me aware of my own. The
university itself, if it is to be worthy of its traditions, must be an
institution whose purposes are identified with the exercise of
liberty, whose autonomy is based on public confidence in the use of
that liberty. 

My friends, it is your task to surprise yourselves, and us, with the
education you succeed in inventing for your children. Our hope of
salvation lies in our being surprised by the Other. Let us learn
always to receive further surprises. I decided long ago to hope for
surprises until the final act of my life -- that is to say, in death
itself.
