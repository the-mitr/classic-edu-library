% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter[A Constitution for Cultural Revolution]{A Constitution for Cultural Revolution}

\begin{chapquot}
At the invitation of the publishers of the annual \emph{Great Ideas}
volume I wrote the article which follows, and which I feel is an
appropriate conclusion to this book. Its purpose was to initiate
discussion about the need of constitutional principles which would
guarantee an ongoing cultural revolution in a technological
society. The article originally appeared in \emph{Great Ideas Today}
1970, published by Encyclopaedia Britannica, Inc.
\end{chapquot}

During the decade just past we have got used to seeing the world
divided into two parts: the developed and the underdeveloped. People
in the development business may prefer to speak of the developed
nations and the less developed or developing nations. This terminology
suggests that development is both good and inevitable. Others,
especially protagonists of revolutionary change, speak of the ``Third
World'' and wait for the day when the wretched of the earth will rise
in armed revolt against the imperialist powers and shift control over
existing institutions from North to South, from White to Black, from
metropolis to colony. 

A vulgar example of the first assumption is the Rockefeller Report on
the Americas. Its doctrine is aptly summed up by President Nixon:
``This I pledge to you tonight: the nation that went to the moon in
peace for all mankind is ready to share its technology in peace with
its nearest neighbours.''  The governor, in turn, proposes that keeping
the pledge might require a lot of additional weaponry in South
America.

The Pearson Report on partnership in development is a much more
sophisticated example of the development mentality. It outlines
policies which will permit a few more countries to join the charmed
circle of the consumer nations but which will actually increase the
poverty of the poor half in these same countries: because the
strategies proposed will sell them ever more thoroughly on goods and
services ever more expensive and out of their reach. The policy goals
of most revolutionary movements and governments I know -- and I do not
know Mao's China reflect another type of cynicism. Their leaders make
futile promises that -- once in power for a sufficient length of time --
more of everything which the masses have learned to know and to envy
as privileges of the rich will be produced and distributed. Both the
purveyors of development and the preachers of revolution advocate more
of the same. They define more education as more schooling, better
health as more doctors, higher mobility as more high-speed
vehicles. The salesmen for United States industry, the experts for the
World Bank and ideologues of power for the poor seem to forget that
heart surgery and college degrees remain beyond the reach of the
majority for generations.

The goals of development are always and everywhere stated in terms of
consumer value packages standardized around the North Atlantic -- and
therefore always and everywhere imply more privileges for a
few. Political reorganization cannot change this fact; it can only
rationalize it. Different ideologies create different minorities of
privileged consumers, but heart surgery or a university education is
always priced out of range for all but a few: be they the rich, the
orthodox or the most fascinating subjects for experiments by surgeons
or pedagogues. 

Underdevelopment is the result of a state of mind common to both
socialist and capitalist countries. Present development goals are
neither desirable nor reasonable. Unfortunately anti-imperialism is no
antidote. Although exploitation of poor countries is an undeniable
reality, current nationalism is merely the affirmation of the right
of colonial elites to repeat history and follow the road travelled by
the rich towards the universal consumption of internationally marketed
packages, a road which can ultimately lead only to universal
pollution and universal frustration. 

The central issue of our time remains the fact that the rich are
getting richer and the poor, poorer. This hard fact is often obscured
by another apparently contradictory fact. In the rich countries the
poor expect a quantity and quality of commodities beyond the dreams of
Louis XIV, while many of the so-called developing countries enjoy much
higher economic growth rates than those of industralized countries at
a similar stage of their own histories. From icebox to toilet and from
antibiotic to television, conveniences are found necessary in Harlem
which Washington could not have imagined at Mount Vernon, just as
Bol\'ivar could not have foreseen the social polarization now
inevitable in Caracas. But neither rising levels of minimum
consumption in the rich countries nor of urban consumption in the poor
countries can close the gap between rich and poor nations or between
the rich and poor of anyone nation. Modern poverty is a by-product of
a world market catering to the ideologies of an industrial middle
class. Modern poverty is built into an international community where
demand is engineered through publicity to stimulate the production of
standard commodities. In such a market, expectations are standardized
and must always out-race marketable resources.

In the United States, for all its gargantuan prosperity, real poverty
levels rise faster than the median income. In the capital starved
countries, median incomes move rapidly away from rising averages. Most
goods now produced for rich and poor alike in the United States are
beyond the reach of all but a few in other areas. In both rich and
poor nations consumption is polarized while expectation is
equalized. 

During the decade now beginning we must learn a new language, a
language that speaks not of development and underdevelopment but of
true and false ideas about man, his needs and his
potential. Development programmes all over the world progressively
lead to violence, either in the form of repression or of
rebellion. This is neither due to the evil intentions of capitalists
nor to the ideological rigidity of communists, but to the radical
inability of men to tolerate the by-products of industrial and welfare
institutions developed in the early industrial age. In the late 1960s
attention has suddenly been drawn to the inability of man to survive
his industry. During the late 1960s it has become evident that less
than 10 per cent of the human race consumes more than 50 per cent of
the world's resources, and produces 90 per cent of the physical
pollution which threatens to extinguish the biosphere. But this is
only one aspect of the paradox of present development. During the
early 1970s it will become equally clear that welfare institutions
have an analogous regressive effect. The international
institutionalization of social service, medicine and education which
is generally identified with development has equally overwhelming
destructive by-products.

We need an alternative programme, an alternative both to development
and to merely political revolution. Let me call this alternative
programme either institutional or cultural revolution, because its aim
is the transformation of both public and personal reality. The
political revolutionary wants to improve existing institutions -- their
productivity and the quality and distribution of their products. His
vision of what is desirable and possible is based on consumption
habits developed during the last hundred years. The cultural
revolutionary believes that these habits have radically distorted our
view of what human beings can have and want. He questions the reality
that others take for granted, a reality that, in his view, is the
artificial by-product of contemporary institutions, created and
reinforced by them in pursuit of their short-term ends. The political
revolutionary concentrates on schooling and tooling for the
environment that the rich countries, socialist or capitalist, have
engineered. The cultural revolutionary risks the future on the
educability of man. 

The cultural revolutionary must not only be distinguished from the
political magician but also from both the neo-Luddite and the promoter
of intermediary technology. The former behaves as if the noble savage
could either be restored to the throne or the Third World transformed
into a reservation for him. He opposes the internal combustion engine
rather than opposing its packaging into a product designed for
exclusive use by the man who owns it. Thus the Luddite blames the
producer; the institutional revolutionary tries to reshape the design
and distribution of the product. The Luddite blames the machine; the
cultural revolutionary heightens awareness that it produces needless
demands. The cultural revolutionary must also be distinguished from
the promoter of intermediary technology who is frequently merely a
superior tactician paving the road to totally manipulated
consumption. 

Let me illustrate what I mean by a cultural revolution within one
major international institution, by taking as an example the
institution which currently produces education. I mean, of course,
obligatory schooling: full-time attendance of age-specific groups at a
graded curriculum. 

Latin America has decided to school itself into development. This
decision results in the production of homemade inferiority. With every
school that is built, another seed of institutional corruption is
planted, and this is in the name of growth. 

Schools affect individuals and characterize nations. Individuals
merely get a bad deal; nations are irreversibly degraded when they
build schools to help their citizens play at international
competition. For the individual, school is always a gamble. The
chances may be very slim, but everyone shoots for the same jackpot. Of
course, as any professional gambler knows, it is the rich who win in
the end and the poor who get hooked. And if the poor man manages to
stay in the game for a while, he will feel the pain even more sharply
when he does lose, as he almost inevitably must. Primary-school
dropouts in a Latin American it increasingly difficult to get
industrial jobs. 

But no matter how high the odds, everyone plays the game, for after
all, there is only one game in town. A scholarship may be a long shot,
but it is a chance to become equal to the world's best trained
bureaucrats. And the student who fails can console himself with the
knowledge that the cards were stacked against him from the
outset. 

More and more, men begin to believe that, in the schooling game, the
loser gets only what he deserves. The belief in the ability of schools
to label people correctly is already so strong that people accept
their vocational and marital fate with a gambler's resignation. In
cities, this faith in school-slotting is on the way to sprouting a
more creditable meritocracy -- a state of mind in which each citizen
believes that he deserves the place assigned to him by school. A
perfect meritocracy, in which there would be no excuses, is not yet
upon us, and I believe it can be avoided. It must be avoided, since a
perfect meritocracy would not only be hellish, it would be
hell. 

Educators appeal to the gambling instinct of the entire population
when they raise money for schools. They advertise the jackpot without
mentioning the odds. And those odds are high indeed for someone who
is born brown, poor or in the pampa. In Latin America, no country is
prouder of its legally obligatory admission-free school system than
Argentina. Yet only one Argentinian of five thousand born into the
lower half of the population gets as far as the university. 

What is only a wheel of fortune for an individual is a spinning wheel
of irreversible underdevelopment for a nation. The high cost of
schooling turns education into a scarce resource, as poor countries
accept that a certain number of years in school makes an educated
man. More money gets spent on fewer people. In poor countries, the
school pyramid of the rich countries takes on the shape of an obelisk,
or a rocket. School inevitably gives individuals who attend it an d
then drop out, as well as those who don't make it at all, a rationale
for their own inferiority. But for poor nations, obligatory schooling
is a monument to self inflicted inferiority. To buy the schooling
hoax is to purchase a ticket for the back seat in a bus headed
nowhere.

Schooling encrusts the poorest nations at the bottom of the city find
educational bucket. The school systems of Latin America are fossilized
deposits of a dream begun a century ago. The school pyramid is
a-building from top to bottom throughout Latin America. All countries
spend more than 20 per cent of their national budget and nearly 5 per
cent of their gross national product on its construction. Teachers
constitute the largest profession and their children are frequently
the largest group of students in the upper grades. Fundamental
education is either redefined as the foundation for schooling, and
therefore placed beyond the reach of the unschooled and the early
dropout, or is defined as a remedy for the unschooled, which will only
frustrate him into accepting inferiority. Even the poorest countries
continue to spend disproportionate sums on graduate schools -- gardens
which ornament the penthouses of skyscrapers built in a slum. 

Bolivia is well on the way to suicide by an overdose of
schooling. This impoverished, landlocked country creates papiermache
bridges to prosperity by spending more than a third of its entire
budget on public education and half as much again on private
schools. A full half of this educational misspending is consumed by 1
per cent of the school-age population. In Bolivia, the university
student's share of public funds is a thousand times greater than that
of his fellow citizen of median income. Most Bolivian people live
outside the city, yet only 2 per cent of the rural population makes it
to the fifth grade. This discrimination was legally sanctioned in
1967 by declaring grade school obligatory for all- a law that made
most people criminal by fiat and the rest immoral exploiters by
decree. In 1970, the university entrance examinations were abolished
with a flourish of egalitarian rhetoric. At first glance, it does seem
a libertarian advance to legislate that all high school graduates have
a right to enter the university -- until you realize that less than 2
per cent of Bolivians finish high school. 

Bolivia may be an extreme example of schooling in Latin America. But
on an international scale, Bolivia \emph{is} typical. Few African or Asian
countries have attained the progress now taken for granted there. 

Cuba is perhaps an example of the other extreme. Fidel Castro has tried to
create a major cultural revolution. He has reshaped the academic
pyramid, and promised that by 1980 the universities can be closed,
since all of Cuba will be one big university with higher learning
going on at work and leisure. Yet the Cuban pyramid is still a
pyramid. There is no doubt that the redistribution of privilege, the
redefinition of social goals and the popular participation in the
achievement of these goals have reached spectacular heights in Cuba
since the revolution. For the moment, however, Cuba is showing only
that, under exceptional political conditions) the base of the present
school system can be expanded exceptionally. But there are built-in
limits to the elasticity of present institutions, and Cuba is at the
point of reaching them. The Cuban revolution will work -- within the
set limits. Which means only that Dr Castro will have masterminded a
faster road to a bourgeois meritocracy than those previously taken by
capitalists or bolsheviks. Sometimes, when he is not promising schools
for all, Fidel hints at a policy of deschooling for all, and the Isle
of Pines seems to be a laboratory for redistribution of educational
functions to other social institutions. But unless Cuban educators
admit that work-education which is effective in a rural economy can be
even more effective in an urban one, Cuba's institutional revolution
will not begin. No cultural revolution can be built on the denial of
reality.

As long as communist Cuba continues to promise obligatory high-school
completion by the end of this decade, it is, in this regard,
institutionally no more promising than fascist Brazil, which has made
a similar promise. In both Brazil and Cuba, enough girls have already
been born to double the number of potential mothers in the 1980s. Per
capita resources available for education can hardly be expected to
double in either country, and even if they could, no progress would
have been made at all. In development-mad Brazil and in humanist Cuba,
waiting for Godot is equally futile. Without a radical change in their
institutional goals, both ``revolutions'' must make fools of
themselves. Unfortunately, both seem headed for manifest foolishness,
albeit by different routes. The Cubans allow work, party and community
involvement to nibble away at the school year, and call this radical
education, while the Brazilians let United States experts peddle
teaching devices that only raise the per capita cost of classroom
attendance. 

The production of inferiority through schooling is more evident in
poor countries and perhaps more painful rich countries. The 10 per
cent in the United States with the highest incomes can provide most of
the education for their children through private institutions. Yet
they also succeed m obtaining ten times more of the public resources
devoted to education than the poorest 10 percent of the population. In
Soviet Russia a more puritanical belief in meritocracy makes the
concentration of schooling privileges on the children of urban
professionals even more painful. 

In the shadow of each national school-pyramid, an international caste
system is wedded to an international class structure. Countries are
ranged like castes, whose educational dignity is determined by the
average years of schooling of its citizens. Individual citizens of all
countries achieve a symbolic mobility through a class system which
makes each man accept the place he believes to have merited. 

The political revolutionary strengthens the demand for schooling by
futilely promising that under his administration more learning and
increased earning will become available to all through more
schooling. He contributes to the modernization of a world class
structure and a modernization of poverty. It remains the task of
the cultural revolutionary to overcome the delusions on which the
support of school is based and to outline policies for the radical
deschooling of society. 

The basic reason for all this is that schooling comes in quanta. Less
than so much is no good and the minimum quantum carries a minimum
price. It is obvious that with schools of equal quality a poor child
can never catch up with a rich one, nor a poor country with a rich
country. It is equally obvious that poor children and poor countries
never have equal schools but always poorer ones, and thus fall ever
further behind, so long as they depend on schools for their
education. 

Another illusion is that most learning is a result of
teaching. Teaching may contribute to certain kinds of learning under
certain circumstances. The strongest motivated student faced with the
task of learning a new code may benefit greatly from the discipline we
now associate mostly with the old-fashioned schoolmaster. But most
people acquire most of their insight, knowledge and skill outside
school -- and in school only in so far as school in a few rich
countries becomes their place of confinement during an increasing part
of their lives. The radical deschooling of society begins, therefore,
with the unmasking by cultural revolutionaries of the myth of
schooling. It continues with the struggle to liberate other men's
minds from the false ideology of schooling -- an ideology which makes
domestication by schooling inevitable. In its final and positive stage
it is the struggle for the right to educational freedom.

A cultural revolutionary must fight for legal protection from the
imposition of any obligatory graded curriculum. The first article of a
bill of rights for a modern and humanist society corresponds to the
first amendment of the United States Constitution. The state shall
make no law with respect to an establishment of education. There shall
be no graded curriculum, obligatory for all. To make this
dis-establishment effective, we need a law forbidding discrimination
in hiring, voting or admission to centres of learning based on
previous attendance at some curriculum. This guarantee would not
exclude specific tests of competence, but would remove the present
absurd discrimination in favour of the person who learns a given skill
with the largest expenditure of public funds. A third legal reform
would guarantee the right of each citizen to an equal share of public
educational resources, the right to verify his share of these
resources, and the right to sue for them if they are denied. A
generalized G.I. bill, or an edu-credit card in the hand of every
citizen, would effectively implement this third guarantee.

Abolition of obligatory schooling, abolition of job discrimination in
favour of persons who have acquired their learning at a higher cost,
plus establishment of edu-credit, would permit the development of a
free exchange for educational services. According to present
political ideology, this exchange could be influenced by various
devices: premiums paid to those who acquire certain needed skills,
interest-bearing edu-credit to increase the privileges of those who
use it later in life, advantages for industries that incorporate
additional formal training into the work routine. 

A fourth guarantee to protect the consumer against the monopoly of the
educational market would be analogous to antitrust laws. 

I have shown in the case of education that a cultural or institutional
revolution depends upon the clarification of reality. Development as
now conceived is just the contrary: management of the environment and
the tooling of man to fit into it. Cultural revolution is a review of
the reality of man and a redefinition of the world in terms which
support this reality. Development is the attempt to create an
environment and then educate at great cost to pay for it. 

A bill of rights for modern man cannot produce cultural revolution. It
is merely a manifesto. I have outlined the principles of an
educational bill of rights. These principles can be generalized. 

The disestablishment of schooling can be generalized to freedom from
monopoly in the satisfaction of any basic need. Discrimination on the
basis of prior schooling can be generalized to discrimination in any
institution because of underconsumption or underprivilege in
another. A guarantee of equal education resources is a guarantee
against regressive taxation. An educational anti-trust law is
obviously merely a special case of anti-trust laws in general, which
in turn are statutory implementations of constitutional guarantees
against monopoly. 

The social and psychological destruction inherent in obligatory
schooling is merely an illustration of the destruction implicit in all
international institutions which now dictate the kinds of goods,
services, and welfare available to satisfy basic human needs. Only a
cultural and institutional revolution which reestablishes man's
control over his environment can arrest the violence by which
development of institutions is now imposed by a few for their own
interest. Maybe Marx has said it better, criticising Ricardo and his
school: ``They want production to be limited to `useful things', but
they forget that the production of too many \emph{useful} things
results in too many \emph{useless} people.''


\vspace{2cm}

\begin{center}
\textls[500]{END}
\end{center}


  
%\newpage
%\pagecolor{red}
%\pagestyle{empty}
%\vspace*{2cm}
%
%\raggedright
%%\sffamily
%{\small
%
%I and many others, known and unknown to me, call upon you:\\[.5cm]
%
%
%To celebrate our joint power to provide all human beings with the food, clothing and shelter they need to delight in living.\\[.5cm]
%
%
%To discover, together with us, what we must do to use mankind's power to create the humanity, the dignity and the joyfulness of each one of us.\\[.5cm]
%
%
%To be responsibly aware of your personal ability to express your true feelings and to gather us together in their expression.'\\[.5cm]
%
%
%This call to celebration' begins the first essay in this book, and sets the keynote for a series of essays, each of which, in Illich's words, records an effort of mine to question the nature of some certainty'. Pre-eminent among such questionable certainties is the value of institutions : charitable foundations, the Church, schools -- all are subject to a remarkably fresh and radical insight.\\[.5cm]
%
%A deeply stimulating thinker' -- \emph{The Times Literary Supplement}\\[.25cm]
%
%'Illich has the power of intellect, personality and life example to command serious attention as a subversive' -- Peter Jenkins in the \emph{Guardian}\\[.25cm]
%
%'Illich and Reimer have asked some of the profoundest questions about education today' -- Ian Lister in \emph{The Times Educational Supplement}
%}