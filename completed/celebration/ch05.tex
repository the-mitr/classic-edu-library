% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{The Seamy Side of Charity}


\begin{chapquot}
In 1960 Pope John XXIII enjoined all United States and Canadian
religious superiors to send, within ten years, 10 per cent of their
effective strength in priests and nuns to Latin America. This papal
request was interpreted by most United States Catholics as a call to
help modernize the Latin American Church along the lines of the North
American model. The continent on which half of all Catholics live had
to be saved from ``Castro-Communism.''

I was opposed to the execution of
this order: I was convinced that it would do serious damage to those
sent, to their clients and to their sponsors back home. I had learned
in Puerto Rico that there are only a few people who are not stunted,
or wholly destroyed, by lifelong work ``for the poor'' in a foreign
country. The transfer of United States living standards and
expectations could only impede the revolutionary changes needed, and
the use of the gospel in the service of capitalism or any other
ideology was wrong. Finally, I knew that while the United States
needed much information about all aspects of Latin America,
``missionaries'' would only hamper its collection: the feedback from
missionaries is notoriously bizarre. The projected crusade had to be
stopped. 

With two friends, Miss Feodora Standoff and Brother Gerry Morris, I
set up a centre in Cuernavaca. (We chose this spot because of its
climate, location and logistics.) Upon the opening of our centre. I
stated two of the purposes of our undertaking. The first was to help
diminish the damage threatened by the papal order. Through our
educational programme for missionaries we intended to challenge them
to face reality and themselves, and either refuse their assignments or
-- if they accepted -- to be a little bit less unprepared. Secondly, we
wanted to gather sufficient influence among the decision-making bodies
of mission-sponsoring agencies to dissuade them from implementing the
plan.

Throughout the 1960s our experience and reputation in the intensive
training of foreign professionals for assignment to South America, and
the fact that we continued to be the only centre specializing in this
type of education, ensured a continuing flow of students through our
centre -- notwithstanding our stated, basically subversive purposes. 

By 1966, instead of the 10 per cent called for in 1960, barely 0.7 per
cent of United States and Canadian clergy had moved south. Among the
educated groups within the United States Church serious doubts had
arisen about the desirability of the entire enterprise. But among
bishops and the great majority of uneducated Catholics the lachrymose
feed back from Latin America and an intense public relations campaign
conducted from Washington continued to raise enthusiasm for the ``Help
Save Latin America'' cause. Under these circumstances public and
intensive controversy had to be sponsored, and for that purpose I
wrote the following article for the Jesuit magazine \emph{America}
in January 1967. It was deliberate timing: I knew that at the end of
that month three thousand churchmen -- Catholic and Protestant, from
the United States and Latin America -- would meet in Boston to give new
impetus to their programmes, an d that \emph{Ramparts} was about to
publish its expose on Central Intelligence Agency help to student
movements, especially in Latin America.  

\end{chapquot}

Five years ago, United States Catholics undertook a peculiar
alliance for the progress of the Latin-American church. By 1970, 10
per cent of more than 225,000 priests, brothers and sisters: would
volunteer to be shipped south of the border. In those five years the
combined United States male and female ``clergy'' in South America has
increased by only 1622. Halfway is a good time to determine whether a
programme launched is still sailing on course, and more importantly,
if its destination still seems worth while. Numerically, the programme
was certainly a flop. Should this fact be a source of disappointment
or relief? 

The project relied on an impulse supported by uncritical imagination
and sentimental judgement. A pointed finger and a ``call for 20,000''
convinced many that ``Latin America needs You''. Nobody dared state
clearly why, though the first published propaganda included several
references to the ``Red danger'' in four pages of text. The Latin
America Bureau of the National Catholic Welfare Conference attached
the word ``papal'' to the programme, the volunteers and the call
itself. 

A campaign for more funds is now being proposed. This is the moment,
therefore, at which the call for 20,000 persons and the need for
millions of dollars should be re-examined. Both appeals must be
submitted to a public debate among United States Catholics, from
bishop to widow, since they are the ones asked to supply the personnel
and pay the bill. Critical thinking must prevail. Fancy and colourful
campaign slogans for another collection, with their appeal to
emotion, will only cloud the real issues. Let us coldly examine the
American Church's outburst of charitable frenzy which resulted in the
creation of ``papal'' volunteers, student ``mission crusades'', the annual
Catholic Inter American Cooperation Programme mass assemblies,
numerous diocesan missions and new religious communities. 

I will not focus on details. The above programmes themselves
continually study and revise minutiae. Rather I dare to point out some
fundamental facts and implications of the so-called papal plan -- part
of the many-faceted effort to keep Latin America within the ideologies
of the West. Church policy makers in the United States must face up to
the socio-political consequences involved in their well-intentioned
missionary ventures. They must review their vocation as Christian
theologians and their actions as Western politicians.

Men and money sent with missionary motivation carry a foreign
Christian image, a foreign pastoral approach and a foreign political
message. They also bear the mark of North American capitalism of the
1950s. Why not, for once, consider the seamy side of charity; weigh
the inevitable burden s foreign help imposes on the South American
Church; taste the bitterness of the damage done by our sacrifices?
If, for example, United States Catholics would simply turn from the
dream of ``10 per cent'', and do some honest thinking about the
implication of their help, the awakened awareness of intrinsic
fallacies could lead to sober, meaningful generosity.

But let me be more precise. The unquestionable joys of giving and the
fruits of receiving should be treated as two distinctly separate
chapters. I propose to delineate only the negative results that
foreign money, men and ideas produce in the South American Church, in
order that the future United States programme may be tailored
accordingly. 

During the past five years, the cost of operating the Church in Latin
America has multiplied many times. There is no precedent for a
similar rate of increase in Church expenses on a continental
scale. Today one Catholic university, mission society or radio chain
may cost more to operate than the whole country's Church a decade
ago. Most of the funds for this kind of growth came from outside and
flowed from two types of sources. The first is the Church itself,
which raised its income in three ways: 
\begin{enumerate}

\item Dollar by dollar, appealing to the generosity of the faithful as
  was done in Germany and the Low Countries by Adveniat, Misereor
  and Oostpriesterhulp. These contributions reach more than
  twenty-five million dollars a year individual.

\item Through lump sum contributions, made by the outstanding
  example; churchmen -- such as Cardinal Cushin or by institutions
  -- such as the National Catholic Welfare Conference, transferring
  one million dollars from the home missions to the Latin America
  Bureau. 

\item By assigning priests, religious and laymen, all trained at
  considerable cost and often backed financially in their apostolic
  undertakings.
\end{enumerate}

This kind of foreign generosity bas enticed the Latin American Church
into becoming a satellite to North Atlantic cultural phenomena and
policy. Increased apostolic resources intensified the need for this
continued flow and created islands of apostolic well-being, each day
further beyond the capacity of local support, The Latin American
Church flowers anew by returning to what the Conquest stamped her: a
colonial plant that blooms because of foreign cultivation. Instead of
learning how to get along with less money or else close up shop,
bishops are being trapped into needing more money now and bequeathing
an institution impossible to run in the future. Education, the one
type of investment that could give long-range returns, is conceived
mostly as training for bureaucrats who will maintain the existing
apparatus.

Recently I saw an example of this in a large group of Latin American
priests who had been sent to Europe for advanced degrees. In order to
relate the Church to the world, nine-tenths of these men were
studying teaching methods -- catechetics, pastoral theology or canon
law -- and thereby not directly advancing their knowledge of either
the Church or the world. Only a few studied the Church in its
history and sources, or the world as it is. 

It is easy to come by big sums to build a new church in a jungle or a
high school in a suburb, and then to staff the plants with new
missionaries. A patently irrelevant pastoral system is artificially
and expensively sustained, while basic research for a new and vital
one is considered an extravagant luxury. Scholarships for
non-ecclesiastical humanist studies; seed money for imaginative
pastoral experimentation, grants for documentation and research to
make specific constructive criticism all run the frightening risk of
threatening our temporal structures, clerical plants and ``good
business'' methods.

Even more surprising than churchly generosity for churchly concern is
a second source of money. A decade ago the Church was like an
impoverished \emph{grande dame} trying to keep up an imperial
tradition of almsgiving from her reduced income. In the more than a
century since Spain lost Latin America, the Church has steadily lost
government grants, patrons' gifts and, finally, the revenue from its
former lands. According to the colonial concept of charity, the Church
lost its power to help the poor. It came to be considered a historical
relic, inevitably the ally of conservative politicians.

By 1966 almost the contrary seems true -- at least, at first sight. The
Church has become an agent trusted to run programmes aimed at social
change. It is committed enough to produce some results. But when it is
threatened by real change, it withdraws rather than permit social
awareness to spread like wildfire. The smothering of the Brazilian
radio schools by a high Church authority is a good example.

Thus Church discipline assures the donor that his money does twice the
job in the hands of a priest. It will not evaporate, nor will it be
accepted for what it is: publicity for private enterprise and
indoctrination to a way of life that the rich have chosen as suitable
for the poor. The receiver inevitably gets the message: the ``padre''
stands on the side of W. R. Grace and Company, Esso, the Alliance for
Progress, democratic government, the AFL-CIO and whatever is holy in
the Western pantheon. 

Opinion is divided, of course, on whether the Church went heavily
into social projects because it could thus obtain funds ``for the
poor'', or whether it went after the funds because it could thus
contain Castroism and assure its institutional respectability. By
becoming an ``official'' agency of one kind of progress, the Church
ceases to speak for the underdog who is outside all agencies but who
is an ever growing majority. By accepting the power to help, the Church
necessarily must renounce a Camilo Torres who symbolizes the power of
renunciation. Money thus builds the Church a ``pastoral'' structure
beyond its means and makes it a political power.

Superficial emotional involvement obscures rational thinking about
American international ``assistance'' . Healthy guilt feelings are
repressed by a strangely motivated desire to `help' in
Vietnam. Finally, our generation begins to cut through the rhetoric of
patriotic ``loyalty''. We stumblingly. recognize the perversity of our
power politics and the destructive direction of our warped efforts to
impose unilaterally ``our way of life'' on all. We have not yet begun
to face the seamy side of clerical manpower involvement and the
Church's complicity in stifling universal awakening too revolutionary to
lie quietly within the ``Great Society''.

I know that there is no foreign priest or nun so shoddy in his work
that through his stay in Latin America he has not enriched some life;
and that there is no missionary so incompetent that through him Latin
America has not made some small contribution to Europe and North
America. But neither our admiration for conspicuous generosity, nor
our fear of making bitter enemies out of lukewarm friends, must stop
us from facing the facts. Missionaries sent to Latin America can make
\begin{enumerate}
\item an alien Church more foreign,
\item an overstaffed Church priest-ridden, and 
\item bishops into abject beggars. 
\end{enumerate}

Recent public discord has shattered the unanimity of the national
consensus on Vietnam. I hope that public awareness of the repressive
and corruptive elements contained in ``official'' ecclesiastical
assistance programmes will give rise to a real sense of guilt: guilt
for having wasted the lives of young men and women dedicated to the
task of evangelization in Latin America.

Massive, indiscriminate importation of clergy helps the ecclesiastical
bureaucracy survive in its own colony, which every day becomes more
foreign and comfortable. This immigration helps to transform the
old-style hacienda of God (on which the people were only squatters)
into the Lord's supermarket, with catechisms, liturgy and other means
of grace heavily in stock. It makes contented consumers out Of
vegetating peasants, demanding clients out of former devotees. It
lines the sacred pockets, providing refuge for men who are frightened
by secular responsibility.

Churchgoers, accustomed to priests, novenas, books and culture from
Spain (quite possibly to Franco's picture in the rectory), now meet a
new type of executive, administrative and financial talent promoting a
certain type of democracy as the Christian ideal. The people soon see
that the Church is distant, alienated from them -- an imported,
specialized operation, financed from abroad, which speaks with a
holy, because foreign, accent. 

This foreign transfusion -- and the hope for more -- gave ecclesiastical
pusillanimity a new lease of life, another chance to make the archaic
and colonial system work. If North America and Europe send enough
priests to fill the vacant parishes, there is no need to consider
laymen -- unpaid for part-time work -- to fulfil most evangelical tasks;
no need to re-examine the structure of the parish, the function of the
priest, the Sunday obligation and clerical sermon; no need to explore
the use of the married diaconate, new forms of celebration of the Word
and Eucharist, and intimate familial celebrations of conversion to the
gospel in the milieu of the home. The promise of more clergy is like a
bewitching siren. It makes the chronic surplus of clergy in Latin
America invisible and it makes it impossible to diagnose this surplus
as the gravest illness of the Church. Today , this pessimistic
evaluation is slightly altered by a courageous and imaginative few --
non-Latins among them -- who see, study and strive for true reform .

A large proportion of Latin American Church personnel are presently
employed in private institutions that serve the middle and upper
classes and frequently produce highly respectable profits; this on a
continent where there is a desperate need for teachers, nurses and
social workers in public institutions that serve the poor. A large
part of the clergy are engaged in bureaucratic functions, usually
related to peddling sacraments, sacramentals and superstitious
``blessings''. Most of them live in squalor. The Church, unable to use
its personnel in pastorally meaningful tasks, cannot even support its
priests and the 670 bishops who govern them. Theology is used to
justify this system, canon law to administer it and foreign clergy to
create a worldwide consensus on the necessity of its continuation.

A healthy sense of values empties the seminaries and the ranks of the
clergy much more effectively than does a lack of discipline and
generosity. In fact, the new mood of well-being makes the
ecclesiastical career more attractive to the self-seeker. Bishops turn
servile beggars, become tempted to organize safaris, hunt out foreign
priests and funds for constructing such anomalies as minor
seminaries. As long as such expeditions succeed, it will be difficult
if not impossible, to take the emotionally harder road: to ask
ourselves honestly if we need such a game. 

Exporting Church employees to Latin America masks a universal and
unconscious fear of a new Church. North and South American
authorities, differently motivated but equally fearful, become
accomplices in maintaining a clerical and irrelevant
Church. Sacralizing employees and property, this Church becomes
progressively more blind to the possibilities of sacralizing person
and community. 

It is hard to help by refusing to give alms. I remember once having
stopped food distribution from sacristies in an area where there was
great hunger. I still feel the sting of an accusing voice saying:
``Sleep well for the rest of your life with the death of dozens of
children on your conscience.'' Even some doctors prefer aspirins to
radical surgery. They feel no guilt having the patient die of cancer,
but fear the risk of applying the knife. The courage needed today is
that expressed by Daniel Berrigan, S.J., writing of Latin America: ``I
suggest we stop sending anyone or anything for three years and dig in
and face our mistakes and find out how not to canonize them.''

From six years' experience in training hundreds of foreign
missionaries assigned to Latin America, I know that real volunteers
increasingly want to face the truth that puts their faith to the
test. Superiors who shift personnel by their administrative decisions
but do not have to live with the ensuing deceptions are emotionally
handicapped facing these realities.

The United States Church must face the painful side of generosity: the
burden that a life gratuitously offered imposes on the recipient. The
men who go to Latin America must humbly accept the possibility that
they are useless or even harmful, although they give all they
have. They must accept the fact that a limping ecclesiastical
assistance programme uses them as palliatives to ease the pain of a
cancerous structure, the only hope being that the prescription will
give the organism enough time and rest to initiate a spontaneous
healing . It is far more probable that the pharmacist's pill will both
stop the patient from seeking a surgeon's advice and addict him to
the drug. 

Foreign missionaries increasingly realize that they heeded a
call to plug the holes in a sinking ship because the officers did not
dare launch the life rafts. Unless this is clearly seen, men who
obediently offer the best years of their lives will find themselves
tricked into a useless struggle to keep a doomed liner afloat as it
limps through uncharted seas. 

We must acknowledge that missionaries can be pawns in a world
ideological struggle and that it is blasphemous to use the gospel to
prop up any social or political system. When men and money are sent
into a society within the framework of a programme, they bring ideas
that live after them. It has been pointed out, in the case of the
Peace Corps, that the cultural mutation catalysed by a small foreign
group might be more effective than all the immediate services it
renders. The same can be true of the North American missionary close
to home, having great means at his disposal, frequently on a
short-term assignment -- who moves into an area of intense United
States cultural and economic colonization. He is part of this sphere
of influence and, at times, intrigue. Through the United States
missionary, the United States shadows and colours the public image of
the Church. The influx of United States missionaries coincides with
the Alliance for Progress, Camelot and CIA projects and looks like a
baptism of all three. The Alliance appears directed by Christian
justice and is not seen for what it is: a deception designed to
maintain the \emph{status quo}, albeit variously motivated. During
the programme's first five years, the net capital leaving Latin
America has tripled . The programme is too small to permit even the
achievement of a threshold of sustained growth. It is a bone thrown
to the dog, that he remain quiet in the back-yard of the Americas.

Within these realities, the United States missionary tends to fulfil
the traditional role of a colonial power's lackey chaplain. The
dangers implicit in Church use of foreign money assume the proportion
of caricature when this aid is administered by a ``gringo'' to keep the
``underdeveloped'' quiet. It is, of course , too much to ask of most
Americans that they make sound, clear and outspoken criticisms of
United States socio-political aggression in Latin America, even more
difficult that they do so without the bitterness of the expatriate or
the opportunism of the turncoat.

Groups of United States missionaries cannot avoid projecting the
image of ``United States outposts''. Only individual Americans mixed in
with local men could avoid this distortion. The missionary of
necessity is an ``under cover'' agent- albeit unconscious -- for United
States social and political consensus. But, consciously and purposely,
he wishes to bring the values of his Church to South America;
adaptation and selection seldom reach the level of questioning the
values themselves.

The situation was not so ambiguous ten years ago, when in good
conscience mission societies were channels for the flow of traditional
United States Church hardware to Latin America. Everything from the
Roman collar to parochial schools, from United States catechisms to
Catholic universities, was considered saleable merchandise in the
new Latin American market  not much salesmanship was needed to
convince the Latin bishops to give the ``Made in U.S.A.'' label a try.

In the meantime however, the situation has changed
considerably. The United-States Church is shaking from the first
findings of a scientific and massive self-evaluation. Not only methods
and institutions, but also the ideologies that they imply, are subject
to examination and attack. The self-confidence of the American
ecclesiastical salesman is therefore shaky. We see the strange
paradox of a man attempting to implant, in a really different culture,
structures and programmes that are now rejected in the country of
their origin. (I recently heard of a Catholic grammar school being
planned by United States personnel in a Central American city parish
where there are already a dozen public schools.)

There is an opposite danger, too. Latin America can no longer tolerate
being a haven for United States liberals who cannot make their point
at home, an outlet for apostles too ``apostolic'' to find their vocation
as competent professionals within their own community. The hardware
salesman threatens to dump second-rate imitations of parishes,
schools and catechisms -- outmoded even in the United States -- all
around the continent. The travelling escapist threatens further to
confuse a foreign world with his superficial protests, which are not
viable even at home.

The American Church of the Vietnam generation finds it difficult to
engage in foreign aid without exporting either its solutions or its
problems. Both are prohibitive luxuries for developing
nations. Mexicans, to avoid offending the sender, pay high duties for
useless or unasked-for gifts sent them by well-meaning American
friends. Gift givers must think not of this moment and of this need,
but in terms of a full generation of the future effects. Gift planners
must ask if the global value of the gift in men, money and ideas is
worth the price the recipient will ultimately have to pay for it. As
Father Berrigan suggests, the rich and powerful can decide not to
give; the poor can hardly refuse to accept. Since almsgiving
conditions the beggar's mind, the Latin American bishops are not
entirely at fault in asking for misdirected and harmful foreign aid. A
large measure of the blame lies with the underdeveloped ecclesiology
of United States clerics who direct the ``sale'' of American good
intentions.

The United States Catholic wants to be involved in an
ecclesiologically valid programme, not in subsidiary political and
social programmes designed to influence the growth of developing
nations according to anybody's social doctrine, be it even described
as the Pope's. The heart of the discussion is therefore not
\emph{how} to send more men and money, but rather \emph{why} they
should be sent at all. The Church, in the meantime, is in no critical
danger. We are tempted to shore up and salvage structures rather than
question their purpose and truth. Hoping to glory in the works of our
hands, we feel guilty, frustrated and angry when part of the building
starts to crumble. Instead of believing in the Church, we frantically
attempt to construct it according to our own cloudy cultural image. We
want to build community, relying on techniques, and are blind to the
latent desire for unity that is striving to express itself among
men. In fear, we plan \emph{our} Church with statistics, rather than
trustingly search for the living Church which is right among us.



