% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter[Planned Poverty]{Planned Poverty: The End Result of Technical
Assistance}

\begin{chapquot}
At the beginning of the second development decade, at the time the
``Pearson Report'' was presented to Robert McNamara it seemed important
to discuss alternatives to the current notions of development, which,
though based on supposedly irrefutable evidence, actually concealed
highly questionable presuppositions.  
\end{chapquot}

It is now common to demand that the rich nations convert their war
machine into a programme for the development of the Third World. The
poorer four-fifths of humanity multiply unchecked while their per
capita consumption actually declines. This population expansion and
decrease of consumption threaten the industrialized nations, who may
still, as a result, convert their defence budgets to the economic
pacification of poor nations. And this in turn could produce
irreversible despair, because the plough of the rich can do as much
harm as their swords. United States trucks can do more lasting damage
than United States tanks. It is easier to create mass demand for the
former than for the latter. Only a minority needs heavy weapons, while
a majority can become dependent on unrealistic levels of supply for
such productive machines as modern trucks. Once the Third World has
become a mass market for the goods, products, and processes which are
designed by the rich for themselves, the discrepancy between demand
for these Western artifacts and the supply will increase
indefinitely. The family car cannot drive the poor into the jet age,
nor can a school system provide the poor with education, nor can the
family refrigerator ensure healthy food for them. 

It is evident that only one man in a thousand in Latin America can
afford a Cadillac, a heart operation or a Ph.D. This restriction
on the goals of development does not make us despair of the fate of
the Third World, and the reason is simple. We have not yet come to
conceive of a Cadillac as necessary for good transportation, or of a
heart operation as normal healthy care, or of a Ph.D. as the
prerequisite of an acceptable education. In fact we recognize at once
that the importation of Cadillacs should be heavily taxed in Peru,
that an organ transplant clinic is a scandalous plaything to justify
the concentration of more doctors in Bogota, and that a betatron is
beyond the teaching facilities of the University of S\~ao
Paulo. 

Unfortunately it is not held to be universally evident that the
majority of Latin Americans -- not only of our generation, but also of
the next and the next again -- cannot afford any kind of automobile, or
any kind of hospitalization, or for that matter an elementary school
education. We suppress our consciousness of this obvious reality
because we hate to recognize the corner into which our imagination has
been pushed. So persuasive is the power of the institutions we have
created that they shape not only our preferences, but actually our
sense of possibilities. We have forgotten how to speak about modern
transportation that does not rely on automobiles and aeroplanes. Our
conceptions of modern health care emphasize our ability to prolong the
lives of the desperately ill. We have become unable to think of better
education except in terms of more complex schools and of teachers
trained for ever longer periods. Huge institutions producing costly
services dominate the horizons of our inventiveness.

We have embodied our world view into our institutions and are now
their prisoners. Factories, news media, hospitals, governments and
schools produce goods and services packaged to contain our view of the
world. We -- the rich -- conceive of progress as the expansion of these
establishments. We conceive of heightened mobility as luxury and
safety packaged by General Motors or Boeing. We conceive of improving
the general well-being as increasing the supply of doctors and
hospitals, which package health along with protracted suffering. We
have come to identify our need for further learning with the demand
for ever longer confinement to classrooms. In other words, we have
packaged education with custodial care, certification for jobs and the
right to vote, and wrapped them all together with indoctrination in
the Christian, liberal or communist virtues. 

In less than a hundred years industrial society has moulded patent
solutions to basic human needs and convened us to the belief that
man's needs were shaped by the Creator as demands for the products we
have invented. This is as true for Russia and Japan as for the North
Atlantic community. The consumer is trained for obsolescence, which
means continuing loyalty toward the same producers who will give him
the same basic packages in different quality or new
wrappings. 

Industrialized societies can provide such packages for personal
consumption for most of their citizens, but this is no proof that
these societies are sane, or economical, or that they promote
life. The contrary is true. The more the citizen is trained in the
consumption of packaged goods and services, the less effective he
seems to become in shaping his environment. His energies and finances
are consumed in procuring ever new models of his staples, and the
environment becomes a by-product of his own consumption habits.

The design of the ``package deals'' of which I speak is the main cause of
the high cost of satisfying basic needs. So long as every man ``needs''
his car, our cities must endure longer traffic jams and absurdly
expensive remedies to relieve them. So long as health means maximum
length of survival, our sick will get ever more extraordinary surgical
interventions and the drugs required to deaden their consequent
pain. So long as we want to use school to get children out of their
parents' hair or to keep them off the street and out of the labour
force, our young will be retained in endless schooling and will need
ever increasing incentives to endure the ordeal. 

Rich nations now benevolently impose a strait jacket of traffic jams,
hospital confinements and classrooms on the poor nations and by
international agreement call this ``development''. The rich and
schooled and old of the world try to share their dubious blessings by
foisting their prepackaged solutions on to the Third World. Traffic
jams develop in S\~ao Paulo while almost a million North-Eastern
Brazilians flee the drought by walking five hundred miles. Latin
American doctors get trained at The Hospital for Special Surgery in New
York, which they apply to only a few, while amoebic dysentery remains
endemic in slums where 90 per cent of the population live. A tiny
minority gets advanced education in basic science in North America --
not infrequently paid for by their own governments. If they return at
all to Bolivia, they become second-rate teachers of pretentious
subjects at La Paz or Cochabamba. The rich export outdated versions of
their standard models.

The Alliance for Progress is a good example of benevolent production
for underdevelopment. Contrary to its slogans, it did succeed -- as an
alliance for the progress of the consuming classes, and for the
domestication of the Latin American masses. The Alliance has been a
major step in modernizing the consumption patterns of the middle
classes in South America by integrating them with the dominant culture
of the North American metropolis. At the same time, the Alliance has
modernized the aspirations of the majority of citizens and fixed their
demands unavailable products.

Each car which Brazil puts on the road denies fifty people good
transportation by bus. Each merchandised refrigerator reduces the
chance of building a community freezer. Every dollar spent in Latin
America on doctors and hospitals costs a hundred lives, to adopt a
phrase of Jorge de Ahumada, the brilliant Chilean economist. Had each
dollar been spent on providing safe drinking water, a hundred lives
could have been saved. Each dollar spent on schooling means more
privileges for the few at the cost of the many; at best it increases
the number of those who before dropping out, have been taught that
those who stay longer have earned the right to more power, wealth and
prestige. What such schooling does is to teach the schooled the
superiority of the better schooled. 

All Latin American countries are frantically intent on expanding their
school systems. No country now spends less than the equivalent of 18
per cent of tax-derived public income on education -- which means
schooling -- and many countries spend almost double that. But even with
these huge investments, no country yet succeeds in giving five full
years of education to more than one-third of its population; supply
and demand for schooling grow geometrically apart. And what is true
about schooling is equally true about the products of most
institutions in the process of modernization in the Third
World. 

Continued technological refinements of products which are already
established on the market frequently benefit the producer far more
than the consumer. The more complex production processes tend to
enable only the largest producer to replace outmoded models
continually, and to focus the demand of the consumer on the marginal
improvement of what he buys, no matter what the concomitant side
effects: higher prices, diminished life span, less general usefulness,
higher cost of repairs. Think of the multiple uses for a simple tin
opener, whereas an electric one, if it works at all, opens only some
kinds of tins, and costs one hundred times as much. 

This is equally true for a piece of agricultural machinery and for an
academic degree. The Mid-Western farmer can become convinced of his
need for a four-axle vehicle which can go 70 m.p.h, on the highways,
has an electric windscreen wiper upholstered seats, and can be turned
in for a new one within year or two. Most of the world's farmers don't
need such speed, nor have they ever met with such comfort, nor are
they interested in obsolescence. They need low-priced transport, in a
world where time is not money, where manual wipers suffice, and where
a piece of heavy equipment should outlast a generation. Such a
mechanical donkey requires entirely different engineering and design
than one produced for the United States market. This vehicle is not in
production. 

Most of South America needs paramedical workers who can function for
indefinite periods without the supervision of a qualified
doctor. Instead of establishing a process to train midwives and
visiting healers who know how to use a very limited arsenal of
medicines while working independently, Latin American universities
establish every year a new school of specialized nursing or nursing
administration to prepare professionals who can function only in a
hospital, and pharmacists who know how to sell increasingly more
dangerous drugs. 

The world is reaching an impasse where two processes converge: ever
more men have fewer basic choices. The increase in population is
widely publicized and creates panic. The decrease in fundamental
choice causes anguish and is consistently overlooked. The population
explosion overwhelms the imagination, but the progressive atrophy of
social imagination is rationalized as an increase of choice between
brands. The two processes converge in a dead end: the population
explosion provides more consumers for everything from food to
contraceptives, while our shrinking imagination can conceive of no
other ways of satisfying their demands except through the packages now
on sale in the admired societies. 

I will focus successively on these two factors, since, in my opinion,
they form the two coordinates which together permit us to define
underdevelopment. 

In most Third World countries, the population grows,
and so does the middle class. Income, consumption and the well-being
of the middle class are all growing while the gap between this class
and the mass of people widens. Even where per capita consumption is
rising, the majority of men have less food now than in 1945, less
actual care in sickness, less meaningful work, less protection . This
is partly a consequence of polarized consumption and partly caused by
the breakdown of traditional family and culture. More people suffer
from hunger, pain and exposure in 1969 than they did at the end of the
Second World War, not only numerically, but also as a percentage of
the world population. 

These concrete consequences of underdevelopment are rampant; but
underdevelopment is also a state of mind, and understanding it as a
state of mind, or as a form of consciousness, is the critical
problem. Underdevelopment as a state of mind occurs when mass needs
are converted to the demand for new brands of packaged solutions which
are forever beyond the reach of the majority. Underdevelopment in this
sense is rising rapidly even in countries where the supply of
classrooms, calories, cars and clinics is also rising. The ruling
groups in these countries build up services which have been designed
for an affluent culture; once they have monopolized demand in this
way, they can never satisfy majority needs. 

Underdevelopment as a form of consciousness is an extreme result of
what we can call in the language of both Marx and Freud
\emph{Verdinglichung}, or reification. By reification I mean the
hardening of the perception of real needs into the demand for mass
manufactured products. I mean the translation of thirst into the need
for a Coke . This kind of reification occurs in the manipulation of
primary human needs by vast bureaucratic organizations which have
succeeded in dominating the imagination of potential consumers. 

Let me return to my example taken from the field of education. The
intense promotion of schooling leads to so close an identification of
school attendance and education that in everyday language the two
terms are interchangeable. Once the imagination of an entire
population has been ``schooled'', or indoctrinated to believe that
school has a monopoly on formal education, then the illiterate can be
taxed to provide free high school and university education for the
children of the rich.  

Underdevelopment is the result of rising levels of aspiration achieved
through the intensive marketing of ``patent'' products. In this sense the
dynamic underdevelopment that is now taking place is the exact
opposite of what I believe education to be: namely, the awakening
awareness of new levels of human potential and the use of one's
creative powers to foster human life. Underdevelopment, however,
implies the surrender of social consciousness to prepackaged
solutions. 

The process by which the marketing of ``foreign'' products increases
underdevelopment is frequently understood in the most superficial
ways. The same man who feels indignation at the sight of a Coca-Cola
plant in a Latin American slum often feels pride at the sight of a new
normal school growing up alongside. He resents the evidence of a
foreign ``licence'' attached to a soft drink which he would like to see
replaced by ``Cola-Mex''. But the same man is willing to impose
schooling -- at all costs on his fellow citizens, and is unaware of the
invisible licence by which this institution is deeply enmeshed in the
world market. 

Some years ago I watched workmen putting up a sixty-foot Coca-Cola
sign on a desert plain in the Mexquital. A serious drought and famine
had just swept over the Mexican highland. My host, a poor Indian in
Ixmiquilpan, had just offered his visitors a tiny tequila glass of the
costly black sugar-water. When I recall this scene I till feel anger;
but I feel much more incensed when I remember UNESCO meetings at
which well-meaning and well-paid bureaucrats seriously discussed Latin
American school curricula, and when I think of the speeches of
enthusiastic liberals advocating the need for more schools. 

The fraud perpetrated by the salesmen of schools is less obvious but
much more fundamental than the self-satisfied salesmanship of the
Coca-Cola Or Ford representative, because the school man hooks his
people on a much more demanding drug. Elementary school attendance is
not a harmless luxury, but more like the coca chewing of the Andean
Indian, which harnesses the worker to the boss. 

The higher the dose of schooling an individual has received, the more
depressing his experience of withdrawal. The seventh grade dropout
feels his inferiority much more acutely than the dropout from the
third grade. The schools of the Third World administer their opium
with much more effect than the Churches of other epochs. As the mind
of a society is progressively schooled, step by step its individuals
lose their sense that it might be possible to live without being
inferior to others. As the majority shifts from the land into the
city, the hereditary inferiority of the peon is replaced by the
inferiority of the school dropout who is held personally responsible
for his failure. Schools rationalize the divine origin of social
stratification with much more rigour than churches have ever
done. 

Until this day no Latin American country has declared youthful
under-consumers of Coca-Cola or cars as lawbreakers, while all Latin
American countries have passed laws which define the early dropout as
a citizen who has not fulfilled his legal obligations. The Brazilian
government recently almost doubled the number of years during which
schooling is legally compulsory and free. From now on any Brazilian
dropout under the age of sixteen will be faced during his lifetime
with the reproach that he did not take advantage of a legally
obligatory privilege. Thus law was passed in a country where not even
the most optimistic could foresee the day when such levels of schooling
would be provided for only 25 per cent of the young. The adoption of
international standards of schooling forever condemns most Latin
Americans to marginality or exclusion from social life -- in a word,
underdevelopment. 

The translation of social goals into levels of consumption is not
limited to only a few countries. Across all frontiers of culture
ideology and geography today, nations are moving towards the
establishment of their own car factories, their own medical and normal
schools -- and most of these are, at best, poor imitations of foreign
and largely North American models.

The Third World is in need of a profound revolution of its
institutions. The revolutions of the last generation were
overwhelmingly political. A new group of men with a new set of
ideological justifications assumed power to administer fundamentally the
same scholastic, medical and market institutions to the interest of a new
group of clients. Since the institutions have not radically changed, the
new group of clients remain approximately the same size as that
previously served. This appears clearly in the case of education. Per
pupil costs of schooling are today comparable everywhere since the
standards used to evaluate the quality of schooling tend to be
internationally shared. Access to publicly financed education,
considered as access to school, everywhere depends on per capita
income. (Places like China and North Vietnam might be meaningful
exceptions.) 

Everywhere in the Third World modern institutions are
grossly unproductive, with respect to the egalitarian purposes for
which they are being reproduced. But so long as the social imagination
of the majority has not been destroyed by its fixation on these
institutions, there is more hope of planning an institutional
revolution in the Third World than among the rich. Hence the urgency
of the task of developing workable alternatives to ``modern''
solutions. 

Underdevelopment is at the point of becoming chronic in many
countries. The revolution of which I speak must begin to take place
before this happens. Education again offers a good example: chronic
educational underdevelopment occurs when the demand for schooling
becomes so widespread that the total concentration of educational
resources on the school system becomes a unanimous political
demand. At this point the separation of education from schooling
becomes impossible. 

The only feasible answer to ever increasing underdevelopment is a
response to basic needs that is planned as a long-range goal for areas
which will always have a different capital structure. It is easier to
speak about alternatives to existing institutions, services and
products than to define them with precision. It is not my purpose
either to paint a Utopia or to engage in scripting scenarios for an
alternative future. We must be satisfied with examples indicating
simple directions that research should take. 

Some such examples have already been given. Buses are alternatives to
a multitude of private cars. Vehicles designed for slow transportation
on rough terrain are alternatives to standard lorries. Safe water is
an alternative to high-priced surgery. Medical workers are an
alternative to doctors and nurses. Community food storage is an
alternative to expensive kitchen equipment. Other alternatives could
be discussed by the dozen. Why not, for example, consider walking as a
long-range alternative for locomotion by machine, and explore the
demands which this would impose on the city planner? And why can't the
building of shelters be standardized, elements be pre-cast, and each
citizen be obliged to learn in a year of public service how to
construct his own sanitary housing? 

It is harder to speak about alternatives in education, partly because
schools have recently so completely preempted the available
educational resources of good will, imagination and money. But even
here we can indicate the direction in which research must be
conducted. 

At present, schooling is conceived as graded, curricular, class
attendance by children, for about one thousand hours yearly during an
uninterrupted succession of years. On the average, Latin American
countries can provide each citizen with between eight and thirty
months of this service. Why not, instead, make one or two months a year
obligatory for all citizens below the age of thirty?

Money is now spent largely on children, but an adult can be taught to
read in one-tenth the time and for one-tenth the cost it takes to
teach a child. In the case of the adult there is an immediate return
on the investment, whether the main importance of his learning is seen
in his new insight, political awareness and willingness to assume
responsibility for his family's size and future, or whether the
emphasis is placed on increased productivity. There is a double return
in the case of the adult, because not only can he contribute to the
education of his children, but to that of other adults as well. In
spite of these advantages, basic literacy programmes have little or no
support in Latin America, where schools have a first call on all
public resources. Worse, these programmes are actually ruthlessly
suppressed in Brazil an elsewhere, where military support of the
feudal or industrial oligarchy has thrown off its former benevolent
disguise. 

Another possibility is harder to define, because there is as yet no
example to point to. We must therefore imagine the use of public
resources for education distributed in such a way as to give every
citizen a minimum chance. Education will become a political concern of
the majority of voters only when each individual has a precise sense
of the educational resources that are owing to him -- and some idea of
how to sue for them. Something like a universal G.I. Bill of Rights
could be imagined, dividing the public resources assigned to education
by the number of children who are legally of school age, and making
sure that a child who did not take advantage of his credit at the age
of seven, eight or nine would have the accumulated benefits at his
disposal at age ten. 

What would the pitiful education credit which a Latin American
republic could offer to its children provide? Almost all of the basic
supply of books, pictures, blocks, games and toys that are totally
absent from the homes of the really poor, but enable a middle-class
child to learn the alphabet, the colours, shapes and other classes of
objects and experiences which ensure his educational progress. The
choice between these things and schools is obvious. Unfortunately,
the poor, for whom alone the choice is real, never get to exercise
this choice. 

Defining alternatives to the products and institutions which now
preempt the field is difficult, not only, as I have been trying to
show, because these products and institutions shape our conception of
reality itself, but also because the construction of new possibilities
requires a concentration of will and intelligence in a higher degree
than ordinarily occurs by chance. This concentration of will and
intelligence on the solution of particular problems regardless of
their nature we have become accustomed over the last century to call
research. 

I must make clear, however, what kind of research I am talking
about. I am not talking about basic research either in physics,
engineering, genetics, medicine or learning. The work of such men as
F. H. C. Crick, Jean Piaget and Murray Gell-Mann must continue to
enlarge our horizons in other fields of science. The labs and
libraries and specially trained collaborators these men need cause
them to congregate in the few research capitals of the world. Their
research can provide the basis for new work on practically any
product. 

I am not speaking here of the billions of dollars annually spent on
applied research, for this money is largely spent by existing
institutions to the perfection and marketing of their own
products. Applied research is money spent on making planes faster and
airports safer; on making medicines more specific and powerful and
doctors capable of handling their deadly side effects; on packaging
more learning into classrooms; on methods to administer large
bureaucracies. This is the kind of research for which some kind of
counter-foil must somehow be developed if we are to have any chance to
come up with basic alternatives to the automobile, the hospital, and
the school, and any of the many other so-called ``evidently necessary
implements for modern life''. 

I have in mind a different, and peculiarly difficult, kind of
research, which has been largely neglected up to now, for obvious
reasons. I am calling for research on alternatives to the products
which now dominate the market; to hospitals and the profession
dedicated to keeping the sick alive; to schools and the packaging
process which refuses education to those who are not of the right age,
who have not gone through the right curriculum, who have not sat in a
classroom a sufficient number of successive hours, who will not pay
for their learning with submission to custodial care, screening and
certification or with indoctrination in the values of the dominant
elite. 

This counter-research on fundamental alternatives to current
prepackaged solutions is the element most critically needed if the
poor nations are to have a liveable future. Such counter research is
distinct from most of the work done in the name of the ``year 2000,''
because most of that work seeks radical changes in social patterns
through adjustments in the organization of an already advanced
technology. The counter-research of which I speak must take as one of
its assumptions the continued lack of capital in the Third World. 

The difficulties of such research are obvious. The researcher must
first of all doubt what is obvious to every eye. Second, he must
persuade those who have the power of decision to act against their own
short-run interests or bring pressure on them to do so. And, finally,
he must survive as an individual in a world he is attempting to
change fundamentally so that his fellows among the privileged minority
see him as a destroyer of the very ground on which all of US stand. He
knows that if he should succeed in the interest of the poor,
technologically advanced societies still might envy the ``poor'' who
adopt this vision. 

There is a normal course for those who make development policies,
whether they live in North or South America, in Russia or Israel. It
is to define development and to set its goals in ways with which they
are familiar, which they are accustomed to use in order to satisfy
their own needs, and which permit them to work through the
institutions over which they have power or control. This formula has
failed, and must fail. There is not enough money in the world for
development to succeed along these lines, not even in the combined
arms and space budgets of the superpowers. 

An analogous course is followed by those who are trying to make
political revolutions, especially in the Third World. Usually they
promise to make the familiar privileges of the present elites, such as
schooling, hospital care, etc., accessible to all citizens; and they base
this vain promise on the belief that a change in political regime will
permit them to sufficiently enlarge the institutions which produce
these privileges. The promise and appeal of the revolutionary are
therefore just as threatened by the counter-research I propose as is
the market of the now dominant producers. 

In Vietnam people on bicycles and armed with sharpened bamboo sticks
have brought to a standstill the most advanced machinery for research
and production ever devised. We must seek survival in a Third World in
which human ingenuity can peacefully outwit machined might. The only
way to reverse the disastrous trend to increasing underdevelopment,
hard as it is, is to learn to laugh at accepted solutions in order to
change the demands which make them necessary. Only free men can change
then minds and be surprised; and while no men are completely free,
some are freer than others.
