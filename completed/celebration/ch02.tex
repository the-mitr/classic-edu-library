% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Violence: A Mirror for Americans}

\vspace*{-5pt}
\begin{chapquot}
The compulsion to do good is an innate American trait. Only North
Americans seem to believe that they always should, may, and actually
can choose somebody with whom to share their blessings. Ultimately
this attitude leads to bombing people into the acceptance of gifts. In
early 1968 I tried with insistence to make some of my friends
understand this image of the American overseas. I was speaking mainly
to resisters engaged in organizing the march on the Pentagon. I wanted
to share with them a profound fear: the fear that the end of the war
in Vietnam would permit hawks and doves to unite in a destructive war
on poverty in the Third World.
\end{chapquot}
%\newpage

The qualified failure of the war on poverty, with its fruits of urban
riots, has begun to open the eyes of Americans to the reasons for the
failure of the Alliance for Progress, with its fruits of threatened
rebellion. Both are related to the failure to win the hearts and minds
of the people of Asia by an outpouring of money and human lives that
Americans perceive as an expression of heroic generosity, in the
defence of South Vietnam. Failure in Harlem, Guatemala and Vietnam has
a common root. All three have miscarried because the United States
gospel of massive material achievement lacks credibility for the
world's overwhelming majorities. I believe that insight into the
meaning of United States good will as perceived by Latin Americans or
Asians would enable Americans to perceive the meaning of the problem
of their own slums; it could even lead to the perception of a new and
more effective policy. 

I have had the opportunity to observe this growing awareness of a
common root of failure in my contacts with students at Cuernavaca,
There, at the Center for Intercultural Documentation, for the past two
years we have offered a sequence of workshops to compare the
experience of poverty in capital-rich and capital-starved
societies. We have witnessed the initial shock in many Americans
dedicated to the war against poverty, when they observed and studied
Latin America and realized for the first time that there is a link
between minority marginality at home and mass margination
overseas. Their emotional reaction is usually more acute than the
intellectual insight that produces it. We have seen more than one man
lose his balance as he suddenly lost the faith that for him had
previously supported that balance, the faith that says: ``The American
way is the solution for all.'' For any good man, whether he is a social
worker in Watts or a missionary on his way to Bolivia, it means pain
and panic to realize that he is seen by 90 per cent of mankind as the
exploiting outsider who shores up his privilege by promoting a
delusive belief in the ideals of democracy, equal opportunity and free
enterprise among people who haven't a remote possibility of profiting
from them. 

At this stage of the war in Vietnam the violent symptoms are too
horrible to permit a lucid analysis of the causes that produce
them. It is therefore more important to focus United States attention
on the other two programmes, the war on poverty and the Alliance for
Progress : one, a war conducted by social workers; the other, an
alliance that has maintained or swept into power military regimes in
two-thirds of the Latin American countries. Both originated in the
name of good will; both are now seen as pacification programmes; both
are pregnant with violence. 

The war on poverty aims at the integration of the so-called
underprivileged minorities of the United States into the mainstream of
the American way of life; the Alliance for Progress aims at the
integration of the so-called underdeveloped countries of Latin America
into the community of industrialized nations. Both programmes were
designed to have the poor join the American dream. Both programmes
failed. The poor refused to dream on command. The order to dream and
the money they got only made them rambunctious. Huge funds were
appropriated to start the United States minorities and the Latin
American majorities on the way of integration into a United
States-style middle class: the world of college attendance, universal
consumer credit, the world of household appliances and insurance, the
world of church and movie attendance. An army of generous volunteers
swarmed through New York ghettos and Latin American jungle canyons,
pushing the persuasion that makes America tick.

The frustrated social worker and the former Peace Corps volunteer are
now among the few who explain to mainline America that the poor are
right in rejecting forced conversion to the American gospel. Only
seven years after the majority missionary enterprise of the Alliance
was launched, riot squads at home, military governments in Latin
America and the army in Vietnam keep asking for more funds. But now it
can be seen that the money is needed not for the uplift of the poor,
but to protect the frail beachhead into the middle class that has been
gained by the few converts who have benefited here or there by the
American way of life. 

Comparison of these three theatres of United States missionary effort
and war will help bring home a truism: the American society of
achievers and consumers, with its two-party system and its universal
schooling, perhaps befits those who have got it, but certainly not the
rest of the world. A 15 per cent minority at home who earn less than
\$3000 a year, and an 80 per cent majority abroad who earn less than
\$300 a year are prone to react with violence to the schemes by which
they are fitted into coexistence with affluence. This is the moment to
bring home to the people of the United States the fact that the way of
life they have chosen is not viable enough to be shared. Eight years
ago I told the late Bishop Manuel Larrain, the president of the
Conference of Latin American Bishops, that I was prepared if necessary
to dedicate my efforts to stop the coming of missionaries to Latin
America. His answer still rings in my ears: ``They may be useless to us
in Latin America, but they are the only North Americans whom we will
have the opportunity to educate. We owe them that much.''

At this moment, when neither the allure of money nor the power of
persuasion nor control through weapons can efface the prospect of
violence, during the summer in the slums and throughout the year in
Guatemala, Bolivia or Venezuela, we can analyse the analogies in the
reactions to United States policy in the three main theatres of its
defensive war: the war by which it defends its quasi-religious
persuasion in Watts, Latin America and Vietnam. Fundamentally this is
the same war fought on three fronts; it is the war to ``preserve the
values of the West.'' Its origin and expression are associated with
generous motives and a high ideal to provide a richer life for all
men. But as the threatening implications of that ideal begin to
emerge, the enterprise grinds down to one compelling purpose: to
protect the style of life and the style of death that affluence makes
possible for a very few; and since that style cannot be protected
without being expanded, the affluent declare it obligatory for
all. ``That they may have more'' begins to be seen in its real
perspective: ``That I may not have less.'' 

In all three theatres of war the same strategies are used: money,
troopers, teachers. But money can benefit only a few in the ghettos,
and a few in Latin America, and a few in Vietnam; and the consequent
concentration of imported benefits on a few requires their ever
tighter protection against the many. For the majority of marginal
people, the economic growth of their surroundings means rising levels
of frustration. On all three frontiers of affluence, therefore, the
gun becomes important to protect the achiever. Police reinforcements
go hand in hand with bands of armed citizens in the United States. In
Guatemala the recently murdered military attache of the United States
had just admitted that the American Embassy had to assist in arming
right-wing goon squads because they are more efficient in maintaining
order (and certainly more cruel) than the army. 

Next to money and guns, the United States idealist turns up in every
theatre of the war; the teacher, the volunteer, the missionary, the
community organizer, the economic developer. Such men define their
role as service. Actually they frequently wind up numbing the damage
done by money and weapons, or seducing the ``underdeveloped'' to the
benefits of the world of affluence and achievement. They especially
are the ones for whom ``ingratitude'' is the bitter reward. They are the
personifications of Good Old Charlie Brown: ``How can you lose when you
are so sincere?''

I submit that, if present trends continue, from now on the violence in
Harlem, in Latin America, in Asia, will increasingly be directed
against the foreign and native ``persuasion pusher'' of this
kind. Increasingly the ``poor'' will slam the door in the face of
salesmen for the United States system of politics, education and
economics as an answer to their needs. This rejection goes hand in
hand with a growing loss of faith in his own tenets on the part of the
salesman of United States social consensus. Disaffection, helplessness
and the response of anger at the United States have undermined the
thrust of the formerly guileless enthusiast of the American way and
American methods. 

I submit that foreign gods (ideals, idols, ideologies, persuasions,
values) are more offensive to the `poor' than the military or economic
power of the foreigner. It is more irritating to feel seduced to the
consumption of overpriced sugar-water called Coca-Cola than to submit
helplessly to doing the same job an American does, only at half the
pay. It angers a person more to hear a priest preach cleanliness,
thrift, resistance to socialism or obedience to unjust authority, than
to accept military rule. If I read present trends correctly, and I am
confident I do, during the next few years violence will break out
mostly against symbols of foreign ideas and the attempt to sell
these. And I fear that this violence , which is fundamentally a
healthy though angry and turbulent rejection of alienating symbols,
will be exploited and will harden into hatred and crime. The recent
violence in Detroit, Washington and Cincinnati after the murder of
Martin Luther King shows how the impatience of the ghetto dwellers in
the United States can erupt into violence and vandalism at the
slightest spark. 

Violence, therefore, covers a broad spectrum of experience: from the
explosion of frustrated vitality to the fanatical rejection of
alienating idols. It is important to stress this distinction. But as
United States thinkers are horrified by the heartless slaughter in
Vietnam, and fascinated by the inability of a white majority to
suppress the life of a people, it is not easy to keep the distinction
clear. The emotional involvement of the average United States student
with Vietnam and the ghettos is so deep, it is almost taboo to call
his attention to the distinction. For this reason we must welcome any
educational effort that allows United States students to perceive
reactions to the United States way of life in the third theatre of the
war against poverty: Latin America. 

In the mirror of Latin America, violence in American ghettos and on
the borders of China can be seen in its new meaning, as a rejection of
American values. From experience of years in Cuemavaca, dealing with
United States ``idea salesmen'' I know this insight is costly to come
by. There is no exit from; way of life built on \$5,000 plus per year,
and there is no possible road leading into this way of life for
nine out of ten men in our generation and the next. And for the nine it
is revolting to hear a message of economic and social salvation
presented by the affluent that, however, sincerely expressed, leads
the ``poor'' to believe that it is their fault that they do not fit into
God's world as it should be and as it has been decreed that it should
be around the North Atlantic. 

It is not the American way of life lived by a handful of millions that
sickens the thousands of millions, but rather the growing awareness
that those who live the American way will not tire until the
superiority of their quasi-religious persuasion is accepted by the
underdogs. Living violence always breaks out against the demand that a
man submit to idols. Planned violence is then promoted as justified by
the need to reduce a man or a people to the service of the idol they
threaten to reject. Francisco Juliao, the peasant leader from
North-east Brazil who now lives in exile in Cuernavaca, recently made a
statement that clarifies these principles. ``Never,'' he said, ``but
never put weapons into the hands of the people. Whosoever puts weapons
into the hands of the people destroys. Weapons put into the hands of
the people will always be used against them. Weapons always defeat the
poor who receive them. Only the brick and the stick a man picks up in
anger will not defile him as a man.''

In this light it is important for the North American citizen to learn
from the insight gained these years by Latin American thinkers. Let
him look at Colombia where there are bandits who kill for gain, and
soldiers and \emph{guerrilleros} who kill each other for the sake of
discipline or in the service of a flag; and there is the angry man who
kills in a mob that erupts in riot; and finally there is the witness,
like Camilo Torres, who purposefully withdraws to the mountains to
demonstrate his ability to survive in the face of an oppressive regime
and thus wants to prove its illegitimacy. Soldier and bandit can
organize; riots can be incited and their frustrated vitality can go
stale or be channelled with deadly rationality into the service of
some ``ideal''. Testimony will always remain a lonely task that ends up
on a hill like Calvary. True testimony of profound non-conformity
arouses the fiercest violence against it, but I do not see how such
witness could ever be organized or institutionalized. 

The study of violence in Latin America deeply touches the life of the
United States observer, but - for a moment still - allows him to stay
disengaged. It is always easier to see the illusions in one's
neighbour's eyes than the delusions in one's own. A critical
examination of the effect that intense social change has on the
intimacy of the human heart in Latin America is a fruitful way to
insight into the intimacy of the human heart in the United States. In
the capital-starved economies of Latin America, a great majority live
excluded, now and forever, from the benefits of a thriving United
States-style elite middle class. In the immensely rich economy of the
United States, a small minority clamours that, in the same way) it is
excluded from the mass of the middle class. The comparison should
enable the United States observer to understand the world-wide growth
of two societies, separate and unequal, and to appreciate the dynamics
that provoke violence between them.





