% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{The Futility of Schooling}

\begin{chapquot}
To provide every citizen in the United States with a level of
schooling now enjoyed by the well-off one-third would require the
addition of forty thousand million dollars per year to the present
cost of elementary and secondary education in the United States, which
is about thirty-seven thousand million. 

This sum exceeds the present
expenditure for the war in Vietnam. Evidently the United States is too
poor to provide compensatory education on this scale. And yet it is
politically inexpedient and intellectually disreputable to question
the elusive goal of providing equal educational opportunities for all
citizens by giving them access to an equal number of years in
school. 

One man's illusions are often best recognized in the light of another
man's delusions. My discussion of the futility of schooling in the
Third World -- published as a magazine article in 1968 -- may help to
demonstrate the general futility of world-wide educational
institutions.  

\end{chapquot}

For the past two decades, demographic considerations have coloured
all discussion about development in Latin America. In 1950 some 200
million people occupied the area extending from Mexico to Chile. Of
these, 120 million lived directly or indirectly on primitive
agriculture. Assuming both effective population controls and the most
favourable possible results from programmes aimed at the increase of
agriculture, by 1985 forty million people will produce most of the
food for a total population of 360 million. The remaining 320 million
will be either marginal to the economy or will have to be incorporated
somehow into urban living and industrial production. 

During these same past twenty years, both Latin American governments
and foreign technical\hyp{}assistance agencies have come to rely
increasingly on the capacity of grammar, trade and high schools to
lead the non-rural majority out of its marginality in shanty towns and
subsistence farms into the type of factory, market and public forum
which corresponds to modem technology. It was assumed that schooling
would eventually produce a broad middle class with values resembling
those of highly industrialized nations, despite the economy of
continued scarcity. 

Accumulating evidence now indicates that schooling does not and cannot
produce the expected results. Some years ago the governments of the
Americas joined in an Alliance for Progress, which has, in practice,
served mainly the progress of the middle classes in the Latin
nations. In most countries the Alliance has encouraged the replacement
of a closed, feudal, hereditary elite by one which is supposedly
``meritocratic'' and open to the few who manage to finish
school. Concomitantly, the urban service proletariat has grown at
several times the rate of the traditional landless rural mass and has
replaced it in importance. The marginal majority and the schooled
minority grow ever further apart. One old feudal society has brought
forth two classes, separate and unequal.

This development has led to educational research focused on the
improvement of the learning process in schools and on the adaptations
of schools themselves to the special circumstances prevailing in
underdeveloped societies. But logic would seem to require that we do
not stop with an effort to improve schools; rather that we question
the assumption on which the school system itself is based. We must not
exclude the possibility that the emerging nation s cannot be
schooled, that schooling is not a viable answer to their need for
universal education. Perhaps this type of insight is needed to clear
the way for a futuristic scenario in which schools as we know them
today would disappear.

The social distance between the growing urban mass and the new elite
is a new phenomenon, unlike the traditional forms of discrimination
known in Latin America. This new discrimination is not a transitory
thing which can be overcome by schooling. On the contrary: I submit
that one of the reasons for the awakening frustration in the
majorities is the progressive acceptance of the ``liberal myth'', the
assumption that schooling is an assurance of social integration. 

The solidarity of all citizens based on their common graduation from
school has been an inalienable part of the modem, Western
self-image. Colonization has not succeeded in implanting this myth
equally in all countries, but everywhere schooling has become the
prerequisite for membership in a managerial middle class. The
constitutional history of Latin America since its independence has
made the masses of this continent particularly susceptible to the
conviction that all citizens have a right to enter -- and, therefore,
have some possibility of entering -- their society through the door
of a school. 

More than elsewhere, in Latin America the teacher as missionary for
the school-gospel has found adherents at the grassroots. Only a few
years ago many of us were happy when finally the Latin American school
system was singled out as the area of privileged investment for
international assistance funds. In fact, during the past years, both
national budgets and private investment have been stimulated to
increase educational allocations. But as second look reveals that
this school system has built a narrow bridge across a widening social
gap. As the only legitimate passage to the middle class, the school
restricts all unconventional crossings and leaves the underachiever to
bear the blame for his marginality.

This statement is difficult for Americans to understand. In the United
States, the nineteenth-century persuasion that free schooling ensures
all citizens equality in the economy and effective participation in
the society survives. It is by no means certain that the result of
schooling ever measured up to this expectation, but the schools
certainly played a more prominent role in this process some hundred
years ago. 

In the United States of the mid-nineteenth century, six years of
schooling frequently made a young man the educational superior of his
book. In a society largely dominated by unschooled achievers, the
little red schoolhouse was an effective road to social equality. A few
years in school for all brought most extremes together. Those who
achieved power and money without schooling had to accept a degree of
equality with those who achieved literacy and did not strike it
rich. Computers, television and aeroplanes have changed this. Today
in Latin America, in the midst of modern technology, three times a
many years of schooling and twenty times as much money as was then
spent on grammar schools will not produce the same social result. The
dropout from the sixth grade is unable to find a job even as a
punchcard operator or a railroad hand.

Contemporary Latin America needs school systems no more than it needs
railroad tracks. Both -- spanning continents -- served to speed the now
- rich and established nations into the industrial age. Both, if now
handle d with care, are harmless heirlooms from the Victorian
period. But neither is relevant to countries emerging from primitive
agriculture directly into the jet age. Latin America cannot afford to
maintain outmoded social institutions amid modem technological
processes. 

By ``school'' of course, I do not mean all organized formal education. I
use the term ``school'' and ``schooling'' here to designate a form of
child care and a \emph{rite de passage} which we take for
granted. We forget that this institution and the corresponding creed
appeared on the scene only with the growth of the industrial
state. Comprehensive schooling today involves year-round obligatory
and universal classroom attendance in small group; for several hours
each day. It is imposed on all citizens for a period of ten to
eighteen years. School divides life into two segments, which are
increasingly of comparable length. As much as anything else, schooling
implies custodial care for persons who are declared undesirable
elsewhere by the simple fact that a school has been built to serve
them. The school is supposed to take the excess population from the
street, the family or the labour force. Teachers are given the power
to invent new criteria according to which new segments of the
population may be committed to a school. This restraint on healthy,
productive and potentially independent human beings is performed by
schools with an economy which only labour camps could
rival. 

Schooling also involves a process of accepted ritual certification for
all members of a ``schooled'' society. Schools select those who are
bound to succeed and send them on their way with a badge marking them
fit. Once universal schooling has been accepted as the hallmark for
the in-members of a society, fitness is measured by the amount of time
and money spent on formal education in youth rather than ability
acquired independently from an ``accredited'' curriculum.

A first important step toward radical educational reform in Latin
America will be taken when the educational system of the United States
is accepted for what it is: a recent, imaginative social invention
perfected since the Second World War and historically rooted in the
American frontier. The creation of the all-pervasive school
establishment, tied into industry, government and the military, is an
invention no less original than the guild-centred apprenticeship of
the Middle Ages, or the \emph{doctrina de los indios} and the
\emph{reducci\'on} of Spanish missionaries in Mexico and Paraguay,
respectively, or the \emph{lyc\'ee} and \emph{les grandes
  \'ecoles} in France. Each one of these systems was produced by its
society to give stability to an achievement; each has been heavily
pervaded by ritual to which society bowed; and each has been
rationalized into an all-embracing persuasion, religion or
ideology. The United States is not the first nation that has been
willing to pay a high price to have its educational system exported by
missionaries to all comers of the world. The colonization of Latin
America by the catechism is certainly a noteworthy precedent.

It is difficult now to challenge the school as a system because we are
so used to it. Our industrial categories tend to define results as
products of specialized institutions and instruments, Armies produce
defence for countries. Churches procure salvation in an
afterlife. Binet defined intelligence as that which his tests
test. Why not, then, conceive of education as the product of schools?
Once this tag has been accepted, unschooled education gives the
impression of something spurious, illegitimate and certainly
unaccredited.

For some generations, education has been based on massive schooling,
just as security was based on massive retaliation and, at least in the
United States, transportation on the family car. The United States,
because it industrialized earlier, is rich enough to afford schools,
the Strategic Air Command, and the car -- no matter what the toll. Most
nations of the world are not that rich; they behave, how ever, as if
they were. The example of nations which ``made it'' leads Brazilians to
pursue the ideal of the family car -- just for a few. It compels
Peruvians to squander on Mirage bombers -- just for a show. And it
drives every government in Latin America to spend up to two-fifths of
its total budget on schools, and to do so unchallenged.

Let us insist, for a moment, on this analogy between the school system
and the system of transportation based on the family car. Ownership of
a car is now rapidly becoming the ideal in Latin America -- at least
among those who have a voice in formulating national goals. During the
past twenty years, roads, parking facilities and services for private
automobiles have been immensely improved. These improvements benefit
overwhelmingly those who have their own cars -- that is, a tiny
percentage. The bias of the budget allocated for transportation thus
discriminates against the best transportation for the greatest number
- and the huge capital investments in this area ensure that this bias
is here to stay. In some countries, articulate minorities now
challenge the family car as the fundamental unit of transportation in
emerging societies . But everywhere in Latin America it would be
political suicide to advocate radical limitations on the
multiplication of schools. Opposition parties may challenge at times
the need for superhighways or the need for weapons which will see
active duty only in a parade. But what man in his right mind would
challenge the need to provide every child with a chance to go to high
school? 

Before poor nations could reach this point of universal schooling,
however, their ability to educate would be exhausted. Even ten or
twelve years of schooling are beyond 85 per cent of all men of our
century if they happen to live outside the tiny islands where capital
accumulates. Nowhere in Latin America do 27 percent of any age group get
beyond the sixth grade, nor do more than 1 per cent graduate from a
university. Yet no government spends less than 18 per cent of its
budget on schools, and many spend more than 30 per cent. Universal
schooling, as this concept has been defined recently in industrial
societies, is obviously beyond their means. The annual cost of
schooling a United States citizen between the ages of twelve an d
twenty-four equals as much as most Latin Americans earn in two or
three years. 

Schools will stay beyond the means of the developing nations: neither
radical population control nor maximum reallocations of government
budgets nor unprecedented foreign aid would end the present
unfeasibility of school systems aimed at twelve years of schooling for
all. Population control needs time to become effective when the total
population is as young as that of tropical America. The percentage of
the world's resources invested in schooling cannot be raised beyond
certain levels, nor can this budget grow beyond foreseeable maximal
rates. Finally, foreign aid would have to increase to 30 per cent of
the receiving nation's national budget to provide effectively for
schooling, a goal not to be anticipated.

Furthermore, the per capita cost of schooling itself is rising
everywhere as schools accept those who are difficult to teach, as
retention rates rise, and as the quality of schooling itself
improves. This rise in cost neutralizes much of the new
investments. Schools do not come cheaper by the dozen. 

In view of all these factors, increases in school budgets must usually
be defended by arguments which imply default. In fact, however,
schools are untouchable because they are vital to the \emph{status
  quo}. Schools have the effect of tempering the subversive potential
of education in an alienated society because, if education is confined
to schools, only those who have been schooled into compliance on a
lower grade are admitted to its higher reaches. In capital-starved
societies not rich enough to purchase unlimited schooling, the
majority is schooled not only into compliance but also into
subservience. 

Since Latin American constitutions were written with an eye on the
United States, the ideal of universal schooling was a creative
utopia. It was a condition necessary to create the Latin American
nineteenth-century bourgeoisie. Without the pretence that every
citizen has a right to go to school, the liberal bourgeoisie could
never have developed; neither could the middle class masses of
present-day Europe, the United States and Russia, nor the managerial
middle elite of their cultural colonies in South America. But the same
school which worked in the last century to overcome feudalism has now
become an oppressive idol which protects those who are already
schooled. Schools grade and, therefore, they degrade. They make the
degraded accept his own submission. Social seniority is bestowed
according to the level of schooling achieved. Everywhere in Latin
America more money for schools means more privilege for a few at the
cost of most, and this patronage of an elite is explained as a
political ideal. This ideal is written into laws which state the
patently impossible; equal scholastic opportunities for all. 

The number of satisfied clients who graduate from schools every year
is much smaller than the number of frustrated dropouts who are
conveniently graded by their failure for use in a marginal labour
pool. The resulting steep educational pyramid defines a rationale for
the corresponding levels of social status. Citizens are ``schooled''
into their places. This results in politically acceptable fonds of
discrimination which benefit the relatively few achievers. 

The move from the farm to the city in Latin America still frequently
means a move from a world where status is explained as a result of
inheritance into a world where it is explained as a result of
schooling. Schools allow a head start to be rationalized as an
achievement. They give to privilege not only the appearance of
equality but also of generosity: should somebody who missed out on
early schooling be dissatisfied with the status he holds, he can
always be referred to a night or trade school. If he does not take
advantage of such recognized remedies, his exclusion from privilege
can be explained as his own fault. Schools temper the frustrations
they provoke. 

The school system also inculcates its own universal acceptance. Some
schooling is not necessarily more education than none, especially in a
country where every year a few more people can get all the schooling
they want while most people never complete the sixth grade. But much
less than six years seems to be sufficient to inculcate in the child
the acceptance of the ideology which goes with the school grade. The
child learns only about the superior status and unquestioned authority
of those who have more schooling than he has. 

Any discussion or radical alternatives to school-centred formal
education upsets our notions of society. No matter how inefficient
schools are in educating a majority, no matter how effective schools
are in limiting the access to the elite, no matter how liberally
schools shower their non-educational benefits on the members of this
elite, schools do increase the national income. They qualify their
graduates for more economic production. In an economy on the lower
rungs of development toward United States-type industrialization, a
school graduate is enormously more productive than a dropout. Schools
are pan and parcel of a society in which a minority is on the way to
becoming so productive that the majority must be schooled in to
disciplined consumption. Schooling therefore -- under the best of
circumstances helps to divide society into two groups: those so
productive that their expectation of annual rise in personal income
lies far beyond the national average, and the overwhelming majority
whose income also rises, but at a rate clearly below the
former's. These rates, of course, are compounded and lead the two
groups further apart. 

Radical innovation in formal education presupposes radical political
changes, radical changes in the organization of production, and
radical changes in man's image of himself as an animal which needs
school. This is often forgotten when sweeping reforms of the schools
are proposed and fail because of the societal framework we accept. For
instance, the trade school is sometimes advocated as a cure-all for
mass schooling. Yet it is doubtful that the products of trade schools
would find employment in a continuously changing, ever more automated
economy. Moreover the capital and operating costs of trade schools, as
we know them today, are several times as high as those for a standard
school of the same grade. Also, trade schools usually take in sixth
graders, who, as we have seen, are already the exception. Trade
schools pretend to educate by creating a spurious facsimile of the
factory within a school building. 

Instead of the trade school, we should think of a subsidized
transformation of the industrial plant. It should be possible to
obligate factories to serve as training centres during off-hours, for
managers to spend part of their time planning and supervising this
training, and for the industrial process to be so redesigned that it
has educational value. If the expenditures for present schools were
partly allocated to sponsor this kind of educational exploitation of
existing resources, then the final results -- both economic and
educational -- might be incomparably greater. If, further, such
subsidized apprenticeship were offered to all who ask for it,
irrespective of age, and not only to those who are destined to be
employees in the particular plant, industry would have begun to assume
an important role now played by school. We would be on the way to
disabuse ourselves of the idea that manpower qualification must
precede employment, that schooling must precede productive work. There
is no reason for us to continue the medieval tradition in which men
are prepared for the ``secular world'' by incarceration in a sacred
precinct, be it monastery, synagogue or school. 

A second, frequently discussed, remedy for the failure of schools is
fundamental, or adult, education. It has been proved by Paulo Freire
in Brazil that those adults who can be interested in political issues
of their community can be made literate within six weeks of evening
classes. The programme teaching such reading and writing skills, of
course, must be built around the emotion-loaded key words of the
adults' political vocabulary. Understandably this fact has got
Freire's programme into trouble. It has also been suggested that the
dollar-cost of ten separate months of adult education is equal that of
one year of early schooling, and can be incomparably more effective
than schooling at its best. 

Unfortunately, ``adult education'' now is conceived principally as a
device to give the ``underprivileged'' a palliative for the schooling he
lacks. The situation would have to be reversed if we wanted to
conceive of all education as an exercise in adulthood. We should
consider a radical reduction of the length of the formal, obligatory
school sessions to only two months each year but spread this type of
formal schooling over the first twenty or thirty years of a man's
life. 

While various forms of in-service apprenticeship in factories and
programmed maths and language teaching could assume a large proportion
of what we have previously called ``instruction'', two months a year of
formal schooling should be considered ample time for what the Greeks
meant by \emph{schol$\bar{e}$} -- leisure for the pursuit of insight. No
wonder we find it nearly impossible to conceive of comprehensive
social changes in which the educational functions of schools would
thus be redistributed in new patterns among institutions we do not
now envisage. We find it equally difficult to indicate concrete ways
in which the non-educational functions of a vanishing school system
would be redistributed. We do not know what to do with those whom we
now label ``children'' or ``students'' and commit to school.

It is difficult to foresee the political consequences of changes as
fundamental as those proposed, not to mention the international
consequences. How should a school-reared society coexist with one
which has gone ``off the school standard'', and whose industry,
commerce, advertising and participation in politics is different as a
matter of principle? Areas which develop outside the universal school
standard would lack the common language and criteria for respectful
coexistence with the schooled. Two such worlds, such as China and the
United States, might almost have to seal themselves off from each
other. 

Rashly, the school-bred mind abhors the educational devices available
to these worlds . It is difficult mentally to ``accredit'' Mao's party
as an educational institution which might prove more effective than
the schools are at their best -- at least when it comes to inculcating
citizenship. Guerrilla warfare in Latin America is another education
device much more frequently misused or misunderstood than
applied. Che Guevara, for instance, clearly saw it as a last
educational resort to teach a people about the illegitimacy of their
political system. Especially in unschooled countries, where the
transistor radio has come to every village, we must never underrate
the educational functions of great charismatic dissidents like Dom
Helder Camara in Brazil or Camilo Torres in Colombia. Castro described
his early charismatic harangues as ``teaching sessions''. 

The schooled mind perceives these processes exclusively as political
indoctrination, and their educational purpose eludes its grasp. The
legitimation of education by schools tends to render all non-school
education an accident, if not an outright misdemeanor. And yet it is
surprising with what difficulty the school-bred mind perceives the
rigour with which schools inculcate their own presumed necessity, and
with it the supposed inevitability of the system they sponsor. Schools
indoctrinate the child into the acceptance of the political system his
teachers represent, despite the claim that teaching is
non-political. 

Ultimately the cult of schooling will lead to violence, as the
establishment of \emph{any} religion has led to it. If the gospel of
universal schooling is permitted to spread in Latin America, the
military's ability to repress insurgency must grow. Only force will
ultimately control the insurgency inspire d by the frustrated
expectation that the propagation of the school-myth enkindles. The
maintenance of the present school system may tum out to be an
important step on the way to Latin American fascism. Only fanaticism
inspired by idolatry of a system can ultimately rationalize the
massive discrimination which will result from another twenty years of
grading a capital-starved society by school marks. 

The time has come to recognize the real burden of the schools in the
emerging nations, so that we may become free to envisage change in the
social structure which now makes schools a necessity. I do not
advocate a sweeping utopia like the Chinese commune for Latin
America. But I do suggest that we plunge our imagination into the
construction of scenarios which would allow a bold reallocation
of educational functions among industry, politics, short scholastic
retreats and intensive preparation of parents for providing early
childhood education. The cost of schools must be measured not only in
economic, social and educational terms, but in political terms as
well. Schools, in an economy of scarcity invaded by automation,
accentuate and rationalize the coexistence of two societies, one a
colony of the other. 

Once it is understood that the cost of schooling is not inferior to
the cost of chaos, we might be on the brink of courageously costly
compromise. Today it is as dangerous in Latin America to question the
myth of social salvation through schooling as it was three hundred
years ago to question the divine rights of the Catholic kings.