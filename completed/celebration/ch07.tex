% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{The Powerless Church}

\begin{chapquot}
In April of 1967 the secretaries for social action of the Anglican
church met for a consultation. I was invited to attend. Dozens of
social issues were on the table) and on some there was more than one
conflicting position. I had the impression that on each issue the
assembly made an effort to determine which position could be labelled
the Christian one, and if this failed, tried at least to designate
one as more Christian than the other. 

One of my contributions to this conference was the address which
follows. It concerns the role of the church in social change and
development.
\end{chapquot}

It is my thesis that only the Church can ``reveal'' to us the
full meaning of development. To live up to this task the Church must
recognize that she is growing powerless to orient or produce
development. The less efficient she is as a power the more effective
she can be as a celebrant of the mystery.

This statement, if understood, is resented equally by the hierarch who
wants to justify collections by increasing his service to the poor,
and by the rebel priest who wants to use his collar as an attractive
banner in agitation. Both make a living off the social service the
Church renders. In my mind both symbolize obstacles to the specific
function of the Church: the annunciation of the gospel.

This specific function of the Church must be a contribution to
development which could not be made by any other institution. I
believe that this contribution is faith in Christ. Applied to
development, faith in Christ means the revelation that the development
of humanity tends towards the realization of the kingdom, which is
Christ already present in the Church. The Church interprets to modern
man development as a growth into Christ. She introduces him to the
contemplation of this mystery in prayer and to its celebration in her
liturgy. 

I believe that the specific task of the Church in the modern world is
the Christian celebration of the experience of change. In order to
fulfil this task the Church will have to renounce progressively the
``power to do good'' she now has, and see this power pass into the hands
of a new type of institution: the voluntary and ever controversial
embodiments of secular religion. Later I will explain what I mean by
the progressive renunciation of power and the growth of secular
religion. Here I wish to explain what I mean by the celebration of
change.

We have ceased to live against a rigid framework. All enveloping,
penetrating change is the fundamental experience of our age, which
comes as a shock to those brought up in a different age. 

In the past the same experience was exceptional and had many
appearances: exile \ldots{} migration \ldots{} imprisonment \ldots{} overseas
assignment \ldots{} education \ldots{} hospitalization. All these traditionally
represent the sudden loss of the environment which had given form to a
man's feelings and concepts. This experience of change is now faced as
a lifelong process by every individual in technological society. 

In Cuernavaca we have set up a centre at which we train persons to
feel with others what change means to their hearts. What happens to
the intimacy of a person when his familiar surroundings suddenly
disappear, and with them the symbols he reveres? What happens when the
words into which he was taught to pour the stream of his life lose
their accustomed meaning?

What happens to the feelings of a mountain Indian thrown into a
factory? What anguish does the Chicago missionary feel when he is
suddenly exposed to the mountains of Bolivia, and find himself used as
a cover-up for napalm bombs? What happens to the heart of a nun who
leaves the convent? 

These questions are precise and elusive: each must be fitted to the
one heart it opens. 

What threat and what challenge does social change represent to this
individual or to that social group? How does this heart or that common
mood react to a change in setting? We speak about threat and about
challenge because the reaction to transition is very ambiguous. It can
allow for new insights, can open new perspectives and therefore
confront the person with new awareness of choice. In other words,
development can be a setting for salvation which leads to
resurrection. But also transition can reduce a bewildered individual
to a defensive self-centredness, to dependence and aggression; it can
lead into the agony of a lived destruction of life, straight into
hell. 

Neither efficiency nor comfort nor affluence is a criterion for the
quality of change. Only the reaction of the human heart to change
indicates the objective value of that change. All measures of change
which disregard the response of the human heart are either evil or
na\"ive. Development is not judged against a rule but against an
experience. And this experience is not available through the study of
tables but through the celebration of shared experience: dialogue,
controversy, play, poetry -- in short, self-realization in creative
leisure. 

The Church teaches us to discover the transcendental meaning of this
experience of life. She teaches us in liturgical celebration to
recognize the presence of Christ in the growing mutual relatedness
which results from the complexity and specialization of development,
And she reveals to us the personal responsibility for our sins: our
growing dependence, solitude and cravings which result from our
self-alienation in things and systems and heroes. She challenges us to
deeper poverty instead of security in achievements; personalization of
love (chastity) instead of depersonalization by idolatry; faith in the
other rather than prediction. 

Thus the Church does not orient change, or teach how to react to
it. It opens a new dimension of specific faith to an ecumenical
experience of transcendent humanism. All men experience life -- the
Christian believes he has discovered its meaning. 

What the Church contributes through evangelization is like the
laughter in the joke. Two hear the same story -- but one gets the
point. It is like the rhythm in the phrase which only the poet
catches. 

The new era of constant development must not only be enjoyed, it must
be brought about. What is the task of the Church in the gestation of
the new world? 

The Church can accelerate time by celebrating its advent, but it is
not the Church's task to engineer its shape. She must resist that
temptation. Otherwise she cannot celebrate the wondrous surprise of
the coming, the advent. 

The future has already broken into the present. We each live in many
times. The present of one is the past of another, and the future of
yet another. We are called to live knowing that the future exists, and
that it is shared when it is celebrated. The change which has to be
brought about can only be lived. We cannot plan our way to
humanity. Each one of us and each of the groups with which we live and
work must become a model of the era we desire to create. The many
models which will develop should give to each one of us an environment
in which we celebrate our creative response to change with others who
need us. 

Let the Church be courageous enough to lead us in the celebration by
highlighting its depth. Let the Church discern the spirit of God
wherever charismatic gifts call the future into the present and thus
create a model to live. 

Let the Church be \emph{mater et magistra} of this play -- accentuate
its beauty; let her teach us to live change because it is enriching
and joyful, and not just produce it because it is useful.

Awareness of change heightens the sense of personal responsibility to
share its benefits. Awareness of change therefore does not only lead
to a call to celebration but also to a call to work; to the
elimination of obstacles which make it impossible for others to free
themselves from toil and illusion. 

Social change always implies a change of social structure, a change of
formalized values, and finally a change of social character. These
three factors constrain invention and creativity, and action against
these constraints becomes a responsibility of those who experience
them as shackles. Hence, social change involves a triple reaction: 
\begin{enumerate}
\item The reorganization of social structure, which is felt as
  subversion or revolution.
\item The attempt to get beyond public illusions which justify
  structures, which implies the ridicule of ideologies and is felt as
  ungodliness or as education.
\item The emergence of a new ``social character'', which is experienced by
many with utter confusion and anguish. 
\end{enumerate}

Throughout history the Church has participated constantly in the
shaping of social change: either as a force of conservation or as a
force of social promotion. She has blessed governments and condemned
them. She has justified sytems and declared them as unholy. She has
recommended thrift and bourgeois values and declared them anathema. 

We believe that now the moment has come for the Church to withdraw
from specific social initiative -- taken in the name of Church
structure. Let us follow the example of the Pope : have the courage to
allow churchmen to make statements so ephemeral that they could never
be construed as being the Church's teaching.

This withdrawal is very painful. The reason is precisely that the
Church still has so much power -- which has so often been used for
evil. Some now argue that, given the power, it should now be used to
make amends. If the Church at present in Latin America does not use
the power she has accumulated for fundamental education, labour
organization, cooperative promotion, political orientation, she leaves
herself open to criticism -- from without, of creating a power vacuum;
and from within, in the terms of ``if anybody, the Church can bear
having power, because she is self-critical enough to renounce its
abuse!'' 

But if the Church uses the power basis she has -- for example, in the
field of education -- then she perpetuates her inability to witness to
that which is specific in her mission. 

Social innovation is becoming an increasingly complex
process. Innovative action must be taken with increasing frequency and
sophistication. This requires men who are courageous, dedicated,
willing to lose their careers. I believe that this innovative action
will increasingly be taken by groups committed to radically humanist
ideals, and not gospel authority, and should therefore not be taken by
Churches.

The modern humanist does not need the gospel as a norm; the Christian
wants to remain free to find through the gospel a dimension of
effective surprise beyond and above the humanistic reason which
motivated social action.

The social-action group needs operational freedom: the freedom to let
convenience or opportunism dictate the choice of priorities of
objectives, tactics and even strategy. The same social goal might be
intended by two opposed groups, one choosing violence as a method, the
other non-violence. 

Social action by necessity divides tactical opponents. But if
organized around deeply held, radically human, ideological tenets, it
also acts as a powerful catalyst for new forms of secular ecumenicism:
the ecumenicism of action springing from common radical
conviction. 

Ideological tenets generate secular-religious, civic-religious
ideas. Social action organized around such ideas, therefore, frees the
Church from the age-old dilemma of risking its unity in the
celebration of faith in favour of its \emph{service} to
controversial charity.

The Christian response has been deeply affected by the acceleration of
time; by change, development, by growth having become normal and
permanence the exception. Formerly the king could be at the opposite
pole from the priest, the sacred from me profane, the churchly from
the secular, and we could speak about the impact which one would have
on the other.

We stand at the end of a century-long struggle to free man from the
constraint of ideologies, persuasions and religions as guiding forces
in his life. A non-thematic awareness of the significance of the
incarnation emerges: an ability to say one great ``Yes'' to the
experience of life. 

A new polarity emerges: a day-by-day insight into the tension between
the manipulation of things and the relationship to persons. 

We become capable of affirming the autonomy of the ludicrous in face
of the useful, of the gratuitous as opposed to the purposeful, of the
spontaneous as opposed to the rationalized and planned, of creative
expression made possible by inventive solution. 

We will need ideological rationalizations for a long time to achieve
purposefully planned inventive solutions to social problems. Let
consciously secular ideology assume this task. 

I want to celebrate my faith for no purpose at all.
