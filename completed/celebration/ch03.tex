% !TEX root = celebration.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\chapter{Not Foreigners, Yet Foreign}


\begin{chapquot}
From 1951 to 1956 I lived as a priest in Incarnation Parish on the
West Side of New York's Manhattan. Puerto Ricans were then being
crowded into the walk-ups between Amsterdam Avenue and Broadway. They
were displacing many families who had moved a generation earlier
straight from Ireland to this area. I became involved in the
inevitable conflict between these peoples and also in the controversy
about its meaning. 

As a newcomer to the United States I was surprised
to see how New Yorkers, from druggist to mayor, fell back upon ready
stereotypes to guide their policy decisions. Whatever was worth
understanding about Puerto Ricans, they apparently felt, could be
explained in old categorical terms coined for preceding groups of
immigrants. That which had served for the Poles or Italians should fit
the Puerto Ricans. 

At that time I tried to obtain recognition of the fact that, at least
for the Roman Catholic Church, the Puerto Rican immigration
represented a phenomenon without precedent. 

Amazingly, I found an inquisitive listener to my opinion in Cardinal Spellman.
\end{chapquot}


%\newpage




After the introduction of the quota system in 1924, it seemed that
the melting process in New York City was finally about to catch up with
the number of people tossed into the pot. Then in the late 1940s, the
city was presented with a novel challenge, an invasion of
American-born ``foreigners'', the Puerto Ricans. In Vito Marcantonio's
heyday (1943) there were less than thirty five thousand Puerto Ricans
in New York; at present (1956) there are more than half a million, and
indications are that the migration has not yet reached its peak. 

These Puerto Ricans are not foreigners, and yet they are more foreign
than most of the immigrants who preceded them. About this seeming
paradox the well-meaning should be well-informed, since to be received
kindly merely because one is a foreigner is a cold kind of
condescension: the chances are that the man who thus receives you is
determined never really to know you. 

If on the one hand a man consistently designates you a foreigner, he
usually precludes any possibility of appreciating that which is unique
to your group -- besides the fact that it is not his own. If on the
other hand, misunderstanding St Paul's instruction to make himself Jew
with the Jews and Greek with the Greeks, he approaches you with an
exegesis such as ``We are all Americans'', he denies your right, and
his, to a heritage, to be human, with roots reaching back in
history.

This fallacy is at the bottom of the attitude of many well meaning
people toward the Puerto Rican immigrants: let them do what the Irish
or Italians did, or let them attempt what the Jews attempted; let them
grow gradually through their own national parishes, territorial
ghettos and political machines to full ``Americanization''; let them
vociferously asert that they are as good Americans as the man next
door. These attitudes are very common in New York, where the arrival
of successive migratory waves is taken for granted. It is too often
gratuitously assumed that the future novel about the Puerto Rican
journey will be fashioned after either \emph{The Last Hurrah or Marjorie
Morning-Star}, or will be a combination of both. 

The welfare investigator who says to Jos\'e Rivera, ``My parents went
through the same experience,'' neither lies nor expresses xenophobia -
he just misunderstands, like the politician who tries again to use
methods which worked when Italian was spoken in Harlem.

When the Irish and the Germans came here a century ago, New York City
was faced with a challenge of a kind never experienced before and of a
size never to be duplicated. In 1855 one-third of the city's
population (500,000) consisted of immigrants who had arrived in the
previous decade; against this proportion the one-fifteenth of the
city's population which in 1955 consisted of recently arrived Puerto
Ricans (again 500,000) seems insignificant. 

In the days of the heavy influx to America, wave after wave of
immigrants arrived, settled, and became accustomed to new patterns of
life. The newcomers spoke different languages, worshipped in different
churches, came from different climates, wooed in different fashions,
ate different dishes, sang different songs. But under these apparent
differences they had much in common. They came from the Old Continent
and arrived as refugees or settlers to become Americans and to stay
for good. They brought their own clergy - rabbi, priest or minister -
and the symbols of pa t millennia which were their own, St Patrick,
the Mafia or Loretto, no less than the Turnverein. They settled in
special sections of the city and kept to themselves for years before
they ventured to take part in that experience new to all of them: life
in a pluralistic society. They fell into a common pattern, and it is
no wonder that those who had been here long enough to consider
themselves part of a settled stratum fell into the habit of assuming a
priori that each new incoming group would be analogous to theirs. This
assumption, in fact, proved to be true until after the Second World
War, with the exception of two groups, the Orientals and the Southern
Negroes. 

Then suddenly the Puerto Ricans arrived en masse. New York
had never before known such an invasion, an invasion of Americans who
came from an older part of the New World into New York, which, by the
way, had been part of the diocese of San Juan long before Henry Hudson
discovered Manhattan. And New York had never had to deal with
American-born citizens who in their schools had learned English as a
foreign language. 

These strange Americans were sons of a Catholic country where for
centuries slaves had found refuge, where the population of a little
over two million is overwhelmingly white but where a difference in the
shade of the skin is no impediment, either to success or to
marriage. Yet theirs was the first sizeable group coming from overseas
into New York to be tagged by many as ``coloured'', much less because of
the racial heredity of some than for the vaguely sensed great
difference between them and former new arrivals.

This was a new type of immigrant: not a European who had left home for
good and strove to become an American, but an American citizen, who
could come here for the time between one harvest and another and
return home for vacation with a week's salary spent on an air-coach
ticket. This was not the fugitive from racial or religious persecution
in his own country, but the child of ``natives'' in a Spanish colony or
perhaps the descendant of a Spanish official in the colonial service;
not a man accustomed to be led by men of his own stock -- priest,
politician, rebel or professor -- but for four hundred years a subject
in a territory administered by foreigners, first Spanish, then
American, only recently come into its own. 

The new arrival from Puerto Rico was not the Christian in his own
right who received the Faith from the sons of his own neighbours, but
the fruit of missionary labour typical of the Spanish Empire. He was a
Catholic, born of parents who were also Catholics, yet he received the
Sacraments from a foreigner because the government was afraid that to
train native priests might be to train political rebels. 

Even the physical configuration of the world from which he came was
different. He was a man from an island where nature is provident and a
friend, where field labour means much more harvesting than
planting. When nature rebels every few decades, he is powerless; in
the hurricanes he cannot but see the finger of God. 

Until recently nobody in Puerto Rico built a house with the idea that
it should survive the elements or withstand the climate by air
conditioning. What a difference from the Pole and the Sicilian, both
of whom built houses to withstand nature, climate and time, both of
whom built houses to separate their lives from that of nature. One
might have come from the Russian steppe or the ghetto and the other
from an olive grove on the coast but both knew what winter meant; they
knew that a house was there to protect them from the cold, a place
within which to make a home. It was easy for the Pole and the Sicilian
to settle in tenements and to live confined there. But the new
immigrant from the tropics knew no winter, and the home he left was a
hut \emph{in} which you slept but \emph{around} which you lived
with your family. The hut was the centre of his day's activities, not
their limit. To come to a tenement, to need heat, to need glass in
your windows, to be frowned on for tending to live beyond your doors --
this was all contrary to the Puerto Rican's traditional habits, and as
surprising to him as it is for the New Yorker to realize that for any
immigrants these basic assumptions of his life should be
surprising. 

The new Puerto Rico, the Puerto Rico of 1956, is studded with concrete
houses and opens a new factory every day. It is a proving ground for
the most advanced forms of community organizations, and it has the
fastest-declining rate for illiteracy and the fastest-falling
mortality in the world. Yet these facts must not make us think that
the traditional outlook of its people has changed or will change
tomorrow. These material improvements are the outcome of the first
decade of M\'u\~noz Mar\'in's administration, but they do not wipe out the
Island's past nor are they intended to make of San Juan a suburb of
New York. 

The differences between the Puerto Rican migration and the influx of
Europeans are fundamental. In deed, in the shortness of its history
Puerto Rico is more foreign to Europe than to America. These
differences account for much of the distinctive behaviour
characteristics of Puerto Ricans in New York, and the lack of
knowledge of these differences accounts for many misunderstandings on
the part of old New Yorkers. 

Many a Puerto Rican does not leave the Island with a clear plan of
settling on the mainland. How can a man who leaves on the spur of the
moment, planning to make a fast dollar in New York and be back as soon
as he has enough to buy a store, take roots in New York? I remember
one woman who was in despair because her husband had disappeared on
his way to the cane fields, carrying his machete. She thought, of
course, that a rival had grabbed him from her. And then, after a week,
she got a money order from Chicago. On the way to the cane field he
had run into a hiring gang and decided to try his luck -- and that was
the reason he neglected to come home for dinner. In a case like this,
in which a man ``drops in'' on New York, with no intentions of staying
but of eventually commuting ``home'', how can the transient have the
same effect on his neighbourhood in New York as the old immigrant who
came to stay? Yet the statistical curve of emigration from the Island
is in exact correlation with the curve of employment on the
mainland. If employment is scarce, the reflux increases
correspondingly. Many, even after years in New York, feel they got
stuck there because of money. 

With the arrival of hundreds of thousands from Puerto Rico and the
other Central American states (it is estimated that more than
one-quarter of New York's Spanish-American population is not Puerto
Rican), not only a new language but a new pattern of living has been
added to the city. Instead of the strangers speaking only a foreign
tongue who formerly arrived exhausted from the long journey, American
citizens, all of whom know some basic English, arrive in aeroplanes
within six hours of leaving their tropical island. 

The old immigrants settled in national neighbourhoods; the new
transatlantic commuter spreads out all over the city; ten years after
the beginning of the Puerto Rican mass influx Spanish has already
become ubiquitous in New York, Unlike European immigrants, all Puerto
Ricans know some English, and this helped, but there is another factor
that has contributed to Latin Americans spreading to all quarters of
the city , In former times when a neighbourhood became a centre for
the newest immigrant group, it was either a slum or tended to become
one. And once a neighbourhood had deteriorated it hardly ever was
redeemed. The great immigration from Puerto Rico started after the
Second World War, due to such factors as cheap air transportation,
acquaintance with the mainland acquired by many during service in the
army, rising education under the new political order on the island,
and, last but not least, the growing pressure of a population which
has more than doubled since the beginning of the century. At that same
time the city was embarking on its great slum clearance programme and
the first blocks to be tom down were almost invariably those where the
newest and poorest immigrant had just settled. As a result, the Puerto
Ricans began to be resettled all around town in new projects and on a
nondiscriminatory basis.

Considering this dispersal and the tendency to commute to the Island,
it is no wonder that there are hardly any Puerto Rican national
neighbourhoods in the traditional sense in New York. One result is
that it is difficult for Puerto Ricans to develop local grass-roots
leadership within their own group; either their concentration per city
block is too thin, or the intention to stick to the neighbourhood is
absent, or the necessity to organize in association with their own is
weak because all are citizens who at least understand some English and
have official ``protection'' from the Commonwealth government labour
office - the first instance of something like a ``Consulate for
American Nationals''. And there is no doubt that another factor
contributing to the relative lack of leadership is caused by hundreds
of years of colonial administration.

Thus Puerto Ricans in New York find it more difficult than groups
which came before them to form their own in-group leadership, if they
do not find it completely impossible. This fact gives them a very real
advantage over former migrations in one sense, because it almost
forces them into an active participation in the established
community. On the other hand, the sudden challenge of having to
participate in a settled New York community proves too arduous for
many who might have been able to become leaders in their own
cliques. 

A lack of consideration on the part of New York civic leaders for the
distinctive character of this new Puerto Rican migration, as compared
to previous immigrant experiences, can do real damage to the community
by either retarding or injuring the new pattern of assimilation which
will have to form. If this lack of understanding should be present in
the leaders of the Catholic Church, it can seriously damage
souls. 

One-third of the baptized Catholics in Manhattan and the lower Bronx
are Spanish-Americans at this moment. The Puerto Ricans are the first
group of Catholics with a distinctly non-European tradition of
Catholicism to come to the East Coast. The lack of native priests, due
to the colonial and imperialistic atmosphere of more than four
hundred years of the Island's history, and also the special approaches
due to missionary conditions, have profoundly moulded the behaviour of
Puerto Ricans as Catholics.

Notwithstanding the very recent trend toward rapid urbanization, the
majority of Puerto Ricans are dispersed over the steep hills of the
interior, living in huts in the midst of small clearings among bananas
and flamboyants, with magnificent views, but too far from church to
attend Mass every Sunday. Traditionally, they take the Sacraments on
those rare occasions when the priest comes to visit them in the chapel
in their barrio -- but for generations they have had to baptize their
own children because the priest came so seldom. Under such
circumstances regular attendance at Sunday Mass is not a confirmed
element of Catholic practice. Living habits of the tropics,
feudal-colonial social organization, and the confluence of Indian,
African and European cultures played their part. The Church's law
declaring a marriage between two Catholics valid even when not entered
into before a priest, if a priest could not be available in less than
a month, made people forget the need for a priest. It had an adverse
effect on the frequency of marriages in church, and still has today.

``Bad habits'' like these are not a sign of lack of Catholic spirit, but
rather the effects of a peculiar ecclesiastical history. Many United
States Catholics are used to a wide variety of national customs in
national parishes and a great difference in practices among various
ethnic groups; when faced with the lack of ``practice'' of their faith
by Puerto Ricans, they might be tempted to identify them with some
other foreign group in whom the effects of a different background show
up in similar behaviour, or might even deny altogether that Puerto
Ricans are Catholic. But for anybody who has ever breathed the
atmosphere of the Island there is no doubt that theirs is a Catholic
folk-culture: children who might never make their First Communion will
regularly ask their parents' blessing before leaving the house; people
who might never have been taught the catechism will devotedly invoke
the names of Our Lord or the Virgin and plaster their homes with holy
pictures and sign themselves with the Cross before leaving home. Even
the fact that a man refuses to get married in church sometimes
testifies for rather than against his Catholicity; he does not want to
bind himself forever by a Church marriage. 

In Puerto Rico God's house extends from the church into the plaza. Not
only do the processions or posedas require the out-of-doors as a
continuation of the church, but also the church is often too small,
and throngs attend Mass by looking doors and windows. Unless his
neighbour on the mainland understands the different meaning ``family'',
``church'', or ``home'' has for a man from the tropics, he will not
understand why Jos\'e plays the guitar on his doorstep, or why Mar\'ia
walks from statue to statue during Mass for a little chat with the
saints or perhaps enters church only after service, because she is
repelled by the formality of the ushers.

All of this points towards the need the Puerto Ricans have to win some
respect for their background. What they need is not more help but less
categorization according to previous schemes, and more
understanding. Only thus will they be able to make the unique
cultural, political and economic contribution for which they seem
destined: Spanish-Christian tradition, a Catholicism in which is taken
for granted an eminently Christian attitude towards the mixing of
races, a freshness and simplicity of outlook proper to the tropics, a
new pattern of political freedom.in association with the United
States, a bridge between the hemispheres politically and culturally no
less than economically -- these are only a few of the assets that the
mass migration of Puerto Ricans to the mainland can contribute to New York
and the United States.

