
% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{An Unteachable Generation}


But this is also a hard generation to teach in colleges, what I think
out to be taught. I do not mean that the students are disrespectful,
or especially lazy, or anything like that; in my experience, they pay
us more respect than we usually deserve and they work as earnestly as
could be expected trying to learn too much on too heavy schedules. Of
course, as I have been arguing, many of the students, probably the
majority, ought not to be in a scholastic setting at all, and their
presence causes dilution and stupefying standardization as well as
overcrowding. But let us here concentrate on several other
difficulties that are in the very essence of present-day higher
education.
\begin{enumerate}

\item The culture we want to pass on is no longer a culture for these
  young; the thread of it has snapped.

\item These young are not serious with themselves; this is a property
  of the kind of culture they do have.

\item And as with the lower schools, the auspices, method and aims of
  the colleges themselves are not relevant to good education for our
  unprecedented present and foreseeable future.
\end{enumerate}

The culture I want to teach -- I am myself trapped in it and cannot
think or strive apart from it -- is our Western tradition: the values
of Greece, the Bible, Christianity, Chivalry, the Free Cities of the
twelfth century, the Renaissance, the heroic age of Science, the
Enlightenment, the French Revolution, early-nineteenth-century
Utilitarianism, late-nineteenth-century Naturalism.

To indicate what I mean, let me mention a typical proposition about
each of these. The Greeks sometimes aspire to a civic excellence in
which mere individual success would be shameful. The Bible teaches
that there is a created world and history in which we move as
creatures. Christians have a spirit of crazy commitment because we are
always in the last times. Chivalry is personal honor and loyalty, in
love or war. Free Cities have invented social corporations with
juridical rights. The Renaissance affirms the imperious right of
gifted individuals to immortality. Scientists carry on a disinterested
dialogue with nature, regardless of dogma or consequence. The
Enlightenment has decided that there is a common sensibility of
mankind. The Revolution has made equality and fraternity necessary for
liberty. Utilitarian economy is for tangible satisfactions, not busy
work, money or power. Naturalism urges us to an honest ethics,
intrinsic in animal and social conditions.

Needless to say, these familiar propositions are often in practical
and theoretical contradiction with one another; but that conflict too
is part of the Western tradition. And certainly they are only ideals
-- they never did exist on land or sea -- but they air the inventions
of the holy spirit and the human spirit that constitute the
university, which is also an ideal.

Naturally, as a teacher I rarely mention such things; I take them for
granted as assumed by everybody. But I am rudely disillusioned when I
find that both the students and my younger colleagues take quite
different things for granted.

For instance, I have heard that the excellence of Socrates was a
snobbish luxury that students nowadays cannot afford. The world
'communicated' in the mass media is, effectually, the only world there
is. Personal loyalty leaves of with juvenile gangs. Law is power. Fame
is prestige and sales. Science is mastering nature. There is no such
thing as humanity, only patterns of culture.  Education and ethics are
what we programme for conditioning reflexes. The purpose of political
economy is to increase the gross national product.

These are not foolish propositions, though I state them somewhat
sarcastically. They make a lot of theoretical sense and they are
realistic. It is far better to believe them than hypocritically to
assert ancient clich\'es. The bother with these views, however, is that
they do not structure enough life or a worthwhile life; that is, as
ideals they are false, and, if they do not pretend to be ideals, what
will one do for ideals!

I think that this lack of structure is felt by most of the students
and it is explicitly mentioned by many young teachers. They regard me,
nostalgically, as not really out of my mind but just out of time and
space -- indeed, I am even envied, because, although the traditional
values are delusions, they do smugly justify, if one believes them and
tries to act them. The current views do not mean to justify, and it is
grim to live without justification.

There is not much mystery about how the thread of relevance
snapped. History has been too disillusioning. Consider just the recent
decades, overlooking hundreds of years of hypocrisy.  During the First
World War, Western culture already disgraced itself irremediably (read
Freud's profound expression of dismay). The Russian Revolution soon
lost its utopian \'elan, and the Moscow Trials of the 1930s were a
terrible blow to many of the best youth. The Spanish Civil War was
perhaps the watershed -- one can almost say that Western culture
became irrelevant in the year 1938. Gas chambers and atom bombs showed
what we were now capable of, yes our scientists. The progress of the
standard of living has sunk into affluence, and nobody praises the
'American Way of Life'. Scholars have become personnel in the
Organization. Rural life has crowded into urban sprawl without
community or the culture of cities. And the Cold War, deterrence of
mutual overkill, is normal politics.

In this context, it is hard to talk with a straight face about
identity, creation, Jeffersonian democracy or the humanities.

But of course, people cannot merely be regimented; and we see that
they find out their own pathetic, amiable or desperate
ideals. Creatureliness survives as the effort to make a `normal'
adjustment and marriage, with plenty of hypochondria. The spirit of
apocalypse is sought in hallucinogenic drugs. There is para-legal
fighting for social justice, but it is hardly thought of as politics
and `justice' is not mentioned. On the other hand, some poor youths
seems to have quite returned to the state of nature. Art regains
certain purity by restricting itself to art-action.  Pragmatic utility
somehow gets confused with doing engineering. Personal integrity is
reaffirmed by `existential commitment', usually without rhyme or
reason.

Unfortunately, none of this, not all of it together, adds up.

I can put my difficulty as a teacher as follows: It is impossible to
convey that Milton and Keats were for real, that they were about
something, that they expended that what they had to say and the way in
which they said it made a difference. The students now (not
brilliantly) tell you about the symbolic structure or even something
about the historical context, though history is not much cultivated:
but, if one goes back more than thirty years, they don't have any
inkling that these peers were writers and in a world. And, not
surprisingly, young people don't have ancient model heroes any more.

Since there are few self-justifying ideas for them to grow up on,
young people do not gain much confidence in themselves or take
themselves as counting. On the other hand, they substitute by having
astonishing private conceits, which many of them take seriously
indeed.  The adults actively discourage earnestness. As James Coleman
of Johns Hopkins has pointed out, the `serious' activity of youth is
going to school and getting at least passing grades; all the rest --
music, driving, ten billions annually of teenage commodities, dating,
friendships, own reading, hobbies, need for one's own money -- all
this is treated by the adults as frivolous. The quality or meaning of
it makes little difference; but a society is in a desperate way when
its music makes little difference. In fact, of course, these frivolous
things are where normally a child would explore his feelings and find
his identity and vocation, learn to be responsible; nevertheless, if
any of them threatens to interfere with the serious business -- a
hobby that interferes with homework, or dating that makes a youth want
to quit school and get a job, it is unhesitatingly interrupted,
sometimes with threats and sanctions.

At least in the middle class, that fills the colleges, this technique
of socializing is unerring, and the result is a generation not notable
for self-confidence, determination, initiative or ingenuous
idealism. It is a result unique in history: \emph{an elite that had imposed
on itself morale fit for slaves}.

The literature favored by youth expresses, as it should, the true
situation. (It is really the last straw when the adults, who have
created the situation for the young, then try to censor their
literature out of existence.) There are various moments of the
hang-up. Some stories simply `make the scene', where making the scene
means touring some social environment in such a way that nothing
happens that adds up, accompanied by friends who do not make any
difference. Such stories do not dwell on the tragic part, what is
missed by making the scene. Alternatively, there are picaresque
adventure-stories, where the hipster heroes exploit the institutions
of society, which are not their institutions, and they win private
triumphs. More probingly, there are novels of sensibility, describing
the early disillusionment with a powerful world that does not suit and
to which one cannot belong, and the subsequent suffering of wry and
plaintive adjustment. Or alternatively, the phony world is
provisionally accepted as the only reality, and the whole
apocalyptically explodes. Finally, there is the more independent Beat
poetry of deliberate withdrawal from the unsatisfactory state of
things, and making up a new world out of one's own guts, with the help
of Japanese sages, hallucinogens and introspective physiology. This
genre, when genuine, does create a threadbare community -- but it
suits very few.

In order to have something of their own in a situation that has
rendered them powerless and irresponsible, many young folk maintain
through thick and thin a fixed self-concept, as if living out
autobiographies of a life that has been already led. They nourish the
conceit on the heroes of their literature, and they defend it with
pride or self-reproach. (If comes to the same thing whether one says,
'I'm the greatest' or `I'm the greatest goof-off'). They absorbedly
meditate this biography and, if vocal, boringly retell it. In this
action, as I have said, they are earnest indeed, but it is an action
that prevents becoming aware of anything else or anybody else.

It is not an attitude with which to learn an objective subject-matter
in college.

It is also a poor attitude for loving or any satisfactory sexual
behavior. Let me devote a paragraph to this.

In my opinion, the virulence of the sexual problems of teenagers is
largely caused by the very technique of socialization, and the
irresponsibility consequent on it. (Of course this is not a new
thing.) If the young could entirely regulate themselves according to
their own intuitions and impulses, there would be far more realism and
responsibility: consideration for the other, responsibility for social
consequences, and sincerity and courage with respect to own feelings.
For example, normally, a major part of attractiveness between two
people is fitness of character -- sweetness, strength, candor,
attentiveness -- and this tends to security and realism. We find
instead that young people choose in conformity to movie-images, or to
rouse the envy of peers, or because of fantasies of brutality or even
mental unbalance. In courting, they lie to one another, instead of
expressing their need; they conceal embarrassment instead of
displaying it, and so prevent feeling from deepening. Normally, mutual
sexual enjoyment would lead to mutual liking, closer knowledge, caring
for; as Sr. Thomas says, the chief human end of sexual contact is to
get to know the other. Instead, sexual activity is used as a means of
conquest and epic boasting, or of being popular in a crowd; and one
wants to be `understood' before making love. Soon, if only in sheer
self-protection, it is necessary \emph{not} to care for or become emotionally
involved. Even worse, in making love, young people do not follow
\emph{actual} desire, which tends to have fine organic discrimination and
organic prudence; rather, they do what they think they ought to
desire, or they are for kicks or for experiment. Normally, pleasure is
a good, though not infallible, index that an activity is healthy for
the organism; but what one \emph{thinks} will give pleasure is no index at
all.  There is fantastic excessive expectation, and pretty inevitable
disappointment or even disgust.  That is, much of the sexual behavior
is not sexual at all, but conformity to gang behavior because one has
no identity; or proving because one has no other proofs; or looking
for apocalyptic experience to pierce the dull feeling of
powerlessness.

The confusion is not helped by the adult hypocrisy that says, `Sex is
beautiful, a divine creation for later.' It is a pretty makeshift
creation that has such poor timing. Is it rather not the duty of
society to make its schedule more livable? Consider the following: A
psychiatrist in charge of Guidance at a very great university gave a
melancholy account of the tragedy of unmarried pregnancy among
co-eds. A co-ed asked him why, then, the infirmary did not provide
contraceptives on request. But he refused to answer her question.

Still, the chief obstacle to college teaching does not reside in the
break with tradition nor in the lack of confidence and earnestness of
the students, but in the methods and aims of the colleges
themselves. Let me devote the remainder of this little book to this.



