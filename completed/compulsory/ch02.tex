% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{Visiting a School}
\label{ch-02-visiting}

Leading a seminar in the philosophy of Education --
it was at Antioch -- I was met by the pointblank question: `What would
you say is the chief use of the elementary schools?' After a moment, I
found myself blurting out, `Why, I suppose it's to undo the damage
done at home, so the child can begin to breathe again and be curious.
At this the student-teachers laughed and asked what was the purpose of
high school. I had to answer of course, `To undo the damage done by
the elementary schools.' And any one who has had the misfortune of
teaching college freshmen will agree that the chief aim of at least
the freshman year in college is to try and undo some of the damage
done by the high schools. No doubt, the chief purpose of the school of
hard knocks is to undo the effects of the colleges. But perhaps, in
our highly interlocked society, it's all or apiece. 

We can look at it the other way and say that the chief purpose of the
elementary schools is to relieve the home to baby-sit. But if is such
an expensive kind of baby-sitting! -- it costs annually \$650 a child
in a New York City grade school, about \$1000 in high school. 

What kind of alternative environment to the home does the school in
fact give?  What is its animal and moral tone? Let me describe a
school visit. (West Side of Manhattan, predominantly a Puerto Rican
area.) 

It was a luckily small class -- I counted less than twenty --
exceptional in the New York system where the classes run to
thirty-five and more, though the technical `average' is twenty-
nine. The lesson was on weather, and what the teacher wanted was to
get them to say that temperature varies with altitude, latitude and
season. She intended to write these words on the board, and that's
what she was after. She was not a bad gal, but a poor teacher. She did
not ask about the weather in Puerto Rico, where several had
undoubtedly been. 

But the salient condition in that class was that every few moments --
during the forty minutes I sat there, about ten times -- she said,
`Now speak louder! I can't hear. Louder!' And she repeated her
question on latitude and altitude. It was obvious enough that the
foreground object, which in fact demanded pedagogy, was that the
children were muffled. They sat in a dejected posture, and they could
not, physically could not, throw out their voices. Psychosomatically,
they could not then have a very aggressive attitude toward the subject
matter either, toward grasping if, taking it in. 

One expedient would have been to go close to the kid and say, `Sit up,
young lady, and breathe from the diaphragm. Here, where I'm touching
you. You aren't using these muscles. See, if I press your ribs they
hurt. Now move your bottom ribs here -- that's the diaphragm, and
throw out your breath and shout \ldots What should you shout? Just shout
Shout!' If she had done that, the children would very soon have been
able to throw their voices; they would have been breathing better;
some of them might have gotten a little dizzy. They would also soon
have had a more aggressive attitude towards the lesson. 

It happened in that school, however, that the principal was a little
of a maniac on noise. He had instituted silent passing in the
halls. He explained to me that the Puerto Rican children were very
wild; if he let them talk to one another between classes, they soon
became boisterous, and then ten minutes of the class time was spent in
calming them down. They were calmed down only too well, for the forty
minutes of every hour. 

While I was talking with him, there was a petty noise in the hall. He
jumped out of his skin and rushed with angry shouts, to find who had
made that noise. Clearly, if the teacher had had the kids shout, a
supervisor would have descended on her, and she would have had
something to answer for. 

All night, one went into the shop class, where there was some
noise. But here was a kid who couldn't hammer a nail; he missed, or he
controlled the swing and only tapped. The teacher did nothing about
it. But it seemed to me that the way the kid kept his shoulders tense
-- perhaps against letting out hostility -- he couldn't hammer, any
more than he could throw a punch. The important thing in a
seventh-grade shop class is not for the kid to make a box, but for him
to do it with force and grace, to become an amateur carpenter, and
then he might make fine boxes. Would not a good teacher work on this
specifically, on force and grace and the emotional liberation of motor
execution? 

In the gym they were doing the President's push-ups, ordained by John
Kennedy who was eager to have the youth physically fit. Apparently,
physical fitness was going to be achieved by the ability to do certain
callisthenic exercises; or at least it would be measured by the
exercises -- the teacher would have something to write down. The
children were doing the prescribed number of push-ups, the prescribed
number of chin-ups. In fact, three quarters of the class were faking
the chin-up outrageously (just as I always did), elbows rigid, holding
breath, but making like they had done the exercise. In a few cases,
this was probably positively damaging, straining; in most cases, the
business was a perfunctory proof performance, meaning `OK, let me
alone'. In some cases, it would be part of a distaste and shame for
the use of one's body altogether, not giving in to the physical
effort. 

It seems to me that the authentic methods for elementary
physical education -- but of course no normal schools are teaching
such methods -- are eurhythmics and what the psychologists call
character analysis, the training of emotion and the liberation of
inhibited emotion, by psychosomatic and muscular behavior. The problem
of each of these children is that he is unable to express his anger,
his grief, and his sexuality. Yet nothing is done to unblock the fear
of expression, of body contact, of nakedness. There is a kind of music
teaching, but it does not seek to draw on actual feeling and to
integrate feeling by rhythm and harmony. 

Rather amusingly, this same school happened to have an
extra-curricular activity, social dancing, with the children as
drummers, initiated by a lively young woman teacher. Dancing their
cha-cha, the children were marvelous little acrobats, with plenty of
grace and force and intricate rhythms that would have delighted
Dalcroze. They had no need of the President's exercises at all. I
chauvinistically thought it was a pity that they were not also
learning to notate all this and go on to composition -- but that no
doubt would have spoiled it. 

What, realistically, was a member of Last Board 6 \& 8 to advise, as a
result of that visit?  Suppose that the gym teacher, or the shop
teacher, or the teacher of meteorology did pursue a proper educational
course. Without doubt, in some cases there would be a great outburst
of dammed up hostility, and plenty of tears. A child might go home and
tell off his father. He might even tell off the teacher. The
expression of that hostility -- and even more, the expression of grief
or sexual desire -- might lead to the most horrendous
consequences. The church would complain. The newspapers that thrive on
pornography and murder would surely note with alarm. The mayor would
be called on. And the teacher would very soon be fired. Not -- my
guess is -- would the Teachers Union come to the rescue. 

But the children might get over their retroflexed rage and shame,
relax their reactive stupidity (almost as stupidity is a `defense'),
and find themselves again in a \emph{possible} environment.
