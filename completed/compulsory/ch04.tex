% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{A Proposal To Extend Compulsory Schooling}


In an address to the American Bankers Association on 24 February 1964,
the Secretary of Labor proposed to \emph{extend} compulsory schooling to the
age of eighteen. This at a time when in New York a Kings County Grand
Jury proposed \emph{reducing} it to fifteen and giving the superintendent of
schools leeway to kick out the unruly. In many schools in the country
policemen are stationed to keep guard over youngsters who do not want
to be there. And the majority of drop-outs who were cajoled into
returning to school in 1963 soon dropped out again, since nothing
essential had been changed in purpose, method or curriculum; they only
suffered a new humiliation by being conned. And -\emph{ verb. sat. sap.} -
older lads tend to be heavier and to carry more powerful armament. Did
the Secretary of Labor think it through?

In some places, e.g. Milwaukee, the compulsory age is at present
eighteen, and the following arrangement: if, after sixteen, a
youngster has a job, he goes to Continuation School (Milwaukee
Vocational) one day a week; if he has no job, he attends full time,
till eighteen. How does this work out? An administrator of the school
tells me, `We don't teach them anything, neither academic subjects nor
a trade. They're ineducable, and there aren't any jobs for them to
train for anyway. But we do try to improve their attitude. Of course,
we usually only have them for about seven months.'  

`Why? What happens to them?'  

 `Oh, they join the Army or end up in Wales'-- Wales is the reform
school.  

Naturally I become indignant and say, `What would become of \emph{your}
attitude if I caged you in a schoolroom and didn't even attempt to
teach you anything? Wouldn't it be better to go to an honest jail?'
But these angry questions do not seem to flurry him at all. Obviously
I am not in touch with the concrete realities of his situation.  

In his address, Secretary Wirtz makes the usual correlation between
employment and years of schooling: `The unemployment rate for
individuals today with less than five years of school is 10.4 per
cent. For those with nine to eleven years of school, it is 7.8 per
cent; for those with thirteen to fifteen years of school, 4 per cent;
but for those with sixteen or more years of school, the unemployment
rate drops to 1.4 per cent.'

But these figures are unimpressive. As he himself implies in another
context, the prima facie explanation of the correlation is the
parents' income: by connections, manners and aspirations, middle-class
children get middle-class jobs; schooling is an incidental part of
it. Lower-class children used to get lower-class jobs, but just these
jobs have petered out; in the present structure of the economy, the
money and jobs do not peter down. Similarly, the docility, neatness of
appearance, etc. that are useful for getting petty jobs, are not
created by years of schooling but they are accurately measured by
them. In my opinion, the same line of criticism strongly applies to
the spectacular correlations between lifetime income and years of
schooling. Looking for his first job, a middle-class youth decides he
wants eighty dollars to start, and he can afford to shop around till
he gets it; a poor boy must take anything and starts at thirty-five
dollars. For obvious reasons, this initial difference will usually
predetermine the whole career. Conversely, a sharp poor boy; seeing
that this is the score, might choose not to bestir himself and prefer
to look for a racket.

Again, Negro college graduates average in a lifetime the same salary
as white high-school graduates. It seems to be not the years of
schooling but the whole context that makes the difference. Consider,
if after seven or eight years the salary increase of Negro or Puerto
Rican high-school graduates over those who have dropped out is perhaps
five dollars a week, is this worth the painful effort of years of
schooling that is intrinsically worthless and spirit-breaking!  

In these circumstances, it is wiser to think exactly the opposite
way. It would probably help to improve the educational aspiration and
educability of poor youngsters to give the money to poor families
\emph{directly}, rather than to channel it through school systems or other
social agencies that drain off most of it for the same middle
class. If we pension the poor as consumers in a consumption-oriented
society, they will also send their children to school, a form of
consumption.  I take it that this is what Galbraith and Theobald are
essentially saying. And the proposals of Myrdal and Keyserling are
meant to accomplish the same purpose: public works to provide
\emph{unskilled} jobs.

It is claimed that society needs more people who are technically
trained. But informed labor people tell me that, for a job requiring
skill but no great genius, a worker can be found at once, or quickly
trained, to fill if. For instance, the average job in General Motors'
most automated plant requires three weeks of training for those who
have no education whatever. It used to require six weeks; for such
jobs, automation has diminished rather than increased the need for
training. In the Army and Navy, fairly complicated skills, e.g. radar
operation and repair, are taught in a year \emph{on the job}, often to
practical illiterates.

Naturally, if diplomas are prerequisite to hiring a youngster, the
correlation of schooling and employment is self-proving. Because of
this fad, there is a fantastic amount of mis-hiring, hiring young
people far too school-trained for the routine jobs they get. I was
struck by a recent report in the \emph{Wall Street Journal} of firms
philanthropically deciding to hire only drop-outs for certain
categories of jobs, since the diploma made no difference in
performance.

Twist it and turn it how you will, there is no logic to the proposal
to extend compulsory schooling except as a device to keep the
unemployed off the streets by putting them into concentration camps
called schools. The Continuation branch of Milwaukee Vocational is,
then, typical of what we can expect. (By the way, Milwaukee Vocational
is otherwise a justly famous school, a fine product of Populism and
right-wing Socialism.)  

As an academic, I am appalled by this motivation for schooling. As a
citizen and human being, this waste of youthful vitality appalls
me. It is time that we stopped using the word `education'
honorifically. We must ask, education how? Where? for what? And under
whose administration?  Certainly every youth should get the best
possible education, but, in my opinion, the present kind of compulsory
schooling under the present administrators, far from being extended,
should be sharply curtailed.

As I have been saying, by and large primary schooling is, and should
be, mainly baby-sitting. It has the great mission of democratic
socialization -- it certainly must not be segregated by race and income;
apart from this, it should be happy, interesting, not damaging. The
noise about stepping- up the primary curriculum is quite uncalled for;
I have seen no convincing evidence -- not by progressive educators
either -- that early schooling makes much academic difference in the
long run. But in the secondary schools, after puberty, the tone of the
baby-sitting must necessarily turn to regimentation and policing, and
it is at peril that we require schooling; it fits some, it hurts
others. A recent study by Edgar Friedenberg concludes that
spirit-breaking is the \emph{principal} function of typical
lower-middle-class schools.

I wonder whether the Secretary of Labor thought through the
constitutionality, not to speak of the morals, of his compulsory
proposal. The legal justifications for compulsory schooling have been
to protect children from exploitation by parents and employers, and to
ensure the basic literacy and civics necessary for a democratic
electorate. It is quite a different matter to deprive adolescents of
their freedom in order to alleviate the difficulties of a faulty
economic and political system. Is this constitutional?

We are back, in another context, to Dr Conant's intolerable
distinction between `individual development' and `national needs'; Dr
Conant was talking about the post-Sputnik putative need for
scientists, and Secretary Wirtz was talking about unemployment. So let
us go over the ground again and look at the picture squarely. At
present, in most states, for ten to thirteen years every young person
is obliged to sit the better part of his day in a room almost always
too crowded, facing front, doing lessons predetermined by a distant
administration at the state capital and that have no relation to his
own intellectual, social or animal interests, and not much relation
even to his economic interests. The overcrowding precludes
individuality or spontaneity, reduces the young to ciphers, and the
teacher to a martinet. If a youth tries to follow his own bent, he is
interrupted and even jailed. If he does not perform, he is humiliated
and threatened, \emph{but he is not allowed to fail and get
out}. Middle-class youth go through this for at least four more years
-- at the college Level, the overcrowding has become an academic
scandal -- but they are steeled to it and supported by their
middle-class anxiety and middle-class perquisites, including money to
spend. Secretary Wirtz now wants poor youth, not thus steeled and
supported, to get two more years of it. What will this seventeen year
old do for spending money?

In his speech the Secretary referred to the admirable extension of
free education from 1850 to, say, 1930. But this is again entirely
misleading with regard to our present situation. To repeat, that
opening of opportunity took place in an open economy, with an
expanding marker for skills and cultural learning. Young people took
advantage of it \emph{of their own volition}; therefore there were no
blackboard jungles and endemic problems of discipline. Teachers taught
those who wanted to learn; therefore there was no especial emphasis on
grading. What is the present situation? The frantic competitive
testing and grading means that the market for skills and learning is
not open, it is tight. There are relatively few employers for those
who score high; and almost none of the high-scorers become independent
enterprisers. This means, in effect, that a few great corporations are
getting the benefit of an enormous weeding-out and selective process -- all children are fed into the mill and everybody pays for it.

If our present high schools, junior colleges and colleges reflected
the desire, freedom and future of opportunity of the young, there
would be no grading, no testing except as a teaching method, and no
blackboard jungles. In fact, we are getting lockstep scheduling and
grading to the point of torture. The senior year of high school is
sacrificed to batteries of national tests, and policemen are going to
stand in the corridors. Even an elite school such as Bronx Science --
singled out by Dr Conant as the best school in the country -- is run
as if for delinquents, with corridor passes and a ban on leaving the
building. The conclusion is inevitable. The scholastically bright are
not following their aspirations but are being pressured and bribed;
the majority -- those who are bright but not scholastic, and those who
are not especially bright but have other kinds of vitality -- are
being subdued.

This is the schooling that Secretary Wirtz says we `ought to make the
biggest industry in the county'. I thought it already was! As one
observes the sprawling expansion of the universities and colleges,
eating up their neighborhoods, dislocating the poor, dictating to the
lower schools, battening an Federal billions for research and
development, and billions for buildings, and billions through the
National Defense Education Act, and billions from foundations and
endowments -- one suddenly realizes that here again is the Dead Hand
of the medieval Church, that inherits and inherits and never dies. The
University, which should be dissident and poor, has become the
Establishment. The streets are full of its monks.

What a bad scene! Its spirit pervades all of society. Let me quote
from a man in Secretary Wirtz's own department, in charge of
retraining: `We retrain him, but before the course is finished, that
job too has vanished. So we begin again. But after the fourth or fifth
retraining, he has a job that doesn't vanish: he becomes a Teacher of
Retaining'. We must remember that, whatever the motive, \emph{Pouring money
into the school and college system and into the academic social work
way of coping with problems, is strictly class legislation that
confirms the inequitable structure of the economy.} I have mentioned
how the professor-ridden Peace Corps needs \$15,000 to get a single
youngster in the field for a year, whereas the dedicated Quakers
achieve almost the same end for \$3500. Again, when thirteen million
dollars are allotted for a local Mobilization for Youth programme, it
is soon found that nearly twelve million dollars have gone for
sociologists doing `research', diplomated social workers, the NY
school system and administrators, but only one million to field
workers and the youths themselves.

In my opinion, the public buys this unexamined `education' because of
the following contradiction: The Americans \emph{are} guilty because these
youths are useless in the present set-up, so they spend money on them
(though they get oddly stingy at crucial moments); on the other hand,
they insist that the youths work hard at something `useful'-- namely
useless training. One can't just let them play ball; they must compete
and suffer.

I agree that we ought to spend more public money an education. And
where jobs are easier and there is need for technical training, the
corporations aught to spend more money on apprenticeships. We are an
affluent society and can afford it. And the conditions of modern life
are far too complicated for independent young spirits to get going on
their own. They need some preparation, though probably not as much as
is supposed; but more important, they need various institutional
frameworks in which they can try out and learn the ropes.

Nevertheless, I would not give a penny more to the present school
administrators. The situation is this: to make the present school
set-up even \emph{tolerable}, not positively damaging -- e.g. to cut the
elementary class size to twenty or to provide colleges enough to
diminish the frantic competition for places -- will require at least
doubling the present school budgets. I submit that this kind of money
should be spent in other ways.

What, then, ought the education of these youth to be? We are back to
our fundamental question: what are the alternatives?

Fundamentally, there is no right education except growing up into a
worthwhile world. Indeed, our excessive concern with problems of
education at present simply means that the grown-ups do not have such
a world. The poor youth of America will not become equal by rising
through the middle class, going to middle-class schools. By plain
social justice, the Negroes, and other societies have the right to,
and must get, equal opportunity for schooling with the rest, but the
exaggerated expectation from the schooling is a chimera -- and, I
fear, will be shockingly disappointing. But also the middle-class
youth will not escape their increasing exploitation and \emph{anomie} in such
schools. A decent education aims at, prepares for, a more worthwhile
future, with a different community spirit, different occupations, and
more real utility than attaining status and salary.

We are suffering from a bad style, perhaps a wrong religion. Although
it is pretty certain, as I have said, that the automated future will
see less employment in the manufacture of hardware and more employment
in service occupations, as well as more leisure, yet astoundingly the
mass- producing and cash-accounting attitude towards the hardware is
earned over un-changed into the thinking about the services and
leisure! The lock-step regimentation and the petty bourgeois credits
and competitive grading in the schooling are typical of all the rest.

(For a charming, and grim, study of the spread of `business methods'
to schooling, from 1900 to 1930, let me refer the reader to Callahan's
\emph{The Cult of Efficiency in American Education}.)

My bias is that we should maximize automation as quickly as possible,
\emph{where it is relevant} -- taking care to cushion job dislocation and to
provide adequate social insurance. But the spirit and method of
automation, logistics, chain of command, and clerical work are
\emph{entirely irrelevant} to humane services, community service,
communications, community culture, high culture, citizenly initiative,
education and recreation. To give a rather special but not trivial
example of what I mean, TV sets should be maximum-mass-produced with
maximum automation, in a good standard model; as cheaply as possible;
but TV programming should, except for a few national services, be as
much decentralized, tailor-made, and reliant on popular and
free-artist initiative as possible.  

The dangers of the highly technological and automated future are
obvious: We might become a brainwashed society of idle and frivolous
consumers. We might continue in a rat race of highly competitive,
unnecessary busy-work with a meaninglessly expanding Gross National
Product. In either case, there might still be an outcast group that
must be suppressed. To countervail these dangers and make active,
competent and initiating citizens who can produce a community culture
and a noble recreation, we need a very different education than the
schooling that we have been getting.  

Large parts of it must be directly useful, rather than useless and
merely aiming at status. Here we think of the spending in the public
sector, advocated by Myrdal, Keyserling, Galbraith and many others
e.g. the money spent on town improvement, community service or rural
rehabilitation can also provide educational occasions. (When these
economists invariably list schooling as high -- and often first -- in
the list of public expenditures, they fail to realize that such
expense is probably wasted and perhaps even further dislocates the
economy. I would say the same about Galbraith's pitch for new
highways.)  

On the whole, the education must be voluntary rather than compulsory,
for no growth to freedom occurs except by intrinsic
motivation. Therefore the educational opportunities must be various
and variously administered. We must diminish rather than expand the
present monolithic school system. I would suggest that, on the model
of the GI Bill, we experiment, giving the school money directly to the
high-school-age adolescents, for any plausible self-chosen educational
proposals, such as purposeful travel or individual enterprise. This
would also, of course, lead to the proliferation of experimental
schools.

Unlike the present inflexible lockstep, our educational policy must
allow for periodic quitting and easy return to the scholastic ladder,
so that the young have time to find themselves and to study when they
are themselves ready. This is Eric Erickson's valuable notion of the
need for \emph{moratoria} in the life-career; and the anthropological
insistence of Stanley Diamond and others, that our society neglects
the crises of growing up.

Education must foster independent thought and expression, rather than
conformity. For example, to countervail the mass communications, we
have an imperative social need, indeed a constitutional need to
protect liberty, for many thousands of independent media: local
newspapers; independent broadcasters, little magazines, little
theatres; and these, under professional guidance, could provide
remarkable occasions for the employment and education of adolescents
of brains and talent. (I have elsewhere proposed a graduated tax on
the audience-size of mass media, to provide a fund to underwrite such
new independent ventures for a period, so that they can try to make
their way.)

Finally, contemporary education must inevitably be heavily weighted
towards the sciences. But this does not necessarily call for school
training of a relatively few technicians, or few creative scientists
(if such can indeed be trained in schools). Our aim must be to make a
great number of citizens at home in a technological environment, not
alienated from the machines we use, not ignorant as consumers, who can
somewhat judge governmental scientific policy, who can enjoy the
humanistic beauty of the sciences, and, above all, who can understand
the morality of a scientific way of life. I try to spell out the
meaning of this below.  

When Secretary Wirtz means by education something like this, and not
compulsory junior college for delinquents, we can think of extending
education as a device for diminishing youth unemployment. Because it
will then be more useful employment than most of the available, or
non-available, jobs. It will be relevant to a good future rather than
a morally bankrupt past.






