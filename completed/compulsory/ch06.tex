% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Programmed}

Programmed teaching adapted for machine use goes a further step than conforming students to
the consensus which is a principal effect of schooling interlocked with the mass media. In this
pedagogic method it is only the programmer -- the administrative decision-maker -- who is to do
any `thinking' at all; the students are systematically conditioned to follow the train of the other's thoughts. `Learning' means to give some final response that the programmer considers
advantageous (to the students). There is no criterion of knowing it, of having learned it, of
concept-forming or simplification. That is, the student has no active self at all; his self, at least as student, is a construct of the programmer.

What does this imply! Let me analyze a very high-level argument for such teaching by Lauren
Resnick, `Programmed Instruction of Complex Skills', in the \emph{Harvard Educational Review} of
Autumn 1963.

In the conclusion of this perspicuous article, Dr Resnick tells us:

``By explicit instruction I mean the deliberate modification of the behavior of other human
beings. Programmed instruction is not interested in the teacher as stimulator of interest, role
model or evaluator of progress. It is interested in him as instructor, or controller of behavior. This means that programmed instruction is applicable only where we do in fact want to change
behavior in a given direction. There are cases where for political or ethical reasons we do not
want to. We do not, for example, want to train all students to be active partisans of a given
political or religious viewpoint, or make everyone like the same kind of literature or music. In
such cases `exposure' is the most we should attempt.''

Let me put this dramatic statement in juxtaposition with an earlier statement in her essay:

``In the context of behavioral analysis, knowledge, skill and ability can be dealt with only in so
far as they can be described in terms of performance. This description is not a matter of listing
`correlates' of ability or knowledge, but of deciding what observable behaviors will be accepted
as evidence of their existence. The behaviorist simply eschews the question of whether
knowledge, for instance, exists apart from observable behaviors. While, in so doing, he may fail
to answer to the philosopher's satisfaction the question, `What is knowledge?', he very effectively
provides himself with a set of usable goals for instruction.''

I do not much want to discuss the pedagogic relevance of these
ideas. The only evidence of `performance' that school-people ever draw
on for their experiments is scoring an academic tests, and it seems to
be impossible to disabuse school people of the notion that
test-passers have necessarily learned anything relevant to their
further progress or careers; or of advantage to the body politic; or
indeed anything whatever that will not vanish in a short time, when
the real life- incentive, of passing the test, has passed away. But I
want to ask if this kind of formulation of teaching does not involve
serious legal difficulties, in terms of civil liberties, especially
where schooling is compulsory, when the child must go to school and
submit to having his behavior shaped.

It may seem odd that I keep referring to the constitutional question;
but it is a way of asking what kind of democracy we envisage in our
curriculum and methods of schooling. Besides, since the young have
become so clearly both an exploited and an outcast class, we must
begin to think of legal rights.

The United States Bill of Rights guarantees were grounded in a very
different epistemological theory from operant conditioning, the method
that Dr Resnick has learned from B. F. Skinner.  Roughly, the
Enlightenment conception was that intellect, like conscience, was
something `inward', and the aim of teaching was to nurture its
'growth' by `knowledge'. Even more important, behavior was the
'external' effect of an initiating or self-moving of the `soul';
therefore the student was or became `responsible'. In my opinion, the
inner-outer metaphor of this conception is quite useless; there is not
much use in a psychological theory for entities that are not
observable as behavior. But the Aristotelian emphasis on the
self-moving organism is solid gold.

Now compulsory schooling, as I have pointed out, was justified in this
theory, e.g. by Jefferson, as necessary to bring children to the point
of self-government, of exercising citizenly initiative, as well as the
animal and social initiative that they had by `nature' and the moral
initiative that they had by `conscience'. Democracy required an
educated electorate. To this was later added the justification that
only by compulsory education could the poor get an equal democratic
opportunity with the rich; poor parents were likely to put their
children to work too early, and not give them a chance to develop to
their full powers.

In turn, any course of the curriculum or detail of method was
justified by showing that it nurtured the growth of the inward
intellect, encouraged initiative and fitted the young to take a free
pan in political society. On this view, school teaching was precisely
not `training', though parents were allowed to train minor children
and the masters of apprentices were allowed to train their bonded
apprentices. School subjects either had to contain values ideal in
themselves, as good, true or beautiful, which were `liberal' by
definition; or they strengthened the `logical faculty', which the
young citizen would then apply to all kinds of matters (This was the
traditional notion of `transfer'); or they gave him orientation in
space and time -- as I have mentioned, especially history was prized,
because its horrible and noble examples inspired youth to preserve
freedom.

Of course, the late-nineteenth-century compulsory education in the
mechanical arts, to the degree that they were merely utilitarian,
could not so easily be justified in these `inward' ways it tended to
seem like apprentice-training at the public expense. But in an
expanding economy with high social mobility, and where there was
considerable self-employment and much new enterprise, there was no
occasion to cavil; a free soul would want such advantageous skills of
its own volition. Few adolescents went to school anyway, and children
never did have many rights, though plenty of privileges.
 
Dr Resnick's system explicitly excludes all notions of `inward'
meaning. And she is also unhappy about the sneaking in of any factor
of initiative. For example, in discussing Shaping -- the approximation
of the responses to the final response -- she sharply disagrees with
those experimenters who wait for the organism to make a small move in
the right direction, to reinforce it. `Programmed instruction,' she
says, `cannot afford to proceed in this way.' (But she never does make
clear, at least to me, how she gets the beast to move \emph{ab extra}, in
order to have something to shape.)

Also, unlike the liberal or `faculty-developing' curriculum of the
Enlightenment theory, no particular subject of learning is chosen
because of its characteristic appeal to or stimulation of the powers,
liberation or needs of the learner. Operant-conditioning theory, she
says, is essentially' content less'; it is a pure technique that can
teach anything to almost anybody. This might be Dr Conant's `national
needs'; it might be the `improved attitudes' of the Continuation
branch of Milwaukee Vocational; it might be the vagaries of Big
Brother.

In sum, an this view, compulsory schooling, so Ear as it is
programmed, is identical with compulsory training to the goals of the
controllers of behavior, and such goals are set by the `we want' of
the first paragraph I have cited. Then I am curious to hear from Dr
Resnick the constitutional justification for compulsory schooling in
terms of the `we want' and `we do not want' of that paragraph. Who,
we? and what limitation is there to `want' or happen to want? The
title of her essay, let us remember, is ``Instruction of Complex
Skills''; she is not restricting behavior-control to rote and drill
subjects, but extending it to the higher branches, to criticism,
problem-solving, appreciation, except where `we do not want to'.

Needless to say, curriculum, methods and the school system itself have
\emph{always} been determined by social goals and National Goals, parental
ambitions and the need to baby-sit and police the young. But it is one
thing to believe -- or pretend -- that these educate the children, and
quire another thing to say that they are behavior-controller

Our author's indifference to this kind of consideration appears
strongly in an otherwise excellent analysis of the `Discovery Method'
as contrasted with step-by-step programmed instruction. One advantage
claimed for the Discovery Method -- for which, we saw, Dr Zacharias
and the National Science Foundation have manifested enthusiasm -- is
that the leap over the gap is itself exciting and reinforcing,
providing stronger motivation. Dr Resnick agrees that this might be
true for bright students; but she wisely points out that culturally
deprived, poorly achieving youngsters get more satisfaction from
steady success, without risk of new failure. A second advantage
claimed is that the trial and error in the Discovery process fits the
student for the kind of learning that he will have to do outside the
class-room; but here Dr Resnick doubts that the student learns from
his errors unless he is trained in what to ask about them, that is, to
notice them. (She is right. For example, a good piano teacher will
have the student deliberately play the wrong note that he repeats
inadvertently.) Finally, it is claimed, the quality of what is learned
by Discovery -- the synoptic, the law, the solution of the problem --
is superior. This, says Dr Resnick, is because programmed instruction
has so far concentrated almost exclusively on teaching mere concepts
and information, rather than complex wholes of learning.

What is astonishing in this thoughtful analysis, however, is that she
entirely omits the \emph{salient} virtue that most teachers, classical or
progressive, have always hoped for in letting the student discover for
himself, namely the development of his confidence that he can, that he
is adequate to the nature of things, can proceed on his own
initiative, and ultimately strike out on an unknown path, where there
is no programme, and assign his own tasks to himself. The classical
maxim of teaching is: to bring the student to where he casts off the
teacher. Dewey's model for curriculum and method was: any study so
pursued that it ends up with the student wanting to find out something
further.

Apparently Dr Resnick cannot even conceive of this virtue, because it
is contradictory to the essence of controlled behavior toward a
predetermined goal. It is open. From her point of view, it is not
instruction at all. In terms of social theory, it posits an open
society of independent citizens - but she and Dr Skinner think there
is a special `we' who `want'. Also, scientifically, it posits a more
open intellectual future than the complex-skill which programming
seems to envisage. Is it indeed the case that much is known -- so
definitely -- that we can tightly programme methods and fundamental
ideas? Much of the programme is bound to be out of dare before the
class graduates.  

This is a fundamental issue. Intellectually, humanly and politically,
our present universal high schooling and vastly increasing college
going are a disaster. I will go over the crude facts still again! A
youngster is compelled for twelve continuous years -- if middle class,
for sixteen years - - to work on assigned lessons, during a lively
period of life when one hopes he might invent enterprises of his
own. Because of the schoolwork, he cannot follow his nose in reading
and browsing in the library, or concentrate on a hobby that fires him,
or get a job, or carry on a responsible love-affair, or travel, or
become involved in political action. The school system as a whole,
with its increasingly set curriculum, stricter grading, incredible
amounts of testing, is already a vast machine to shape acceptable
responses. Programmed instruction closes the windows a little tighter
and it rigidifies the present departmentalization and dogma. But worst
of all, it tends to nullify the one lively virtue that any school does
have, that it is a community of youth and of youth and adults.

Dr. Resnick can assert that there are areas where `we do not want' to
control behavior -- political, religious, aesthetic, perhaps
social. But the case is that for sixteen years it is precisely
docility to training and boredom that is heavily rewarded with
approval, legitimacy and money; whereas spontaneous initiation is
punished by interruption, by being considered irrelevant, by anxiety
of failing in the `important' work, and even by humiliation and
jail. Yet somehow, after this hectic course of conditioning, young men
and women are supposed, on commencement, suddenly to exercise
initiative in the most extreme matters: to find jobs for themselves in
a competitive market, to make long career plans, to undertake original
artistic and scientific projects, to marry and become parents, to vote
for public officers. But their behavior has been shaped only too
well. Inevitably must of them will go on with the pattern of assigned
lessons, as Organization Men or on the assembly-line; they will vote
Democratic-Republican and buy right brands.

I am rather miffed at the vulgarity of the implication that, in
teaching the humanities, we should at most attempt exposure -- as if
appreciation were entirely a private matter, or a matter of
unstructured `emotion'. (There is no such thing, by the way, as
unstructured emotion.) When Dr Resnick speaks of the un-shaped
response to the kind of literature or music `they like', she condemns
their aesthetic life to being a frill, without meaning for character,
valuation, recreation or how one is in the world. Frankly, as a man of
letters I would even prefer literature to be programmed, as in Russia.


That is, \emph{even if behavioral analysis and programmed instruction were the adequate analysis of learning and method of teaching, it would still be questionable for overriding political reasons, whether they are generally appropriate for the education of free citizens.}

To be candid, I think operant conditioning is vastly overrated. It
teaches us the not newsy proposition that if an animal is deprived of
its natural environment and society, sensorial deprived, made mildly
anxious, and restricted to the narrowest possible spontaneous motion,
it will emotionally identify with its oppressor and respond -- with
low-grade grace, energy and intelligence -- in the only way allowed to
it. The poor beast must do something, just to live on a little. There
is no doubt that a beagle can be trained to walk on its hind legs and
balance a ball on the rip of its nose. But the dog will show much more
intelligence, force and speedy feedback when chasing a rabbit in the
field. It is an odd thought that we can increase the deficiency of
learning by nullifying a priori most of an animal's powers to learn
and taking if out of its best field.

It has been a persistent error of behaviorist psychologies to overlook
that there are oven criteria that are organically part of \emph{meaningful}
acts of an organism in its environment; we can observe grace, ease,
force, style, sudden simplification -- and some such characteristics
are at least roughly measurable. It is not necessary, in describing
insight, knowledge, the kind of assimilated learning that Aristotle
called `second nature', to have recourse to mental entities. It is not
difficult to \emph{see} when a child \emph{knows} how to ride a bicycle; and he
never forgets it, which would not be the case if the learning were by
conditioning with reinforcement, because that can easily be wiped away
by a negative reinforcement. (Kurt Goldstein has gone over this ground
demonstratively.)  

On the other hand, it is extremely dubious that by controlled
conditioning one \emph{can} teach organically meaningful behavior. Rather,
the attempt to control \emph{prevents} learning. This is obvious to anyone
who has ever tried to teach a child to ride a bicycle; the more you
try, the more he falls.  The best one can do is to provide him a
bicycle, allay his anxiety, tell him to keep going, and \emph{not} to try to
balance. I am convinced that the same is true in teaching reading.

As is common in many (sometimes excellent) modern scientific papers --
whether in linguistics or studies of citizen participation or the
theory of delinquency -- Dr Resnick asks for more money; and of
course, for purpose of pure research, the higher investigations that
she asks for should be pursued as long as her enthusiasm lasts and
should be supported. Any definite hypothesis that is believed in by a
brilliant worker is likely to yield useful by-products that can then
be reinterpreted; nor is there any other guide for the advancement of
science except the conviction and competence of the researchers.

But I am puzzled al what widespread social benefits she has in mind
that warrant a \emph{lot} of expense in brains and machinery. She seems to
agree that bright children do not learn most efficiently by these
extrinsic methods; and for the average the picture is as I have
described it: average employment in a highly automated technology
requires a few weeks' training on the job and no schooling at all, and
for the kind of humane employment and humane leisure that we hopefully
look rewards, we require a kind of education and habit entirely
different from programmed instruction.

But I am more impressed by what is perhaps Dr Resnick's deepest
concern, the possible \emph{psychotherapeutic} use of more complex
programming for the remedial instruction of kids who have developed
severe blocks to learning and are far behind. For youngsters who have
lost all confidence in themselves, there is a security in being able
to take small steps entirely at their own pace and entirely by their
own control of the machine. Also, though the chief use of schools is
their functioning as a community, under present competitive and
stratified conditions it is often less wounding for a kid who has
fallen behind to be allowed to withdraw from the group and
recover. And this time can usefully and curatively be spent in
learning the standard `answers' that can put him in the game again.

There is a pathos in our technological advancement, well exemplified
by programmed instruction. A large part of it consists in erroneously
reducing the concept of animals and human beings in order to make them
machine-operable. The social background in which this occurs,
meanwhile, makes many people outcast and in fact tends to reduce them
as persons and make them irresponsible. The refined technique has
little valid use for the dominant social group for which it has been
devised, e.g. in teaching science; but it dues prove to have a use for
the reduced outcasts, in teaching remedial arithmetic.
