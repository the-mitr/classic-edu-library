% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode


\NewPage

\thispagestyle{empty}

\begin{chapquot}`One had to cram all this stuff into one's mind,
whether one liked it or not. This coercion had such a deterring effect
that, after I had passed the final examination, I found the
consideration of any scientific problems distasteful to me for an
entire year \ldots  is in fact nothing short of a miracle that the
modern methods of instruction have not yet entirely strangled the holy
curiosity of inquiry; for this delicate little plant, aside from
stimulation, stands mainly in need of freedom; without this it goes to
wreck and ruin without fail. It is a very grave mistake to think that
the enjoyment of seeing and searching can be promoted by means of
coercion and a sense of duty. To the contrary, I believe that it would
be possible to rob even a healthy beast of prey of its voraciousness,
if it were possible, with the aid of a whip, to force the beast to
devour continuously, even when not hungry - especially if the food,
handed out under such coercion, were to be selected accordingly.'%\\[0.2cm]
\begin{flushright}
\textit{Albert Einstein}
\end{flushright}
\end{chapquot}

\NewPage


%\newpage
\chapter{Preface}
%\markboth{Preface}{}

In these remarks on the schools, I do not try to be generous or fair,
but I have seen what I am talking about and I hope I am rational. This
case is that we have been swept on a flood-tide of public policy and
popular sentiment into an expansion of schooling and an aggrandizement
of school-people that is grossly wasteful of wealth and effort and
does positive damage to the young. Yet I do not hear any fundamental
opposition in principle, not even prudent people (rather than stingy
people) saying, go warily. The dominance of the present school
auspices prevents any new thinking about education, although we face
unprecedented conditions.

It is uncanny. When, at a meeting, I offer that perhaps we already
have too much formal schooling and that, under present conditions, the
more we get the less education we will get, the others look at me
oddly and proceed to discuss how to get more money for schools and how
to upgrade the schools. I realize suddenly that I am confronting a
mass superstition.  In this little book, I keep resorting to the
metaphor school-monks, the administrators, professors, academic
sociologists and licensees with diplomas who have proliferated into an
invested intellectual class worse than anything since the time of
Henry VIII. 

Yet I am convinced - as they got their grants and
buildings and State laws that give them sole competence - that the
monks are sincere in their bland faith in the school. The schools
provide the best preparation for everybody for a complicated world,
are the logical haven for unemployed youth, can equalize opportunity
for the underprivileged, administer research in all fields, and be the
indispensable mentor for creativity, business-practice, social work,
mental hygiene, genuine literacy - name it, and there are credits for
it leading to a degree. The schools offer very little evidence of
their unique ability to perform any of these things - there is plenty
of evidence to the contrary - but they do not need to offer evidence,
since nobody opposes them or proposes alternatives. 

A major pressing
problem of our society is the defective structure of the economy that
advantages the upper middle-class and excludes the lower class. The
school-people and Ph.D.  sociologists loyally take over also this
problem, in the war on poverty, the war against delinquency,
retraining those made jobless, training the Peace Corps, and so
forth. But as it turns out, just by taking over the problem, they
themselves gobble up the budgets and confirm the defective structure
of the economy.

And inevitably, expanding and aggrandizing, becoming the universal
trainer, baby-sitter and fix-it, the schools are losing the beautiful
academic and community functions that by nature they do have.

The ideas in this book were called up for specific busy occasions. The
remarks on the drop-outs were the substance of a contribution to a
national conference on the problem, called by the National Education
Association. The notes on psychosomatic education were, first, the
report of a school visit when I was a member of a local school board
in New York; the note on progressive education was a recruiting talk
for a Summerhill-variant school of which I am a trustee. 

The remarks
on the Secretary of Labor's proposal and on the hang-ups of getting a
job were asked for by the National Committee on Employment of Youth,
and printed in \textit{The American Child}. The discussion of
adolescent difficulties in communication was commissioned for a
freshman course at the University of Western Michigan; and the
discussion of unteachability was commissioned by the Methodists for a
freshman-orientation programme. The critique of programmed instruction
was part of a controversy in the \textit{Harvard Educational
  Review}. The analysis of teaching science was the gist (as I saw it)
of a couple of seminars with people from the government science
institutes that I attended at the Institute for Policy Studies in
Washington. And the proposals for the liberal arts colleges were the
gist of a section at the 1964 meeting of the Association for Higher
Education.

(At that meeting, I asked the AHE to urge society to find various
other means of coping with youth unemployment, rather than putting the
entire burden an the colleges. Not surprisingly, this modest
resolution went crashingly nowhere.)

Rewriting, I have kept in evidence these busy and polemical
contexts. For this is where my story is. John Dewey somewhere makes
the remarkable observation that the essential part of philosophy is
the philosophy of education, the rest being the subject of special
sciences. But I am not able, or prepared, to write such a
philosophy. What I can, and do, write is this fighting recall to plain
sense; holding action, attempt to lay the ground-work of a decent
future.

The immediate future of the United States seems to me to have two
looming prospects, both gloomy. If the powers-that-be proceeds as
stupidly, timidly and `politically' as they have been doing, there
will be a bad breakdown and the upsurge of a know-nothing fascism of
the Right.  Incidentally, let me say that I am profoundly unimpressed
by our so-called educational system when, as has happened, Governor
Wallace comes from the South as a candidate in Northern states and
receives his highest number of votes (in some places a majority) in
suburbs that have had the \textit{most} years of schooling, more than
sixteen.

The other prospect -- which, to be frank, seems to me to be the goal
of the school-monks themselves -- is a progressive regimentation and
brainwashing, on scientific principles, directly toward a fascism of
the Center. 1984. Certainly this is not anybody's deliberate purpose;
but given the maturing of automation and the present dominance of the
automating spirit in schooling, so that all of life becomes geared to
the automatic system, that is where we will land.  

Therefore in this book I do not choose to be `generous' and `fair'.

Underlying the present superstition, however, is an objective
fact. Major conditions of modern life \textit{are} unprecedented and
we do not know how to cope with them. Confused, people inevitably try
to ward off anxiety by rigidifying the old methods of dominant
economic and intellectual groups. Omitting the changed international
conditions, let me just mention some unprecedented domestic
developments that are crucial for even primary education.

Within the United States, we have reached a point of productivity when
it becomes absurd to use the rate of growth and the Gross National
product as measures of economic health. To be useful, new production
must be much more narrowly qualified, e.g. serve the public sector or
eliminate grinding poverty. Unqualified growth already does more harm
than good. Thus, we must consider concepts like `work' and `leisure'
and `unemployment' in a new way, and we must begin to distinguish
between economic well-being and mere affluence. Yet only a handful of
economists are thinking along these lines, and almost no one escapes
the mesmerism of the GNP.  We cannot expect educators to be far ahead.

Correspondingly, the social valuation of scientific technology and
science must change. Up to now, the emphasis has been on the products,
including the research products, the Knowledge Explosion. But these
become diminishingly useful, and the more they hood the environment,
the less skilful the average man becomes. The problem for general
education, rather, is to learn to \textit{live} in a high
technology. The emphasis ought to be on the moral virtues of science
itself, both austere and liberating; on its humane beauty; on the
selectivity and circumspect reasonableness of sciences like ecology
and psychosomatic medicine. These are very different values from the
present gearing of general education to the processing of Ph.D.'s.

Urbanization is almost total; independent farming, farming as `a way
of life', is at the point of extinction. Yet this development is
unexamined and uncontrolled. The disastrous pattern of blighted
center, suburbs and conurbation is taken for granted, and highway,
fax, housing and schooling policies serve only to intensify it. Then,
astoundingly, we come to suffer from what looks like a population
explosion, even though, in the United States, vast and beautiful
regions are being depopulated. One weeps to see if, yet nothing is
done to find possible principles of rural recovery and better
balance. Or, in the dense cities, to find functional equivalents for
the lost self- reliance extended family and community.

There is anomie and an alarming rate of urban mental illness. My own
view is that an important factor in these is powerlessness; it is
impossible to become engaged or usefully to identify when one cannot
initiate and have a say in deciding. If this is so, we should be
studying new patterns of decentralizing while we centralize. But there
are no such studies and, in my opinion, the bureaucratic methods of
social psychiatry probably worsen the social diseases. Certainly we
are in a political crisis, for, though the forms of democracy are
intact, the content is vanishing. Such political vitality as there is
finds its expression in para-legal ways; but these will eventually
either renovate the constitution or degenerate into violence and gross
injustice. Meantime, there is a proliferation of media of
communication and messages communicated, for people need to be
informed and want to be informed; yet, partly just because of the
communications, there is brainwashing and conformity.

Such are some of the extraordinary conditions for which our schooling
fails to educate. It is essential to find alternative ways of
educating.
\\[0.2cm]
\textit{North Stratford, New Hampshire
\\
July 1964
}

\NewPage




