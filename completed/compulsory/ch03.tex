% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{The Present Moment in Progressive Education}


The programme of progressive education always anticipates the crucial
social problems that everybody will be concerned with a generation
later, when it is too late for the paradisal solutions of progressive
educators. This is in the nature of the case. Essentially, progressive
education is nothing but the attempt to naturalize, to humanize, each
new social and technical development that is making traditional
education irrelevant. It is not a reform of education, but a
reconstruction in terms of the new era. If society would once adopt
this reconstruction, we could at last catch up with ourselves and grow
naturally into the future. But equally in the nature of the case,
society rejects, half-accepts, bastardizes the necessary changes; and
so we are continually stuck with `unfinished revolutions', as I called
them in \emph{Growing Up Absurd}. Then occur the vast social problems that
could have been avoided -- that indeed the older progressive education
had specifically addressed -- but it is too late. And progressive
educators stoically ask: What is the case \emph{now}?


During the current incredible expansion of increasingly unnatural
schooling, and increasing alienation of the young, it is useful to
trace the course of progressive education in this century, from John
Dewey to the American version of A. S. Neill.

The recent attacks on Deweyan progressive education, by the Rickovers
and Max Raffertys, have really been outrageous -- one gets
impatient. Historically, the intent of Dewey was exactly the opposite
of what the critics say. Progressive education appeared in the United
States in the intellectual, moral and social crisis of the development
of big centralized industrialism after the Civil War. It was the first
thoroughgoing analysis of the crucial modern problem of every advanced
country in the world: how to cope with high industrialism and
scientific technology which are strange to people; how to restore
competence to people who are becoming ignorant; how to live in the
rapidly growing cities so that they will not be mere urban sprawl; how
to have a free society in mass conditions; how to make the high
industrial system good for something, rather than a machine running
for its own sake.

That is, progressive education was the correct solution of a real
problem that Rickover is concerned with, the backwardness of people in
a scientific world. To put it more accurately, if progressive
education had been generally adopted, we should not be so estranged
and ignorant today.  

The thought of John Dewey was part of a similar tendency in
architecture, the functionalism of Louis Suliivan and Frank Lloyd
Wright, that is trying to invent an urbanism and an aesthetic suited
to machine-production and yet human; and it went with the engineering
orientation of the economic and moral theory of Veblen. These thinkers
wanted to train, teach -- perhaps accustom is the best word -- the new
generation to the actualities of industrial and technical life,
working practically with the machinery, learning by doing. People
could then be at home in the modern world, and possibly become free.

At-homeness had also a political aspen. Dewey was distressed by both
the robber-baron plutocracy and the bossed mass-democracy; and he was
too wise to espouse Veblen's technocracy, engineer's values. Dewey put
a good deal of faith in industrial democracy, overestimating the labor
movement -- he did not foresee the bureaucratization of the unions. As
a pragmatist he probably expected that the skilled would become
initiators in management and production; he did not foresee that labor
demands would diminish to wages and working conditions.

But the school, he felt, could combine all the necessary elements:
practical learning of science and technology, democratic community,
spontaneous feeling liberated by artistic appreciation, freedom to
fantasize, and animal expression freed from the parson's morality and
the schoolmaster's ruler. This constituted the whole of Deweyan
progressive education. There would be spontaneous interest (including
animal impulse), harmonized by art-working; this spontaneity would be
controlled by the hard pragmatism of doing and making the doing
actually work; and thus the young democratic community would learn the
modern world and also have the will to change it. Progressive
education was a theory of continual scientific experiment and orderly,
non- violent social revolution.

As was inevitable, this theory was entirely perverted when it began to
be applied, either in private schools or in the public system. The
conservatives and the businessmen cried out, and the programme was
toned down. The practical training and community democracy, whose
purpose was to live scientifically and change society, was changed
into `socially useful' subjects and a psychology of `belonging'. In
our schools, driver training survives as the type of the `useful'. (By
now, I suspect, Dewey would have been urging us to curtail the number
of cars.) Social dancing was the type of the `belonging'. The
Americans had no intention of broadening the scientific base and
taking technological expertness and control out of the hands of the
top managers and their technicians. And democratic community became
astoundingly interpreted as conformity, instead of being the matrix of
social experiment and political change.

Curiously, just in the past few years, simultaneous with the attack on `Dewey', his ideas have been getting most prestigious official
endorsement (though they are not attributed to Dewey). In the great
post-Sputnik cry to increase the scientific and technical pool, the
critics of `Dewey' call for strict lessons and draconian grading and
weeding-out (plus bribes), to find the elite group. (Dr. Conant says
that the `academically talented' are 15 per cent and these, selected
by national tests, will be at home \emph{for} us in the modern technical
world as its creative spirits.) However, there is an exactly contrary
theory, propounded by the teachers of science, e.g. the consensus of
the Woods Hole Conference of the National Science Foundation, reported
in Professor Bruner's \emph{The Processes of Education}. This theory counsels
practical learning by doing, entirely rejects competition and grading,
and encourages fantasy and guesswork. There is no point, it claims, in
learning the `answers', for very soon there will be different
answers. Rather, what must be taught are the underlying ideas of
scientific thought, continuous with the substance of the youngster's
feelings and experience. In short, the theory is Deweyan progressive
education.  

To be sure, Professor Bruner and his associates do not go on to
espouse democratic community.  But I am afraid they will eventually
find that also this is essential, for it is impossible to do creative
work of any kind when the goals are pre-determined by outsiders and
cannot be criticized and altered by the minds that have to do the
work, even if they are youngsters. (Dewey's principle is, simply, that
good teaching is that which leads the student to want to learn
something more.)  

The compromise of the National Science Foundation on this point is
rather comical. `Physical laws are not asserted; they are, it is
hoped, discovered by the student'; `there is a desire to allow each
student to experience some of the excitement that scientific pursuits
afford' -- I am quoting from the NSF's \emph{Science Course Improvement
Project}. That is, the student is to make a leap of discovery to what
is already known, in a course pre-charted by the Ph.D.s at MIT. Far
from being elating, such a process must be profoundly disappointing;
my guess is that the `discovery' will be greeted not by a cheer but by
a razz. The excitement of discovery is reduced to the animation of
puzzle solving. I doubt that puzzle solving is what creative thought
is about, though it is certainly what many Ph.D.s are about.

Authentic progressive education, meantime, has moved into new
territory altogether, how to cope with the over centralized
organization and Organization Men of our society, including the
top-down direction of science by the National Science Foundation. The
new progressive theory is `Summerhill'.

The American Summerhill movement is modeling itself on A. S. Neill's
school in England, but with significant deviations -- so that Neill
does not want his name associated with some of the offshoots.

Like Dewey, Neill stressed free animal expression, learning by doing,
and very democratic community processes (one person one vote,
enfranchising small children!). But he also asserted a principal that
to Dewey did not seem important, the freedom to choose to go to class
or stay away altogether. A child at Summerhill can just hang around;
he'll go to class when he damned well feels like it -- and some
children, coming from compulsory schools, don't damned well feel like
it for eight or nine months. But after a while, as the curiosity in
the soul revives -- and since their friends go -- they give it a try.

It is no accident, as I am trying to show in this book, that it is
just \emph{this} departure in progressive education that is catching on in
America, whereas most of the surviving Deweyan schools are little
better than the good suburban schools that imitated them. The
advance-guard problem is that the compulsory school system, like the
whole of our economy, politics and standard of living, has become a
lockstep. It is no longer designed for the maximum growth and future
practical utility of the children into a changing world, but is inept
social engineering for extrinsic goals, pitifully short-range. Even
when it is benevolent, it is in the bureaucratic death grip of a
uniformity of conception, from the universities down, that cannot
possibly suit the multitude of dispositions and conditions. Yet 100
per cent of the children are supposed to remain for at least twelve
years in one kind of ban; and of course those who attend private
Deweyan schools are being aimed for four to eight years more. Thus, if
we are going to experiment with real universal education that
educates, we have to start by getting rid of compulsory schooling
altogether.

One American variant of Summerhill has developed in a couple of years
in an unforeseen direction. Like Summerhill this school is not urban,
but, unlike Summerhill, it is not residential.  Many of the children
come from a near-by colony of artists, some of them of international
fame.  The artist parents, and other parents, come into the school as
part-time teachers, of music, painting, building, and dancing.

Being strong-minded, they, and the regular teachers, soon fell out
with the headmaster, the founder, who had been a Summerhill teacher;
they stripped him of important prerogatives and he
resigned. Inevitably other parents had to join the discussions and
decisions, are real and difficult issues. The result seems to have
been the formation of a peculiar kind of extended family, unified by
educating the children, and incorporating a few professional
teachers. But meantime, imitated from Neill, there is the democratic
council, in which the children have a very loud voice and an equal
vote, and this gives them an institutional means to communicate with,
and get back at, their parents. It is stormy and factional. Some
parents have pulled out and teachers have quit. Yet, inadvertently,
there is developing a brilliant solution to crucial problems of
American life: how can children grow up in live contact with many
adults; how can those who do the work run the show; how to transcend a
rigid professionalism that is wasteful of human resources.

At present one of the teachers at this school is preparing to try out
a little Summerhill school in a slum area in New York.

Another Summerhill variant has taken a different course: to use the
school almost directly as social action. To overcome the artificial
stratification of modern society, it brings together not only Negroes
and whites but also delinquents and the well behaved. Naturally this
gets the school into trouble with its surroundings. It has had to flee
from the South to the North, and it is in trouble again in the North.

Such a combination of education and direct social action is springing
up on all sides. The so- called Northern Student Movement is a group
of college students who take a year off from college to tutor urban
under privileged kids referred by the public schools; but the NSM has
now declared as its policy not to restrict itself to the curriculum
and aims of the school system. The Student Non-Violent Coordinating
Committee is about --I am writing in June 1964 -- to go down to the
deep South, primarily to help in the voter-registration of
disenfranchised Negroes, but also to try out little colleges for
adolescents, with five graduate students teaching twenty-five
teenagers a curriculum relevant to their economic and political
advancement. Accompanying the numerous school boycotts there have
sprung up `Freedom' schools that started as one-day propaganda
demonstrations but have been lively enough to warrant continuing.

In my opinion, the highly official Peace Corps has the same underlying
educational significance. At present it is rigidly selective and
super-collegiate; indeed it is, by and large, an operation for
upper-middle-class youth and well-paid professors and administrators:
it costs \$15,000 to get one youngster in the field for a
year. Nevertheless, the whole conception is unthinkable except as
dissatisfaction with orthodox schooling and with the status-careers
that that schooling leads to.  

The future -- if we survive and have a future, which is touch and go
-- will certainly be more leisurely. If that leisure is not to be
completely inane and piggishly affluent, there must be a community and
civic culture. There must be more employment in human services and
less in the production of hardware gadgets; more citizenly initiative
and less regimentation; and in many; spheres, decentralization of
control and administration. For these purposes, the top-down dictated
national plans and educational methods that are now the fad are quite
irrelevant. And on the contrary, it is precisely the society of free
choice, lively engagement and social action of Summerhill and American
Summerhill that are relevant and practical.  

Thus, just as with Dewey, the new advance of progressive Education is
a good index of what the real situation is. And no doubt society will
again seek to abuse this programme, which it needs but is afraid of.
