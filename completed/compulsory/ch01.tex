% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{The Universal Trap}


A conference of experts on school drop-outs will discuss the
background of poverty, cultural deprivations, race prejudice, family
and emotional troubles, neighborhood uprooting, urban mobility. It
will explore ingenious expedients to counteract these conditions,
though it will not much look to remedying them - that is not its
business. And it will suggest propaganda - e.g. no school, no job -
to get the youngsters back in school. It is axiomatic that they ought
to be in school. 

After a year, it proves necessary to call another conference to cope
with the alarming fact that more than 75 per cent of the drop-outs who
have been cajoled into returning, have dropped out again. They persist
in failing; they still are not sufficiently motivated. What curricular
changes must there be?  How can the teachers learn the life-style of
the underprivileged?  

Curiously muffed in these conferences is the question that puts the
burden of proof the other way: What are they drop-outs from? Is the
schooling really good for them, or much good for anybody? Since, for
many, there are such difficulties with the present arrangements, might
not some better arrangements be invented?  Or bluntly, since schooling
undertakes to be compulsory, must if not continually review its claim
to be useful? Is it the only means of education? Isn't it unlikely
that \textit{any} single type of social institution could fit almost
every youngster up to the age of sixteen and beyond? (It is predicted
that by 1970, 50 per cent will go to college.)

But conferences on drop-outs are summoned by school professionals, so
perhaps we cannot hope that such elementary questions will be raised,
yet neither are they raised by layman. There is a mass superstition,
underwritten by additional billions every year, that adolescents must
continue going to school. The middle class know that no professional
competence - i.e. status and salary -can be attained without many
diplomas; and poor people have allowed themselves to be convinced that
the primary remedy for their increasing deprivation is to agitate for
better schooling. Nevertheless, I doubt that, \textit{at present or
  with any reforms that are conceivable under current school
  administration}, going to school is the best use for the time of
life of the majority of youth.

Education is a natural community function and occurs inevitably, since
the young grow up on the old, towards their activities, and into (or
against) their institutions; and the old foster, teach, train, exploit
and abuse the young. Even neglect of the young, except physical
neglect, has an educational effect - not the worst possible. Formal
schooling is a reasonable auxiliary of the inevitable process,
whenever an activity is best learned by singling it out or special
attention with a special person to teach it. Yet it by no means
follows that the complicated artifact of a school system has much to
do with education, and certainly not with good education. 

Let us bear in mind the way in which a big school system might have
nothing to do with education at all. The New York system turns over
\$700 millions annually, not including capital improvements. There are
750 schools, with perhaps fifteen annually being replaced at an extra
cost of two to five million dollars each. There are 40,000 paid
employees. This is a vast vested interest, and it is very probable
that - like much of our economy and almost all of our political
structure, of which the public schools are a part - it goes on for
its own sake, keeping more than a million people busy, wasting wealth,
and pre-empting time and space in which something else could be going
on. It is a gigantic market for textbook manufacturers, building
contractors and graduate schools of education. 

The fundamental design of such a system is ancient, yet it has not
been altered although the present operation is altogether different in
scale from what it was, and therefore it must have a different
meaning. For example, in 1900, 6 per cent of the seventeen year olds
graduated from high school, and less than 0.5 percent went to college;
whereas in 1963, 65 per cent graduated from high school and 35 per
cent went on to something called college. Likewise, there is a vast
difference between schooling intermitted in life on a farm or in a
city with plenty of small jobs, and schooling that is a child's only
`serious' occupation and often his only adult contact. Thus, a perhaps
outmoded institution has become almost the only allowable way of
growing up. And with this pre-empting, there is an increasing
intensification of the one narrow experience, e.g. in the shaping of
the curriculum and testing according to the increasing requirements of
graduate schools far off in time and place. Just as our American
society as a whole is more and more tightly organized, so its school
system is more and more regimented as part of that organization. 

In the organizational plan, the schools play a non-educational and an
educational role. The non-educational role is very important. In the
tender grades, the schools are a baby-sitting service during a period
of collapse of the old-type family and during a time of extreme
urbanization and urban mobility. In the junior and senior high-school
grades, they are an arm of the police, providing cops and
concentration camps paid for in the budget under the heading `Board of
Education'. The educational role is, by and large, to provide - at
public and parents' expense -apprentice-training for corporations,
government and the teaching profession itself, and also to train the
young, as New York's Commissioner of Education has said (in the Morley
case), `to handle constructively their problems of adjustment to
authority'. 

The public schools of America have indeed been a powerful, and
beneficent force for the democratizing of a great mixed
population. But we must be careful to keep reassessing them when, with
changing conditions, they become a universal trap and democracy begins
to look like regimentation. 

Let me spend a page on the history of the compulsory nature of the
school systems. In 1961, in \textit{The Child, the Parent, and the
  State}, James Conant mentions a possible incompatibility between
`individual development' and `national needs'; this, to my mind, is a
watershed in American philosophy of education and puts us back to the
ideology of Imperial Germany, or on a par with contemporary Russia.

When Jefferson and Madison conceived of compulsory schooling, such an
incompatibility would have been unthinkable. They were in the climate
of the Enlightenment, were strongly influenced by Congregational
(town-meeting) ideas, and were of course makers of a revolution. To
them, `citizen' meant society-maker, not one `participating in' or
`adjusted to' society. It is clear that they regarded themselves and
their friends as citizens existentially, so to speak; to make society
was their breath of life. But obviously such conceptions are worlds
removed from, and diametrically opposed to, our present political
reality, where the ground rules and often the score are
predetermined. 

For Jefferson, people had to be taught in order to multiply the
sources of citizenly initiative and to be vigilant for
freedom. Everybody had to become literate and study history, in order
to make constitutional innovations and be fired to defend free
institutions, which was presumably the moral that history taught. And
those of good parts were to study a technological natural philosophy,
in order to make inventions and produce useful goods for the new
country. By contrast, what are the citizenly reasons for which we
compel everybody to be literate, etc.! To keep the economy expanding,
to understand the mass communications, to choose between
indistinguishable Democrats and Republicans. Planning and
decision-making are lodged in top managers; rarely, and at most, the
electorate serves as a pressure group. There is a new emphasis on
teaching science - we will discuss this in another context - but the
vast majority will never use this knowledge and will forget it; they
are consumers. 

Another great impulse for compulsory education came from the new
industrialism and urbanism during the three or four decades after the
Civil War, a time also of maximum immigration. Here the curricular
demands were more mundane: in the grades, literacy and arithmetic; in
the colleges, professional skills to man the expanding economy. But
again, no one would have spoken of an incompatibility between
`individual development' and `national needs', for it was considered
to be an open society, abounding in opportunity. Typically, the novels
of Horatio Alger, Jr, treats schooling as morally excellent as well as
essential for getting ahead; and there is no doubt that the immigrants
saw education-for-success as also a human value for their
children. Further, the school system was not a trap. The 94 per cent
who in 1900 did not finish high school had other life opportunities,
including making a lot of money and rising in politics. But again, by
and large this is not our present situation. There is plenty of social
mobility, opportunity to rise - except precisely for the ethnic
minorities who are our main concern as dropouts - but the statuses
and channels are increasingly stratified, rigidified, cut and
dried. Most enterprise is parceled out by feudal corporations, or by
the State; and these determine the requirements. Ambition with average
talent meets these rules or fails; those without relevant talent, or
with unfortunate backgrounds, cannot even survive in decent
poverty. The requirements of survival are importantly academic,
attainable only in schools and universities; but such schooling is
ceasing to have an initiating or moral meaning.

We do not have an open economy; even when jobs are not scarce, the
corporations and State dictate the possibilities of
enterprise. General Electric swoops down on the high schools, or IBM
on the colleges, and skims off the youth who have been pre-trained for
them at public or private expense. (Private college tuition runs
upward of \$6000, and this is estimated as a third or less of the
actual cost for `education and educational administration'.) Even a
department store requires a diploma for its sales people, not so much
because of the skills they have learned as that it guarantees the
right character: punctual and with a smooth record. And more
generally, since our powers-that-be have opted for an expanding
economy with a galloping standard of living, and since the powers of
the world are in an arms and space race, there is a national need for
many graduates specifically trained. Thus, even for those selected,
the purpose is irrelevant to citizenly initiative, the progress of an
open society, or personal happiness, and the others have spent time
and effort in order to be progressively weeded out. Some drop out. 

It is said that our schools are geared to `middle-class values', but
this is a false and misleading use of terms. The schools less and less
represent \textit{any} human values, but simply adjustment to a
mechanical system.

Because of the increasing failure of the schools with the poor urban
mass, there has developed a line of criticism - e.g. Oscar Lewis,
Patricia Sexton, Frank-Riessman, and even Edgar Friedenberg -
asserting that there is a `culture of poverty' which the
`middle-class' schools do not fit, but which has its own virtues of
spontaneity, sociality, animality. The implication is that the `middle
class', for all its virtues, is obsessional, prejudiced, and
prudish. 

Pedagogically, this insight is indispensable. A teacher must try to
teach each child in terms of what he brings, his background, his
habits, the language he understands. But if taken to be more than
technical, it is a disastrous conception. The philosophic aim of
education must be to get each one out of his isolated class and into
the one humanity. Prudence and responsibility are not middle-class
virtues but human virtues; and spontaneity and sexuality are not
powers of the simple but of human health. One has the impression that
our social-psychologists are looking not to a human community but to a
future in which the obsessionals will take care of the impulsives! 

In fact, some of the most important strengths that have historically
belonged to the middle class are flouted by the schools: independence,
initiative, scrupulous honesty, earnestness, utility, respect for
thorough scholarship. Rather than bourgeois, our schools have become
petty bourgeois, bureaucratic, time-serving, gradgrind-practical,
timid and \textit{nouveau riche} climbing. In the upper grades and
colleges, they often exude a cynicism that belongs to rotten
aristocrats.

Naturally, however, the youth of the poor and of the middle class
respond differently to the petty bourgeois atmosphere. For many poor
children, school is orderly and has food, compared to chaotic and
hungry homes, and it might even be interesting compared to total
deprivation of toys and books. Besides, the wish to improve a child's
lot, which on the part of a middle-class parent might be frantic
status-seeking and pressuring, on the part of a poor parent is a
loving aspiration. There is here a gloomy irony. The school that for a
poor Negro child might be a great joy and opportunity is likely to be
dreadful; whereas the middle-class child might be better off
\textit{not} in the `good' suburban school he has.

Other poor youths herded into a situation that does not suit their
disposition, for which they are unprepared by their background, and
which does not interest them, simply develop a reactive stupidity very
different from their behavior on the street or ball held. They fall
behind, play truant, and as soon as possible drop out. The school
situation is immediately useless and damaging to them, their response
must be said to be life-preservative. They thereby somewhat diminish
their chances of a decent living, but we shall see that the usual
propaganda - that schooling is a road to high salaries - is for most
poor youths a lie; and the increase in security is arguably not worth
the torture involved. 

The reasonable social policy would be not to have these youths in
school, certainly not in high school, but to educate them otherwise
and provide opportunity for a decent future in some other way. How? I
shall venture some suggestions later; in my opinion, the wise thing
would be to have our conferences on this issue, and omit the idea of
drop-out altogether. But the brute fact is that our society isn't
really interested; the concern for the drop-outs is mainly because
they are a nuisance and a threat and can't be socialized by the
existing machinery. 

Numerically far more important than these drop-outs at sixteen,
however, are the children who conform to schooling between the ages of
six to sixteen or twenty, but who drop out internally and day-dream,
their days wasted, their liberty caged and scheduled. And there are
many such in the middle class, from backgrounds with plenty of food
and some books and art, where the youth is seduced by the prospect of
money and status, but even more where he is terrified to jeopardize
the only pattern of life be knows. 

It is in the schools and from the mass media, rather than at home or
from their friends, that the mass of our citizens in all classes learn
that life is inevitably routine, depersonalized, venally graded; that
it is best to toe the mark and shut up; that there is no place for
spontaneity, open sexuality, free spirit. Trained in the schools, they
go on to the same quality of jobs, culture, and politics. This
education is, miseducation, socializing to the national norms and
regimenting to the national `needs'.

John Dewey used to hope, na\"ively, that the schools could be a
community somewhat better than society and serve as a lever for social
change. In fact, our schools reflect our society closely, except that
they \textit{emphasize} many of its worst features, as well as having
the characteristic defects of academic institutions of all times and
places.

Let us examine realistically half a dozen aspects of the school that
is dropped out \textit{from}.
\begin{enumerate}

\item There is widespread anxiety about the children not learning to
  read, and hot and defensive argument about the methods of teaching
  reading. Indeed, reading deficiency is an accumulating scholastic
  disadvantage that results in painful feeling of inferiority, truancy
  and drop-out. Reading is crucial for school success - all subjects
  depend on it - and therefore for the status success that the
  diploma is about. Yet in all the anxiety and argument, there is no
  longer any mention of the freedom and human cultivation that
  literacy is supposed to stand for.

  In my opinion, there is something phony here. For a change, let us
  look at this `reading' coldly and ask if it is really such a big
  deal except precisely in the school that is supposed to teach it and
  is sometimes failing to do so.

  With the movies, TV and radio that the illiterate also share, there
  is certainly no lack of `communications'. We cannot say that as
  humanities or science, the reading-matter of the great majority is
  in any way superior to the content of these other media. And in the
  present stage of technology and economy, it is probably
  \textit{less} true than it was in the late nineteenth century - the
  time of the great push to universal literacy and arithmetic - that
  the mass teaching of reading is indispensable to operate the
  production and clerical system. It is rather our kind of urbanism,
  politics and buying and selling that require literacy. These are not
  excellent.

Perhaps in the present dispensation we should be as well off if it
were socially acceptable for large numbers not to read. It would be
harder to regiment people if they were not so well `informed'; as
Norbert Wiener used to point out, every repetition of a cliché only
increases the noise and prevents communication. With less literacy,
there would be more folk culture. Much suffering of inferiority would
be avoided if youngsters did not have to meet a perhaps unnecessary
standard. Serious letters could only benefit if society were less
swamped by trash, lies and bland verbiage. Most important of all, more
people might become genuinely literate if it were understood that
reading is not a matter of course but a special useful art with a
proper subject-matter, imagination and truth, rather than a means of
communicating top-down decisions and advertising. (The advertising is
a typical instance: when the purpose of advertising was to give
information - `New shipment of salt fish arrived, very good, foot of
Barclay Street'- it was useful to be able to read; when the point of
advertising is to create a synthetic demand, it is better not to be
able to read.)


\item Given their present motives, the schools are not competent to teach
authentic literacy, reading as a means of liberation and
cultivation. And I doubt that most of us who seriously read and write
the English language ever learned it by the route of `Run, Spa, Run'
to Silas Marner. Rather, having picked up the rudiments either in
cultured homes or in the first two grades, we really learned to read
by our own will and free exploration, following our bent, generally
among books that are considered inappropriate by school librarians!

A great neurologist tells me that the puzzle is not how to teach
reading, but why some children fail to learn to read. Given the amount
of exposure that any urban child gets, any normal animal should
spontaneously catch on to the code. What prevents? It is almost
demonstrable that, for many children, it is precisely going to school
that prevents - because of the schools alien style, banning of
spontaneous interest, extrinsic rewards and punishments. (In many
underprivileged schools, the IQ steadily falls the longer they go to
school.) Many of the backward readers might have had a better chance
on the streets.

But let me say something, too, about the `successful' teaching of
reading and writing in the schools. Consider, by contrast, the method
employed by Sylvia Ashton-Warner in teaching little Maoris. She gets
them to ask for their own words, the particular gut-word of fear, lust
or despair that is obsessing the child that day; this is written for
him on strong cardboard; he learns it instantaneously and never
forgets it; and soon he has an exciting, if odd, vocabulary. From the
beginning, writing is by demand, practical, magical; and of course it
is simply an extension of speech - it is the best and strongest
speech, as writing should be. What is read is what somebody is
importantly trying to tell. Now what do our schools do? We use tricks
of mechanical conditioning. These do positive damage to spontaneous
speech, meant expression, earnest understanding. Inevitably, they
create in the majority the wooden attitude toward `writing', as
entirely different from speech, that college teachers later try to
cope with in Freshman Composition. And reading inevitably becomes a
manipulation of signs, e.g. for test-passing, that has no relation to
experience.

(Until recently, the same discouragement by school teachers plagued
children's musical and plastic expression, but there have been
attempts to get back to spontaneity - largely, I think, because of
the general revolution in modern art and musical theory. In teaching
science, there is just now a strong movement to encourage imagination
rather than conditioned `answers'. In teaching foreign languages, the
emphasis is now strongly on vital engagement and need to speak. Yet in
teaching reading and writing, the direction has been the contrary;
even progressive education has gone back to teaching spelling. These
arts are regarded merely as `tools'.)


\item The young rightly resist animal constraint. But, at least in New
  York where I have been a school-board visitor, most teachers - and
  the principals who supervise their classes - operate as if
  progressive education had not proved the case for noise and freedom
  of bodily motion. (Dewey stresses the salutary alternation of
  boisterousness and tranquility.)  The seats are no longer bolted to
  the floor, but they still face front. Of course, the classes are too
  large to cope with without `discipline'. Then make them smaller, or
  don't wonder if children escape out of the cage; either into truancy
  or baffled daydream. Here is a typical case: an architect replacing
  a Harlem school is forbidden by the board to spend money on
  soundproofing the classrooms, even though the principal has called
  it a necessity for the therapy of pent-up and resentful
  children. The resentment, pent-up hostility, is a major cause of
  reactive stupidity; yet there is usually an absolute ban on even
  expression of hostility, or even of normal anger and aggression.

Again, one has to be blind not to see that, from the onset
of puberty, the dissidence from school is importantly
sexual. Theoretically, the junior high school was introduced to fit
this change of life; yet astoundingly, it is sexless. My own view, for
what it's worth, is that sexuality is lovely, there cannot be too much
of it, it is self-limiting if it is satisfactory, and satisfaction
diminishes tension and clears the mind for attention and
learning. Therefore, sexual expression should be approved in and out
of season, also in school, and where necessary made the subject of
instruction. But whether or not this view is correct, it certainly is
more practical than the apparent attempt of the schools to operate as
if sexual drives simply did not exist. When, on so crucial an issue,
the schools act a hundred years out of date, they are crucially
irrelevant. 
But the following is something new:

Trenton, 24 May (AP) - A state health official believes some
over-anxious New Jersey parents are dosing their children with
tranquilizers before sending them to school ... the Health Department
pediatrician assigned to the State Education Department said the
parents apparently are trying to protect the children from cracking
under pressure for good grades. 


\item Terrible damage is done to children simply by the size and
  standardization of the big system. Suppose a class size of twenty is
  good for average purposes, it does not follow that thirty-five is
  better than nothing. Rather, it is likely to be positively harmful,
  because the children have ceased to be persons and the teacher is
  destroyed as a teacher. A teacher with a ten-year-old class reading
  at seven-year level will have to use the content as well as the
  vocabulary Dick and Jane since that is the textbook bought by the
  hundred thousands. The experience of a wise principal is that the
  most essential part of his job is to know every child's name and be
  an available `good father', so he wants a school for 400. Yet the
  city will build the school for 2000, because only that is practical,
  even though the essence is entirely dissipated. The chief part of
  learning is in the community of scholars, where class work and
  social life may cohere; yet social engineers like Dr Conant will,
  for putative efficiencies, centralize the high schools - the
  `enriched' curriculum with equipment is necessary for the national
  needs.

A programme - e.g. to prevent drop-out - will be, by an attentive
teacher, exquisitely tailored to the children he works with; he will
have a success. Therefore his programme must be standardized, watered
down, for seventy-five schools - otherwise it cannot be financed
- although now it is worthless. But here is an unbearable anecdote: An
architect is employed to replace a dilapidated school but is forbidden
to consult the principal and teachers of the school about their needs,
since his building must confirm to uniform plans at headquarters, the
plans being two generations out of date. As a functionalist, the
architect demurs, and it requires an ad hoc assembly of all the
superintendents to give him special permission.

Presumably all this is administratively necessary, but then it is also
necessary for bruised children to quit. Our society makes a persistent
error in metaphysics. We are so mesmerized by the operation of a
system with the appropriate name, for instance `Education', that we
assume that it must be working somewhat, though admittedly not
perfectly, when perhaps it has ceased to fulfill its function
altogether and might even be preventing the function, for instance
education.


\item Especially today, when the hours of work will sharply diminish,
  the schools are supposed to educate for the satisfaction of life and
  for the worthwhile use of leisure. Again, let us try to be
  realistic, as a youngster is. For most people, I think, a candid
  self-examination will show that their most absorbing, long and
  satisfactory hours are spent in activities like friendly competitive
  sports, gambling, looking for love and love-making, earnest or
  argumentative conversation, political action with signs and sit-ins,
  solitary study and reading, contemplation of nature and cosmos, arts
  and crafts, music and religion. Now none of these requires much
  money. Indeed, elaborate equipment takes the heart out of
  them. Friends use one another as resources. God, nature and
  creativity are free. The media of the fine arts are cheap
  stuff. Health, luck and affection are the only requirements for good
  sex. Good food requires taking pains more than spending money. 

  What is the moral for our purposes? Can it be denied that in some
  respects the drop-outs make a wiser choice than many who go to
  school, not to get real goods but to get money? Their choice of the
  `immediate' - their notorious `inability to tolerate delay' - is
  not altogether impulsive and neurotic. The bother is that in our
  present culture, which puts its entire emphasis on the consumption
  of expensive commodities, they are so nagged by inferiority,
  exclusion and despair of the future that they cannot employ their
  leisure with a good conscience. Because they know little, they are
  deprived of many profound simple satisfactions and they never know
  what to do with themselves. Being afraid of exposing themselves to
  awkwardness and ridicule, they just hang around. And our urban
  social arrangements - e.g. high rent - have made it impossible for
  anybody to be decently poor on a `low' standard. One is either in
  the rat race or has dropped out of society altogether.


\item As a loyal academic, I must make a further observation. Mainly
  to provide Ph.D.s, there is at present an overwhelming pressure to
  gear the `better' elementary schools to the
  graduateuniversities. This is the great current reform, genre of
  kickover. But what if the top of the ladder is corrupt and corrupts
  the lower grades? On visits to seventy colleges everywhere in the
  country, I have been appalled at how rarely the subjects are studied
  in a right academic spirit, for their truth and beauty and as part
  of humane international culture. The students are given, and seek, a
  narrow expertise, `mastery', aimed at licenses and salary. They are
  indoctrinated with a national thoughtlessness that is not even
  chauvinistic. Administrators sacrifice the community of scholars to
  aggrandizement and extramurally sponsored research. 

  Conversely, there is almost never conveyed the sense in which
  learning is truly practical, to enlighten experience, give courage
  to initiate and change, reform the state, deepen personal and social
  peace. On the contrary, the entire educational system itself creates
  professional cynicism or the resigned conviction that Nothing Can Be
  Done. If this is the university, how can we hope for aspiring
  scholarship in the elementary schools? On the contrary, everything
  will be grades and conforming, getting ahead not in the subject of
  interest but up the ladder. Students `do' Bronx Science in order to
  `make' MIT and they `do' MIT in order to `make' Westinghouse; some
  of them have `done' Westinghouse in order to `make' jail.
\end{enumerate}

What then? The compulsory system has become a universal trap, and it
is no good. Very many of the youth, both poor and middle class, might
be better off if the system simply did not exist, even if they then
had no formal schooling at all. (I am extremely curious for a
philosophic study of Prince Edward County in Virginia, where for some
years schooling did not exist for Negro children.) 

But what would become of these children? For very many, both poor and
middle class, their homes are worse than the schools, and the city
streets are worse in another way. Our urban and suburban environments
are precisely not cities or communities where adults naturally attend
to the young and educate to a viable life. Also, perhaps especially in
the case of the overt drop-outs, the state of their body and soul is
such that we must give them refuge and remedy, whether it be called
school, settlement house, youth worker or work camp. 

There are thinkable alternatives. Throughout this little book, as
occasion arises, I shall offer alternative proposals that I as a
single individual have heard of or thought up. Here are half a dozen
directly relevant to the subject we have been discussing, the system
as compulsory trap. In principle, when a law begins to do more harm
than good, the best policy is to alleviate it or try doing without
it. 
\begin{enumerate}


\item Have `no school at all' for a few classes. These children
should be selected from tolerable, though not necessarily cultured,
homes. They should be neighbors and numerous enough to be a society
for one another so that they do not feel merely `different'. Will they
learn the rudiments anyway? This experiment cannot do the children any
academic harm, since there is good evidence that normal children will
make up the first seven years schoolwork with four to seven months of
good teaching.


\item Dispense with the school building for a few classes; provide
teachers and use the city itself as the school - its streets,
cafeterias, stores, movies, museums, parks and factories. Where
feasible, it certainly makes more sense to teach using the real
subject matter than to bring an abstraction of the subject matter into
the school building as `curriculum'. Such a class should probably not
exceed ten children for one pedagogue. The idea - it is the model of
Athenian education - is not dissimilar to youth-gang work, but not
applied to delinquents and not playing to the gang ideology. 


\item Along the same lines, but both outside and inside the school
  building, use appropriate unlicensed adults of the community - the
  druggist, the storekeeper, and the mechanic - as the proper
  educators of the young into the grown-up world. By this means we can
  try to overcome the separation of the young from the grown-up world
  so characteristic in modern urban life, and to diminish the
  omnivorous authority of the professional school-people. Certainly
  it would be a useful and animating experience for the adults. (There
  is the beginning of such a volunteer programme in the New York and
  some other systems.)


\item Make class attendance not compulsory, in the manner of
  A. S. Neill's Summerhill. If the teachers are good, absence would
  tend to be eliminated if they are bad, let them know it. The
  compulsory law is useful to get the children away from the parents,
  but it must not result in trapping the children. A fine modification
  of this suggestion is the rule used by Frank Brown in Florida: he
  permits the children to be absent for a week or a month to engage in
  any worthwhile enterprise or visit any new environment.

\item Decentralize an urban school (or do not build a new big
  building) into small units, twenty to fifty, in available
  storefronts or clubhouses. These tiny schools, equipped with
  record-player and pin-ball machine, could combine play, serializing,
  discussion and formal teaching. For special events, the small units
  can be brought together into a common auditorium or gymnasium, so as
  to give the sense of the greater community. Correspondingly, I think
  it would be worthwhile to give the Little Red Schoolhouse a spin
  under modern urban conditions, and see how it works out: that is, to
  combine all the ages in a little room for twenty-five to thirty,
  rather than to grade by age.


\item Use a pro rata part of the school money to send children to
  economically marginal farms for a couple of months of the year,
  perhaps six children from mixed backgrounds to a farmer. The only
  requirement is that the farmer feed them and not beat them; best, of
  course, if they take part in the farm work. This will give the
  farmer cash, as part of the generally desirable programme to redress
  the urban-rural ratio to something nearer to 70 per cent to 30 per
  cent. (At present, less than 8 per cent of families are rural.)
  Conceivably, some of the urban children will take to the other way
  of life, and we might generate a new kind of rural culture.
\end{enumerate}

I frequently suggest these and similar proposals at teachers'
colleges, and I am looked at with an eerie look - do I really mean to
diminish the state-aid grant for each student-day? But mostly the
objective is that such proposals entail intolerable administrative
difficulties.

Above all, we must apply these or any other proposals to particular
individuals and small groups, without the obligation of
uniformity. There is a case for uniform standards of achievement,
lodged in the Regents, but they cannot be reached by uniform
techniques. The claim that standardization of procedure is more
efficient, less costly, or alone administratively practical, is often
false. Particular inventiveness requires thought, but thought does not
cost money.
