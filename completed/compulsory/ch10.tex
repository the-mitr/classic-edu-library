% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{Two Simple Proposals}


Jacques Barzun, the Dean of Faculties at Columbia, has pre-dined for
us the end of the liberal arts college; it cannot survive the emphasis
on technical and professional education and the overwhelming financing
of scientific research by Federal money, corporation money and most of
the foundation money.

I think his prediction is justified -- I am not so sure I am gloomy about it; if there is a revival of
real education in this country, its form and auspices will not look like what we have been used to.
But I do not think that the Dean thoroughly understands the causes, or the extent, of the debacle.
For the same social trend, of vocational training and contracted research, spells the end not only
of the colleges but of the universities as well, regarded as schools for independent professionals,
communities of scholars, and centers of free inquiry. The crucial issue is not the change from
'general' education to `specialism'; and there is nothing amiss in the sciences having a run as the
preponderant center of studies, since that are the nature of the environment. The medieval
universities were mainly professional schools dominated by a kind of metaphysical science,
according to their lights. The crucial issue is the change from the humanism of independent guilds
of scholars, whether in the liberal arts or the professions, to a system of social engineering for the
national economy and polity. The medieval professions and specialties were structured in an ideal
world that allowed for communication, that was international, and in which -- in an important
sense -- the professions were oddly spontaneous and free. Our learning is increasingly
departmentalized and prescribed.

Our educational reality can be seen in operation in the present kind
of scheduling, testing and grading; and if Dean Barzun is interested
in making a change, he can start right here.

Let me repeat the fans. From early childhood, the young are subjected
to a lockstep increasingly tightly geared to the extramural
demands. There is little attention to individual pace, rhythm or
choice, and none whatever to the discovery of identity or devotion to
intellectual goals. The aptitude and achievement testing and the
fierce competition for high grades are a race up the ladder to
high-salaried jobs in the businesses of the world, including the
schooling business. In this race, time is literally
money. Middle-class youngsters -- or their parents -- realistically
opt for advanced placement and hasten to volunteer for the National
Merit examinations. Negro parents want the same for their children,
although the children have less tendency to cooperate.

Disappointingly, but not surprisingly, the colleges and universities
go along with this spiritual destruction, and indeed devise the tests
and the curricula to pass the tests. Thereby they connive at their own
spiritual destruction, yet it is not surprising, for that is where the
money and the grandeur are. I do not expect for a moment that they
will, in the foreseeable future, recall their primary duties: to pass
on the tradition of disinterested learning, to provide a critical
standard, to educate the free young (\emph{liberi}) to be free citizens and
independent professionals.

The question is, could the colleges and universities act otherwise,
even if they wished to? Of course they could. Most of them are
autonomous corporations. Let me here suggest two modest changes, that
are feasible almost immediately, that would entail no risk whatever,
and yet would immensely improve these academic communities and
importantly liberate them in relation to society.

First, suppose that half a dozen of the most prestigious liberal arts
colleges -- say Amherst, Swarthmore, Connecticut, Weslyan, Carleton,
etc. -- would announce that, beginning in 1966, they required for
admission a two-year period, after high school, spent in some maturing
activity.  These colleges are at present five times oversubscribed;
they would not want for applicants on any conditions that they set;
and they are explicitly committed to limiting their expansion.  By
'maturing activity' could be meant: working for a living, especially
if the jobs art gotten without too heavy reliance on connections;
community service, such as the Northern Student Movement, volunteer
service in hospital or settlement house, domestic Peace Corps; the
army -- though I am a pacifist and would urge a youngster to keep out
of the army; a course of purposeful travel that met required
standards; independent enterprise in art, business or science, away
from home, with something to show for the time spent.

The purpose of this proposal is twofold: to get students with enough
life-experience to be educable on the college level, especially in the
social sciences and humanities; and to break the lockstep of twelve
years of doing assigned lessons for grades, so that the student may
approach his college studies with some intrinsic motivation, and
therefore perhaps assimilate something that might change him. Many
teachers remember with nostalgia the maturer students who came under
the G I Bill, though to be sure a large number of them were pretty
shell-shocked 

A subsidiary advantage of the plan would be to relieve the colleges of
the doomed, and hypocritical, effort to serve in \emph{loco parentis} on
matters of morality. If young persons have been out working for a
living, or have traveled in foreign parts, or have been in the army, a
college can assume that they can take care of themselves.

The American tradition of colleges for adolescents made a kind of
sense when the curriculum was largely unquestioned classics, history
and mathematics, taught dogmatically in a seminarian atmosphere, and
to an elite that thought it had a justified social role. Present
college teaching tries to be something different. It emphasizes
method, background reading, criticism and research, and offers a range
of choice or prescription quite baffling to most seventeen year
olds. In a curious way, the present dominance of mathematics and
physical sciences has resulted in the students being even less mature,
yet has obscured the true picture of student ineptitude and
professional frustration. It is possible to teach mathematics and
physics to boys and girls, especially boys.  These abstract subjects
suit their alert and schematizing minds, the more so if the teaching
treats science as the solution of puzzles. But it is not possible to
teach sociology, anthropology, and world literature to boys and girls,
for they have no experience and judgment. When it is done, the message
is merely verbal. The harsh facts and the relativity of morals are
bound to be embarrassing and shocking. Regarded as `assignments' -- as
high-school graduates must regard them -- the voluminous readings are
indigestible straw and are annotated by lore; more mature students
might be able to take them as books. In brief, whip-bang youngsters
who have found their identity as mathematicians, chemists or
electronic technicians might well speed on to MIT at age fifteen. The
liberal arts colleges, that are essentially concerned with educating
citizens and statesmen, social scientists and social professionals,
scholars and men of letters, require more maturity to begin with. If
the average age of entrance were higher, these colleges would also
serve as the next step for the many disappointed science students, who
can hardly be expected to backtrack among the seventeen's. (A very
numerous group switch from the physical sciences to the social
sciences and humanities.)

Throughout our educational system there is a desperate need for
institutional variety and interims in which a youth can find
himself. If we are going to require as much schooling as we do, we
must arrange for breaks and return-points, otherwise the schooling
inevitably becomes spirit breaking and regimentation. In my opinion,
however, a much more reasonable overall pattern is to structure all of
society and the whole environment as educative, with the schools
playing the much more particular and traditional role of giving
intensive training when it is needed and sought, or of being havens
for those scholarly by disposition.

My other proposal is even simpler, and not at all novel. Let half a
dozen of the prestigious universities -- Chicago, Stanford, the Ivy
League -- abolish grading, and use testing only and entirely for
pedagogic purposes as teachers see fit.

Anyone who knows the frantic temper of the present schools will
understand the trans-valuation of values that would be affected by
this modest innovation. For most of the students, the competitive
grade has come to be the essence. The naive teacher points to the
beauty of the subject and the ingenuity of the research; the shrewd
student asks if he is responsible for that on the final exam.

Let me at once dispose of an objection whose unanimity is quire
fascinating. I think that the great majority of professors agree that
grading hinders teaching and creates a bad spirit, going as far as
cheating and plagiarizing. I have before me the collection of essays;
\emph{Examining in Harvard College}, and this is the consensus. It is
uniformly asserted, however, that the grading is inevitable; for how
else will the graduate schools, the foundations, the corporations know
whom to accept, reward, hire? How will the talent scouts know whom to
tap?

By testing the applicants, of course, according to the specific
task-requirements of the inducting institution, just as applicants for
the Civil Service or for licenses in medicine, law and architecture
are tested. Why should Harvard professors do the testing for
corporations and graduate schools?

The objection is ludicrous. Dean Whitla, of the Harvard Office of
Tests, points out that the scholastic-aptitude and achievement tests
used for admission to Harvard are a super- excellent index for
all-around Harvard performance, better than high-school grades or
particular Harvard course-grades. Presumably, these college-entrance
rests are tailored for what Harvard and similar institutions want. By
the same logic, would not an employer do far better to apply his own
job- aptitude test rather than to rely on the vagaries of Harvard
section-men. Indeed, I doubt that many employers bother to look at
such grades; they are more likely to be interested merely in the fact
of a Harvard diploma, whatever that connotes to them. The grades have
most of their weight with the graduate schools -- here, as elsewhere;
the system runs mainly for its own sake.

It is really necessary to remind our academics of the ancient history
of Examination. In the medieval university, the whole point of the
grueling trial of the candidate was whether or not to accept him as a
peer. His disputation and lecture for the Master's was just that, a
masterpiece to enter the guild. It was not to make comparative
evaluations. It was not to weed out and select for an extra-mural
licensor or employer. It was certainly not to pit one young fellow
against another in an ugly competition. My philosophic impression is
that the medieval thought they knew what a good job of work was and
that we are competitive because we do not know. But the more status is
achieved by largely irrelevant competitive evaluation, the less will
we ever know.

(Of course, our American examinations never did have this purely guild
orientation, just as our faculties have rarely had absolute autonomy;
the examining was to satisfy Overseers, Elders, distant Regents -- and
they as paternal superiors have always doted on giving grades rather
than accepting peers. But I submit that this set-up itself \emph{makes it
impossible for the student to become a master, to have grown up}, and
to commence on his own. He will always be making A or B for some
overseer. And in the present atmosphere, he will always be climbing on
his friend's neck.)

Perhaps the chief objectors to abolishing grading would be the
students and their parents. The parents should be simply disregarded;
their anxiety has done enough damage already. For the students, it
seems to me that a primary duty of the university is to deprive them
of their props, their dependence on extrinsic valuation and
motivation, and to force them to confront the difficult enterprise
itself and finally lose themselves in it.

A miserable effect of grading is to nullify the various uses of
testing. Testing, for both student and teacher, is a means of
structuring, and also of finding out what is blank or wrong and what
has been assimilated and can be taken for granted. Review -- including
high-pressure review -- is a means of bringing together the fragments,
so that there are Rashes of synoptic insight.

There are several good reasons for testing, and kinds of test. But if
the aim is to discover weakness, what is the point of downgrading and
punishing it, and thereby inviting the student to conceal his
weakness, by faking and bulling, if not cheating! The natural
conclusion of synthesis is the insight itself, not a grade for having
had it. For the important purpose of placement, if one can establish
in the student the belief that one is testing not to grade and make
invidious comparisons but for his own advantage, the student should
normally seek his own level, where he is challenged and yet capable,
rather than trying to get by. If the student dares to accept himself
as he is, a teacher's grade is a crude instrument compared with a
student's self-awareness. But it is rare in our universities that
students are encouraged to notice objectively their vast confusion.
Unlike Socrates, our teachers rely on power-drives rather than shame
and ingenuous idealism.

Many students are lazy, so teachers try to goad or threaten them by
grading. In the long run this must do more harm than good, Laziness is
a character-defense. It may be a way of avoiding learning; in order to
protect the conceit that one is already perfect (deeper, the despair
that one never can). It may be a way of avoiding just the risk of
failing and being downgraded. Sometimes it is a way of politely
saying, `I won't'. But since it is the authoritarian grown-up demands
that have created such attitudes in the first place, why repeat the
trauma? There comes a time when we must treat people as adults,
laziness and all. It is one thing courageously to fire a do-nothing
out of your class; it is quite another thing to evaluate him with a
lordly F.

Most important of all, it is often obvious that balking in doing the
work, especially among bright young people who get to great
universities, means exactly what it says. The work does not suit me,
not this subject, or not at this time, or not in this school, or not
in school altogether. The student might not be bookish; he might be
school-tired; perhaps his development ought now to take another
direction. Yet unfortunately, if such a student is intelligent and is
not sure of himself, he can be bullied into passing, and this obscures
everything. My hunch is that I am describing a common situation. What
a grim waste of young life and teacherly effort! Such a student will
retain nothing of what he has `passed' in. Sometimes he must get
mononucleosis to tell his story and be believed.

And ironically, the converse is also probably commonly true. A student
flunks and is mechanically weeded out, who is really ready and eager
to learn in a scholastic setting, but he has not quite caught on. A
good teacher can recognize the situation, but the computer wreaks its
will.

