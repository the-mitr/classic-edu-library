% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode



\chapter{I Don't Want To Work Why Should I?}

At seventeen and eighteen, nearly half go off to something called
college, and the others go to work, if there are jobs. We shall see
that college is not a very educative environment, but by and large the
world of work is even less so.

For most poor urban youth, the strongest present reason to go to work
is family pressure; to bring in some money and not be a drag on the
hard-working parents who are supporting them.  Needless to say, such a
reason springs from a complex of problematic emotions: resentment,
spite, need for dependency and independence; and from conditions of
poverty often at a crisis just at this juncture. As an incentive for
finding a job, finding the right job, or preparing oneself for a job,
these are unhappy auspices, and they often operate in reverse, toward
balkiness and truancy.

But the more objective social form of this reason -- `You ought to
pull your oar as a member of society; by the sweat of thy brow shalt
thou eat bread' -- is nowadays much less telling. We do not have, in
America, an economy of scarcity, only an enormous number of poor
people. To expand the economy still further might well be politically
expedient, to diminish unemployment and keep up the rate of profit,
but the facts are pretty plain that there is a synthetic demand and an
absurd standard of living. Every kid jeers at the ads. And the
prestigious flashy desirable goods are not such as poor youth
beginning in jobs are going to get. In poor neighborhoods the men who
do get them -- on credit -- are not usually models for modest labor.

Nor do the idle actually starve. For political and humanitarian
reasons the affluent society doles out subsistence, although stingy in
this as in other public goods such as education, neighborhood beauty,
and care for the delinquent and insane. And we can hardly expect a
youth to have a sense of responsibility to his community when every
force in modern urban life tends to destroy community sentiment and
community functioning.

Perhaps most important of all is that the moral ideology and the
dominant economic behavior are entirely inconsistent. Managers adopt
as many labor saving machines as possible, but the saving of labor is
not passed on to society as a whole in shorter work hours, or even
cheaper prices. And even in service-operations where there is no
automation, such as restaurants, there is a cutback of employment:
bigger crowds and fewer people to serve them. Yet there is political
excitement about unemployment.  

Add, finally, that at least 25 per cent of the gross national product
is rather directly devoted to the thousand overkill.

It is hard to know how much these philosophical considerations weigh
with simple folk and children. In a profound sense, people are not
fools, and they sniff the atmosphere correctly. In any case, the
argument, `you work, you can hold your head up with self-respect',
does not have the overpowering force among our poor youth that it once
did. Hipster notions of finding a racket seem also to satisfy the
community ethic. And there is even the ethic that to work for a mere
living is to be a fool.

There is an evident and sickening disproportion between the money that
people work hard for, whether as dish-washer, hospital orderly,
stenographer, school-teacher or artist, and the `soft' money that
comes from expense accounts, tax-dodge foundations, having `made it'
as a personality. I have referred to the disproportionate cut of the
pie that falls to the academic monks in any welfare operation. Then
why should those who are not going to be in the Establishment work for
money, rather than look for an angle or wait for luck? And it does not
help when kids see an immense part of their parents' hard-earned money
go on usurious installment payments for high-priced hardware, and rent
swallowing up more than a quarter of income.

My guess is that many poor kids are in the cruel dilemma of feeling
guilty about not working, and yet uneasy among their peer and even in
their own better judgment if they do try to get jobs -- especially
when trying to get a job has its own degrading humiliations, of
confronting prejudice, bureaucratic callousness and gorging agencies,
and often when the young are frustrated by sheer ignorance of how to
look for a job at all.

And there is another philosophical aspect, usually overlooked, that is
obviously important for these young. I have mentioned it before. So
far as they can see -- and they see clearly -- the absorbing
satisfactions of life do not require all this work and rat race. In
societies where it is possible to be decently poor, persons of
superior education and talent often choose to be poor rather than
hustle for money.

In the inflationary American economy, however, decent poverty is
almost impossible. To be secure at all, one has to engage in the
competition and try to rise; and the so-called `education' is geared
to economic advancement. Thus, a common sensible youth and especially
a poor one whose opportunities for advancement are limited and whose
cultural background is unacademic might reasonably judge that games,
sex and the gang are preferable to school and work, but he will then
get not independence but misery. He will be so out of things that he
will have nothing to occupy his mind. He is disqualified for
marriage. He is inferior, outcast.

As it is, the only ones who can afford the absorbing and simple
satisfactions that do not cost much money are those who have succeeded
economically and are by then likely unfit to enjoy anything. From this
point of view, the chief blessing that our copious society could
bestow on us would be a kind of subsistence work that allowed spirited
people to be decently poor without frantic insecurity and long
drudgery.
 
If we turn to the deeper human and religious answers to the question
'Why should I work?'-- for example, work as fulfillment of one's
potentialities, work as the vocation that gives justification -- our
present economy has little to offer to youth.

Unskilled and semi-skilled jobs are parts of elaborate enterprises
rationalized for then own operation and not to fulfill the lives of
workmen. Work processes are rarely interesting. Workmen are not taught
the rationale of the whole. The products are often humanly pretty
worthless, so there is no pride of achievement or utility. Craft and
style are built into the machines, lost to the workmen. Labor unions
have improved the conditions and dignity of the job, but they have
also become bureaucratized and do not give the sense of solidarity.

It is only in the higher job brackets, beyond most poor youths, that
there begins to be a place for inventiveness and independent
initiative belongs only to top management and expert advisers.  There
are fewer small shops. Neighborhood stores give way to centralized
supermarkets where the employees have no say. There is a great
increase in social services, but these require official licenses and
are not open to those otherwise qualified who wish to work in them.

The total background of poor youth, including the inadequacies of the
schools, conduces to dropping out; but the simplest worthwhile jobs
require diplomas.

Here again, it may be asked if these considerations, of vocation,
job-interest, job-worthiness, weigh with poor youth. They weigh with
everybody. Indeed, the hard task of a youth worker is to find same
objective activity that a youth might be interested in, and proud of
achieving, that will save him from recessive narcissism and reactive
hostility or withdrawal, and give him something to grow on. Further,
as I argued in \emph{Growing Up Absurd}, the high premium that workmen put on
'security' is largely a substitute for the feeling of being needed,
fully and indispensable.

Some of the human deficiencies in the jobs can be ameliorated -- at
least until automation makes the whole matter nugatory by vanishing
the jobs. For example, with elementary thoughtfulness, a big plant
that has many positions on allows a prospective employee to visit and
try out various stations, rather than making an arbitrary
assignment. Work processes can be efficiently designed on
psychological grounds; for instance, a small group assembling a big
lathe from beginning to end, so they have something to show for their
day, as the crane carries the product away. In a form of `collective
contract' or gang-system used in Coventry in England, fifty workers
contract with the firm on a piecework basis, and then settle among
themselves the particular assignments, personnel, schedule and many of
the processes; there must be many industries where this humanizing
procedure is feasible. With technical education paid for by industry
in co-operation with the schools, we could have more understanding
workmen.

The important point is that job-worthiness, the educative value of the
job, must be recognized by managers and labor unions as a specific
good.

But of course this is part of the much larger need, to make our whole
environment more educative, rather than rigidly restricting the
concept of education to what happens in schools.

Socially useful work is probably an indispensable element in the
education of most adolescents.  It provides an objective structure, a
bridge of norms and values, in the transition from being in the family
to being oneself. This is the rationale of a good youth-work camp, as
I described it in \emph{Utopian Essays}; a community of youth democratically
directing itself, and controlling itself, to do a job. Many colleges
have adopted the Antioch plan of alternating academic periods with
periods of work in the economy, which are then made the subject of
further academic criticism. But what a pity that precisely the poor
youth, who have to go to work, get no value from the jobs they work
at!  

Finally, let me say a word about the miserable job induction at
present. I have already mentioned the degrading and humiliating
conditions that accompany looking for scarce jobs.  Again, we do not
appreciate the terrors and hang-ups for the semi-literate and socially
paranoiac in filling out personnel forms. Often young human beings are
tormented and talent is lost simply for the convenience of business
machines. And naturally, for those disposed to feel rejected and
inferior, each further frustration rapidly accumulates into an
impassable black. A lad soon turns in the form not filled out, or even
turns back outside the door. Or, pathetically, there is manic
excitement at landing a job, which he soon quits or cannot do
anyway. The entire process is hopelessly and irrelevantly charged with
emotion. And the pitiful and anxious lies that are written on those
forms!

Certainly the current proposals to make the school the employment
agency are reasonable; the school is at least familiar, even if the
kid hates it and has dropped out.

Our classical ideology is that the job should be looked for with
resolution and ambition. But how are these possible on the basis of
ignorance and alienation? Here as elsewhere, our problem is lapse of
community. Our society has less and less community between its adults
and its youth.  Traditional and family crafts and trades no longer
exist, and a youth has few chances to form himself on model workmen in
his neighborhood and learn the ropes and opportunities. The
difficulties of getting into a union seem, and often are,
insuperable. Middle-class academic youth in their colleges have at
least some contact with the adults who belong to the ultimate job
world, and placement is pretty good. But poor urban youth, in schools
whose culture is quite alien to them and whose aims fit neither their
desires nor their capacities, are among gaolers, not models.

These remarks are not optimistic rewards solving the problems of
employment, and unemployment, of youth. By and large, I think those
problems are insoluble, and should be insoluble, until our affluent
society becomes worthier to work in, more honorable in its functions,
and more careful of its human resources.

