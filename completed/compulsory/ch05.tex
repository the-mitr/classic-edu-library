% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{ The Universe Of Discourse In Which They Grow Up}


 Let us now consider the interaction of school and the general culture
 as a climate of communication and ask: what happens to the language
 and thought of young Americans as they grow up towards and through
 adolescence?

In the institutional speech, a child hears only one world-view. In the
nature of the case, every mass medium caters to a big common
denominator of opinion and taste, but even more influential is that
the mass-media interlock. `News', for instance, is what is selected as
newsworthy by two or three news-services; three almost identical
broadcasting networks abstract from the same; and the same is again
abridged for the \emph{Junior Scholastic}.

Even for this news, only sixty towns in America now have competing
newspapers (in 1900 there were 600). Similarly, the `standard of
living', the way to live respectably and decently, is what is shown in
the ads in a few mass-circulation magazines and identically in the TV
commercials. Movie-sets of respectable life come from the same kind of
engineers. Similarly, `political thought' is the platforms of two
major parties that agree on all crucial issues, like the Cold War and
the Expanding Economy, and that get practically all of the coverage by
the same newspapers and broadcasters.

Much of this public speech is quite meaningless. The ads compete with
high rhetoric but the commodities are nearly the same, and a child can
see that our lives are not so vastly occupied by soap, cigarettes and
beer. Politicians are very polemical, but they avoid any concrete
issues that might differentiate the candidates and lose votes. The
real meaning of the speeches, the goal of profits and power, is never
stated. By age eleven or twelve, bright children, perhaps readers of
Mad magazine, recognize that most of the speech is mere words.  The
interlocking of the schools into the system is more serious, for here
the children have to work at it and cooperate. The story is the
same. The galloping increase of national tests guarantee that the
class work will become nothing but preparation for these same
tests. Corporation talent scouts hover in the high schools, and even
the primary schools are flooded with corporation brochures. Excellent
scientists in Washington who chart courses in science and mathematics
understand that there must be leeway for individuality and guesswork
but in the hands of incompetent teachers, the national standard
naturally becomes an inflexible ruler. And TV and machine teaching \emph{are
formal statements that everybody approaches in the same way, with no
need for dialogue.}

Apart from family, children have little speech with any adults except
schoolteachers. But the crowding and scheduling in school allow little
chance or time for personal contact. Also, increasingly in grade
schools as well as in colleges, the teachers have abdicated their
personal role to specialist counselors and administrators, so that
confiding and guidance tend to occur only in extreme situations. One
must be `deviant' to be attended to as a human being.  

This public speech cannot easily be tested against direct observation
This public speech cannot easily be tested against direct observation
or experience. Urban and suburban children do not see crafts and
industries. Playthings are prefabricated toys; there is little
practical carpentry, plumbing or mechanics; but there are
do-it-yourself kits. The contrast of city and country vanishes in
endless conurbation. Few children know animals. Even basic foods are
packaged and distributed, and increasingly pre-cooked, in the official
style.

And a child hears less of any rival style or thought. The rival
world-view of (even hypocritical) religion is no longer
influential. Children do not know the Bible. Eccentric classical
children's literature is discouraged by librarians because it does not
fit educators' word-lists and is probably unhygienic. The approved
books are concocted according to the official world-view. Other more
exciting reading, like comic books, does not contrast to life but
withdraws from it, is without reality or feeling. The movies are the
same more insidiously, because they are apparently adult and
real. Finally, the ideal models of careers with their characters and
philosophies -- scientist, explorer, nurse, and writer -- have been
normalized to TV stereotypes: they are all the same Organization Man,
though they wear various costumes.

Nevertheless, this one system of meaning, although homogeneous and
bland, is by no means sparse or quiet. On the contrary, the quantity
of public speech, plays, information, and cartoons is swamping. The
tone is jumpy and distracting, in the schools, exposure occurs with
intense pressure of tests for retention and punishment for failure to
retain.

No one can critically appreciate so many images and ideas; and there
is very little solitude or moratorium to figure them out. A child is
confused. And he is also anxious, because if the information is not
correctly parroted, he will fall off the school ladder and be a
drop-out; or he will not be hep among his little friends.

At a childish level, all this adds up to brainwashing. The components
are \begin{enumerate*}[label=(\alph*)]
\item a uniform world-view, 
\item the absence of any viable alternative,
\item confusion about the relevance of one's own experience and feelings, and 
\item a chronic anxiety, so that one clings to the one
world-view as the only security. This is brainwashing.
\end{enumerate*}

Of course, in all societies and periods of history small children are
subject to brainwashing, for they are weak, ignorant, economically
dependent and subject to bullying. In some ways in our society the
brainwashing of children is not so pernicious as it has been at other
times, for there is less corporal punishment, less extreme poverty,
less fear of death, and less brutal toilet-training and sexual
disciplining. On the other hand, the ideological exposure is unusually
swamping, systematic and thorough. Profit societies, like garrison
states, invade every detail of life. But worst of all is that parents
are as baffled as the children; since the areas of choice and
initiative are so severely limited, they too lose touch with personal
and practical information.

Thus, despite our technology of surplus, our civil peace(?), and so
much educational and cultural opportunity, it is hard for an American
child to grow towards independence, to find his identity, to retain
his curiosity and initiative, and to acquire a scientific attitude,
scholarly habits, productive enterprise, poetic speech.

Unfortunately, the pervasive philosophy to which children are
habituated as they grow up is the orthodoxy of a social machine not
interested in persons, except to man and aggrandize itself.
Especially not young persons.

Then what happens when, with this background of impersonal and stereotyped language, the child becomes adolescent: awkward and self-conscious, sexually hungry and falling in love, searching for identity, metaphysical, shaken in religious faith or undergoing religious conversion, his Oedipus\hyp{}complex reviving, making a bid for freedom from home, grandiosely ambitious, looking for a vocation, eager to be serviceable as a human being? At best, in organic communities, rational communication breaks down and the community has recourse to rites of passage.

The American world-view is worse than inadequate; it is irrelevant and
uninterested, and adolescents are spiritually abandoned. They are
insulated by not being taken seriously. The social machine does not
require or desire its youth to find identity or vocation; it is
interested only in aptitude. It does not want new initiative, but
conformity. Our orthodoxy does not bear metaphysics. Religious
troubles are likely to be treated as psychotic; they are certainly
disruptive of urban order and scholastic scheduling. Many, maybe most,
of the careers that are open are not services to humanity; that is not
why businesses are run, nor why bombs are stockpiled. Idealism is
astonishingly without prestige.

The adolescent sexual situation is peculiarly ambiguous. We are in a
transitional phase of the sexual revolution and there is a breakdown
of repression (keeping out of mind) and also less inhibition of sexual
behavior. Yet neither in the economy, the housing nor the family
pattern is there any provision for the changed mores. Quite the
contrary, the years of tutelage even tend to lengthen, especially for
middle-class youth in colleges whose administrations regard themselves
as in \emph{loco parentis}. The official mental-hygienic ideology bears
little relation to the stormy images and imperative demands of
adolescent love. In the elementary and junior high schools, sexual
facts do not officially exist. But an adolescent is supposed to be
sexual or there is alarm.  Embarrassment -- the inability to express
or reveal one's needs and feelings to the others -- is universal among
adolescents. But in our society it is especially problematic. The
embarrassment contains or will contain hostility to those who will not
pay attention or will put one down; and also despair at the futility
of trying to make oneself clear. For there is not even a common
language relevant to one's burning private facts -- how pathetic it is
to hear adolescents using the language of TV marriage-counselors or of
movies! Inevitably, silent hostility is retroflexed as
self-denigration. An adolescent ceases to believe in the rightness of
his own wants, and soon he even doubts their existence. His rebellious
claims seem even to himself to be groundless, immature, and
ridiculous.

Broadly speaking, the difficulties of adolescent communication, both
in speaking and listening, are kinds of embarrassment. Let us here
discuss adolescent speechlessness, in group language and sub-culture,
and how adolescents finally give up their own meaning and swallow the
official adult philosophy hook, line and sinker.

Embarrassment may be grounded in too strong desire and confusion, or
in hostility and fear.  

Paling and blushing embarrassment in expressing lust or aspiration is
largely due to confusion caused by powerful feelings that have been
untried, or vague new ideas that seem presumptuous.  It is akin to
ingenuous shame, which is exhibition suddenly inhibited because it is
(or might be) unacceptable. With courage and encouragement, such
speechless embarrassment can falter into sweet or ringing poetic
speech, by which the youth explains himself, also to himself. More
common with use, however, is for the youth to inhibit his stammering
and to brazen out the situation with a line imitated from the mass
media or salesmanship. For example, the strategy is to `snow' the girl
rather than talk to her. Thereby he proves that he is grown-up, has an
erection, etc., but he sacrifices feeling, originality, the
possibility of growth and the possibility of love.

The speechless embarrassment of hostility is fear of retaliation if
one reveals onself. Suppose a youth is reprimanded, advised, or
perhaps merely accosted by an authoritative adult, e.g. a guidance
counselor; he will maintain a sullen silence and not give the adult
the time of day. His presumption is that the adult is setting a trap,
could not understand, and does not care anyway.  The youth cannot
adopt a breezy line, as with a peer, for the adult has more words. He
will be taken as fresh, hostile or in bad taste. Therefore it is best
to say nothing, expressing (perhaps unconsciously) a blazing
contempt. In this situation, the youth's interpretation is not too
erroneous, except that the authority is usually not malevolent but
busy and perhaps insensitive.  

Suppose, however, the adult is a good teacher who does care for the
young persons and would like to teach them in meaningful terms, not
the orthodoxy. Then, as Frank Pinner has pointed out, it is likely
that the teacher's dissenting ideas will be met by a wall of silence
that makes communication impossible. The young are so unsure, and
their distrust is such, that in the crisis of possible contact they
prefer to cling to safe conformity, even though among themselves they
may bitterly attack these same conformist ideas.

Even worse, there is a hermetic silence about anything deeply felt or
threatening; such things are unspeakable even to one's peers, no less
to adults. One may boast to a friend about a sexual conquest or fret
about poor grades, but one may not reveal that one is in love or has a
lofty aspiration. Or to give a tragic example: Puerto Rican boys will
chatter endless small talk and one- up one another, but nobody will
mention that one of their number has just been sent to jail or that
one has just died of an overdose of heroin. If the forbidden subject
is mentioned, they do not hear it. They cannot psychologically afford
to relate themselves, their verbal personalities, to the terrible
realities of life. (Incidentally, I have heard from teachers in the
New York schools that there is a similar cleavage in many young Puerto
Rican's knowledge of the English language.  They seem to talk English
fluently as long as the subject is superficial and `grown-up'; but
they are blank in many elementary words and phrases, and are quite
unable to say, in English, anything that they really want or need.)

To diminish embarrassment, since communication with the adults is cut
off, there is developed an increasingly exaggerated adolescent
'sub-culture', with its jargon, models, authors and ideology. Let us
first distinguish between a `sub-culture' and a `sub-society'.  

An intense youth sub-society is common in most cultures, the interest
in sexual exploration, dancing, simple exciting music, athletics, cars
and races, clubs and jackers, one-upping conversation, seems to be
natural to youth -- just as many adult interests are naturally
irrelevant and boring to them. Also, the sharing of secrets, often
mysterious even to themselves, is everywhere a powerful bond of union
among adolescents; and certainly their business is nobody else's
business. The Youth Houses of some primitive communities
institutionalize all this rather better than our own boarding-schools
and colleges, which are too ridden with in \emph{loco parentis} regulations.

The development of such a sub-society into a full-blown sub-culture,
however, is not normal, but reactive. It signifies that the adult
culture is hostile to adolescent interests, or is not to be trusted;
that parents are not people and do not regard their children as
people; that the young are excluded from adult activities that might
be interesting and, on the other hand, that most adult activities are
not worth growing up into as one becomes ready for them. Rather, on
the contrary, the adults are about to exploit the young, to pressure
them into intrinsically boring careers, regardless of proper time or
individual choice.

Normally there is not a `youth culture' and an `adult culture', but
youth is the period of growing up in the one culture. With us,
however, youth feels itself to be almost outcast, or at least
manipulated. It therefore has secrets, jargon and lore of sabotage and
defense against the adult culture.

But then, since the intellectual life of callow boys and girls in
isolation from the grown-up economy and culture is thin gruel, youth
interests are vastly puffed up into fads, disc-jockeys, politically
organized gangs and wars, coterie literature, drugs and liquor, all
frantically energized by youthful animal spirits -- and cleverly
managed by adult promoters. The teenage market is more than ten
billion dollars a year, in jackers, portable radios, sporting goods,
hair-dos, bikes and additional family cars. Needless to say, this
secondary development is simply a drag on the youthful spirit. It is
largely frivolous and arbitrary, yet it is desperately conservative
and exerts a tremendous pressure of blackmail against non-conformers
or those ignorant of the latest, who will be unpopular. It makes it
hard to talk sense to them, or for them to talk sense, whether
adolescent or adult. And of course there is no chance for intelligent
dissent from the official philosophy and standard of life. Naturally,
too, especially in the middle class, the regressed adults play at and
sponsor every teenage idiocy.

Inevitably, the high school -- with its teenage majority and adult
regime -- becomes a prime area for sabotage and other fun and games. I
have heard James Coleman, who has most studied these phenomena,
express the opinion that the average adolescent is really in school,
academically, for about ten minutes a day! Not a very efficient
enterprise.

A certain number of the young gang up and commit defiant
delinquencies. These are partly the revolt of nature for there is much
in our society that is insulting and intolerably frustrating. They are
partly reactive against whatever happens to constitute `correct'
behavior. And they are partly a pathetic bid for attention, as it is
said, `We're so bad they give us a youth worker.'

A pathetic characteristic of recent middle-class adolescent
sub-culture is taking on the language and culture of marginal groups,
Negroes and Latin Americans, addicts, beat drop-outs from the colleges
and the Organized System. This is appropriate, for these others too
are abused and disregarded; they are in the same case as the
adolescents. But such a culture is hardly articulate.  Also there is
something exploiting about imitating authentic outcast people, who
live as they do not by choice but by necessity.

Nevertheless, for many of the woefully embarrassed, this
semi-articulate speech -- saying `man' and `cat' and `like, man' --
makes conversation possible. The adolescent culture is something to
talk about and this is a style to talk in. The words of one syllable
of jive, the thoughts of one syllable of Beat, the content of kicks,
movies and high-school dances, are not a wide discourse, but they
foster bringing together, and everybody can democratically
participate.  

Unfortunately, the small talk drives out real talk. It is incredibly
snobbish and exclusive of sincerity and originality. Embattled against
the adult world that must inexorably triumph, adolescent society
jealously protects itself against meaning.

To adolescents of sixteen, the adult world must seem like a prison
door slamming shut. Some must get jobs which are sure not to fit them
and in which they will exercise no initiative whatever. Others must
engage in the factitious competition for college: entrance. Either
process is formidable with forms and tests. The kids are ignorant of
the ropes and ignorant of what they want. Disregarded by the adults,
they have in turn excluded adult guidance or ideas looking towards the
future. But their adolescent bravado is now seen to be unrealistic and
even ridiculous.  Having learned nothing, not fought any battles, they
are without morale.  

Their weakness can be observed vividly on college campuses. Students
gripe about the moral rules by which they are still absurdly harassed
at eighteen and nineteen years of age. It's ironical; had they quit
school and were assembly-line workers, they would be considered
responsible enough to come and go, have sex and drink. Yet it comes to
nothing but griping; they do not feel justified to enforce their
demands, for they have never had this issue, or any issue, out with
their parents. Similarly, they are unhappy about the overcrowded
classes, the credits, the grading; they know they are disappointed in
the education they are getting; yet they are so confused about what
really they do want that they are speechless.  

And just in the colleges, which are supposed to be communities of
scholars, face-to-face communication is diminished. The adolescent
sub-culture that persists is irrelevant to the business going on,
except to sabotage it, but the adolescent community is not replaced by
close acquaintance with learned adults. The teachers hold the students
off and, as I argued in \emph{The Community of Scholars}, it is a chief
function of orderly administration to keep the students out of contact
with the teachers and the teachers out of contact with one
another. Naturally, as long as the students are isolated with one
another, they can be treated as immature, which they are.  

The dialogue with the subject matter, with Nature and History, is as
skimpy as with the teacher.  Colleges are not interested in such
things any more -- it has little Ph.D. value. The student is told the
current doctrine and is trained to give it back accurately. And still
proving his masculinity and doing a snow-job, the student thinks that
the purpose of a course is to `master the subject'.  Necessarily, in
the conflict with the adult world, the young suffer a crushing
defeat. There are various ways of surviving it. Some give up on
themselves and conform completely -- a few indeed become more royalist
than the king (but these are often psychopathic, middle-class
delinquents). Others make rationalizations: they will return to the
fray later when they are better prepared'. Or, `The most important
thing is to get married and raise a normal family', they will hold on
to feeling and meaning for their family life, or perhaps for their
'personal' behavior. A surprising number tell you that the goal of
life is \$50,000 a year.  

The psychology of the introjections is evident: defeated, they
identify with what has conquered them, in order to fill the gap with
some meaning or other. Once they have made the new identification,
they feel strong in it; they defend it by every rationalization.  

An alternative philosophy that has recommended itself to some older
adolescents is hipsterism.  A hipster cushions the crushing defeat by
society by \emph{deliberately} assuming convenient roles in the dominant
system, including its underworld, to manipulate it for his own power
or at least safety.  The bother with this idea -- it is the argument
of Thrasymachus in Plato's \emph{Republic} -- is that the hipster cannot
afford to lose himself, or even to become unselfconscious. He must be
ahead of every game. Then he cannot grow by loving or believing
anything worthwhile, and he exhausts himself in business with what he
holds in contempt, deepening his own cynicism and self- contempt. But
hipsterism does provide a satisfaction of mastery and vinery which
ward off his panic of powerlessness, passivity and emasculation. It is
a philosophy for chronic emergency, during which communication
consists inevitably of camouflage and secrecy, `playing it cool', or
of gambits of attack to get the upper hand.

The conditions that I have been describing, and the youthful responses
to them, sadly limit human communication and even the concept of
it. `Communication' comes to be interpreted as the transfer of a
processed meaning from one head to another, which will privately put
it in a niche in its own system of meanings. This system is presumably
shared with the others -- one can never know. And in this presumptive
consensus, the exchanged information adds a detail or a specification,
but it does not disturb personality or alter characteristic behavior,
for the self has not been touched. At most, the information serves as
a signal for action from the usual repertory.

Among Americans, this sentiment of consensus, `understanding', is so
important that much speech and reading does not even give new
information, but is a ritual touching of familiar bases.  (This is
evident in much newspaper reading, in after-dinner speeches and so
forth.) But the case is not much different with active speech that is
supposed to affect choice, e.g. in politics, for no disturbing issues
are broached, nor anything that one would have to think new thoughts
about.  The underlying consensus is assumed -- is signaled by the
usual words -- and no important alternative is offered.

The consensus is \emph{presumably} shared, but any dialectic to test this
assumption is in bad form, just as it is impolite to question a loose
generalization made in small talk and say `Prove it'. In ideal
cybernetic theory, the exchange of information is supposed to alter
the organisms conversing, since they must make internal readjustments
to it; but my observation is that no such alteration occurs. The chief
meaning of conversation is its own smooth going on.  

By contrast, the active speech of salesmanship is more lively, because
it is meant importantly to change behavior, towards buying something;
it is not meant merely to soothe. Thus, strikingly, TV commercials are
the only part of TV that makes novel use of the medium itself,
employing montage and inventive music, playing with the words, images
and ideas. The pitch of a salesman is likely to be \emph{ad hominem}, in bad
form, both flattering and threatening. (Needless to say, there is no
dialogue; the hearer is passive or dumbly resistant.) But of course,
in salesmanship, apart from the one pre-thought transaction, the
consensus is powerfully protected; the TV ad and the programme that it
sponsors avoid anything that might surprise, provoke or offend any
single person in an audience of millions.  

Consider what is lost by this narrow concept of communication as the
exchange of processed information with which each communicant copes
internally.
\begin{enumerate}
\item The function of speech as the shaping expression of pre-verbal
  needs and experiences, by which a speaker first discovers \emph{what} he is
  thinking. Such speech cannot be entirely pre-thought and controlled;
  it is spontaneous.
\item The function of speech as personally initiating something by
  launching into an environment that is \emph{unlike} oneself. Initiating,
  one presumes there is no consensus; otherwise why bother speaking?
\item Most important of all, the function of speech as dialogue between
  persons \emph{committed to the conversation} -- or between a person and a
  subject matter in which he is absorbed. This results in change of
  the persons because of the very act of speaking; they are not fixed
  roles playing a game with rules.
\end{enumerate}

Speaking is a way of making one's identity, of losing oneself with
others in order to grow. It depends not on prior consensus with the
others, but on trust of them. But, in my opinion, the speech defined
in most contemporary communication theory is very like the speech of
the defeated adolescents I have been describing. It is not pragmatic,
communal, poetic or heuristic. Its function is largely to report in a
processed \emph{lingua franca}.

Speech cannot be personal and poetic when there is embarrassment of
self-revelation, including revelation to oneself, nor when there is
animal diffidence and communal suspicion, shame of exhibition and
eccentricity, clinging to social norms. Speech cannot be initiating
when the chief social institutions are bureaucratized and predetermine
all procedures and decisions, so that in fact individuals have no
power anyway that is useful to express. Speech cannot be exploratory
and heuristic when pervasive chronic anxiety keeps people from risking
losing themselves in temporary confusion and from relying for help
precisely on communicating, even if the communication is Babel.

As it is, people have to `think' before they speak, rather than
risking speaking and finding out what they mean by trying to make
sense to others and themselves. In fact, they finally speak English as
though they were in school.

