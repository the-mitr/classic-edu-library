% !TEX root = compulsory.tex
%!TEX TS-program = pdflatex
%!TEX encoding = UTF-8 Unicode

\chapter{Teaching Science}


A century ago, Matthew Arnold and Thomas Huxley debated whether
science, rapidly growing in importance, should become preponderant
over the humanities in the popular curriculum. Arnold opted for the
humanities because they give us `criticism of life' and teach us
'conduct', the main business of most men. He conceded that for the
unusual persons who are scientifically gifted, the philosophy and
practice of science it self provides a guide to life. But Huxley's
view -- in the line leading from the Encyclopedists and Comte to the
naturalistic novelists and Veblen -- was basically that there is a
scientific way of life, a new and better ethic, possible for the
majority.

We have seen that the progressive education of the early twentieth
century shared this belief in the scientific ethic and added to it the
great modern issue: how to be at home in the modern environment which
has, willy-nilly, become overwhelmingly industrial and
technological. In this country, Dewey was a leader in the struggle to
secure for science a big place in education. Yet by 1916, Dewey spoke
of his `painful' disappointment in the fruits of the scientific
curriculum; it had not paid off in life-values, but had become
scholastic and arid. And we know that in his last period, he estimated
more and more highly the experience and structured emotion of art.

By the time we come to C. P. Snow in the 1950s, the debate between the
humanities and science, like most other serious topics, has become
pretty vulgarized. As Snow speaks of them, the humanities are little
better than frills and snobbery; but science, correspondingly, is
mainly praised as if it were identical with technology and must be
universally studied to improve the standard of living of the Africans
(though he offers no evidence that, with modern methods of production,
we need quite so many technicians to achieve this unexamined
purpose). But the nadir of this kind of pitch for science has been, I
suppose, the calamitous sentence in the late President Kennedy's
message on Education of 1963: `Vast arena of the unknown are daily
being explored for economic, military, medical and other reasons.'
(The `other reasons' include those of Galileo and Darwin.) Neither the
scientific conduct of life, nor any conduct, is thought of as a
purpose of education, or thought of at all. The intervention of the
National Science Foundation in improving the science and mathematics
courses in the elementary and high schools is on a much higher plane;
its avowed aims are reasonable and not base. (It began a few years
before the panic about Sputnik, though all the public enthusiasm has
been since.)

Literacy in science is becoming essential for all citizens who wish to
comprehend the world they live and work in, and to participate in the
increasing number of decisions, some of the gravest import, that
require an understanding of science. Further, more and more students
must be attracted to scientific and technical pursuits, and these
students must be prepared to work with increasingly sophisticated
ideas and techniques.... And there is another aspect... more emphasis
should be given to disciplined, creative, intellectual activity as an
end in itself ... for each student to experience some of the
excitement, beauty and intellectual satisfaction that scientific
pursuit affords (from the Foreword to \emph{Science Course Improvement
Projects}).

This is a well-rounded educational prospectus, including the
citizenly, the vocational and the humanistic. It is a far cry from
'other reasons'.

But now another danger has arisen, intrinsic in the composition of the
NSF: the kind of people they consult, the kind of people they do not
consult. Looking at some of the improved projects and methods reported
and the TV films sponsored, one cannot avoid the impression that the
curriculum improvers are professors in graduate schools and cannot
finally think of education in science otherwise than as the producing
of Ph.D's. `Society,' says the Foreword a little petulantly, `can no
longer afford to waits generation or more for new knowledge to make
its way gradually into school and college programmes.' By society we
may understand MIT, etc.

Of course, the Foreword contains the sanitizing disclaimer: Decisions
on what to teach remain, in the healthy American tradition, the
exclusive responsibility of individual schools and teachers.  The
National Science Foundation does not recommend any specific book,
film, etc. It is hopeful, however, that the products of these projects
will prove to merit serious consideration by every school and college.

This is disingenuous, with the incredible amounts of national testing,
and with actual courses tailored just for the tests, and with `making
the prestige universities' as the grand goal of all middle-class
parents, college-guidance counselors and superintendents of schools,
the humble proposals of the NSF have pretty nearly the force of
statutes\ldots{} The snag is that there aren't enough competent teachers
of the new programmes. Bright young graduates in science are more
likely to stay in the universities or go to the corporations than to
teach children and adolescents.  (The science course-improvement
studies by the National Science Foundation cost fifty million
dollars. When I raised my eyebrows at this sum, a representative of
the NSF pointed out to me that this was only thirty-five cents a
person in the United States. `Naw,' said a chap from the United
Automobile Workers, `it's fifty million dollars.')

What ought science teaching to be about for the great majority who are
not going to be graduate students in science?

On refection, there seem to be more than half a dozen plausible
reasons for teaching science as a major part of popular education.

Let us spell them out and ask their relevance at present:
\begin{enumerate}

\item The pre-training of technicians is not a good reason. The fact
  that such apprentices must be prepared to work with `increasingly
  sophisticated ideas and techniques' is rather a reason \emph{not} to
  emphasize their preparation by general schooling, or obviously the
  great majority are not going to become such technicians, and the
  more intense the specialized instruction necessary for some, the
  less useful for the future of most. Indeed, with the maturing of
  automation this objection will be even stronger: many of the middle
  technical skills will surely vanish; semi-skilled `technicians' will
  require \emph{less} pre-training, not more; whereas the high technical
  skills required will be so far beyond average aptitude that general
  schools are hardly the place to pre-train them. Like most
  apprenticeships, this kind can be taught more practically, more
  specifically, and more quickly by the ultimate employer, without
  wasting the time of the majority of youngsters on a kind of
  mathematics and science that they will promptly forget. And it is
  hard to see why the public should bear the expense for the
  pre-selection of lively algebraists for General Electric.


\item On the other hand, the NSF intention of producing original
  creative scientists seems to me both pretentious and naive. We
  simply do not know how to breed these, not whether `schools' are the
  best place, not even whether they thrive better by exposure to
  up-to-date teaching or reaction to out-dated teaching. With a good
  deal of fanfare, the Woods Hole Conference on science teaching, on
  which the NSF relies heavily, arrived at the excellent insights that
  Dewey had prescribed for teaching any subject whatever: encouraging
  spontaneity, imagination, courage to guess; avoiding `correct'
  answers and rejecting all grading and competition; maintaining
  continuity with emotional and day-to-day experience and having each
  youngster follow his own path. But are the Woods Hole scientists
  serious about these insights? I have not heard either Dr Bruner or
  the NSF lambasting the achievement feats and the National Merit
  examination; and what plan do they have to make genuine progressive
  education acceptable in the school systems where it is now less
  acceptable year by year?

Even more important, Dewey would never have claimed that these methods have anything to do with high creative invention, any more than finger-painting and teaching art on sound psychological principles will produce masters. 

Studying the `sources and conditions that stimulate creativity', the late Harold Rugg came to ideas like `preparatory period of baffled struggle', `interlude in  which the scientist apparently given up, pushes the problem ``out of mind'' and leaves it for the non-conscious to work upon', `blinding and unexpected flash of insight'. Does the NSF have a clear and distinct idea of such processes in the school-system! We have seen that Dr Zacharias's cautious reliance on the Discovery Method is not for keeps; it is pre-structured to the already known answers of the  Ph.D. If the kind of bafflement and resignation necessary for creativity were seriously meant, the appropriate response of a youngster to such hearing would be not insight but disgust or rage, as in Zen teaching.


\item The NSF intentions to teach science for its own `excitement,
  beauty and intellectual satisfaction' are entirely acceptable; this
  is science as one of the humanities. The intention is reinforced by
  the Woods Hole prescription to teach the `fundamental ideas and
  methods' rather than the current theories which may soon be outmoded
  anyway. (A sample list of fundamental concepts is Interaction,
  Physical System, Relativity of Motion, Equilibrium, Energy, Force,
  Entropy, and Organic Evolution.)

  Yet once we push these fundamental concepts beyond the stage of
  philosophical discussion, there arises a dilemma in the nature of
  the present state of science. It used to be that the chief
  excitement of science, which is the exploration and discovery of the
  nature of things, was easy to keep in the foreground; systematic
  theory was not too far from observation and experiment. Now,
  however, observation and experiment occur in a vast framework of
  systematic explanation, and (I would guess) it must be hard to
  convey the excitement of discovering the truth without what almost
  amounts to a specialist training. To get to this excitement of
  actual exploration requires spelling out the fundamental concepts
  very far; yet without this excitement, the unique contribution of
  science to the humanities is lost.

  Thus, it might still be best, in order to convey to the majority the
  wonders of exploring and explaining nature, to have recourse to the
  history of classical experiments, as at St John's of Annapolis -- on
  the theory that these demonstrate the scientific spirit of man in
  action; or to stick to the spectacular popular demonstrations that
  Helmholtz or Huxley used to go in for; or perhaps just to explore
  the solar system with a six-inch telescope, plate spoons with
  silver, cut up dead cats, plant hybrid squash, or time the traffic
  lights and count the can, in order to show children and adolescents
  that there is an observable world that can be made intelligible by
  explanations (what Plato in the \emph{Timaeus} calls `likely stories').


\item An even stronger reason for teaching science, which the NSF does
  not talk about, is its austere morality, accuracy, and scrupulous
  respect for what occurs. (I myself never learned this and have
  always regretted it.) This, I think, is the heart of what Huxley,
  Veblen and Dewey means by the scientific ethic. But for the
  majority, unfortunately, this virtue is almost incompatible with
  picking up much `content', or in preparing to become a graduate
  student.  Simply, the average youngster's chemistry experiment
  usually does not balance out; and moral science teaching would then
  have him spend the entire semester in explaining the `failure' and
  cleaning his test-tubes better. We all know how the student's
  drawing of what is seen through the microscope looks remarkably like
  the picture in the textbook; this is necessarily blinked at by the
  instructor who wants to proceed and get to the `subject'. But of
  course to condone it destroys science. The defeat is glaringly
  exaggerated in the TV lessons, which not only occur as a sleight-
  of-hand by experts, but -- in every show I have seen -- occur
  entirely too fast and cover too much ground. Undoubtedly the live
  instructor is supposed to retrace the path more thoroughly, but for
  the average youngster the effect must be to acquire a system of
  ideas and explanations, rather than science. The best students who
  continue in science will eventually learn, as real apprentices, the
  scientific attitude that has been by-passed; yet even graduate
  students, like medical students, are often mainly interested merely
  in going through the pacer and adding to the `system of science',
  with appropriate status and rewards.

\item The NSF purpose is to comprehend the world one lives and works
  in is excellent. But for this purpose, I wonder whether the NSF
  projects are not too fancy. The underlying fact in that the average
  person uses ever more and more complicated scientific appliances,
  yet fewer and fewer of us practically understand or can repair the
  pump, the electric motor, the automobile that we use. Inevitably,
  people become slaves to repair men, and as purchasers and consumers
  our ignorance is colossal. Correspondingly, the design of scientific
  appliances it increasingly less transparent, and the manufacturers
  take no account of their comprehensibility and reparability except
  by experts. Nor have I heard that industrialists make efforts to
  instruct their workmen in the overall rationale of the jobs they
  work at; not that the labor unions demand it. When the NSF speaks of
  the need to comprehend in order to overcome the dangerous alienation
  of modern urban people and workmen, they ought to mean something
  akin to what progressive educators called `learning by doing';
  philosophic concepts and their structuring are not sufficient.


\item The purpose `to participate in grave political decisions on
  scientific matters' is also extremely valid. If I do not mistake the
  tone, it is seriously meant by the gentlemen in the NSF, who
  appreciate the necessity and the dilemma. Decisions involving
  billions of dollars not only for incomprehensible hardware but in
  sponsoring research where only a band of experts can even guess the
  value, and also how to detect phony cover-ups from honorable
  failures -- all these pose an absolutely new problem for
  democracies. The hope is that by overcoming superstition, including
  the superstition of `science', by making people more technically at
  home, and by teaching the relevant economics and sociology of
  science expenditures and scientific costs, the dilemma can be
  alleviated. (I do not see how it can be solved.) Perhaps we can
  learn to ask the right questions and judge the authenticity, if not
  the content, of the answers. And at least on some issues like
  transportation policy or the export of technologies in foreign aid,
  intelligent people would be able to decide better if they dared to
  criticize the experts at all.


\item Finally, there is a kind of active participation, more and more
  incumbent on citizens, that requires a new scientific understanding
  and judgment. These are the matters of ecology, urbanism and mental
  health, where physical, biological and social sciences interact,
  that so directly determine everybody's everyday life that we simply
  cannot afford to leave them to experts. This kind of inquiry -- in
  the line of Patrick Geddes, Lewis Mumford, the decentralists and
  regionalists -- is of course partly political and aesthetic; it
  overrides the distinction that we began with, between the sciences
  and the humanities.

\end{enumerate}

The kind of science teaching that emerges from this critique of the
improved curricula does not fit easily into the up-the-ladder academic
system culminating in graduate schools and the production of Ph.D.s. A
good deal of training is best done in real apprenticeships; learning
to be at home with our technology is best done in workshops, and even
requires the cooperation of designers and manufacturers; the humanity
of science is perhaps best taught to somewhat older students, eighteen
to twenty-one, and it looks like an excellent subject for folk schools
like those in Denmark; ecology and urbanism are surely best learned
actively in the field, as in the remarkable work of Karl Linn among
the underprivileged adolescents of Philadelphia.


Thus we again come to the same conclusion. We ought to spend more of
our wealth on education; perhaps especially we need more understanding
and practice of science; but it does not follow that the present
system of schools is the appropriate institution for the job.
